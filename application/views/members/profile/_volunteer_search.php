<div id="search" class="col s12">
    <?php $this->load->view('frontend/include/innerPageLoader') ?>

    <div class="users-page-contents" ng-controller="searchProjectCtrl">
        <div class="row">
            <div class="col s12 m4 l4">
                <span class="filtertitle">Set Filters</span>
                <form>
                    <div class="project-filter">
                        <span>Please select the project you want our volunteers for.</span>
                        <div class="input-field">
                            <select id="projectId" ng-model="searchForm.agencyProject">
                                <option value="" selected>Select Project</option>
                                <?php foreach($projectList as $projectData) { ?>
                                    <option value="<?php echo $projectData->id ?>"><?php echo $projectData->name ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="project-filter">
                        <span>Set filters for your search results</span>
                        <label>Select Zone</label>
                        <div class="input-field">
                            <select id="selectZone" ng-model="searchForm.zoneId">
                                <option value="" selected>Select Zone</option>
                                <?php foreach($zones as $zone) { ?>
                                    <option value="<?php echo $zone->id ?>" class="circle"><?php echo trim($zone->name) ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <label>Select District</label>
                        <div class="input-field">
                            <select id="selectDistrict" name="districtId" ng-model="searchForm.districtId">
                                <option value="" selected>Select District</option>
                                <?php foreach($districts as $district) { ?>
                                    <option value="<?php echo $district->id ?>" class="circle"><?php echo trim($district->name) ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <label>Select VDC/Municipality</label>
                        <div class="input-field">
                            <select id="selectMunicipality" name="municipalityId" ng-model="searchForm.municipalityId">
                                <option value="" selected>Select VDC/Municipality</option>
                                <?php /* foreach($municipalities as $municipality) { ?>
                                    <option value="<?php echo $municipality->id ?>" class="circle"><?php echo trim($municipality->name) ?></option>
                                <?php } */ ?>
                            </select>
                        </div>

                        <label>Select Gender</label>
                        <div class="input-field">
                            <select multiple class="icons" ng-model="searchForm.genderId">
                                <option value="" selected>Select Gender</option>
                                <?php foreach($allGender as $gender) { ?>
                                    <option value="<?php echo $gender->id ?>" data-icon="<?php echo base_url($gender->icon) ?>" class="circle"><?php echo $gender->name ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <!--<div class="input-field">
                            <select multiple ng-model="searchForm.ageGroup">
                                <option value="" selected>Select Age Group</option>
                                <option value="1">Option 1</option>
                                <option value="2">Option 2</option>
                                <option value="3">Option 3</option>
                                <option value="1">Option 1</option>
                                <option value="2">Option 2</option>
                                <option value="3">Option 3</option>
                                <option value="1">Option 1</option>
                                <option value="2">Option 2</option>
                                <option value="3">Option 3</option>
                                <option value="1">Option 1</option>
                                <option value="2">Option 2</option>
                                <option value="3">Option 3</option>
                                <option value="1">Option 1</option>
                                <option value="2">Option 2</option>
                                <option value="3">Option 3</option>
                                <option value="1">Option 1</option>
                                <option value="2">Option 2</option>
                                <option value="3">Option 3</option>
                            </select>
                        </div>-->

                        <label>Select Skill</label>
                        <div class="input-field">
                            <select multiple ng-model="searchForm.skillId">
                                <option value="" selected>Select Skill</option>
                                <?php foreach($skills as $skill) { ?>
                                    <option value="<?php echo $skill->id ?>" class="circle"><?php echo $skill->name ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <label>Select Academic Qualification</label>
                        <div class="input-field">
                            <select multiple ng-model="searchForm.academicQualificationId">
                                <option value="" selected>Select Academic Qualification</option>
                                <?php foreach($academicQualifications as $academicQualification) { ?>
                                    <option value="<?php echo $academicQualification->id ?>" class="circle"><?php echo $academicQualification->name ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <label>Select Language</label>
                        <div class="input-field">
                            <select multiple ng-model="searchForm.languageId">
                                <option value="" selected>Select Language</option>
                                <?php foreach($languages as $language) { ?>
                                    <option value="<?php echo $language->id ?>" class="circle"><?php echo $language->name ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <label>Select Training</label>
                        <div class="input-field">
                            <select multiple ng-model="searchForm.trainingId">
                                <option value="" selected>Select Training</option>
                                <?php foreach($trainings as $training) { ?>
                                    <option value="<?php echo $training->id ?>" class="circle"><?php echo $training->name ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="center">
                        <div class="btn-large waves-effect waves-light btn-content btn-form">
                            <input type="submit" ng-click="searchVolunteer()" value="Apply">
                        </div>
                    </div>
                </form>
            </div>

            <div class="col s12 m8 l8">
                <input type="hidden" value="{{totalVolunteers}}" id="total-volunteers"/>
                <span class="search-reasult">{{ totalVolunteers }} results found.</span>
                <div class="search-reasult-wrap">
                    <div class="row center">
                        <div class="col s12 m4 l4">
                            <div class="results">
                                <img src="<?php echo base_url('assets/img/icon/icon-gender-male.png') ?>"/>
                                <h3><span>Male</span> Volunteers</h3>

                                <span>{{ maleVolunteers }}</span>
                            </div>
                        </div>

                        <div class="col s12 m4 l4">
                            <div class="results">
                                <img src="<?php echo base_url('assets/img/icon/icon-gender-female.png') ?>"/>
                                <h3><span>Female</span> Volunteers</h3>

                                <span>{{ femaleVolunteers }}</span>
                            </div>
                        </div>

                        <div class="col s12 m4 l4">
                            <div class="results">
                                <img src="<?php echo base_url('assets/img/icon/icon-gender-other.png') ?>"/>
                                <h3><span>Other</span> Volunteers</h3>

                                <span>{{ otherVolunteers }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="center">
                    <a data-agency="<?php echo get_userdata('active_user') ?>" class="btn-large waves-effect waves-light btn-content" href="#" id="send-request">Send Request</a>
                </div>
            </div>
        </div>
    </div>
</div>