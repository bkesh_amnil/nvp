<div class="col s12 m6 l8">
    <ul class="tabs user-tab">
        <li class="tab"><a class="active" href="#profile"><i class="mdi-maps-local-library"></i> Profile</a></li>
        <li class="tab"><a href="#changePassword"><i class="mdi-action-lock"></i> Change Password</a></li>
    </ul>
</div>

<div id="profile" class="col s12">
    <div class="users-page-contents">
        <?php frontFlash() ?>
        <form action="" id="agency-profile-form" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col s12 m6 l6">
                    <input type="hidden" id="id" value="<?php echo $agencyData->id?>">
                    <input id="table" value="nvp_agency" type="hidden" name="table">

                    <span class="filtertitle">Agency Information</span>
                    <div class="row profile-group-wrap">
                        <div class="col s12 m12 l12">
                            <label for="name">Agency Name *</label>
                            <input id="name" value="<?php echo $agencyData->name ?>" name="name" type="text">
                        </div>
                        <div class="col s12 m12 l12">
                            <label for="address">Agency Address *</label>
                            <textarea name="address" class="materialize-textarea" id="address"><?php echo $agencyData->address ?></textarea>
                        </div>
                        <div class="col s12 m12 l12" id="agencynumber">
                            <label for="phoneNumber">Agency Phone Number *</label>
                            <input id="phoneNumber" name="phoneNumber" value="<?php echo $agencyData->phoneNumber ?>" type="text">                            
                        </div>
                        <div class="col s12 m12 l12">
                            <label for="email">Email *</label>
                            <input id="email" name="email" value="<?php echo $agencyData->email ?>" type="text" <?php echo (!empty($agencyData->email)? 'disabled': '')?>>
                        </div>
                        <?php
                        $hideSelectClass = 'hide-select';
                        if(!$agencyData->agencyTypeId) {
                            $hideSelectClass = 'show-select';
                        }
                        ?>
                        <div class=" col s12 m12 l12">
                            <label>Agency Type *</label>
                            <select name="agencyTypeId" id="agengyType" class="agency-type">
                                <option value="" disabled selected>Select Agency Type</option>
                                <?php foreach($agencyTypes as $agencyType) { ?>
                                    <option value="<?php echo $agencyType->id ?>" <?php echo ($agencyData->agencyTypeId == $agencyType->id) ? 'selected' : '' ?> class="circle"><?php echo $agencyType->name ?></option>
                                <?php } ?>
                                <option <?php echo ($agencyData->agencyTypeId == 0) ? 'selected' : '' ?> value="0">Others</option>
                            </select>
                        </div>
                        <div class="col s12 m12 l12 agency-name <?php echo $hideSelectClass ?>">
                            <label for="agencyTypeName">Agency Type Name *</label>
                            <input id="agencyTypeName" value="<?php echo $agencyData->agencyTypeName ?>" name="agencyTypeName" type="text">
                        </div>
                        <div class="col s12 m12 l12">
                            <label for="registrationNumber">Registration Number *</label>
                            <input id="registrationNumber" value="<?php echo $agencyData->registrationNumber ?>" name="registrationNumber" type="text">
                        </div>
                    </div>
                </div>

                <div class="col s12 m6 l6">
                    <span class="filtertitle">Contact Information</span>
                    <div class="row profile-group-wrap">
                        <div class="col s12 m12 l12">
                            <label for="primaryContactPersonFullName">Primary Contact Full Name *</label>
                            <input id="primaryContactPersonFullName" value="<?php echo $agencyData->primaryContactPersonFullName ?>" name="primaryContactPersonFullName" type="text">
                        </div>
                        <div class="col s12 m12 l12">
                            <label for="primaryContactPersonDesignationInAgency">Primary Contact Designation *</label>
                            <input id="primaryContactPersonDesignationInAgency" value="<?php echo $agencyData->primaryContactPersonDesignationInAgency ?>" name="primaryContactPersonDesignationInAgency" type="text">
                        </div>
                        <div class="col s12 m12 l12">
                            <label for="primaryContactPersonPersonalPhoneNumber">Primary Contact Phone Number *</label>
                            <input id="primaryContactPersonPersonalPhoneNumber" value="<?php echo $agencyData->primaryContactPersonPersonalPhoneNumber ?>" name="primaryContactPersonPersonalPhoneNumber" type="text">
                        </div>
                        <div class="col s12 m12 l12">
                            <label for="primaryContactPersonEmail">Primary Contact Email *</label>
                            <input id="primaryContactPersonEmail" value="<?php echo $agencyData->primaryContactPersonEmail ?>" name="primaryContactPersonEmail" type="text">
                        </div>
                        <div class="col s12 m12 l12">
                            <label for="secondaryContactPersonFullName">Secondary Contact Full Name *</label>
                            <input id="secondaryContactPersonFullName" value="<?php echo $agencyData->secondaryContactPersonFullName ?>" name="secondaryContactPersonFullName" type="text">
                        </div>
                        <div class="col s12 m12 l12">
                            <label for="secondaryContactPersonDesignationInAgency">Secondary Contact Designation *</label>
                            <input id="secondaryContactPersonDesignationInAgency" value="<?php echo $agencyData->secondaryContactPersonDesignationInAgency ?>" name="secondaryContactPersonDesignationInAgency" type="text">
                        </div>
                        <div class="col s12 m12 l12">
                            <label for="secondaryContactPersonPersonalPhoneNumber">Secondary Contact Phone Number *</label>
                            <input id="secondaryContactPersonPersonalPhoneNumber" value="<?php echo $agencyData->secondaryContactPersonPersonalPhoneNumber ?>" name="secondaryContactPersonPersonalPhoneNumber" type="text">
                        </div>
                        <div class="col s12 m12 l12">
                            <label for="secondaryContactPersonEmail">Secondary Contact Email *</label>
                            <input id="secondaryContactPersonEmail" value="<?php echo $agencyData->secondaryContactPersonEmail ?>" name="secondaryContactPersonEmail" type="text">
                        </div>
                    </div>
                    
                </div>
                
                <div class="row">
                    <div class="col s12 m6 l6">
                        <div class="col s12 file-field input-field">
                            <div class="btn btn-content">
                                <span>Logo *</span>
                                <input type="file" name="logo" id="logo">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path" type="text" placeholder="should not exceed 2MB">

                            </div>
                        </div>
                        <?php if($agencyData->logo != '') { ?>
                            <input type="hidden" id="logoCheck" value="1">
                            <div class="col s12 file-field input-field">
                                <img src="<?php echo image_thumb($agencyData->logo, 200, 200, '', true) ?>" class="img-responsive">
                                <!--<a class="remove-img" href="<?php /*echo base_url('members/profile/remove/logo/') */?>">Remove Logo</a>-->
                            </div>
                        <?php }else {?>
                            <input type="hidden" id="logoCheck" value="0">
                        <?php }?>
                    </div>
                    
                    <div class="col s12 m6 l6">
                        <div class="col s12 file-field input-field">
                            <div class="btn btn-content">
                                <span>Certificate Image *</span>
                                <input type="file" name="registrationCertificateImage" id="registrationCertificateImage">
                            </div>


                            <div class="file-path-wrapper">
                                <input class="file-path"  type="text"  placeholder="should not exceed 2MB">
                            </div>
                        </div>
                        <?php
                        if($agencyData->registrationCertificateImage != '') {
                            ?>
                            <input type="hidden" id="registrationCertificateImageCheck" value="1">
                            <div class="col s12 file-field input-field">
                                <img src="<?php echo image_thumb($agencyData->registrationCertificateImage, 200, 200, '', true) ?>" class="img-responsive">
                                <!--<a class="remove-img" href="<?php /*echo base_url('members/profile/remove/certificate/') */?>">Remove Certificate Image</a>-->
                            </div>
                        <?php } else {?>
                        <input type="hidden" id="registrationCertificateImageCheck" value="0">
                        <?php } ?>
                    </div>
                </div>

                <div class="signin-option-btn-wrap col s12 m12 l12">
                    <div class="btn-large waves-effect waves-light btn-content btn-form">
                        <input type="submit" value="Save">
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div id="changePassword" class="col s12">
    <div class="users-page-contents">
        <form action="<?php echo base_url('members/profile/changepassword') ?>" id="change-password-form" method="post">
            <div class="row">
                <div class="col s12 m6 l6">
                    <span class="filtertitle">Password Information</span>
                    <div class="row profile-group-wrap">
                        <div class="col s12 m12 l12">
                            <label for="oldPassword">Your old password</label>
                            <input id="oldPassword" value="" name="oldPassword" type="password">
                        </div>
                        <div class="col s12 m12 l12">
                            <label for="newPassword">Your new password</label>
                            <input id="newPassword" value="" name="newPassword" type="password">
                        </div>
                        <div class="col s12 m12 l12">
                            <label for="confirmNewPassword">Confirm your new password</label>
                            <input id="confirmNewPassword" value="" name="confirmNewPassword" type="password">
                        </div>
                    </div>
                </div>

                <div class="signin-option-btn-wrap col s12 m12 l12">
                    <div class="btn-large waves-effect waves-light btn-content btn-form">
                        <input type="submit" value="Save" onclick="showModal(this, 'md1'); return false">
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div id="md1" class="modal">
    <div class="modal-content">
        <h4>Please Confirm</h4>
        <p>Are you sure to proceed? You will be logged out. Please login with a new password </p>
    </div>
    <div class="modal-footer">
        <a href="#" class="waves-effect waves-red btn-flat" onclick="$('#md1').closeModal(); return false;">Cancel</a>
        <a href="#" class="waves-effect waves-green btn-flat" id="md1_YesBtn">OK</a>
    </div>
</div>

<script>
    function showModal(but, modal){
        $('#' + modal).openModal();
        $('#' + modal + '_YesBtn').click(function(){ $('#' + modal).closeModal(); $('#change-password-form').submit(); });
    }
</script>
