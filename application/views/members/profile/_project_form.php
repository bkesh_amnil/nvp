<div class="col s12 m6 l6">
    <ul class="tabs user-tab">
        <li class="tab"><a class="active" href="#project"><i class="mdi-maps-local-library"></i> Project Detail</a></li>
        <li class="tab <?php echo (segment(4) == '') ? 'disabled' : '' ?>"><a href="#project-criteria"><i class="mdi-maps-local-library"></i> Project Criteria</a></li>
    </ul>
</div>

<div id="project" class="col s12">
    <div class="users-page-contents">
        <a href="<?php echo base_url('members/profile#projects') ?>" class="mdi-content-clear right card__btn-close">
        </a>
        <?php frontFlash() ?>
        <form action="" id="project-detail-form" method="post">
            <input type="hidden" name="projectId" value="<?php echo $projectData->id ?>"/>
            <div class="row">
                <div class="col s12 m6 l6">
                    <span class="filtertitle">Basic Project Information</span>
                    <div class="row profile-group-wrap">
                        <div class=" col s12 m12 l12">
                            <label>Project Type *</label>
                            <select name="projectType">
                                <option value="" disabled selected>Select Project Type</option>
                                <?php foreach($GLOBALS['projectTypes'] as $key => $projectType) { ?>
                                    <option value="<?php echo $key ?>" <?php echo ($projectData->projectType == $projectType) ? 'selected' : '' ?> class="circle"><?php echo $projectType ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col s12 m12 l12">
                            <label for="name">Name*</label>
                            <input id="name" value="<?php echo $projectData->name ?>" name="name" type="text">
                        </div>
                        <div class="col s12 m12 l12">
                            <label for="purpose">Purpose *</label>
                            <textarea class="materialize-textarea" id="purpose" name="purpose" maxlength="150"><?php echo $projectData->purpose ?></textarea>
                        </div>
                        <div class="col s12 m12 l12">
                            <label for="additionalFacilitiesProvidedByAgency">Additional Facility Provided</label>
                            <textarea class="materialize-textarea" name="additionalFacilitiesProvidedByAgency" id="additionalFacilitiesProvidedByAgency"><?php echo $projectData->additionalFacilitiesProvidedByAgency ?></textarea>
                        </div>
                    </div>
                </div>

                <div class="col s12 m6 l6">
                    <span class="filtertitle">Project Detail</span>
                    <div class="row profile-group-wrap">
                        <div class="col s12 m12 l12">
                            <label for="totalNumberOfDaysVolunteersAreNeeded">Total Number of Days Volunteers are Required *</label>
                            <input id="totalNumberOfDaysVolunteersAreNeeded" value="<?php echo $projectData->totalNumberOfDaysVolunteersAreNeeded ?>" name="totalNumberOfDaysVolunteersAreNeeded" type="text">
                        </div>
                        <div class="col s12 m12 l12">
                            <label for="totalNumberOfHoursPerVolunteer">Total Number of Hour Per Volunteers *</label>
                            <input id="totalNumberOfHoursPerVolunteer" value="<?php echo $projectData->totalNumberOfHoursPerVolunteer ?>" name="totalNumberOfHoursPerVolunteer" type="text">
                        </div>
                        <div class="col s12 m12 l12">
                            <label for="timeOfEvent">Date of Project *</label>
                            <input id="dateOfEvent" class="datepicker" value="<?php echo $projectData->dateOfEvent ?>" name="dateOfEvent" placeholder="Date of Project" type="date">
                        </div>
                        <div class="col s12 m12 l12">
                            <label for="timeOfEvent">Time of Project *</label>
                            <input id="timeOfEvent" value="<?php echo $projectData->timeOfEvent ?>" name="timeOfEvent" type="text">
                        </div>
                        <div class="col s12 m12 l12">
                            <label for="additionalRemarks">Additional Remarks</label>
                            <textarea class="materialize-textarea" name="additionalRemarks" id="additionalRemarks"><?php echo $projectData->additionalRemarks ?></textarea>
                        </div>
                    </div>
                </div>

                <div class="signin-option-btn-wrap col s12 m12 l12">
                    <div class="btn-large waves-effect waves-light btn-content btn-form">
                        <input type="submit" value="Save">
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<?php if(segment(4) != '') { ?>
    <div id="project-criteria" class="col s12">
        <div class="users-page-contents">
            <a href="<?php echo base_url('members/profile#projects') ?>" class="mdi-content-clear right card__btn-close">
            </a>
            <form action="<?php echo base_url('members/profile/projectcriteria/'.segment(4)) ?>" id="project-criteria-form" method="post">
                <div class="row">
                    <?php
                    $academicHideClass = $projectCriteriaData->isMinimumEducationRequired == 'Yes' ? 'show-select' : 'hide-select';
                    $genderHideClass = $projectCriteriaData->isGenderRequired == 'Yes' ? 'show-select' : 'hide-select';
                    $skillHideClass = $projectCriteriaData->isSkillRequired == 'Yes' ? 'show-select' : 'hide-select';
                    $trainingHideClass = $projectCriteriaData->isTrainingRequired == 'Yes' ? 'show-select' : 'hide-select';
                    ?>
                    <div class="col s12 m12 l12">
                        <span class="filtertitle">Other Criteria</span>
                        <div class="row profile-group-wrap">
                            <div class="project-criteria">
                            <div class="switch">
                                <label>Is Minimum Education Required?</label>
                                <label class="right">
                                    No
                                    <input class="toggle-checkbox" data-view-toggle="criteria-academic" name="projectCriteria[isMinimumEducationRequired]" value="Yes" <?php echo ($projectCriteriaData->isMinimumEducationRequired == 'Yes') ? 'checked' : '' ?> type="checkbox">
                                    <span class="lever"></span>
                                    Yes
                                </label>
                            </div>
                            <div class="input-field col s12 m12 l12 <?php echo $academicHideClass ?>" id="criteria-academic">
                                <div>Select Academic Qualification</div>
                                <?php foreach($academicQualifications as $academicQualification) { ?>
                                <span class="checkboxes-wrap">
                                    <input type="checkbox" <?php echo ($projectCriteriaAcademicData && in_array($academicQualification->id, $projectCriteriaAcademicData)) ? 'checked' : '' ?> id="academicQualificationId-<?php echo $academicQualification->id ?>" name="academicQualificationId[]" value="<?php echo $academicQualification->id ?>"/>
                                    <label for="academicQualificationId-<?php echo $academicQualification->id ?>"><?php echo $academicQualification->name.'*' ?></label>
                                </span>
                                <?php } ?>
                            </div>
                            </div>

                            <div class="project-criteria">
                            <div class="switch">
                                <label>Is Gender Required?</label>
                                <label class="right">
                                    No
                                    <input class="toggle-checkbox" data-view-toggle="criteria-gender" name="projectCriteria[isGenderRequired]" value="Yes" <?php echo ($projectCriteriaData->isGenderRequired == 'Yes') ? 'checked' : '' ?> type="checkbox">
                                    <span class="lever"></span>
                                    Yes
                                </label>
                            </div>
                            <div class="input-field col s12 m12 l12 <?php echo $genderHideClass ?>" id="criteria-gender">
                                <div>Select Gender</div>
                                <?php foreach($allGender as $gender) { ?>
                                <span class="checkboxes-wrap">
                                    <input type="checkbox" <?php echo ($projectCriteriaGenderData && in_array($gender->id, $projectCriteriaGenderData)) ? 'checked' : '' ?> id="genderId-<?php echo $gender->id ?>" name="genderId[]" value="<?php echo $gender->id ?>"/>
                                    <label for="genderId-<?php echo $gender->id ?>"><?php echo $gender->name ?></label>
                                </span>
                                <?php } ?>
                            </div>
                            </div>

                            <div class="project-criteria">
                                <div class="switch">
                                    <label>Is Skill Required?</label>
                                    <label class="right">
                                        No
                                        <input class="toggle-checkbox" data-view-toggle="criteria-skill" name="projectCriteria[isSkillRequired]" value="Yes" <?php echo ($projectCriteriaData->isSkillRequired == 'Yes') ? 'checked' : '' ?> type="checkbox">
                                        <span class="lever"></span>
                                        Yes
                                    </label>
                                </div>
                                <div class="col s12 m12 l12 <?php echo $skillHideClass ?>" id="criteria-skill">
                                    <span>Select Skills</span>
                                    <?php foreach($skills as $skill) { ?>
                                    <span class="checkboxes-wrap">
                                        <input type="checkbox" <?php echo ($projectCriteriaSkillData && in_array($skill->id, $projectCriteriaSkillData)) ? 'checked' : '' ?> id="skillId-<?php echo $skill->id ?>" name="skillId[]" value="<?php echo $skill->id ?>"/>
                                        <label for="skillId-<?php echo $skill->id ?>"><?php echo $skill->name ?></label>
                                    </span>
                                    <?php } ?>
                                <div class="row">
                                <div class="col s12 m6 l12">
                                    <label for="otherSkills">Others (Specify)</label>
                                    <textarea class="materialize-textarea" name="otherSkills" id="otherSkills"><?php echo $projectData->otherSkills ?></textarea>
                                </div>
                                </div>
                                </div>
                            </div>

                            <div class="project-criteria">
                                <div class="switch">
                                    <label>Is Training Required?</label>
                                    <label class="right">
                                        No
                                        <input class="toggle-checkbox" data-view-toggle="criteria-training" name="projectCriteria[isTrainingRequired]" value="Yes" <?php echo ($projectCriteriaData->isTrainingRequired == 'Yes') ? 'checked' : '' ?> type="checkbox">
                                        <span class="lever"></span>
                                        Yes
                                    </label>
                                </div>
                                <div class="col s12 m12 l12 <?php echo $trainingHideClass ?>" id="criteria-training">
                                    <span>Select Training</span>
                                    <?php foreach($trainings as $training) { ?>
                                    <span class="checkboxes-wrap">
                                    <input type="checkbox" <?php echo ($projectCriteriaTrainingData && in_array($training->id, $projectCriteriaTrainingData)) ? 'checked' : '' ?> id="trainingId-<?php echo $training->id ?>" name="trainingId[]" value="<?php echo $training->id ?>"/>
                                        <label for="trainingId-<?php echo $training->id ?>"><?php echo $training->name ?></label>
                                    </span>
                                    <?php } ?>
                                <div class="row">
                                <div class="col s12 m6 l12">
                                    <label>Others (Specify)</label>
                                    <textarea class="materialize-textarea" name="otherTraining" id="otherTraining"><?php echo $projectData->otherTraining ?></textarea>
                                </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col s12 m12 l12">
                        <span class="filtertitle">Main Project Criteria</span>
                        <div class="row profile-group-wrap">
                            <div class="col s12 m4 l4">
                                <label for="totalNumberOfVolunteersRequired">Total Number of Volunteers Required</label>
                                <input id="totalNumberOfVolunteersRequired" value="<?php echo $projectCriteriaData->totalNumberOfVolunteersRequired ?>" name="projectCriteria[totalNumberOfVolunteersRequired]" type="number">
                            </div>
                            <div class="col s12 m4 l4">
                                <label for="ageGroupFrom">Age Group From</label>
                                <input id="ageGroupFrom" value="<?php if($projectCriteriaData->ageGroupFrom=='0'){echo"";}else{echo $projectCriteriaData->ageGroupFrom;} ?>" name="projectCriteria[ageGroupFrom]" type="number">
                            </div>
                            <div class="col s12 m4 l4">
                                <label for="ageGroupTo">Age Group To</label>
                                <input id="ageGroupTo" value="<?php if($projectCriteriaData->ageGroupTo=='0'){echo"";}else{echo $projectCriteriaData->ageGroupTo;} ?>" name="projectCriteria[ageGroupTo]" type="number">
                            </div>
                        </div>
                        <div class="row profile-group-wrap">
                            <div class="col s12 m4 l4">
                                <label>Select Zone</label>
                                <select class="icons" id="selectZone" name="projectCriteria[zoneId]">
                                    <option value="" disabled selected>Select Zone</option>
                                    <?php foreach($zones as $zone) { ?>
                                        <option <?php echo ($zone->id == $projectCriteriaData->zoneId) ? 'selected' : '' ?> value="<?php echo $zone->id ?>" class="circle"><?php echo trim($zone->name) ?></option>
                                    <?php } ?>
                                </select>                                
                            </div>
                            <div class="col s12 m4 l4">
                                <label>Select District</label>
                                <select class="icons" id="selectDistrict" name="projectCriteria[districtId]">
                                    <option value="" disabled selected>Select District</option>
                                    <?php foreach($districts as $district) { ?>
                                        <option <?php echo ($district->id == $projectCriteriaData->districtId) ? 'selected' : '' ?> value="<?php echo $district->id ?>" class="circle"><?php echo trim($district->name) ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col s12 m4 l4">
                                <label>Select VDC/Municipality</label>
                                <select class="icons" id="selectMunicipality" name="projectCriteria[municipalityId]">
                                    <option value="" disabled selected>Select VDC/Municipality</option>
                                    <?php foreach($municipalities as $municipality) { ?>
                                        <option <?php echo ($municipality->id == $projectCriteriaData->municipalityId) ? 'selected' : '' ?> value="<?php echo $municipality->id ?>" class="circle"><?php echo trim($municipality->name) ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="row profile-group-wrap">
                            <div class="col s12 m4 l4">
                                <label for="wardNumber">Ward Number</label>
                                <input id="wardNumber" value="<?php if($projectCriteriaData->wardNumber=='0'){echo "";}else{echo $projectCriteriaData->wardNumber;} ?>" name="projectCriteria[wardNumber]" type="number">
                            </div>
                        </div>
                    </div>

                    <div class="signin-option-btn-wrap col s12 m12 l12">
                        <div class="btn-large waves-effect waves-light btn-content btn-form">
                            <input type="submit" value="Save">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php } ?>