<div id="projects" class="col s12">
    <div class="users-page-contents">
        <div class="row">
            <?php
            $userType = get_userdata('userType');
            if($userType == 'Agency') { ?>
                <div class="col s12">
                    <a class="btn-create-project" href="<?php echo base_url('members/profile/project') ?>"><i class="mdi-content-add-circle"></i>Create Project</a>
                </div>
            <?php } ?>

            <?php if($userType == 'Volunteer') { ?>
                <form action="<?php echo base_url('members/profile') ?>" method="get">
                    <div class="col s6 m6 l3">
                        <select name="projectStatus" class="browser-default" onchange="this.form.submit()">
                            <option>Project status</option>
                            <option value="" <?php echo $this->input->get('projectStatus') == '' ? 'selected'  : '' ?>>All</option>
                            <option value="completed" <?php echo $this->input->get('projectStatus') == 'completed' ? 'selected'  : '' ?>>Completed</option>
                            <option value="ongoing" <?php echo $this->input->get('projectStatus') == 'ongoing' ? 'selected'  : '' ?>>On Going</option>
                        </select>
                    </div>
                    <div class="col s6 m6 l3">
                        <input type="text" placeholder="Search project" name="projectName" value="<?php echo $this->input->get('projectName') ?>"/>
                    </div>
                    <div class="col s12 l6 m12 right-align">
                        <input class="btn-large btn-content" type="submit" value="Apply filter"/>
                        <a href="<?php echo base_url('members/profile/?filter') ?>" class="btn-large btn-content">
                            Remove filter
                        </a>
                    </div>
                </form>
            <?php } ?>
            <div class="col s12">
                <table width="100%" class="striped responsive-table highlight projects-list-table">
                    <thead>
                    <tr>
                        <th data-field="id">SN</th>
                        <th data-field="name">Particulars</th>
                        <th data-field="status">Status</th>
                        <?php if($userType == 'Agency') { ?>
                            <th data-field="edit">Edit</th>
                            <th data-field="delete">Delete</th>
                        <?php } ?>
                    </tr>
                    </thead>

                    <tbody>
                    <?php if($projectList) { ?>
                        <?php foreach($projectList as $key => $project) { ?>
                            <tr>
                                <td><?= $key+1 ?></td>
                                <td>
                                    <?php if($userType == 'Volunteer') { ?>
                                        <a href="#project-<?php echo $project->id ?>" class="<?php echo $project->short_description != '' ? 'modal-trigger' : '' ?>"><?php echo $project->name ?></a>
                                        <?php if($project->short_description != '') { ?>
                                            <div id="project-<?php echo $project->id ?>" class="modal contact-wrap">
                                                <div class="modal-header">
                                                    <a class="modal-close right">
                                                        <i class="mdi-content-clear"></i>
                                                    </a>
                                                </div>
                                                <div class="modal-content">
                                                    <h5><?php echo $project->name ?></h5>
                                                    <p>
                                                        <?php echo $project->short_description ?>
                                                    </p>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } else {
                                        echo $project->name;
                                    } ?>
                                    <?php if($userType == 'Agency') { ?>
                                    <span> (Volunteers: <?php echo $project->totalVolunteers ?>, Hours: <?php echo $project->totalHours ?>)</span>
                                    <?php } ?>
                                </td>
                                <td><?php if($project->status == 'Ongoing')
                                        $class='active-project';
                                    else{
                                        $class='completed-project';
                                    }?>
                                    <span class="<?php echo $class?>" title="<?php echo $project->status ?>"></span></td>
                                <?php if($userType == 'Agency') {  ?>
                                    <td><a href="<?php echo base_url('members/profile/project/'.$project->slug) ?>"><i class="mdi-image-edit"></i></a></td>
                                    <td><a class="modal-trigger" href="<?php echo base_url('members/profile/projectremove/'.$project->slug) ?>" onclick="showModal(this, 'md1'); return false;"><i class="material-icons">delete</i></a>
                                    </td>
                                <?php } ?>
                            </tr>
                        <?php } ?>
                    <?php } else { ?>
                        <p>No Projects Found</p>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div id="md1" class="modal">
    <div class="modal-content">
        <h4>Please Confirm</h4>
        <p>Are you sure to proceed?</p>
    </div>
    <div class="modal-footer">
        <a href="#" class="waves-effect waves-red btn-flat" onclick="$('#md1').closeModal(); return false;">No</a>
        <a href="#" class="waves-effect waves-green btn-flat" id="md1_YesBtn">Yes</a>
    </div>
</div>

<script>
function showModal(but, modal){
        $('#' + modal).openModal();
        $('#' + modal + '_YesBtn').click(function(){ $('#' + modal).closeModal(); document.location = but.href; });
    }
</script>

