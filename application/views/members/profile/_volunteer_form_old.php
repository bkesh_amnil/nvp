<div class="col s12 m6 l6">
    <ul class="tabs user-tab">
        <li class="tab"><a class="active" href="#profile"><i class="mdi-maps-local-library"></i> Profile</a></li>
        <li class="tab"><a href="#settings"><i class="mdi-action-settings"></i> Settings</a></li>
        <li class="tab"><a href="#changePassword"><i class="mdi-action-lock"></i> Change Password</a></li>
    </ul>
</div>

<div id="profile" class="col s12">
    <div class="users-page-contents">
        <?php
        frontFlash();
        $profileCompletion = get_userdata('profileCompletion');
        if($profileCompletion < 100) {
        ?>

        <div class="meter animate" style="display: none">
            <span id="progressMeter"><span></span></span>
            <p id="progressMeterText"></p>
        </div>

        <?php } ?>
        <form action="<?php echo base_url('members/profile/edit') ?>" id="volunteer-profile-form" enctype="multipart/form-data" method="post">
            <div class="row">
                <div class="col s12 m12 l6">
                    <span class="filtertitle">Personal Information</span>
                    <div class="row profile-group-wrap">
                        <div class="col s12 file-field input-field">
                            <div>
                                <?php if(!empty($volunteerData->profilePicture))
                                $src = base_url($volunteerData->profilePicture);
                                else
                                $src = base_url('uploads/volunteer/profile-picture/default.jpg')?>
                                <img src="<?php echo $src?>" style="height:100px;width:100px;">
                                <input type="file" name="profilePicture" id="profilePicture">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path" type="text" placeholder="should not exceed 2MB">
                            </div>
                        </div>
                        <div class="input-field col s12 m12 l12">
                            <input id="full-name" value="<?php echo $volunteerData->fullName ?>" name="fullName" type="text">
                            <label for="full-name">Your full name</label>
                        </div>

                        <div class=" col s12 m12 l12 input-field">
                            <select class="icons" name="genderId">
                                <option value="" disabled selected>Select Gender</option>
                                <?php foreach($allGender as $gender) { ?>
                                    <option <?php echo ($gender->id == $volunteerData->genderId) ? 'selected' : '' ?> value="<?php echo $gender->id ?>" data-icon="<?php echo base_url($gender->icon) ?>" class="circle"><?php echo $gender->name ?></option>
                                <?php } ?>
                            </select>
                            <label>Select Gender</label>
                        </div>

                        <div class="col s12 m12 l12 input-field">
                            <input id="email" value="<?php echo $volunteerData->email ?>" name="email" type="text">
                            <label for="email">Email</label>
                        </div>

                        <div class="col s12 m12 l12 input-field">
                            <input id="dob" value="<?php echo $volunteerData->dateOfBirth ?>" name="dateOfBirth" type="date" class="datepicker validate" placeholder="Date of Birth">
                        </div>

                        <div class="col s12 m12 l12 input-field">
                            <input id="pnumber" value="<?php echo $volunteerData->phoneNumberMobile ?>" name="phoneNumberMobile" type="text">
                            <label for="pnumber">Mobile Number</label>
                        </div>

                        <div class="col s12 m12 l12 input-field">
                            <input id="rnumber" value="<?php echo $volunteerData->phoneNumberResidential ?>" name="phoneNumberResidential" type="text">
                            <label for="rnumber">Residential Number</label>
                        </div>

                        <div class="col s12 m12 l12 input-field">
                            <input id="emergencyContactFullName" value="<?php echo $volunteerData->emergencyContactFullName ?>" name="emergencyContactFullName" type="text">
                            <label for="emergencyContactFullName">Emergency Contact Full Name</label>
                        </div>

                        <div class="col s12 m12 l12 input-field">
                            <input id="emergencyContactPhoneNumber" value="<?php echo $volunteerData->emergencyContactPhoneNumber ?>" name="emergencyContactPhoneNumber" type="text">
                            <label for="emergencyContactPhoneNumber">Emergency Contact Phone Number</label>
                        </div>
                    </div>
                </div>

                <div class="col s12 m12 l6">
                    <span class="filtertitle">Address Information</span>
                    <div class="row profile-group-wrap">
                        <div class="col s12 m12 l12 input-field">
                            <select class="icons" id="selectZone" name="zoneId">
                                <option value="" disabled selected>Select Zone</option>
                                <?php foreach($zones as $zone) { ?>
                                    <option <?php echo ($zone->id == $volunteerData->zoneId) ? 'selected' : '' ?> value="<?php echo $zone->id ?>" class="circle"><?php echo trim($zone->name) ?></option>
                                <?php } ?>
                            </select>
                            <label>Select Zone</label>
                        </div>

                        <div class="input-field col s12 m12 l12">
                            <select class="icons" id="selectDistrict" name="districtId">
                                <option value="" disabled selected>Select District</option>
                                <?php foreach($districts as $district) { ?>
                                    <option <?php echo ($district->id == $volunteerData->districtId) ? 'selected' : '' ?> value="<?php echo $district->id ?>" class="circle"><?php echo trim($district->name) ?></option>
                                <?php } ?>
                            </select>
                            <label>Select District</label>
                        </div>

                        <div class="input-field col s12 m12 l12">
                            <select class="icons" id="selectMunicipality" name="municipalityId">
                                <option value="" disabled selected>Select VDC/Municipality</option>
                                <?php foreach($municipalities as $municipality) { ?>
                                    <option <?php echo ($municipality->id == $volunteerData->municipalityId) ? 'selected' : '' ?> value="<?php echo $municipality->id ?>" class="circle"><?php echo trim($municipality->name) ?></option>
                                <?php } ?>
                            </select>
                            <label>Select VDC/Municipality</label>
                        </div>

                        <div class="input-field col s12 m12 l12">
                            <input id="warNumber" value="<?php echo $volunteerData->wardNumber ?>" name="wardNumber" type="text">
                            <label for="warNumber">Ward Number</label>
                        </div>

                        <div class="input-field col s12 m12 l12">
                            <select class="icons" name="primaryModeOfCommunication">
                                <option value="" disabled selected>Select </option>
                                <?php foreach($GLOBALS['modeOfCommunications'] as $ind => $modeOfCommunication) { ?>
                                    <option <?php echo ($ind == $volunteerData->primaryModeOfCommunication) ? 'selected' : '' ?> value="<?php echo $ind ?>" class="circle"><?php echo $modeOfCommunication ?></option>
                                <?php } ?>
                            </select>
                            <label>First Preferred Option for contact</label>
                        </div>

                        <div class="input-field col s12 m12 l12">
                            <select class="icons" name="secondaryModeOfCommunication">
                                <option value="" disabled selected>Select </option>
                                <?php foreach($GLOBALS['modeOfCommunications'] as $ind => $modeOfCommunication) { ?>
                                    <option <?php echo ($ind == $volunteerData->secondaryModeOfCommunication) ? 'selected' : '' ?> value="<?php echo $ind ?>" class="circle"><?php echo $modeOfCommunication ?></option>
                                <?php } ?>
                            </select>
                            <label>Second Preferred Option for contact</label>
                        </div>
                    </div>
                </div>

                <div class="signin-option-btn-wrap col s12 m12 l12">
                    <div class="btn-large waves-effect waves-light btn-content btn-form">
                        <input type="submit" value="Save">
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div id="settings" class="col s12">
    <div class="users-page-contents">
        <form action="<?php echo base_url('members/profile/settings') ?>" id="volunteer-setting-form" method="post">
            <div class="row">
                <div class="col s12 m12 l6">
                    <span class="filtertitle">Academic Qualifications/Skills/Training</span>
                    <div class="row profile-group-wrap">
                        <div class="col s12 m12 l12 input-field">
                            <select class="icons" name="academicQualificationId">
                                <option value="" disabled selected>Select </option>
                                <?php foreach($academicQualifications as $academicQualification) { ?>
                                    <option <?php echo ($academicQualification->id == $volunteerData->academicQualificationId) ? 'selected' : '' ?> value="<?php echo $academicQualification->id ?>" class="circle"><?php echo trim($academicQualification->name) ?></option>
                                <?php } ?>
                            </select>
                            <label>Select Academic Qualification</label>
                        </div>

                        <div class="input-field col s12 m12 l12">
                            <select class="icons" name="availability">
                                <option value="" disabled selected>Select </option>
                                <?php foreach($GLOBALS['volunteerAvailability'] as $ind => $availability) { ?>
                                    <option <?php echo ($ind == $volunteerData->availability) ? 'selected' : '' ?> value="<?php echo $ind ?>" class="circle"><?php echo $availability ?></option>
                                <?php } ?>
                            </select>
                            <label>Select Availability</label>
                        </div>

                        <div class="input-field col s12 m12 l12">
                            <select class="icons volunteer-type" name="volunteerType">
                                <option value="" disabled selected>Select </option>
                                <?php foreach($GLOBALS['volunteerTypes'] as $ind => $volunteerType) { ?>
                                    <option <?php echo ($ind == $volunteerData->volunteerType) ? 'selected' : '' ?> value="<?php echo $ind ?>" class="circle"><?php echo $volunteerType ?></option>
                                <?php } ?>
                            </select>
                            <label>Select Volunteer Type</label>
                        </div>

                        <?php
                        if($volunteerData->volunteerType == 'fullTime') {
                            $hidePartTimeClass = 'hide-select';
                        } else if ($volunteerData->volunteerType == 'partTime') {
                            $hidePartTimeClass = 'show-select';
                        }
                        ?>
                        <div class="input-field col s12 m12 l12 volunteer-subtype <?php echo isset($hidePartTimeClass) ?  $hidePartTimeClass : '' ?>">
                            <select class="icons" name="volunteerSubType">
                                <option value="" disabled selected>Select Volunteer Sub Type</option>
                                <?php foreach($GLOBALS['volunteerSubTypes'] as $ind => $volunteerSubType) { ?>
                                    <option <?php echo (isset($hidePartTimeClass) && $ind == $volunteerData->volunteerSubType) ? 'selected' : '' ?> value="<?php echo $ind ?>" class="circle"><?php echo $volunteerSubType ?></option>
                                <?php } ?>
                            </select>
                            <label>Select Volunteer Sub Type</label>
                        </div>
                    </div>
                </div>

                <div class="col s12 m12 l6">
                    <span class="filtertitle">Volunteer Preferences</span>
                    <div class="row profile-group-wrap">
                        <div class=" col s12 m12 l12">
                            <div class="project-criteria">
                                <h6>Select Languages</h6>
                                <div class="selection-group">
                                    <?php foreach($languages as $language) { ?>
                                    <span class="checkboxes-wrap">
                                        <input type="checkbox" id="volunteerLanguage-<?php echo $language->id ?>" name="volunteerLanguage[]" <?php echo $volunteerLanguage && in_array($language->id, $volunteerLanguage) ? 'checked' : '' ?> value="<?php echo $language->id ?>">
                                        <label for="volunteerLanguage-<?php echo $language->id ?>"><?php echo $language->name ?></label>
                                    </span>
                                    <?php } ?>
                                </div>
                            </div>                            
                        </div>

                        <div class=" col s12 m12 l12">
                            <div class="project-criteria">
                                <h6>Select Skills</h6>
                                <div class="selection-group">
                                    <?php foreach($skills as $skill) { ?>
                                    <span class="checkboxes-wrap">
                                        <input type="checkbox" id="volunteerSkill-<?php echo $skill->id ?>" name="volunteerSkill[]" <?php echo $volunteerSkill && in_array($skill->id, $volunteerSkill) ? 'checked' : '' ?> value="<?php echo $skill->id ?>">
                                        <label for="volunteerSkill-<?php echo $skill->id ?>"><?php echo $skill->name ?></label>
                                    </span>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                        <div class=" col s12 m12 l12">
                            <div class="project-criteria">
                                <h6>Select Received Trainings</h6>
                                <div class="selection-group">
                                    <?php foreach($trainings as $training) { ?>
                                    <span class="checkboxes-wrap">
                                        <input type="checkbox" id="volunteerTraining-<?php echo $training->id ?>" name="volunteerTraining[]" <?php echo $volunteerTrainingReceived && in_array($training->id, $volunteerTrainingReceived) ? 'checked' : '' ?> value="<?php echo $training->id ?>">
                                        <label for="volunteerTraining-<?php echo $training->id ?>"><?php echo $training->name ?></label>
                                    </span>
                                    <?php } ?>
                                </div>
                            </div>                            
                        </div>

                        <div class=" col s12 m12 l12">
                            <div class="project-criteria">
                            <h6>Select Trainings Interested In</h6>
                                <div class="selection-group">
                                    <?php foreach($trainings as $training) { ?>
                                    <span class="checkboxes-wrap">
                                        <input type="checkbox" id="volunteerTrainingInterestedIn-<?php echo $training->id ?>" name="volunteerTrainingInterestedIn[]" <?php echo $volunteerTrainingInterestedIn && in_array($training->id, $volunteerTrainingInterestedIn) ? 'checked' : '' ?> value="<?php echo $training->id ?>">
                                        <label for="volunteerTrainingInterestedIn-<?php echo $training->id ?>"><?php echo $training->name ?></label>
                                    </span>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="signin-option-btn-wrap col s12 m12 l12">
                    <div class="btn-large waves-effect waves-light btn-content btn-form">
                        <input type="submit" value="Save">
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div id="changePassword" class="col s12">
    <div class="users-page-contents">
        <form action="<?php echo base_url('members/profile/changepassword') ?>" id="change-password-form" method="post">
            <div class="row">
                <div class="col s12 m6 l6">
                    <span class="filtertitle">Password Information</span>
                    <div class="row profile-group-wrap">
                        <div class="col s12 m12 l12 input-field">
                            <input id="oldPassword" value="" name="oldPassword" type="password">
                            <label for="oldPassword">Your old password</label>
                        </div>
                        <div class="col s12 m12 l12 input-field">
                            <input id="newPassword" value="" name="newPassword" type="password">
                            <label for="newPassword">Your new password</label>
                        </div>
                        <div class="col s12 m12 l12 input-field">
                            <input id="confirmNewPassword" value="" name="confirmNewPassword" type="password">
                            <label for="confirmNewPassword">Confirm your new password</label>
                        </div>
                    </div>
                </div>

                <div class="signin-option-btn-wrap col s12 m12 l12">
                    <div class="btn-large waves-effect waves-light btn-content btn-form">
                        <input type="submit" value="Save">
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>