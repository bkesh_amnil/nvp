<div class="col s12 m6 l8">
    <ul class="tabs user-tab">
        <li class="tab"><a class="active" href="#profile"><i class="mdi-maps-local-library"></i> Profile</a></li>
        <li class="tab"><a href="#settings"><i class="mdi-action-settings"></i> Settings</a></li>
        <?php if (empty($volunteerData->socialId)) { ?>
            <li class="tab"><a href="#changePassword"><i class="mdi-action-lock"></i> Change Password</a></li>
        <?php } ?>
    </ul>
</div>

<div id="profile" class="col s12">
    <div class="users-page-contents">
        <?php
        frontFlash();
        $profileCompletion = get_userdata('profileCompletion');
        if ($profileCompletion < 100) {
            ?>

            <div class="meter animate" style="display: none">
                <span id="progressMeter"><span></span></span>

                <p id="progressMeterText"></p>
            </div>

        <?php } ?>

        <div class="row">
            <div class="col s12 m12 l12">
                <div class="user-profile-name row">
                    <div class="col s12 m12 offset-m2 l12 offset-l4">
                        <div class="card-panel">
                            <div class="row valign-wrapper">
                                <div id="croppic"></div>
                                <div class="col s12 m12 l12">
                                    <?php $src = get_userdata('profilePicture'); ?>
                                    <div id="cropPP" class="circle">
                                        <img class="responsive-img" src="<?php echo $src; ?>" alt=""/>

                                        <div class="upload-pic-layout">
                                            <i class="mdi-file-cloud-upload"></i>
                                            <span>Upload Profile</span>
                                        </div>
                                    </div>
                                    <!--<input type="file" name="profilePicture"
                                           id="profilePicture">-->
                                </div>
                                <div class="col s12 m12 l12">
                                    <span class="user-name"><?php echo $volunteerData->fullName; ?></span>
                                    <span class="user-id">NVP<?php echo $volunteerData->id; ?></span>
                                    <?php
                                    if ($volunteerHours && $volunteerHours->totalHours) { ?>
                                        <span class="login-status">
                                            You have logged a total of <?php echo $volunteerHours->totalHours ?> hours with NVP.
                                        </span>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <form action="<?php echo base_url('members/profile/edit') ?>" id="volunteer-profile-form"
              enctype="multipart/form-data" method="post">
            <div class="row" >
                <div class="col l2">
                    <label>Notification Status</label>

                    <div class="switch">
                        <label>
                            Off
                            <input class="toggle-checkbox" name="notification_status" value="Active" <?php echo ($volunteerData->notification_status == 'Active') ? 'checked' : '' ?> type="checkbox">
                            <span class="lever"></span>
                            On
                        </label>
                    </div>
                </div>
                <div class="col l2">
                    <label>Account Status</label>

                    <div class="switch">
                        <label>
                            Off
                            <input class="toggle-checkbox" name="account_status" value="Active" <?php echo ($volunteerData->account_status == 'Active') ? 'checked' : '' ?> type="checkbox">
                            <span class="lever"></span>
                            On
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <input id="id" value="<?php echo $volunteerData->id?>" type="hidden" name="id">
                <input id="table" value="nvp_volunteer" type="hidden" name="table">

                <div class="col s12 m12 l6">
                    <span class="filtertitle">Personal Information</span>

                    <div class="row profile-group-wrap">
                        <div class="col s12 m12 l12">
                            <label for="full-name">Your full name *</label>
                            <input id="full-name" value="<?php echo $volunteerData->fullName ?>" name="fullName"
                                   type="text">
                        </div>

                        <div class=" col s12 m12 l12">
                            <label>Select Gender *</label>
                            <select class="icons" name="genderId">
                                <option value="" disabled selected>Select Gender</option>
                                <?php foreach ($allGender as $gender) { ?>
                                    <option <?php echo ($gender->id == $volunteerData->genderId) ? 'selected' : '' ?>
                                        value="<?php echo $gender->id ?>"
                                        data-icon="<?php echo base_url($gender->icon) ?>"
                                        class="circle"><?php echo $gender->name ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="col s12 m12 l12">
                            <label for="email">Email *</label>
                            <input id="email" value="<?php echo $volunteerData->email ?>" name="email"
                                   type="text" <?php echo(!empty($volunteerData->email) ? 'disabled' : '') ?>>
                        </div>

                        <div class="col s12 m12 l12">
                            <label>Date of Birth *</label>
                            <input id="dob" value="<?php echo $volunteerData->dateOfBirth ?>" name="dateOfBirth"
                                   type="date" class="datepicker validate" placeholder="Date of Birth">
                        </div>

                        <div class="col s12 m12 l12" id="vounteernumber">
                            <label for="pnumber">Mobile Number *</label>
                            <input id="pnumber" value="<?php echo $volunteerData->phoneNumberMobile ?>"
                                   name="phoneNumberMobile" type="text">
                        </div>
                        <div class="col s12 m12 l12">
                            <label for="rnumber">Residential Number</label>
                            <input id="rnumber" value="<?php echo $volunteerData->phoneNumberResidential ?>"
                                   name="phoneNumberResidential" type="text">
                        </div>

                        <div class="col s12 m12 l12">

                            <label for="emergencyContactFullName">Emergency Contact Full Name *</label>
                            <input id="emergencyContactFullName"
                                   value="<?php echo $volunteerData->emergencyContactFullName ?>"
                                   name="emergencyContactFullName" type="text">
                        </div>

                        <div class="col s12 m12 l12">
                            <label for="emergencyContactPhoneNumber">Emergency Contact Phone Number *</label>
                            <input id="emergencyContactPhoneNumber"
                                   value="<?php echo $volunteerData->emergencyContactPhoneNumber ?>"
                                   name="emergencyContactPhoneNumber" type="text">
                        </div>
                    </div>
                </div>

                <div class="col s12 m12 l6">
                    <span class="filtertitle">Address Information</span>

                    <div class="row profile-group-wrap">

                        <div class="col s12 m12 l12">
                            <label>Select Zone *</label>
                            <select class="icons" id="selectZone" name="zoneId">
                                <option value="" disabled selected>Select Zone</option>
                                <?php foreach ($zones as $zone) { ?>
                                    <option <?php echo ($zone->id == $volunteerData->zoneId) ? 'selected' : '' ?>
                                        value="<?php echo $zone->id ?>"
                                        class="circle"><?php echo trim($zone->name) ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="col s12 m12 l12">
                            <label>Select District *</label>
                            <select class="icons" id="selectDistrict" name="districtId">
                                <option value="" disabled selected>Select District</option>
                                <?php foreach ($districts as $district) { ?>
                                    <option <?php echo ($district->id == $volunteerData->districtId) ? 'selected' : '' ?>
                                        value="<?php echo $district->id ?>"
                                        class="circle"><?php echo trim($district->name) ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="col s12 m12 l12">
                            <label>Select VDC/Municipality *</label>
                            <select class="icons" id="selectMunicipality" name="municipalityId">
                                <option value="" disabled selected>Select VDC/Municipality</option>
                                <?php foreach ($municipalities as $municipality) { ?>
                                    <option <?php echo $municipality->id == $volunteerData->municipalityId ? 'selected' : '' ?> value="<?php echo $municipality->id ?>" class="circle"><?php echo trim($municipality->name) ?></option>
                                <?php } ?>
                            </select>
                        </div>


                        <div class="col s12 m12 l12">
                            <label for="warNumber">Ward Number *</label>
                            <input id="warNumber" value="<?php echo $volunteerData->wardNumber ?>" name="wardNumber"
                                   type="text">
                        </div>

                    </div>
                </div>

                <div class="signin-option-btn-wrap col s12 m12 l12">
                    <div class="btn-large waves-effect waves-light btn-content btn-form">
                        <input type="submit" value="Save">
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div id="settings" class="col s12">
    <div class="users-page-contents">
        <form action="<?php echo base_url('members/profile/settings') ?>" id="volunteer-setting-form" method="post">
            <div class="row">
                <div class="col s12 m12 l12">
                    <span class="filtertitle">Academic Qualifications/Skills/Training</span>

                    <div class="row profile-group-wrap">
                        <div class=" col s12 m4 l4">
                            <label>Select Academic Qualification</label>
                            <select class="icons" name="academicQualificationId">
                                <option value="" disabled selected>Select</option>
                                <?php foreach ($academicQualifications as $academicQualification) { ?>
                                    <option <?php echo ($academicQualification->id == $volunteerData->academicQualificationId) ? 'selected' : '' ?>
                                        value="<?php echo $academicQualification->id ?>"
                                        class="circle"><?php echo trim($academicQualification->name) ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class=" col s12 m12 l12">
                            <div class="project-criteria">
                                <h6>Select Skills</h6>

                                <div class="selection-group">
                                    <?php foreach ($skills as $skill) { ?>
                                        <span class="checkboxes-wrap">
                                        <input type="checkbox" id="volunteerSkill-<?php echo $skill->id ?>"
                                               name="volunteerSkill[]" <?php echo $volunteerSkill && in_array($skill->id, $volunteerSkill) ? 'checked' : '' ?>
                                               value="<?php echo $skill->id ?>">
                                        <label
                                            for="volunteerSkill-<?php echo $skill->id ?>"><?php echo $skill->name ?></label>
                                    </span>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                        <div class=" col s12 m12 l12">
                            <div class="project-criteria">
                                <h6>Select Received Trainings</h6>

                                <div class="selection-group">
                                    <?php foreach ($trainings as $training) { ?>
                                        <span class="checkboxes-wrap">
                                        <input type="checkbox" id="volunteerTraining-<?php echo $training->id ?>"
                                               name="volunteerTraining[]" <?php echo $volunteerTrainingReceived && in_array($training->id, $volunteerTrainingReceived) ? 'checked' : '' ?>
                                               value="<?php echo $training->id ?>">
                                        <label
                                            for="volunteerTraining-<?php echo $training->id ?>"><?php echo $training->name ?></label>
                                    </span>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                        <div class=" col s12 m12 l12">
                            <div class="project-criteria">
                                <h6>Select Trainings Interested In</h6>

                                <div class="selection-group">
                                    <?php foreach ($trainings as $training) { ?>
                                        <span class="checkboxes-wrap">
                                        <input type="checkbox"
                                               id="volunteerTrainingInterestedIn-<?php echo $training->id ?>"
                                               name="volunteerTrainingInterestedIn[]" <?php echo $volunteerTrainingInterestedIn && in_array($training->id, $volunteerTrainingInterestedIn) ? 'checked' : '' ?>
                                               value="<?php echo $training->id ?>">
                                        <label
                                            for="volunteerTrainingInterestedIn-<?php echo $training->id ?>"><?php echo $training->name ?></label>
                                    </span>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                        <div class=" col s12 m12 l12">
                            <div class="project-criteria">
                                <h6>Select Languages</h6>

                                <div class="selection-group">
                                    <?php foreach ($languages as $language) { ?>
                                        <span class="checkboxes-wrap">
                                        <input type="checkbox" id="volunteerLanguage-<?php echo $language->id ?>"
                                               name="volunteerLanguage[]" <?php echo $volunteerLanguage && in_array($language->id, $volunteerLanguage) ? 'checked' : '' ?>
                                               value="<?php echo $language->id ?>">
                                        <label
                                            for="volunteerLanguage-<?php echo $language->id ?>"><?php echo $language->name ?></label>
                                    </span>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col s12 m12 l12">
                    <span class="filtertitle">Volunteer Avaibility</span>

                    <div class="row profile-group-wrap">
                        <div class="col s12 m4 l4">
                            <label>Select Availability</label>
                            <select class="icons" name="availability">
                                <option value="" disabled selected>Select</option>
                                <?php foreach ($GLOBALS['volunteerAvailability'] as $ind => $availability) { ?>
                                    <option <?php echo ($ind == $volunteerData->availability) ? 'selected' : '' ?>
                                        value="<?php echo $ind ?>" class="circle"><?php echo $availability ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="col s12 m4 l4">
                            <label>Select Volunteer Type</label>
                            <select class="icons volunteer-type" name="volunteerType">
                                <option value="" disabled selected>Select</option>
                                <?php foreach ($GLOBALS['volunteerTypes'] as $ind => $volunteerType) { ?>
                                    <option <?php echo ($ind == $volunteerData->volunteerType) ? 'selected' : '' ?>
                                        value="<?php echo $ind ?>" class="circle"><?php echo $volunteerType ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <?php
                        if ($volunteerData->volunteerType == 'fullTime') {
                            $hidePartTimeClass = 'hide-select';
                        } else if ($volunteerData->volunteerType == 'partTime') {
                            $hidePartTimeClass = 'show-select';
                        }
                        ?>
                        <div
                            class="col s12 m4 l4 volunteer-subtype <?php echo isset($hidePartTimeClass) ? $hidePartTimeClass : '' ?>">
                            <label>Select Volunteer Sub Type</label>
                            <select class="icons" name="volunteerSubType">
                                <option value="" disabled selected>Select Volunteer Sub Type</option>
                                <?php foreach ($GLOBALS['volunteerSubTypes'] as $ind => $volunteerSubType) { ?>
                                    <option <?php echo (isset($hidePartTimeClass) && $ind == $volunteerData->volunteerSubType) ? 'selected' : '' ?>
                                        value="<?php echo $ind ?>"
                                        class="circle"><?php echo $volunteerSubType ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="col s12 m12 l12">
                    <span class="filtertitle">Mode of Communication</span>

                    <div class="row profile-group-wrap">
                        <div class="col s12 m12 l6">
                            <label>First Preferred Option for contact (Default Email) </label>
                            <select class="icons" name="primaryModeOfCommunication">
                                <option value="" disabled selected>Select</option>
                                <?php foreach ($GLOBALS['modeOfCommunications'] as $ind => $modeOfCommunication) { ?>
                                    <option <?php echo ($ind == $volunteerData->primaryModeOfCommunication) ? 'selected' : '' ?>
                                        value="<?php echo $ind ?>"
                                        class="circle"><?php echo $modeOfCommunication ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="col s12 m12 l6">
                            <label>Second Preferred Option for contact (Default Email)</label>
                            <select class="icons" name="secondaryModeOfCommunication">
                                <option value="" disabled selected>Select</option>
                                <?php foreach ($GLOBALS['modeOfCommunications'] as $ind => $modeOfCommunication) { ?>
                                    <option <?php echo ($ind == $volunteerData->secondaryModeOfCommunication) ? 'selected' : '' ?>
                                        value="<?php echo $ind ?>"
                                        class="circle"><?php echo $modeOfCommunication ?></option>
                                <?php } ?>
                            </select>
                        </div>

                    </div>

                </div>

                <div class="signin-option-btn-wrap col s12 m12 l12">
                    <div class="btn-large waves-effect waves-light btn-content btn-form">
                        <input type="submit" value="Save">
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php if (empty($volunteerData->socialId)) { ?>
    <div id="changePassword" class="col s12">
        <div class="users-page-contents">
            <form action="<?php echo base_url('members/profile/changepassword') ?>" id="change-password-form"
                  method="post">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <span class="filtertitle">Password Information</span>

                        <div class="row profile-group-wrap">
                            <div class="col s12 m12 l12">
                                <label for="oldPassword">Your old password*</label>
                                <input id="oldPassword" value="" name="oldPassword" type="password">
                            </div>
                            <div class="col s12 m12 l12">
                                <label for="newPassword">Your new password*</label>
                                <input id="newPassword" value="" name="newPassword" type="password">
                            </div>
                            <div class="col s12 m12 l12">
                                <label for="confirmNewPassword">Confirm your new password*</label>
                                <input id="confirmNewPassword" value="" name="confirmNewPassword" type="password">
                            </div>
                        </div>
                    </div>

                    <div class="signin-option-btn-wrap col s12 m12 l12">
                        <div class="btn-large waves-effect waves-light btn-content btn-form">
                            <input type="submit" value="Save"
                                   onclick="showModal(this, 'md1'); return false">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div id="md1" class="modal">
    <div class="modal-content">
        <h4>Please Confirm</h4>
        <p>Are you sure to proceed? You will be logged out. Please login with a new password </p>
    </div>
    <div class="modal-footer">
        <a href="#" class="waves-effect waves-red btn-flat" onclick="$('#md1').closeModal(); return false;">Cancel</a>
        <a href="#" class="waves-effect waves-green btn-flat" id="md1_YesBtn">OK</a>
    </div>
</div>

<script>
    function showModal(but, modal){
        if($('#change-password-form').valid()) {
            $('#' + modal).openModal();
            $('#' + modal + '_YesBtn').click(function(){ $('#' + modal).closeModal(); $('#change-password-form').submit(); });
        }
    }
</script>
<?php } ?>