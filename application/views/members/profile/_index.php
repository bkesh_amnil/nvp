<div class="container">
    <div class="row">
        <?php $this->load->view($subBody) ?>
    </div>
</div>

<!-- Modal Structure -->
<div id="user-posts" class="modal contact-wrap">
    <div class="modal-header">
        <a class="btn-floating btn-large waves-effect waves-light red modal-close right"><i
                class="mdi-content-clear"></i></a>

        <div class="message-holder" style="display: none;">
            <span class="close"><i class="mdi-content-clear"></i></span>
            <span class="message"></span>
        </div>
    </div>
    <form class="submit-review" action="<?php echo base_url('members/feeds') ?>"
          method="post" enctype="multipart/form-data">
        <div class="modal-content">
            <h4>Your Post</h4>

            <div class="row">
                <div class="input-field col s12">
                    <i class="mdi-social-person prefix"></i>
                    <input id="title" name="name" type="text">
                    <label for="title">Title</label>
                </div>
                <div class="input-field col s12">
                    <i class="mdi-editor-insert-comment prefix"></i>
                    <textarea id="description" name="description" class="materialize-textarea"></textarea>
                    <label for="description">What's on your mind?</label>
                </div>
                
            </div>
        </div>
        <div class="col l12">
            <div class="btn-large waves-effect waves-light btn-content right btn-form">
                <input type="submit" value="Save Post" id="save-post">
            </div>
        </div>
    </form>
</div>