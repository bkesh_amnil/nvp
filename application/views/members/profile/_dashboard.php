<div class="col s12 m6 l6">
    <ul class="tabs user-tab">
        <li class="tab"><a href="#news-events"><i class="mdi-maps-local-library"></i> News Feed</a></li>
        <li class="tab"><a href="#projects" class="<?php echo ($this->input->get()) ? 'active' : '' ?>"><i class="mdi-action-work"></i>Projects</a></li>
        <?php if(get_userdata('userType') == 'Agency') { ?>
            <li class="tab"><a href="#search"><i class="mdi-action-search"></i>Search Volunteer</a></li>
        <?php } ?>
    </ul>
</div>

<!--========================Start News & Events Tab===================-->
<?php $this->load->view('members/feeds/_list') ?>
<!--========================End News & Events Tab=====================-->

<!--========================Start Projects Tab========================-->
<?php $this->load->view('members/profile/_project_list') ?>
<!--========================End Projects Tab==========================-->

<?php if(get_userdata('userType') == 'Agency') { ?>
<!--========================Start Search Tab==========================-->
    <?php $this->load->view('members/profile/_volunteer_search') ?>
<!--========================End Search Tab============================-->
<?php } ?>