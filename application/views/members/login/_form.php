<div class="container">
    <div class="center">
        <?php $this->load->view('frontend/include/headings', array('headingData' => $signInContent)) ?>
    </div>

    <div class="signin-sign-up-wrap">
        <div class="row">
            <div class="col s12 m6 l6">
                <div class="center">
                    <h3>SIGN IN USING YOUR SOCIAL ACCOUNT</h3>
                    <a class="connet-facebook" href="<?php echo base_url('members/sociallogin/login/Facebook') ?>">CONNECT WITH FACEBOOK</a>
                    <a class="connet-twitter" href="<?php echo base_url('members/sociallogin/login/Twitter') ?>">CONNECT WITH TWITTER</a>
                </div>
            </div>
            <form class="col s12 m6 l6" id="sign-in-form" action="" method="post">
                <h3>SIGN IN WITH EMAIL / Phone Number</h3>
                <?php frontFlash() ?>
                <div class="row">
                    <div class="input-field col s12">
                        <i class="mdi-maps-local-post-office prefix"></i>
                        <input name="username" type="text" placeholder="Email / Phone number*">
                    </div>
                    <div class="input-field col s12">
                        <i class="mdi-action-lock prefix"></i>
                        <input name="password" type="password" placeholder="Password*">
                    </div>
                    <div class="col s12">
                        <input type="checkbox" name="rememberMe" id="rememberMe" />
                        <label for="rememberMe">Remember me</label>
                    </div>
                    <div class="signin-option-btn-wrap col l12">
                        <div class="btn-large waves-effect waves-light btn-content btn-form">
                            <input type="submit" value="SIGN IN">
                        </div>
                        <a class="forgot-pass modal-trigger" href="#forgetPassword">Forgot your password ?</a>
                        <span>
                            Don’t have an account yet ? <a href="<?php echo base_url('members/register') ?>">JOIN NOW</a>
                        </span>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- forget password modal -->
<div id="forgetPassword" class="modal password-forgot">
    <form action="<?php echo base_url('members/retrievepassword') ?>" method="post">
        <div>
        <div class="modal-header">
            <div class="row">
                <div class="col s12">
                    <h4>Retrieve Password</h4>
                    <a class="mdi-content-clear right card__btn-close modal-close"></a>
                </div>     
            </div>     

            <div class="message-holder" style="display: none;">
                <span class="close"><i class="mdi-content-clear"></i></span>
                <span class="message"></span>
            </div>
        </div>
        <div class="modal-content">
            <div class="row">           
                <div class="input-field col s12">
                    <i class="mdi-maps-local-post-office prefix"></i>
                    <input id="retrieveEmail" name="retrieveEmail" type="text" placeholder="Enter your email address">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="row">
                <div class="col s12">
                    <div class="btn-large waves-effect waves-light btn-content btn-form">
                        <input type="submit" value="Retrieve Password">
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>