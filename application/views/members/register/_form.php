<div class="container">
    <div class="center">
        <?php $this->load->view('frontend/include/headings', array('headingData' => $signUpContent)) ?>
    </div>

    <div class="row">
        <div class="col s12">
            <ul class="tabs sign-up-tab">
                <li class="tab col s3"><a class="active" href="#be-volunteer">Be A Volunteer <i class="mdi-social-person"></i></a></li>
                <li class="tab col s3"><a class="need-volunteer" href="#need-volunteer">Ask for Volunteers <i class="mdi-action-account-balance"></i></a></li>
            </ul>
        </div>

        <div id="be-volunteer" class="col s12">
            <div class="signin-sign-up-wrap sign-up">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <div class="center">
                            <h3>SIGN UP USING YOUR SOCIAL ACCOUNT</h3>
                            <a class="connet-facebook" href="<?php echo base_url('members/sociallogin/login/Facebook') ?>">CONNECT WITH FACEBOOK</a>
                            <a class="connet-twitter" href="<?php echo base_url('members/sociallogin/login/Twitter') ?>">CONNECT WITH Twitter</a>
                        </div>
                    </div>

                    <form action="" method="post" class="col s12 m6 l6" id="register-form" novalidate>
                        <input type="hidden" value="Volunteer" name="userType"/>
                        <h3>SIGN UP WITH EMAIL</h3>
                        <div class="row">
                            <div class="input-field col s12 m12 l12">
                                <i class="mdi-social-person prefix"></i>
                                <input id="full-name" name="fullName" type="text" class="validate" placeholder="Your full name *">
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12 m12 l6">
                                <select name="genderId" class="icons">
                                    <option value="" disabled selected>Select gender</option>
                                    <?php foreach($allGender as $gender) { ?>
                                        <option value="<?php echo $gender->id ?>" data-icon="<?php echo base_url($gender->icon) ?>" class="circle"><?php echo $gender->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="col s12 m12 l6 input-field">
                                <i class="mdi-editor-insert-invitation prefix"></i>
                                <input id="dob" name="dateOfBirth" type="date" class="datepicker validate" placeholder="Date of Birth">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col s12 m12 l6 input-field" id="number">
                                <i class="mdi-communication-dialpad prefix"></i>
                                <input id="phnumber" name="phoneNumberMobile" type="text" class="validate" placeholder="Mobile Number *">
                            </div>

                            <div class="col s12 m12 l6 input-field">
                                <i class="mdi-communication-phone prefix"></i>
                                <input id="rnumber" name="phoneNumberResidential" type="text" class="validate" placeholder="Residential Number">
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12 m12 l12" id="volunteer-email">
                                <i class="mdi-maps-local-post-office prefix"></i>
                                <input id="email" name="email" type="text" class="validate" placeholder="Email *">
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12 m12 l12">
                                <i class="mdi-action-lock prefix"></i>
                                <input id="password" name="password" type="password" class="validate" placeholder="Password *">
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12 m12 l12">
                                <i class="mdi-action-lock prefix"></i>
                                <input id="repassword" name="repassword" type="password" class="validate" placeholder="Re Password *">
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12 m12 l12">
                                <input type="checkbox" name="agree" id="agree" />
                                <label for="agree">By creating an account, you agree to our <a href="<?php echo base_url('terms-and-conditions') ?>" target="_blank">Terms and Conditions</a> and <a href="<?php echo base_url('privacy-policy') ?>" target="_blank">Privacy Policy </a>.</label>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="signin-option-btn-wrap col s12 m12 l12">
                                <div class="btn-large waves-effect waves-light btn-content btn-form">
                                    <input type="submit" value="SIGN UP">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="need-volunteer" class="col s12">
            <div class="signin-sign-up-wrap sign-up">
                <div class="row">
                    <form action="" method="post" id="agency-register-form" novalidate>
                        <input type="hidden" name="userType" value="Agency"/>
                        <div class="row">
                            <div class="col s12 m12 l4">
                                <span class="filtertitle">Agency Information</span>
                                <div class="row profile-group-wrap">
                                    <div class="input-field col s12 m12 l12">
                                        <i class="mdi-social-person prefix"></i>
                                        <input name="name" type="text" class="validate" placeholder="Agency Name *">
                                    </div>
                                    <div class="input-field col s12 m12 l12" id="agencynumber">
                                        <i class="mdi-communication-dialpad prefix"></i>
                                        <input name="phoneNumber" id="agencyphnumber" type="text" class="validate" placeholder="Agency Phone Number *">                                        <label for="phoneNumber"></label>
                                    </div>
                                    <div class="input-field col s12 m12 l12" id="agency-sign-email">
                                        <i class="mdi-maps-local-post-office prefix"></i>
                                        <input name="email" id="agency-email" type="text" class="validate" placeholder="Agency Email *">
                                    </div>
                                    <div class="input-field col s12 m12 l12">
                                        <i class="mdi-social-person prefix"></i>
                                        <textarea name="address" class="materialize-textarea" placeholder="Agency Address *"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="col s12 m12 l4">
                                <span class="filtertitle">Primary Contact Information</span>
                                <div class="row profile-group-wrap">
                                    <div class="input-field col s12 m12 l12">
                                        <i class="mdi-social-person prefix"></i>
                                        <input name="primaryContactPersonFullName" type="text" class="validate" placeholder="Primary Contact Name *">
                                    </div>
                                    <div class="input-field col s12 m12 l12">
                                        <i class="mdi-maps-local-post-office prefix"></i>
                                        <input id="primaryContactPersonEmail" name="primaryContactPersonEmail" type="text" class="validate" placeholder="Primary Contact Email *">
                                    </div>
                                    <div class="input-field col s12 m12 l12">
                                        <i class="mdi-communication-dialpad prefix"></i>
                                        <input id="primaryContactPersonPersonalPhoneNumber" name="primaryContactPersonPersonalPhoneNumber" type="text" class="validate" placeholder="Primary Contact Phone No *">
                                    </div>
                                    <div class="input-field col s12 m12 l12">
                                        <i class="mdi-social-person prefix"></i>
                                        <input id="primaryContactPersonDesignationInAgency" name="primaryContactPersonDesignationInAgency" type="text" class="validate" placeholder="Primary Contact Designation">
                                    </div>
                                </div>
                            </div>

                            <div class="col s12 m12 l4">
                                <span class="filtertitle">Password</span>
                                <div class="row profile-group-wrap">
                                    <div class="input-field col s12 m12 l12">
                                        <i class="mdi-action-lock prefix"></i>
                                        <input id="agency-password" name="password" type="password" class="validate" placeholder="Password *">
                                    </div>
                                    <div class="input-field col s12 m12 l12">
                                        <i class="mdi-action-lock prefix"></i>
                                        <input id="agency-repassword" name="repassword" type="password" class="validate" placeholder="Re Password *">
                                    </div>
                                </div>
                            </div>

                            <div class="input-field col s12 m12 l12">
                                <input type="checkbox" name="agency_agree" id="agency-agree" />
                                <label for="agency-agree">By creating an account, you agree to our <a href="<?php echo base_url('terms-and-conditions') ?>" target="_blank">Terms and Conditions</a> and <a href="<?php echo base_url('privacy-policy') ?>" target="_blank">Privacy Policy </a>.</label>
                            </div>

                            <div class="signin-option-btn-wrap col s12 m12 l12">
                                <div class="btn-large waves-effect waves-light btn-content btn-form">
                                    <input type="submit" value="Save">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>