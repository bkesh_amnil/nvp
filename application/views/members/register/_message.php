<div class="container">
    <div class="center">
        <div class="signin-sign-up-wrap registration-box-sucess">
            <h4><?php echo $type?> Successful</h4>
            <?php echo $message->$registration_message ?>
            <a class="btn-large waves-effect waves-light btn-content btn-form" href="<?php echo base_url('/members')?>">OK</a>
        </div>
    </div>
</div>
