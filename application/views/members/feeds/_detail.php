<div class="detail-banner-img parallax-container">
    <div class="parallax">
        <img class="img-responsive" src="<?php echo base_url($feedDetail->image) ?>" alt="<?php echo $feedDetail->name ?>"/>
    </div>    
</div>
<section class="detail-wrap">
    <div class="container">
        <div class="details">
            <a href="<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : current_url() ?>" class="mdi-content-clear right card__btn-close">
            </a>
            <h3><?php echo $feedDetail->name ?></h3>
            <span class="news-date">Posted at <?php echo date('jS F, Y', $feedDetail->created_on) ?></span>
            <p>
                <?php echo $feedDetail->description ?>
            </p>
        </div>
    </div>
</section>