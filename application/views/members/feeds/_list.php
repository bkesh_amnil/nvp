<input type="hidden" id="activeUsername" value="<?php echo get_userdata('userName') ?>">
<input type="hidden" id="activeUserProfilePicture" value="<?php echo get_userdata('profilePicture') ?>">
<div>
    <div ng-controller="feedCtrl">
        <div id="news-events" class="col s12">
            <div class="users-page-contents">
                <?php if (get_userdata('userType') == 'Volunteer') { ?>
                    <div class="write-post-wrap">
                        <div class="message-holder" style="display: none;">
                            <span class="close"><i class="mdi-content-clear"></i></span>
                            <span class="message"></span>
                        </div>
                        <span id="post-trigger">Write Your Post Here</span>
                        <span class="btn-close-comment btn-write-close"><i class="mdi-navigation-close"></i></span>

                        <form class="submit-review show-write-post-form" id="volunteer-write-post-form"
                              action="<?php echo base_url('members/feeds') ?>"
                              method="post" enctype="multipart/form-data">
                            <input type="hidden" name="location" value="" id="feedFromLocation"/>

                            <div class="row">
                                <div class="col s12 m6 l6">
                                    <div class="row">
                                        <div class="col s12">
                                            <label>Title</label>
                                            <input id="title" name="name" type="text">
                                        </div>

                                        <div class="col s12 file-field input-field">
                                            <div class="btn btn-content">
                                                <i class="mdi-image-crop-original"></i>
                                                <input type="file" name="image" id="image">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path" type="text" placeholder="Upload Photo">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col s12 m6 l6">
                                    <div class="row">
                                        <div class="col s12">
                                            <label>Write Your Post</label>
                                            <textarea id="description" name="description"
                                                      class="materialize-textarea"></textarea>
                                        </div>

                                        <div class="col s12">
                                            <div class="btn-large waves-effect waves-light btn-content right btn-form">
                                                <div id="post-save-loader" style="display: none">
                                                    <img src="<?php echo base_url('assets/img/loader.gif') ?>">
                                                </div>
                                                <input type="submit" value="Post" id="save-post">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                <?php } ?>
                <div class="row">
                    <?php if ($newsFeeds) {
                        foreach ($newsFeeds as $key => $newsFeed) { ?>
                            <div class="row <?php echo ($key % 2 == 0) ? 'even' : 'odd' ?>">
                                <div class="col s12 m12 l12">
                                    <div class="feeds-wrap">
                                        <h4><?php echo $newsFeed->name ?></h4>

                                        <div class="date-time">
                                            <span><?php echo date('d F, Y', $newsFeed->created_on) ?></span>
                                            <span>at <?php echo date('H:i', $newsFeed->created_on) ?></span>
                                        </div>
                        <span class="location">
                        <?php if (isset($newsFeed->location) && !empty($newsFeed->location)) {
                            $location = explode(',', $newsFeed->location);
                            $lat = $location['0'];
                            $lng = $location['1'];
                            echo getaddress($lat, $lng); ?>
                        <?php } ?>
                            </span>
                                        <p class="post-by">- <?php echo $newsFeed->post_by ?></p>                                        <div class="feeds">
                                            <p>
                                                <?php echo mySubStr(strip_tags($newsFeed->description), 220);
                                                echo (strlen(strip_tags($newsFeed->description)) > 220) ? '...' : '' ?>
                                            </p>
                                            <a href="javascript:void(0);"
                                               ng-click="getFeedDetail('<?php echo $newsFeed->slug ?>')">Read more</a>

                                            <div class="feed-img-wrap">
                                                <?php
                                                $feedImageDetail = getimagesize(base_url($newsFeed->image));
                                                if ($feedImageDetail[1] < $feedImageDetail[0]) { ?>
                                                    <img class="original-img"
                                                         src="<?php echo image_thumb($newsFeed->image, 540, 265, '', true) ?>"
                                                         alt="<?php echo $newsFeed->name ?>"/>
                                                <?php } else { ?>
                                                    <img class="original-img small-feed-image"
                                                         src="<?php echo image_thumb($newsFeed->image, 265, 265, '', true, 0) ?>"
                                                         alt="<?php echo $newsFeed->name ?>"/>
                                                <?php } ?>
                                                <img class="feed-img-blur"
                                                     src="<?php echo image_thumb($newsFeed->image, 540, 265, '', true) ?>"
                                                     alt="<?php echo $newsFeed->name ?>"/>
                                            </div>
                                            <div class="feed-likes-counts">
                                                <span><span id="likecount-<?php echo $newsFeed->id ?>"><?php echo $newsFeed->totalLikes ? $newsFeed->totalLikes : '0' ?></span>
                                                    Likes</span>
                                                <span><span id="commentcount-<?php echo $newsFeed->id ?>"><?php echo $newsFeed->totalComments ? $newsFeed->totalComments : '0' ?></span>
                                                    Comments</span>
                                                <span><span id="sharecount-<?php echo $newsFeed->id ?>"><?php echo $newsFeed->totalShares ? $newsFeed->totalShares : '0' ?></span>
                                                    Shares</span>
                                            </div>
                                        </div>
                                        <?php
                                        $userType = get_userdata('userType');
                                        $userId = get_userdata('active_user');
                                        ?>
                                        <div class="feed-section-misc">
                                            <a href=""
                                               ng-click="likePost('<?php echo $newsFeed->id ?>', '<?php echo $userId ?>', '<?php echo $userType ?>')"
                                               class="btn-like <?php echo ($newsFeed->userLiked) ? 'active' : '' ?>"><i
                                                    class="small mdi-action-favorite"></i>
                                                Like</a>
                                            <a href="" ng-click="getComments('<?php echo $newsFeed->id ?>')"
                                               data-commentbody="#comment-<?php echo $newsFeed->id ?>"
                                               class="btn-comment"><i
                                                    class="small mdi-communication-message"></i> Comments</a>
                                            <a href="" class="btn-share" ng-click="increaseShare('<?php echo $newsFeed->id ?>')">
                                                <span class="addthis_button" addthis:url="<?php echo base_url('members/feeds/'.$newsFeed->slug)?>" addthis:title="<?php echo $newsFeed->name?>"><i class="small mdi-social-share"></i>Share</span>
                                            </a>

                                            <div id="comment-<?php echo $newsFeed->id ?>"
                                                 class="feed-comments-wrap comments-hide">
                                                <span class="btn-close-comment"><i
                                                        class="mdi-navigation-close"></i></span>

                                                <div class="row">
                                                    <div class="col s12 m12 l12">
                                                        <div class="comment" ng-repeat="comment in comments">
                                                            <div class="card-panel">
                                                                <div class="row valign-wrapper">
                                                                    <div class="col s2">
                                                                        <img
                                                                            ng-src="{{imageProcess(comment.profilePicture)}}"
                                                                            alt=""
                                                                            class="responsive-img">
                                                                    </div>
                                                                    <div class="col s10">
                                                                        <span
                                                                            class="user-name">{{ comment.fullName }}</span>
                                                                            <span class="black-text">
                                                                            {{ comment.comment }}
                                                                            </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col s12 m12 l12">
                                                        <div class="write-comment">
                                                            <div class="card-panel">
                                                                <div class="row valign-wrapper">
                                                                    <div class="col s2">
                                                                        <img
                                                                            src="<?php echo get_userdata('profilePicture') ?>"
                                                                            alt=""
                                                                            class="responsive-img">
                                                                    </div>
                                                                    <div class="col s10">
                                                                        <form>
                                                                            <textarea
                                                                                ng-keyup="$event.keyCode == 13 && saveComment('<?php echo $newsFeed->id ?>', '<?php echo $userId ?>', '<?php echo $userType ?>')"
                                                                                ng-model="userComment" id="textarea1"
                                                                                class=""
                                                                                placeholder="Write a Comment"></textarea>
                                                                            <label>Press enter to post</label>
                                                                            <span
                                                                                class="comments-more" ng-click="getComments('<?php echo $newsFeed->id ?>', 'yes')">More comments</span>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }
                    }
                    ?>
                </div>
                <?php echo $newsFeedPagination ?>
            </div>
        </div>

        <!-- feed detail modal -->
        <div id="feedDetail" class="modal">
            <div class="modal-header">
                <a class="mdi-content-clear right card__btn-close modal-close"></a>
            </div>
            <div class="modal-content">
                <div class="row">
                    <div class="col s12">
                        <h4>{{ newsDetailTitle }}</h4>
                        <span class="news-date">{{ newsDetailCreatedOn }} at {{ newsDetailCreatedTime }}</span>
                        <span ng-if="newsDetailLocation" class="location">{{ newsDetailLocation }}</span>

                        <p>
                            {{ newsDetailBody }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>