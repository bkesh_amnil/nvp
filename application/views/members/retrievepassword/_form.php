<div class="container">
    <div class="center">
        <?php $this->load->view('frontend/include/headings', array('headingData' => array(array('title' => 'Reset Password')))) ?>
    </div>

    <div class="signin-sign-up-wrap">
        <div class="row">
            <form class="col s12 m6 l6" id="retrievepassword" action="" method="post">
                <input type="hidden" value="<?php echo $userType ?>" name="userType"/>
                <input type="hidden" value="<?php echo $userId ?>" name="userId"/>
                <h3>Reset Your Password</h3>
                <?php frontFlash() ?>
                <div class="row">
                    <div class="input-field col s12">
                        <i class="mdi-action-lock prefix"></i>
                        <input id="password" name="password" type="password">
                        <label for="password">Password</label>
                    </div>
                    <div class="input-field col s12">
                        <i class="mdi-action-lock prefix"></i>
                        <input id="repassword" name="repassword" type="password">
                        <label for="repassword">Confirm Password</label>
                    </div>
                    <div class="signin-option-btn-wrap col l12">
                        <div class="btn-large waves-effect waves-light btn-content btn-form">
                            <input type="submit" value="Change Password">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>