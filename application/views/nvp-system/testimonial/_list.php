<table class="table table-bordered table-hover list-datatable">
    <thead>
    <tr>
        <th>SN</th>
        <th>Testimonial</th>
        <th>Testimonial Type</th>
        <th>Action</th>
    </tr>
    </thead>
    <tfoot id="table-search-row">
    <tr>
        <th></th>
        <th>Testimonial</th>
        <th>Testimonial Date</th>
        <th></th>
    </tr>
    </tfoot>
    <tbody>
    <?php if ($testimonials) : $serial_number = 1; ?>
        <?php foreach ($testimonials as $testimonial) : ?>
            <tr>
                <td><?php echo $serial_number; $serial_number++; ?></td>
                <td><?php echo $testimonial->name ?></td>
                <td><?php echo date('d F, Y', $testimonial->date) ?></td>
                <td>
                    <?php
                    $this->data['actionBtnData'] = [
                        'module' => 'testimonial',
                        'moduleData' => $testimonial,
                        'options' => 'EDS'
                    ];
                    $ci = &get_instance();
                    $ci->partialRender(BACKENDFOLDER.'/include/actionButton');
                    ?>
                </td>
            </tr>
        <?php endforeach ?>
    <?php else : ?>
        <tr>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>