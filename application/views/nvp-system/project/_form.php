<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="name">Project Name</label>
                <input type="text" name="name" value="<?php echo $project->name ?>" class="form-control" id="name" placeholder="Project Name">
            </div>
            <div class="form-group">
                <label for="image">Project Image (Best Image Visibility: W:884px, H:292px)</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="false" name="image" value="<?php echo $project->image ?>" class="form-control" id="image" placeholder="Image">
                <?php if($project->image != '') { ?>
                    <div class="image-wrapper">
                        <img class="img-responsive" src="<?php echo base_url($project->image) ?>" />
                    </div>
                <?php } ?>
            </div>
            <div class="form-group">
                <label for="description">Project Description</label>
                <textarea rows="7" name="description" class="form-control" id="description" placeholder="Project Description"><?php echo $project->description ?></textarea>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="long_description">Project Full Description</label>
                <textarea rows="7" name="long_description" class="form-control" id="long_description" placeholder="Project Full Description"><?php echo $project->long_description ?></textarea>
            </div>
            <div class="form-group">
                <label for="logo">Project Logo (Best Image Visibility: W:200px, H:200px)</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="false" name="logo" value="<?php echo $project->logo ?>" class="form-control" id="logo" placeholder="Project Logo">
                <?php if($project->image != '') { ?>
                    <div class="image-wrapper">
                        <img class="img-responsive" src="<?php echo base_url($project->logo) ?>" />
                    </div>
                <?php } ?>
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo $project->status == 'Active' || $project->status == '' ? 'selected' : '' ?>>Publish</option>
                    <option value="0" <?php echo $project->status == 'InActive' ? 'selected' : '' ?>>UnPublish</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</form>
<script>
    window.onload = function() {
        load_ckeditor('description', true);
        load_ckeditor('long_description');
    };
</script>