<table class="table table-bordered table-hover list-datatable">
    <thead>
    <tr>
        <th>SN</th>
        <th>Skill</th>
        <th>Action</th>
    </tr>
    </thead>
    <tfoot id="table-search-row">
    <tr>
        <th></th>
        <th>Skill</th>
        <th></th>
    </tr>
    </tfoot>
    <tbody>
    <?php if ($skills) : $serial_number = 1; ?>
        <?php foreach ($skills as $skill) : ?>
            <tr>
                <td><?php echo $serial_number; $serial_number++; ?></td>
                <td><?php echo $skill->name ?></td>
                <td>
                    <?php
                    $this->data['actionBtnData'] = [
                        'module' => 'skill',
                        'moduleData' => $skill,
                        'options' => 'EDS'
                    ];
                    $ci = &get_instance();
                    $ci->partialRender(BACKENDFOLDER.'/include/actionButton');
                    ?>
                </td>
            </tr>
        <?php endforeach ?>
    <?php else : ?>
        <tr>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>