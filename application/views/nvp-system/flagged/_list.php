<table class="table table-bordered table-hover list-datatable">
    <thead>
    <tr>
        <th>SN</th>
        <th>Content Type</th>
        <th>Reason</th>
        <th>Content Detail Link</th>
    </tr>
    </thead>
    <tfoot id="table-search-row">
    <tr>
        <th></th>
        <th>Content Type</th>
        <th>Reason</th>
        <th></th>
    </tr>
    </tfoot>
    <tbody>
    <?php if ($flagged) : $serial_number = 1; ?>
        <?php foreach ($flagged as $flagged_row) : ?>
            <tr>
                <td><?php echo $serial_number; $serial_number++; ?></td>
                <td><?php echo $flagged_row->data_type ?></td>
                <td><?php echo $flagged_row->reason ?></td>
                <td>
                    <?php $content_url = $flagged_row->data_type == 'Feed' ? BACKENDFOLDER . '/post_data/view_detail/' . $flagged_row->data_id : BACKENDFOLDER . '/comment/view_detail/' . $flagged_row->data_id ?>
                    <a href="<?php echo base_url($content_url) ?>" target="_blank">
                        Detail
                    </a>
                </td>
            </tr>
        <?php endforeach ?>
    <?php else : ?>
        <tr>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>