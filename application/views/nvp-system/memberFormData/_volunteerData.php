<div class="table-responsive">
    <table class="table table-bordered table-hover list-datatable">
        <thead>
        <tr>
            <th>SN</th>
            <th>Title</th>
            <th>Email</th>
            <th>Volunteer ID</th>
            <th>Total Hours Volunteered</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
        </thead>
        <tfoot id="table-search-row">
        <tr>
            <th></th>
            <th>Title</th>
            <th>Email</th>
            <th>Volunteer ID</th>
            <th></th>
            <th>Account Status</th>
            <th></th>
        </tr>
        </tfoot>
        <tbody>
        <?php if ($volunteers) : $serial_number = 1; ?>
            <?php foreach ($volunteers as $volunteer) : ?>
                <tr>
                    <td><?php echo $serial_number; $serial_number++; ?></td>
                    <td><?php echo $volunteer->fullName ?></td>
                    <td><?php echo $volunteer->email ?></td>
                    <td><?php echo 'NVP' . $volunteer->id ?></td>
                    <td>
                        <a href="#" data-toggle="modal" data-target="#volunteer-<?= $volunteer->id ?>"><?php echo $volunteer->total_hours ?></a>
                        <?php $this->load->view(BACKENDFOLDER . '/include/_hour_detail', array('volunteer_id' => $volunteer->id)) ?>
                    </td>
                    <td><?php echo $volunteer->status ?></td>
                    <td>
                        <input id="backendfolder" type="hidden" value="<?php echo BACKENDFOLDER ?>">
                        <input id="data" type="hidden" value="volunteer_data">
                        <input id="page" type="hidden" value="index">
                        <a title="View Details" href="<?php echo base_url(BACKENDFOLDER.'/volunteer_data/view_detail/'.$volunteer->id) ?>" class="btn btn-info">
                            <i class="fa fa-list"></i></a>
                        <div class="form-group">
                            <input class ="id" type="hidden" value="<?php echo $volunteer->id ?>">
                            <select name="status" class="form-control status">
                                <option value="Active" <?php if($volunteer->status == 'Active'){echo 'selected';} else{  echo'';} ?>>Active</option>
                                <option value="InActive" <?php if($volunteer->status == 'InActive'){echo 'selected';} else{  echo'';} ?>>InActive</option>
                                <option value="Suspended" <?php if($volunteer->status == 'Suspended'){echo 'selected';} else{  echo'';} ?>>Suspended</option>
                            </select>
                        </div>
                        <?php if(!$volunteer->verifiedUser) { ?>
                            <a href="<?php echo base_url(BACKENDFOLDER . '/volunteer_data/verifyUser/'.$volunteer->id) ?>">Verify User</a>
                        <?php } else { ?>
                            <span class="badge bg-green"><i class="fa fa-fw fa-check"></i> Verified</span>
                            <a href="<?php echo base_url(BACKENDFOLDER . '/volunteer_data/verifyUser/'.$volunteer->id) ?>">Discredit User</a>
                        <?php } ?>
                        <!--<a title="Change status" href="<?php /*echo base_url(BACKENDFOLDER.'/volunteer_data/status/'.$volunteer->id) */?>" class="btn btn-info">
                        <i class="fa fa-list"></i></a>-->

                    </td>
                </tr>
            <?php endforeach ?>
        <?php else : ?>
            <tr>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</div>