<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="<?php echo base_url().BACKENDFOLDER.'/volunteer_data/register_volunteer';?>" method="post" id="volunteer_register" novalidate>
    <input type="hidden" value="Volunteer" name="userType"/>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="name">Full Name *</label>
                <input id="fullName" class="form-control" name="fullName" type="text" placeholder="Full Name *">
            </div>
            <div class="form-group">
                <label for="gender">Gender *</label>
                <select name="genderId" class="form-control">
                    <option value="" disabled selected>Select gender</option>
                    <?php foreach($allGender as $gender) { ?>
                        <option value="<?php echo $gender->id ?>"><?php echo $gender->name ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="dob">Date of Birth *</label>
                <input id="date" name="dateOfBirth" class="form-control" type="date"  placeholder="Date of Birth *">
            </div>
            <div class="form-group">
                <label for="phoneNumberMobile">Mobile Number *</label>
                <input id="phnumber" class="form-control" name="phoneNumberMobile" type="text"  placeholder="Mobile Number *">
            </div>
            <div class="form-group">
                <label for="phoneNumberResidential">Residential Number</label>
                <input id="rnumber" class="form-control" name="phoneNumberResidential" type="text" placeholder="Residential Number">
            </div>
            <div class="form-group">
                <label for="email">Email *</label>
                <input id="email" class="form-control" name="email" type="text"  placeholder="Email *">
            </div>

            <div class="form-group">
                <label for="password">Password *</label>
                <input id="password" name="password" type="password" class="form-control" placeholder="Password *">
            </div>
            <div class="form-group">
                <label for="repassword">Re Password *</label>
                <input id="repassword" name="repassword" type="password" class="form-control" placeholder="Re Password *">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</form>



 

                   