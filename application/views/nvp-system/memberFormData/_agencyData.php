<table class="table table-bordered table-hover list-datatable">
    <thead>
    <tr>
        <th>SN</th>
        <th>Title</th>
        <th>Action</th>
    </tr>
    </thead>
    <tfoot id="table-search-row">
    <tr>
        <th></th>
        <th>Title</th>
        <th></th>
    </tr>
    </tfoot>
    <tbody>
    <?php if ($agencys) : $serial_number = 1; ?>
        <?php foreach ($agencys as $agency) : ?>
            <tr>
                <td><?php echo $serial_number; $serial_number++; ?></td>
                <td><?php echo $agency->name ?></td>
                <td>
                    <input id="backendfolder" type="hidden" value="<?php echo BACKENDFOLDER ?>">
                    <input id="data" type="hidden" value="agency_data">
                    <input id="page" type="hidden" value="index">
                    <a title="View Details" href="<?php echo base_url(BACKENDFOLDER.'/agency_data/view_detail/'.$agency->id) ?>" class="btn btn-info">
                        <i class="fa fa-list"></i></a>
                    <div class="form-group">
                        <input class="id" type="hidden" value="<?php echo $agency->id ?>">
                        <select name="status" class="form-control status">
                            <option value="Active" <?php if($agency->status == 'Active'){echo 'selected';} else{  echo'';} ?>>Active</option>
                            <option value="InActive" <?php if($agency->status == 'InActive'){echo 'selected';} else{  echo'';} ?>>InActive</option>
                            <option value="Suspended" <?php if($agency->status == 'Suspended'){echo 'selected';} else{  echo'';} ?>>Suspended</option>
                        </select>
                    </div>
                    <?php /* if(!$agency->verifiedUser) { ?>
                        <a href="<?php echo base_url(BACKENDFOLDER . '/volunteer_data/verifyUser/'.$agency->id) ?>">Verify User</a>
                    <?php } else { ?>
                        <span class="badge bg-green"><i class="fa fa-fw fa-check"></i> Verified</span>
                        <a href="<?php echo base_url(BACKENDFOLDER . '/volunteer_data/verifyUser/'.$agency->id) ?>">Discredit User</a>
                    <?php } */ ?>
                    <!--<a title="Change status" href="<?php /*echo base_url(BACKENDFOLDER.'/agency_data/status/'.$agency->id) */?>" class="btn btn-info">
                        <i class="fa fa-list"></i></a>-->

                </td>
            </tr>
        <?php endforeach ?>
    <?php else : ?>
        <tr>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>