<a href="<?php echo base_url().BACKENDFOLDER.'/agency_project_data/delete/'.$id?>" class="btn btn-danger" title="Delete Agency Project Permanently"> <i class="fa fa-trash"></i> Delete</a>
<a href="<?php echo base_url().BACKENDFOLDER.'/agency_project_data/volunteer/'.$id?>" class="btn btn-info" title="Add Volunteers to Project"> <i class="fa fa-plus"></i> Add Volunteers</a>
<table class="table table-bordered table-hover">
    <?php foreach ($data as $key => $a) {
        if ($key == 'id') {
            $id = $a;
        }
        $string = $key;
        $words = preg_replace('/(?<!\ )[A-Z]/', ' $0', $string);
        $title = ucwords($words);
        if ($key != 'id' && $key != 'short_description') {
            if ($key == 'dateOfEvent') {
                ?>
                <tr>
                    <th><?php echo $title; ?></th>
                    <td><?php echo date('d M Y', $a); ?></td>
                </tr>
            <?php } else { ?>
                <tr>
                    <th><?php echo $title; ?></th>
                    <td><?php echo $a; ?></td>
                </tr>
            <?php }
        }
    } ?>
    <tr>
        <form action="" method="post">
            <th>
                Short Project Description
            </th>
            <td>
                <div class="form-group">
                    <textarea id="text" name="short_description" class="form-control"><?php echo $data->short_description ?></textarea>
                    <div id="counter"></div>
                </div>
                <div class="form-group">
                    <input type="submit" value="Save Project Description" class="btn btn-primary"/>
                </div>
            </td>
        </form>
    </tr>
</table>
</div>
</div>
<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Agency Project Detail Criteria</h3>

        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <?php if (isset($agencyProjectCriteria) && !empty($agencyProjectCriteria)) { 
            //print_r($agencyProjectCriteria);die;
            ?>
            <table class="table table-bordered table-hover list-datatable">
                <?php foreach ($agencyProjectCriteria as $key => $a) {
                    if ($key == 'id') {
                        $id = $a;
                    }
                    $string = $key;
                    $words = preg_replace('/(?<!\ )[A-Z]/', ' $0', $string);
                    $title = ucwords($words);
                    if ($key != 'id') {
                        ?>
                        <tr>
                            <th><?php echo $title; ?></th>
                            <td><?php if($a == NULL || $a == '0'){echo "";}else{echo $a;} ?></td>
                        </tr>
                    <?php }
                } ?>
            </table>
        <?php } else { ?>
            No criteria defined for this project!!!!!
        <?php } ?>

