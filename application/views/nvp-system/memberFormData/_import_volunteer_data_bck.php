<?php if ($existing_data) { ?>
    <div class="row">
        <div class="col-lg-12">
            <label>The following email and phone number are dublicate</label>
        </div>
    </div>
    <table class="table table-bordered table-hover list-datatable">
        <tbody>
        <tr>
            <th>Row Number</th>
            <th>Name</th>
            <th>Mobile</th>
            <th>Email</th>
        </tr>
        <?php foreach ($existing_data as $key => $data) { ?>
            <tr>
                <td><?php echo $key ?></td>
                <td><?php echo $data[0] ?></td>
                <td><?php echo $data[3] ?></td>
                <td><?php echo $data[4] ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php }if ($empty_data) { ?>
    <div class="row">
        <div class="col-lg-12">
            <label>The following email or phone number are empty</label>
        </div>
    </div>
    <table class="table table-bordered table-hover list-datatable">
        <tbody>
        <tr>
            <th>Row Number</th>
            <th>Name</th>
            <th>Mobile</th>
            <th>Email</th>
        </tr>
        <?php foreach ($empty_data as $key => $data) { ?>
            <tr>
                <td><?php echo $key ?></td>
                <td><?php echo $data[0] ?></td>
                <td><?php echo $data[3] ?></td>
                <td><?php echo $data[4] ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php } if (empty($existing_data) && empty($empty_Data)) { ?>
    <span> Sucessfully imported Volunteers</span>
<?php } ?>
