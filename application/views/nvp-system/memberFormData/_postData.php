<label>Filter by date: </label>
From: <input type="text" class="add_datepicker" id="dateStart">
To: <input type="text" class="add_datepicker" id="dateEnd">
<table class="table table-bordered table-hover list-datatable">
    <thead>
    <tr>
        <th>SN</th>
        <th>Title</th>
        <th>Created Date</th>
        <th>Status</th>
        <th>Action</th>
    </tr>
    </thead>
    <tfoot id="table-search-row">
    <tr>
        <th></th>
        <th>Title</th>
        <th></th>
        <th>Status</th>
        <th></th>
    </tr>
    </tfoot>
    <tbody>
    <?php if ($posts) : $serial_number = 1; ?>
        <?php foreach ($posts as $post) : ?>
            <tr>
                <td><?php echo $serial_number; $serial_number++; ?></td>
                <td><?php echo $post->name ?></td>
                <td><?php echo date('d F, Y', $post->created_on) ?></td>
                <td><?php echo $post->status ?></td>
                <td id="post-<?php echo $post->id ?>">
                    <a title="View Details" href="<?php echo base_url(BACKENDFOLDER.'/post_data/view_detail/'.$post->id) ?>" class="btn btn-info">
                        <i class="fa fa-list"></i></a>
                    <input type="hidden" class="post-id" value="<?php echo $post->id ?>">
                    <input type="hidden" class="post-page" value="list">
                    <select class="form-control post-data-status">
                        <option value="InActive" <?php if ($post->status == 'InActive') {
                            echo 'selected';
                        } else {
                            echo '';
                        } ?>>InActive
                        </option>
                        <option value="Active" <?php if ($post->status == 'Active') {
                            echo 'selected';
                        } else {
                            echo '';
                        } ?>>Active
                        </option>
                    </select>
                </td>
            </tr>
        <?php endforeach ?>
    <?php else : ?>
        <tr>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>