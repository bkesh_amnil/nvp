<form action="<?php echo base_url('nvp-system/import_volunteer/import') ?>" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="excel">Import File</label>
                <input type="file" name="excel" id="excel" />
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Import</button>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <a class="btn-excel" href="<?php echo base_url('nvp-system/import_volunteer/download_excel') ?>"><img class="img-responsiveS" src="<?php echo base_url('assets/img/icon/icon-excel.png');?>" /><label>Sample format</label></a>
            </div>
        </div>
    </div>
</form> 