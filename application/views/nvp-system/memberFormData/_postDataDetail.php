<table class="table table-bordered table-hover list-datatable">
    <tr>
        <th>Image</th>
        <td><img class="responsive-img" src="
                        <?php echo image_thumb($data->image, '150', '100', '', true) ?>"
                 alt="<?php echo $data->image ?>"/></td>
    </tr>
    <tr>
        <th>Post Name</th>
        <td> <?php echo ucwords($data->name); ?></td>
    </tr>
    <tr>
        <th>Post Description</th>
        <td> <?php echo $data->description ?></td>
    </tr>
    <tr>
        <th>User Type</th>
        <td><?php echo $data->userType ?></td>
    </tr>
    <tr>
        <th>User Name</th>
        <td><?php echo isset($user) ? ucwords($user->fullName) : '' ?></td>
    </tr>
    <tr>
        <th>Status</th>
        <td><input type="hidden" class="post-id" value="<?php echo $data->id ?>">
            <input type="hidden" class="post-page" value="detail">
            <select class="form-control post-data-status">
                <option value="InActive" <?php if ($data->status == 'InActive') {
                    echo 'selected';
                } else {
                    echo '';
                } ?>>InActive
                </option>
                <option value="Active" <?php if ($data->status == 'Active') {
                    echo 'selected';
                } else {
                    echo '';
                } ?>>Active
                </option>
            </select></td>
    </tr>
</table>
