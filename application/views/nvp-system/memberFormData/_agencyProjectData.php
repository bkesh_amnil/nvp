<input id="backendfolder" type="hidden" value="<?php echo BACKENDFOLDER ?>">
<table class="table table-bordered table-hover list-datatable">
    <thead>
    <tr>
        <th>SN</th>
        <th>Agency Name</th>
        <th>Project Name</th>
        <th>Project Type</th>
        <th>Total Volunteers</th>
        <th>Total Hours</th>
        <th>Action</th>
    </tr>
    </thead>
    <tfoot id="table-search-row">
    <tr>
        <th></th>
        <th>Agency Name</th>
        <th>Project Name</th>
        <th>Project Type</th>
        <th></th>
        <th></th>
        <th></th>
    </tr>
    </tfoot>
    <tbody>
    <?php if ($agency_projects) : $serial_number = 1; ?>
        <?php foreach ($agency_projects as $agency_project) : ?>
            <tr>
                <td><?php echo $serial_number; $serial_number++; ?></td>
                <td><?php echo $agency_project->agencyName ?></td>
                <td><?php echo $agency_project->name ?></td>
                <td><?php echo $agency_project->projectType ?></td>
                <td><?php echo $agency_project->totalVolunteers ?></td>
                <td><?php echo $agency_project->totalHours ?></td>
                <td>
                    <a title="View Details" href="<?php echo base_url(BACKENDFOLDER.'/agency_project_data/view_detail/'.$agency_project->id) ?>" class="btn btn-info">
                        <i class="fa fa-list"></i></a>

                    <?php if($agency_project->status == 'Ongoing') { ?>
                    <a title="Add and View Volunteers" href="<?php echo base_url(BACKENDFOLDER.'/agency_project_data/volunteer/'.$agency_project->id) ?>" class="btn btn-info">
                        <i class="fa fa-plus"></i></a>
                    <?php } ?>

                    <?php if($agency_project->totalVolunteers) { ?>
                    <a title="Generate Report" href="<?php echo base_url(BACKENDFOLDER.'/agency_project_data/generatereport/'.$agency_project->id) ?>" class="btn btn-info">
                        <i class="fa fa-clipboard"></i></a>
                    <?php } ?>
                    <input class="project-id" value="<?php echo $agency_project->id?>" type="hidden"/>
                    <select class="form-control agency-project-status">
                        <option value="Ongoing" <?php if ($agency_project->status == 'Ongoing') {
                            echo 'selected';
                        } else {
                            echo '';
                        } ?>>Ongoing
                        </option>
                        <option value="InActive" <?php if ($agency_project->status == 'InActive') {
                            echo 'selected';
                        } else {
                            echo '';
                        } ?>>InActive
                        </option>
                        <option value="Completed" <?php if ($agency_project->status == 'Completed') {
                            echo 'selected';
                        } else {
                            echo '';
                        } ?>>Completed
                        </option>
                    </select>
                </td>
            </tr>
        <?php endforeach ?>
    <?php else : ?>
        <tr>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>