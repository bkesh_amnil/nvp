<table class="table table-bordered table-hover list-datatable">
    <tr>
        <th>Post Name</th>
        <td> <?php echo $data->post_title; ?></td>
    </tr>
    <tr>
        <th>Comment</th>
        <td> <?php echo $data->comment ?></td>
    </tr>
    <tr>
        <th>Comment By</th>
        <td><?php echo isset($user) ? ucwords($user->fullName) : '' ?></td>
    </tr>
    <tr>
        <th>Comment By User Type</th>
        <td><?php echo $data->userType ?></td>
    </tr>
    <tr>
        <th>Comment Status</th>
        <td><input type="hidden" class="comment-id" value="<?php echo $data->id ?>">
            <input type="hidden" class="post-page" value="detail">
            <select class="form-control comment-status">
                <option value="InActive" <?php if ($data->status == 'InActive') {
                    echo 'selected';
                } else {
                    echo '';
                } ?>>InActive
                </option>
                <option value="Active" <?php if ($data->status == 'Active') {
                    echo 'selected';
                } else {
                    echo '';
                } ?>>Active
                </option>
            </select></td>
    </tr>
</table>
