<?php if ($existing_data) { ?>
    <div class="message-holder alert alert-info" style="display: none;">
        <span class="close-volunteer btn float-right" style="float: right;"><i class="fa fa-times"></i></span>
        <span class="message"></span>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <label>The following email and phone number already exist.</label>
        </div>
    </div>
    <table class="table table-bordered table-hover list-datatable">
        <thead>
        <tr>
            <th>Row Number</th>
            <th>Name</th>
            <th>Mobile</th>
            <th>Email</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($existing_data as $key => $data) { ?>
            <tr>
                <td><?php echo $key ?></td>
                <td><?php echo $data['fullName'] ?></td>
                <td><?php echo $data['phoneNumberMobile'] ?></td>
                <td><?php echo $data['email'] ?></td>
                <td>
                    <?php $value = json_encode($data);?>
                    <input type="hidden" class="data" value='<?php echo $value?>'/>
                    <a href="<?php echo base_url(BACKENDFOLDER . '/import_volunteer/update_volunteer_data/') ?>" class="update-volunteer btn btn-info" >UPDATE</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php }
if ($empty_data) { ?>
    <div class="row">
        <div class="col-lg-12">
            <label>The following email or phone number are empty</label>
        </div>
    </div>
    <table class="table table-bordered table-hover list-datatable">
        <thead>
        <tr>
            <th>Row Number</th>
            <th>Name</th>
            <th>Mobile</th>
            <th>Email</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($empty_data as $key => $data) { ?>
            <tr>
                <td><?php echo $key ?></td>
                <td><?php echo $data['fullName'] ?></td>
                <td><?php echo $data['phoneNumberMobile'] ?></td>
                <td><?php echo $data['email'] ?></td>
                <td></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php }
if (empty($existing_data) && empty($empty_Data)) { ?>
    <span> Sucessfully imported Volunteers</span>
<?php } ?>
