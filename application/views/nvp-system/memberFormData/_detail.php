<?php
if(segment(2) == 'volunteer_data') {
    $volunteer_id = segment(4);
    $ci = &get_instance();
    $ci->load->model('common_model', 'common');
    $query = "SELECT
              ifnull(sum(pv.hours), 'N/A') as total_hours
            FROM nvp_project_volunteer pv
            WHERE pv.volunteerId = ".$volunteer_id;
    $total_hours = $ci->common->query($query);
    $total_hours = $total_hours[0]->total_hours;
}
?>
<table class="table table-bordered table-hover list-datatable">
    <?php foreach ($data as $key => $a) {
        if($key == 'id'){
            $id=$a;
        }
        $string = $key;
        $words = preg_replace('/(?<!\ )[A-Z]/', ' $0', $string);
        $title = ucwords($words);
        if ($key != 'id') {
            if ($key == 'logo') {
                ?>
                <tr>
                    <th><?php echo $title; ?></th>
                    <td>
                        <?php if(file_exists($a)) { ?>
                            <a href="<?php echo base_url($a) ?>" target="_blank">
                                <img class="responsive-img" src="
                                    <?php echo image_thumb($a, '150', '100', '', true) ?>"
                                     alt="<?php echo $a ?>"/>
                            </a>
                        <?php } else { ?>
                            No Logo Uploaded
                        <?php } ?>
                    </td>
                </tr>
            <?php } elseif($key == 'registrationCertificateImage') { ?>
                <tr>
                    <th><?php echo $title; ?></th>
                    <td>
                        <?php if(file_exists($a)) { ?>
                            <a href="<?php echo base_url($a) ?>" target="_blank">
                                <img class="responsive-img" src="
                        <?php echo image_thumb($a, '150', '100', '', true) ?>"
                                     alt="<?php echo $a ?>"/>
                            </a>
                        <?php } else { ?>
                            No Certificate Uploaded
                        <?php } ?>
                    </td>
                </tr>
            <?php } elseif ($key == 'status') { ?>
                <th><?php echo $title; ?></th>
                <input id="page" type="hidden" value="detail">
                <input id="data" type="hidden" value="<?php echo $type?>">
                <input id="backendfolder" type="hidden" value="<?php echo BACKENDFOLDER ?>">
                <td>
                    <input class="id" type="hidden" value="<?php echo $id ?>">
                    <div class="form-group">
                        <select name="status" class="form-control status">
                            <option value="Active" <?php if ($a == 'Active') {
                                echo 'selected';
                            } else {
                                echo '';
                            } ?>>Active
                            </option>
                            <option value="InActive" <?php if ($a == 'InActive') {
                                echo 'selected';
                            } else {
                                echo '';
                            } ?>>InActive
                            </option>
                            <option value="Suspended" <?php if ($a == 'Suspended') {
                                echo 'selected';
                            } else {
                                echo '';
                            } ?>>Suspended
                            </option>
                        </select>
                    </div>
                </td>
            <?php }elseif($key == 'dateOfBirth'){?>
                <tr>
                    <th><?php echo $title; ?></th>
                    <td><?php echo date('d M Y', $a); ?></td>
                </tr>
            <?php } else {?>
                <tr>
                    <th><?php echo $title; ?></th>
                    <td><?php echo $a; ?></td>
                </tr>
            <?php }
        }
    }
    if(segment(2) == 'volunteer_data') {
        echo "<tr>";
        echo "<th>Total Hours</th>";
        echo "<td><a href=\"#\" data-toggle=\"modal\" data-target=\"#volunteer-".segment(4)."\">$total_hours</a></td>";
        echo "</tr>";
    } ?>
</table>
<?php $this->load->view(BACKENDFOLDER . '/include/_hour_detail', array('volunteer_id' => segment(4))); ?>
