<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Volunteer List Report</title>
</head>
<body style="padding: 15px;">
<div style="text-align: center; margin-bottom: 20px;">
    <img src="<?php echo base_url('assets/img/logo.png') ?>"/>
</div>

<div style="line-height: 24px;"><span style="font-weight: bold;">Project Name :</span> <?php echo $projectData->name ?></div>
<div style="line-height: 24px;"><span style="font-weight: bold;">Project Start Date :</span> <?php echo date('F d, Y', $projectData->dateOfEvent) ?></div>
<div style="line-height: 24px; margin-bottom: 10px;"><span style="font-weight: bold;">Number of Volunteers Requested :</span> <?php echo $projectData->numberOfVolunteersNeeded ?></div>
<div>
    <strong style="text-transform: uppercase;">Volunteer List</strong>
</div>
<table border="1" style="border-collapse: collapse; padding: 10px; width: 100%;">
    <thead>
    <tr>
        <th style="padding: 10px; text-align: left;">SN</th>
        <th style="padding: 10px; text-align: left;">Volunteer Name</th>
        <th style="padding: 10px; text-align: left;">Volunteer Hour Log</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($volunteerList as $key => $volunteer) { ?>
        <tr>
            <td style="padding: 5px 10px;"><?php echo $key + 1 ?></td>
            <td style="padding: 5px 10px;"><?php echo $volunteer ?></td>
            <td style="padding: 5px 10px;"><?php echo $volunteerHourList[$key] ?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>
<a href="http://www.nvp.org.np" style="text-decoration: none; margin: 10px auto; display: block; text-align: center;">nvp.org.np</a>
</body>
</html>