<div class="message-holder alert alert-info" style="display: none;">
    <span class="close-volunteer btn float-right" style="float: right;"><i class="fa fa-times"></i></span>
    <span class="message"></span>
</div>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="name">Agency Name</label>
                <input type="text" value="<?php echo $data->agencyName ?>" class="form-control" disabled>
                <input type="hidden" name="agencyProjectId" value="<?php echo $data->id ?>" id="agencyProjectId">
            </div>
            <div class="form-group">
                <label for="name">Project Name</label>
                <input type="text" value="<?php echo $data->projectName ?>" class="form-control" disabled>
                <input type="hidden" name="agencyProjectId" value="<?php echo $data->id ?>" id="agencyProjectId">
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="name">Select Volunteers for this project</label>
                <select id="addVolunteerSelect" class="volunteer form-control">
                    <option value="">Select volunteer</option>
                    <?php foreach ($volunteers as $volunteer) { ?>
                        <option value="<?php echo $volunteer->id ?>"><?php echo $volunteer->fullName . ' (NVP' . $volunteer->id . ')' ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
    <table class="table table-bordered table-hover add-volunteers">
        <thead>
        <tr>
            <th>Volunteers</th>
            <th>Hours Required</th>
            <th></th>
        </tr>
        </thead>
        <?php if (isset($agency_volunteers) && !empty($agency_volunteers)) { ?>
        <?php foreach ($agency_volunteers as $agency_volunteer) { ?>
            <tr>
                <td>
                    <div class="form-group">
                        <input type="hidden" name="volunteerId[]"
                               value="<?php echo $agency_volunteer->volunteerId ?>">
                        <input type="text" value="<?php echo $agency_volunteer->fullName ?>" class="form-control"
                               disabled>
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <input type="text" name="hours[]" value="<?php echo $agency_volunteer->hours ?>"
                               class="form-control">
                    </div>
                </td>
                <td>
                    <a href="<?php echo base_url(BACKENDFOLDER . '/agency_project_data/volunteer_delete/' . $agency_volunteer->id) ?>"
                       class="delete-volunteer">Delete</a></td>
            </tr>
        <?php } ?>
    <?php }?>

    </table>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</form>
