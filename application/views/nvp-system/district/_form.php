<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="zone">Zone</label>
                <select name="zoneId" class="form-control" id="zone">
                    <?php foreach($allZones as $zone) { ?>
                        <option value="<?php echo $zone->id ?>" <?php echo ($district->zoneId == $zone->id) ? 'selected' : '' ?>>
                            <?php echo $zone->name ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="name">District Name</label>
                <input type="text" name="name" value="<?php echo $district->name ?>" class="form-control" id="name" placeholder="District Name">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</form>