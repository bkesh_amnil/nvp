<?php if (validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="volunteer_registration_message">Message Displayed After Registration Of Volunteer</label>
                <textarea rows="7" name="volunteer_registration_message" class="form-control"
                          id="volunteer_registration_message"
                          placeholder="Volunteer Registration Message"><?php echo $registrationmsg->volunteer_registration_message ?></textarea>
            </div>
            <div class="form-group">
                <label for="volunteer_email_activate_message">Message Displayed After Email Activation Of
                    Volunteer</label>
                <textarea rows="7" name="volunteer_email_activate_message" class="form-control"
                          id="volunteer_email_activate_message"
                          placeholder="volunteer_email_activate_message"><?php echo $registrationmsg->volunteer_email_activate_message ?></textarea>
            </div>

        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="agency_registration_message">Message Displayed After Registration Of Agency</label>
                <textarea rows="7" name="agency_registration_message" class="form-control"
                          id="agency_registration_message"
                          placeholder="agency_registration_message"><?php echo $registrationmsg->agency_registration_message ?></textarea>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</form>
<script>
    window.onload = function () {
        load_ckeditor('volunteer_email_activate_message', true),
            load_ckeditor('agency_registration_message', true),
            load_ckeditor('volunteer_registration_message', true)
    };
</script>