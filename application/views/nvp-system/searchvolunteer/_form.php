<style>
    label.error {
        color: red;
        font-size: 13px;
        font-weight: normal;
    }
</style>
<?php if (validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
    <form action="" method="get">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Select Zone</label>
                    <select id="selectZone" name="zoneId" class="form-control">
                        <option value="" selected>Select Zone</option>
                        <?php foreach ($zones as $zone) { ?>
                            <option <?php addChecked('zoneId', $zone->id, 'selected') ?> value="<?php echo $zone->id ?>"
                                                                                         class="circle"><?php echo trim($zone->name) ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="form-group">
                    <label>Select District</label>
                    <select id="selectDistrict" class="form-control" name="districtId" name="districtId">
                        <option value="" selected>Select District</option>
                        <?php /* foreach ($districts as $district) { ?>
                            <option <?php addChecked('districtId', $district->id, 'selected') ?>
                                value="<?php echo $district->id ?>"
                                class="circle"><?php echo trim($district->name) ?></option>
                        <?php } */ ?>
                    </select>
                </div>

                <div class="form-group">
                    <label>Select VDC/Municipality</label>
                    <select id="selectMunicipality" class="form-control" name="municipalityId" name="municipalityId">
                        <option value="" selected>Select VDC/Municipality</option>
                        <?php /* foreach ($municipalities as $municipality) { ?>
                            <option <?php addChecked('municipalityId', $municipality->id, 'selected') ?>
                                value="<?php echo $municipality->id ?>"
                                class="circle"><?php echo trim($municipality->name) ?></option>
                        <?php } */ ?>
                    </select>
                </div>

                <div class="form-group">
                    <label>Select Gender</label>
                    <select class="form-control" name="genderId">
                        <option value="" selected>Select Gender</option>
                        <?php foreach ($allGender as $gender) { ?>
                            <option <?php addChecked('genderId', $gender->id, 'selected') ?>
                                value="<?php echo $gender->id ?>" data-icon="<?php echo base_url($gender->icon) ?>"
                                class="circle"><?php echo $gender->name ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="col-md-6">

                <!--<div class="form-group">
                    <select ng-model="searchForm.ageGroup">
                        <option value="" selected>Select Age Group</option>
                        <option value="1">Option 1</option>
                        <option value="2">Option 2</option>
                        <option value="3">Option 3</option>
                        <option value="1">Option 1</option>
                        <option value="2">Option 2</option>
                        <option value="3">Option 3</option>
                        <option value="1">Option 1</option>
                        <option value="2">Option 2</option>
                        <option value="3">Option 3</option>
                        <option value="1">Option 1</option>
                        <option value="2">Option 2</option>
                        <option value="3">Option 3</option>
                        <option value="1">Option 1</option>
                        <option value="2">Option 2</option>
                        <option value="3">Option 3</option>
                        <option value="1">Option 1</option>
                        <option value="2">Option 2</option>
                        <option value="3">Option 3</option>
                    </select>
                </div>-->

                <div class="form-group">
                    <label>Select Skill</label>
                    <select class="form-control" name="skillId">
                        <option value="" selected>Select Skill</option>
                        <?php foreach ($skills as $skill) { ?>
                            <option <?php addChecked('skillId', $skill->id, 'selected') ?>
                                value="<?php echo $skill->id ?>" class="circle"><?php echo $skill->name ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="form-group">
                    <label>Select Academic Qualification</label>
                    <select class="form-control" name="academicQualificationId">
                        <option value="" selected>Select Academic Qualification</option>
                        <?php foreach ($academicQualifications as $academicQualification) { ?>
                            <option <?php addChecked('academicQualificationId', $academicQualification->id, 'selected') ?>
                                value="<?php echo $academicQualification->id ?>"
                                class="circle"><?php echo $academicQualification->name ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="form-group">
                    <label>Select Language</label>
                    <select class="form-control" name="languageId">
                        <option value="" selected>Select Language</option>
                        <?php foreach ($languages as $language) { ?>
                            <option <?php addChecked('languageId', $language->id, 'selected') ?>
                                value="<?php echo $language->id ?>"
                                class="circle"><?php echo $language->name ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="form-group">
                    <label>Select Training</label>
                    <select class="form-control" name="trainingId">
                        <option value="" selected>Select Training</option>
                        <?php foreach ($trainings as $training) { ?>
                            <option <?php addChecked('trainingId', $training->id, 'selected') ?>
                                value="<?php echo $training->id ?>"
                                class="circle"><?php echo $training->name ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <input class="btn btn-primary" type="submit" value="Search">
        </div>
    </form>

<?php if (isset($volunteerList)) {
    if ($volunteerList) { ?>
        <div class="modal fade" id="sendNotificationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Send notification</h4>
                    </div>
                    <form id="send-notification-form" action="<?php echo base_url(BACKENDFOLDER . '/notification/projectnotification') ?>"
                          method="post">
                        <input type="hidden" name="userList" id="userList"/>

                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Notification title</label>
                                        <input type="text" name="title" class="form-control required"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Select project</label>
                                        <select class="form-control notification-project-list required" id="projectList" name="project_id">
                                            <option value="">Select project</option>
                                            <?php foreach ($projects as $project) { ?>
                                                <option value="<?php echo $project->id ?>">
                                                    <?php echo $project->name ?> (Project start
                                                    date: <?php echo date('d F, Y', $project->dateOfEvent) ?>)
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Last respond date</label>
                                        <input type="text" name="lastRespondDate" id="respondDate"
                                               class="form-control required"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Notification short description</label>
                                        <textarea id="notification-desc" class="form-control required" name="short_description"></textarea>
                                        <div id="counter"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save and Send</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <h3 class="box-title">Volunteer List</h3>
        <div class="form-group">
            <button class="btn btn-info" id="sendBulkNotification">Send Bulk</button>
        </div>
        <div class="table-responsive">
                <table class="table table-striped table-bordered list-datatable" id="volunteer-search-table">
                    <tfoot id="table-search-row">
                    <tr>
                        <th></th>
                        <th></th>
                        <th>Volunteer Name</th>
                        <th>Volunteer Email</th>
                        <th></th>
                    </tr>
                    </tfoot>
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th><input type="checkbox"  id="volunteer-search"></th>
                        <th>Volunteer Name</th>
                        <th>Volunteer Email</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <form id="user-list-form" method="post">
                        <?php foreach ($volunteerList as $key => $volunteer) { ?>
                            <tr>
                                <td><?php echo $key + 1 ?></td>
                                <th>
                                    <input type="checkbox" class="select-user" value="<?php echo $volunteer->id ?>">
                                </th>
                                <td><?php echo $volunteer->volunteerName ?></td>
                                <td><?php echo $volunteer->email ?></td>
                                <td>
                                    <a href="" data-userid="<?php echo $volunteer->id ?>"
                                       class="btn btn-info singleUser">
                                        <i class="fa fa-paper-plane"></i> Notify
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    </form>
                    </tbody>
                </table>
        </div>

    <?php } else { ?>
        <div class="alert alert-info">
            <i class="fa fa-info-circle fa-fw"></i> Volunteers not found matching the search criteria.
        </div>
    <?php }
} ?>