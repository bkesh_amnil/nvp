<table class="table table-bordered table-hover list-datatable">
    <thead>
    <tr>
        <th>SN</th>
        <th>Name</th>
        <th>Email</th>
        <th>Message</th>
        <th>Category</th>
        <th>Action</th>
    </tr>
    </thead>
    <tfoot id="table-search-row">
    <tr>
        <th></th>
        <th>Name</th>
        <th>Email</th>
        <th>Message</th>
        <th>Category</th>
        <th></th>
    </tr>
    </tfoot>
    <tbody>
    <?php if ($contacts) : $serial_number = 1; ?>
        <?php foreach ($contacts as $contact) : ?>
            <tr>
                <td><?php echo $serial_number; $serial_number++; ?></td>
                <td><?php echo $contact->name ?></td>
                <td><?php echo $contact->email ?></td>
                <td><?php echo $contact->message ?></td>
                <td><?php echo ucwords($contact->type) ?></td>
                <td>

                    <a title="Send Email" href="<?php echo base_url(BACKENDFOLDER.'/contact/form/'.$contact->id) ?>" class="btn btn-primary send-email">
                        <i class="fa fa-paper-plane"></i>
                    </a>
                </td>
            </tr>
        <?php endforeach ?>
    <?php else : ?>
        <tr>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>