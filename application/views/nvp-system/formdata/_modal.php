<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Message</h4>
        </div>
        <div class="modal-body">
            <form action="<?php echo base_url(BACKENDFOLDER . '/contact/send') ?>"
                  method="post">
                <input type="hidden" name="name" value="<?php echo $data->name ?>">
                <input type="hidden" name="email" value="<?php echo $data->email ?>">
                <div class="form-group">
                    <input name="subject" class="form-control" placeholder="Subject">
                </div>
                <div class="form-group">
                    <textarea name="message" class="form-control" placeholder="Message"></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Send Email</button>
                </div>
            </form>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->