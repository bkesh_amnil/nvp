<div class="table-responsive">
    <table class="table table-bordered table-hover list-datatable">
        <thead>
        <tr>
            <th>SN</th>
            <th>Project Name</th>
            <th>Volunteer Name</th>
            <th>Primary Mode of Communication</th>
            <th>Secondary Mode of Communication</th>
            <th>Volunteer Mobile</th>
            <th>Volunteer Email</th>
            <th>Last Respond Date</th>
            <th>Response Date</th>
            <th>Invitation Sent By</th>
            <th>Invitation Status</th>
        </tr>
        </thead>
        <tfoot id="table-search-row">
        <tr>
            <th></th>
            <th>Project Name</th>
            <th>Volunteer Name</th>
            <th>Primary Mode of Communication</th>
            <th>Secondary Mode of Communication</th>
            <th>Volunteer Mobile</th>
            <th>Volunteer Email</th>
            <th>Last Respond Date</th>
            <th>Response Date</th>
            <th>Invitation Sent By</th>
            <th>Invitation Status</th>
        </tr>
        </tfoot>
        <tbody>
        <?php if ($agencyprojectvolunteerinvitees) : $serial_number = 1; ?>
            <?php foreach ($agencyprojectvolunteerinvitees as $agencyprojectvolunteerinvitee) : ?>
                <tr>
                    <td><?php echo $serial_number; $serial_number++; ?></td>
                    <td><?php echo $agencyprojectvolunteerinvitee->projectName ?></td>
                    <td><?php echo $agencyprojectvolunteerinvitee->volunteerName ?></td>
                    <td><?php getModeOfCommunication($agencyprojectvolunteerinvitee->primaryModeOfCommunication) ?></td>
                    <td><?php getModeOfCommunication($agencyprojectvolunteerinvitee->secondaryModeOfCommunication) ?></td>
                    <td>
                        <a href="tel:<?php echo $agencyprojectvolunteerinvitee->phoneNumberMobile ?>">
                            <?php echo $agencyprojectvolunteerinvitee->phoneNumberMobile ?>
                        </a>
                    </td>
                    <td>
                        <a href="mailto:<?php echo $agencyprojectvolunteerinvitee->email ?>">
                            <?php echo $agencyprojectvolunteerinvitee->email ?>
                        </a>
                    </td>
                    <td><?php echo date('d F, Y', $agencyprojectvolunteerinvitee->lastRespondDate) ?></td>
                    <td><?php echo ($agencyprojectvolunteerinvitee->responseDate == '0') ? 'No response yet' : date('d F, Y', $agencyprojectvolunteerinvitee->responseDate) ?></td>
                    <td><?php echo $agencyprojectvolunteerinvitee->adminName ?></td>
                    <td>
                        <select class="form-control" onchange="changeInvitationStatus(this.value, <?php echo $agencyprojectvolunteerinvitee->id ?>)">
                            <option <?php echo ($agencyprojectvolunteerinvitee->status == 'Invitation Sent') ? 'selected' : '' ?>>Invitation Sent</option>
                            <option <?php echo ($agencyprojectvolunteerinvitee->status == 'Rejected') ? 'selected' : '' ?>>Rejected</option>
                            <option <?php echo ($agencyprojectvolunteerinvitee->status == 'Accepted') ? 'selected' : '' ?>>Accepted</option>
                            <option <?php echo ($agencyprojectvolunteerinvitee->status == 'Approved By NVP') ? 'selected' : '' ?>>Approved By NVP</option>
                        </select>
                    </td>
                </tr>
            <?php endforeach ?>
        <?php else : ?>
            <tr>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</div>
