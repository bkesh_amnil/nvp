<table class="table table-bordered table-hover list-datatable">
    <thead>
    <tr>
        <th>SN</th>
        <th>User Name</th>
        <th>App User Device Token</th>
        <th>Action</th>
    </tr>
    </thead>
    <tfoot id="table-search-row">
    <tr>
        <th></th>
        <th>User Name</th>
        <th></th>
        <th></th>
    </tr>
    </tfoot>
    <tbody>
    <?php if ($appusers) : $serial_number = 1; ?>
        <?php foreach ($appusers as $appuser) : ?>
            <tr>
                <td><?php echo $serial_number; $serial_number++; ?></td>
                <td><?php echo $appuser->name ?></td>
                <td><?php echo $appuser->deviceToken ?></td>
                <td>
                    <?php
                    $this->data['actionBtnData'] = [
                        'module' => 'appuser',
                        'moduleData' => $appuser,
                        'options' => 'D'
                    ];
                    $ci = &get_instance();
                    $ci->partialRender(BACKENDFOLDER.'/include/actionButton');
                    ?>
                </td>
            </tr>
        <?php endforeach ?>
    <?php else : ?>
        <tr>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>