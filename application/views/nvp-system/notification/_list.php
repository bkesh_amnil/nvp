<table class="table table-bordered table-hover list-datatable">
    <thead>
    <tr>
        <th>SN</th>
        <th>Notification Title</th>
        <th>Notification Type</th>
        <th>Action</th>
    </tr>
    </thead>
    <tfoot id="table-search-row">
    <tr>
        <th></th>
        <th>Notification Title</th>
        <th>Notification Type</th>
        <th></th>
    </tr>
    </tfoot>
    <tbody>
    <?php if ($notifications) : $serial_number = 1; ?>
        <?php foreach ($notifications as $notification) : ?>
            <tr>
                <td><?php echo $serial_number;
                    $serial_number++; ?></td>
                <td><?php echo $notification->title ?></td>
                <td><?php echo ($notification->projectId > 0) ? 'Project Notification' : 'General Notification' ?></td>
                <td>
                    <?php
                    $this->data['actionBtnData'] = [
                        'module' => 'notification',
                        'moduleData' => $notification,
                        'options' => 'ED'
                    ];
                    $ci = &get_instance();
                    $ci->partialRender(BACKENDFOLDER . '/include/actionButton');
                    ?>
                    <?php /* <a title="Send Notification"
                       href="<?php echo base_url(BACKENDFOLDER . '/notification/send_notification' . $notification->id) ?>"
                       class="btn btn-info" onclick="return confirm('Are you sure?')">
                        <i class="fa fa-send fa-fw"></i>
                    </a> */ ?>
                </td>
            </tr>
        <?php endforeach ?>
    <?php else : ?>
        <tr>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>