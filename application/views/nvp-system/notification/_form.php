<?php if (validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="title">Notification Title</label>
                <input type="text" name="title" value="<?php echo $notification->title ?>" class="form-control"
                       id="title" placeholder="Notification Title">
            </div>
            <div class="form-group">
                <label for="status">Content</label>
                <select name="content_id" id="content_id" class="form-control notificationContent">
                    <option>Select Content</option>
                    <?php foreach ($contents as $key => $content) { ?>
                        <option value="<?php echo $key; ?>" <?php echo $notification->content_id == $key ? 'selected' : '' ?>>
                            <?php echo $content ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="title">Notification Short Description</label>
                <textarea rows="7" name="short_description" class="form-control"
                          id="short_description"
                          placeholder="Notification Short Description"><?php echo $notification->short_description ?></textarea>
                <div id="counter"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <?php if(segment(4) != '' || $notification->projectId <= 0) { ?>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Save and Send</button>
                </div>
            <?php } ?>
        </div>
    </div>
</form>
<script>
    window.onload = function () {
        //load_ckeditor('short_description', true)
    };
</script>