<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="site_title">Site Title</label>
                <input type="text" name="site_title" value="<?php echo $configuration->site_title ?>" class="form-control" id="site_title" placeholder="Site Title">
            </div>
            <div class="form-group">
                <label for="site_email">Site Email</label>
                <input type="text" name="site_email" value="<?php echo $configuration->site_email ?>" class="form-control" id="site_email" placeholder="Site Email">
            </div>
            <div class="form-group">
                <label for="site_logo">Site Logo</label>
                <input type="text" name="site_logo" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="false" value="<?php echo $configuration->site_logo ?>" class="form-control" id="site_logo" placeholder="Site Logo">
                <?php if($configuration->site_logo != '') { ?>
                    <img src="<?php echo base_url($configuration->site_logo) ?>" class="img-responsive" width="20%"/>
                <?php } ?>
            </div>
            <div class="form-group">
                <label for="phone">Phone(Can add multiple phone numbers using comma. EG: 1 4217455, 9849151515)</label>
                <input type="text" name="phone" value="<?php echo $configuration->phone ?>" class="form-control" id="phone" placeholder="Phone">
            </div>
            <div class="form-group">
                <label for="facebook">Facebook Link</label>
                <input type="text" name="facebook" value="<?php echo $configuration->facebook ?>" class="form-control" id="facebook" placeholder="Facebook">
            </div>
            <div class="form-group">
                <label for="skype">Skype Link</label>
                <input type="text" name="skype" value="<?php echo $configuration->skype ?>" class="form-control" id="skype" placeholder="Skype">
            </div>
            <div class="form-group">
                <label for="twitter">Twitter Link</label>
                <input type="text" name="twitter" value="<?php echo $configuration->twitter ?>" class="form-control" id="twitter" placeholder="Twitter">
            </div>
            <div class="form-group">
                <label for="gplus">Google Plus Link</label>
                <input type="text" name="gplus" value="<?php echo $configuration->gplus ?>" class="form-control" id="gplus" placeholder="Google Plus">
            </div>
            <div class="form-group">
                <label for="youtube">Youtube Link</label>
                <input type="text" name="youtube" value="<?php echo $configuration->youtube ?>" class="form-control" id="youtube" placeholder="Youtube">
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="address">Address</label>
                <textarea rows="7" name="address" class="form-control" id="address" placeholder="Address"><?php echo $configuration->address ?></textarea>
            </div>
            <div class="form-group">
                <label for="meta-keyword">Meta Keyword</label>
                <input type="text" name="meta_keyword" value="<?php echo $configuration->meta_keyword ?>" class="form-control" id="meta-keyword" placeholder="Meta Keyword">
            </div>
            <div class="form-group">
                <label for="meta-description">Meta Description</label>
                <textarea name="meta_description" rows="7" class="form-control" id="meta-description" placeholder="Meta Description"><?php echo $configuration->meta_description ?></textarea>
            </div>
            <div class="form-group">
                <label for="meta-keyword">Latitude</label>
                <input type="text" name="latitude" value="<?php echo $configuration->latitude ?>" class="form-control" id="latitude" placeholder="Latitude">
            </div>
            <div class="form-group">
                <label for="longitude">Longitude</label>
                <input type="text" name="longitude" value="<?php echo $configuration->longitude ?>" class="form-control" id="longitude" placeholder="Longitude">
            </div>
            <div class="form-group">
                <label for="infobox">Map Marker Info Box</label>
                <textarea rows="7" name="infobox" class="form-control" id="infobox" placeholder="Map Marker Info Box"><?php echo $configuration->infobox ?></textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</form>
<script>
    window.onload = function() {
        load_ckeditor('address', true);
        load_ckeditor('infobox', true);
    };
</script>