<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="name">Title</label>
                <input type="text" name="name" value="<?php echo $content->name ?>" class="form-control" id="name" placeholder="Title">
            </div>
            <div class="form-group">
                <label for="sub_title">Sub Title</label>
                <input type="text" name="sub_title" value="<?php echo $content->sub_title ?>" class="form-control" id="sub_title" placeholder="Sub Title">
            </div>
            <div class="form-group">
                <label for="source">Source</label>
                <input type="text" name="source" value="<?php echo $content->source ?>" class="form-control" id="source" placeholder="Source">
            </div>
            <div class="form-group">
                <label for="cover_image">Cover Image</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="false" name="cover_image" value="<?php echo $content->cover_image ?>" class="form-control" id="ocver_image" placeholder="Cover Image">
                <?php if($content->cover_image != '') { ?>
                    <div class="image-wrapper">
                        <img class="img-responsive" src="<?php echo base_url($content->cover_image) ?>" />
                    </div>
                <?php } ?>
            </div>
            <div class="form-group">
                <label for="image">Image</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="false" name="image" value="<?php echo $content->image ?>" class="form-control" id="image" placeholder="Image">
                <?php if($content->image != '') { ?>
                    <div class="image-wrapper">
                        <img class="img-responsive" src="<?php echo base_url($content->image) ?>" />
                    </div>
                <?php } ?>
            </div>
            <div class="form-group">
                <label for="category_id">Parent Content</label>
                <select name="category_id" id="category_id" class="form-control">
                    <option value="">Select Parent</option>
                    <?php if($contents) : ?>
                        <?php foreach($contents as $c) : ?>
                            <option value="<?php echo $c->id ?>" <?php echo ($content->parent_id == $c->id) ? 'selected' : '' ?>>
                                <?php echo $c->name ?>
                            </option>
                        <?php endforeach ?>
                    <?php endif ?>
                </select>
            </div>
            <div class="form-group">
                <label for="long_description">Long Description</label>
                <textarea rows="7" name="long_description" class="form-control" id="long_description" placeholder="Long Description"><?php echo $content->long_description ?></textarea>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="publish_date">Publish Date</label>
                <input class="form-control" type="text" id="publish_date" name="publish_date" value="<?php echo $content->publish_date ?>" placeholder="Publish Date"/>
            </div>
            <div class="form-group">
                <label for="short_description">Short Description</label>
                <textarea rows="7" name="short_description" class="form-control" id="short_description" placeholder="Short Description"><?php echo $content->short_description ?></textarea>
            </div>
            <div class="form-group">
                <label for="category_id">Category</label>
                <select name="category_id" id="category_id" class="form-control">
                    <option value="">Select Category</option>
                    <?php if($categories) : ?>
                        <?php foreach($categories as $category) : ?>
                            <option value="<?php echo $category->id ?>" <?php echo ($content->category_id == $category->id) ? 'selected' : '' ?>>
                                <?php echo $category->name ?>
                            </option>
                        <?php endforeach ?>
                    <?php endif ?>
                </select>
            </div>
            <div class="form-group">
                <label for="featured">
                    Featured <input <?php echo (isset($content->featured) && $content->featured == 'Yes') ? 'checked' : '' ?> type="checkbox" name="featured"/>
                </label>
            </div>
            <div class="form-group">
                <label for="meta_description">Meta Description</label>
                <input type="text" name="meta_description" value="<?php echo $content->meta_description ?>" class="form-control" id="meta_description" placeholder="Meta Description">
            </div>
            <div class="form-group">
                <label for="meta_keyword">Meta Keyword</label>
                <input type="text" name="meta_keyword" value="<?php echo $content->meta_keyword ?>" class="form-control" id="meta_keyword" placeholder="Meta Keyword">
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo $content->status == 'Active' || $content->status == '' ? 'selected' : '' ?>>Publish</option>
                    <option value="0" <?php echo $content->status == 'InActive' ? 'selected' : '' ?>>UnPublish</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</form>
<script>
    window.onload = function() {
        load_ckeditor('short_description', true);
        load_ckeditor('long_description');
    };
</script>