<table class="table table-bordered table-hover list-datatable">
    <thead>
    <tr>
        <th>SN</th>
        <th>Title</th>
        <th>Category</th>
        <th>Action</th>
    </tr>
    </thead>
    <tfoot id="table-search-row">
    <tr>
        <th></th>
        <th>Title</th>
        <th>Category</th>
        <th></th>
    </tr>
    </tfoot>
    <tbody>
    <?php if ($contents) : $serial_number = 1; ?>
        <?php foreach ($contents as $content) : ?>
            <tr>
                <td><?php echo $serial_number; $serial_number++; ?></td>
                <td><?php echo $content->name ?></td>
                <td><?php echo $content->category_name ?></td>
                <td>
                    <?php
                    $this->data['actionBtnData'] = [
                        'module' => 'content',
                        'moduleData' => $content,
                        'options' => 'EDS',
                        'no_delete' => array(67),
                        'no_publish' => array(67)
                    ];
                    $ci = &get_instance();
                    $ci->partialRender(BACKENDFOLDER.'/include/actionButton');
                    ?>
                </td>
            </tr>
        <?php endforeach ?>
    <?php else : ?>
        <tr>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>