<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="name">Partner Name</label>
                <input type="text" name="name" value="<?php echo $partner->name ?>" class="form-control" id="name" placeholder="Partner Name">
            </div>
            <div class="form-group">
                <label for="description">Partner Description</label>
                <textarea rows="7" name="description" class="form-control" id="description" placeholder="Partner Description"><?php echo $partner->description ?></textarea>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="partner_category">Partner Type</label>
                <select name="partner_category" id="partner_category" class="form-control">
                    <?php foreach($partnerCategory as $cType) { ?>
                        <option value="<?php echo $cType->id ?>" <?php echo $partner->partner_category == $cType->id ? 'selected' : '' ?>>
                            <?php echo $cType->name ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="image">Partner Image(Best Image Visibility : 314*178)</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="false" name="image" value="<?php echo $partner->image ?>" class="form-control" id="image" placeholder="Image">
                <?php if($partner->image != '') { ?>
                    <div class="image-wrapper">
                        <img class="img-responsive" src="<?php echo base_url($partner->image) ?>" />
                    </div>
                <?php } ?>
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo $partner->status == 'Active' || $partner->status == '' ? 'selected' : '' ?>>Publish</option>
                    <option value="0" <?php echo $partner->status == 'InActive' ? 'selected' : '' ?>>UnPublish</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</form>
<script>
    window.onload = function() {
        load_ckeditor('description');
    };
</script>