<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="name">Impact Name</label>
                <input type="text" name="name" value="<?php echo $impact->name ?>" class="form-control" id="name" placeholder="Impact Name">
            </div>
            <div class="form-group">
                <label for="statistic">Impact Statistic</label>
                <input type="text" name="statistic" value="<?php echo $impact->statistic ?>" class="form-control" id="statistic" placeholder="Impact Statistic">
            </div>
            <div class="form-group">
                <label for="color">Text Color Code</label>
                <input type="text" name="color" value="<?php echo $impact->color ?>" class="form-control" id="color" placeholder="Text Color Category">
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="icon">Impact Icon (Best Image Visibility: W:198px, H:198px)</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="false" name="icon" value="<?php echo $impact->icon ?>" class="form-control" id="icon" placeholder="Impact Icon">
            </div>
            <?php if($impact->icon != '') { ?>
                <div class="image-wrapper">
                    <img class="img-responsive" src="<?php echo base_url($impact->icon) ?>" />
                </div>
            <?php } ?>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo $impact->status == 'Active' || $impact->status == '' ? 'selected' : '' ?>>Publish</option>
                    <option value="0" <?php echo $impact->status == 'InActive' ? 'selected' : '' ?>>UnPublish</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</form>