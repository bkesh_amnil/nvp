<table class="table table-bordered table-hover list-datatable">
    <thead>
    <tr>
        <th>SN</th>
        <th>Title</th>
        <th>Statistic</th>
        <th>Icon</th>
        <th>Action</th>
    </tr>
    </thead>
    <tfoot id="table-search-row">
    <tr>
        <th></th>
        <th>Name</th>
        <th>Statistic</th>
        <th></th>
        <th></th>
    </tr>
    </tfoot>
    <tbody>
    <?php if ($impacts) : $serial_number = 1; ?>
        <?php foreach ($impacts as $impact) : ?>
            <tr>
                <td><?php echo $serial_number; $serial_number++; ?></td>
                <td><?php echo $impact->name ?></td>
                <td><?php echo $impact->statistic ?></td>
                <td>
                    <img src="<?php echo base_url($impact->icon) ?>" class="img-responsive" width="20%" />
                </td>
                <td>
                    <?php
                    $this->data['actionBtnData'] = [
                        'module' => 'impact',
                        'moduleData' => $impact,
                        'options' => 'EDS'
                    ];
                    $ci = &get_instance();
                    $ci->partialRender(BACKENDFOLDER.'/include/actionButton');
                    ?>
                </td>
            </tr>
        <?php endforeach ?>
    <?php else : ?>
        <tr>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>