<?php if (validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="name">Gallery Name</label>
                <input type="text" name="name" value="<?php echo $gallery->name ?>" class="form-control" id="name"
                       placeholder="Gallery Name">
            </div>
            <div class="form-group">
                <label for="short_description">Gallery Short Description</label>
                <textarea name="short_description" class="form-control" id="short_description"
                          placeholder="Short Description"><?php echo $gallery->short_description ?></textarea>
            </div>
            <div class="form-group">
                <label for="name">Media Type</label>
                <?php if (isset($savedMedia)) { ?>
                    <input type="hidden" value="<?php echo strtolower($savedMedia[0]->type) ?>" name="mediaTypeSelect"/>
                <?php } ?>
                <label>
                    <input type="radio" <?php echo (segment(4) != '') ? 'disabled' : '' ?> name="mediaTypeSelect"
                           class="mediaTypeSelect" <?php echo ($gallery->type == 'Image' || $gallery->type == '') ? 'checked' : '' ?>
                           value="image"> Image
                </label>
                <label>
                    <input type="radio" <?php echo (segment(4) != '') ? 'disabled' : '' ?> name="mediaTypeSelect"
                           class="mediaTypeSelect" <?php echo ($gallery->type == 'Video') ? 'checked' : '' ?>
                           value="video"> Video
                </label>
            </div>
            <div class="form-group"
                 id="selectImages" <?php echo (isset($savedMedia) && $savedMedia[0]->type != 'Image') ? 'style="display:none;"' : '' ?>>
                <label for="image">Images</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="true"
                       name="image" data-show-detail="true" class="form-control" id="image" placeholder="Images">
            </div>
            <div class="form-group"
                 id="selectVideos" <?php echo (isset($savedMedia) && $savedMedia[0]->type != 'Video') ? 'style="display:none;"' : (!isset($savedMedia)) ? 'style="display:none;"' : '' ?>>
                <label for="video">Videos</label>

               <!-- <div class="form-group">
                    <input type="text" name="media[]" class="form-control" placeholder="Videos">
                </div>

                <div class="form-group">
                    <input class="form-control" placeholder="Title" name="title[]"/>
                </div>
                <div class="form-group">
                    <textarea class="form-control" placeholder="Description"
                              name="description[]">
                    </textarea>
                </div>-->
                <div class="form-group">
                    <a href="#" class="add-videos btn btn-success" title="Add video Link"> + </a></div>
            </div>
            <?php
            if (isset($savedMedia)) { ?>
            <?php foreach ($savedMedia as $media) { ?>
                <div class="mediaWrapper">
                    <?php if ($media->type == 'Image') { ?>
                        <div class="row">
                            <div class="col-md-4">
                                <img class="img-responsive" src="<?php echo base_url($media->media) ?>"/>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" class="form-control" readonly="readonly"
                                           value="<?php echo $media->media ?>" name="media[]"/>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Title"
                                           value="<?php echo $media->title ?>" name="title[]"/>
                                </div>
                                <div class="form-group">
                                        <textarea class="form-control" placeholder="Description"
                                                  name="description[]"><?php echo $media->caption ?></textarea>
                                </div>
                            </div>
                        </div>
                    <?php } else { ?>
                        <input type="hidden" value="<?php echo $media->media ?>" name="media[]"/>
                        <div class="image-wrapper">
                            <img class="img-responsive" src="
                                http://img.youtube.com/vi/<?php echo getYoutubeVideoId($media->media) ?>/3.jpg"/>
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Title" value="<?php echo $media->title ?>"
                                   name="title[]"/>
                        </div>
                        <div class="form-group">
                                        <textarea class="form-control" placeholder="Description"
                                                  name="description[]"><?php echo $media->caption ?></textarea>
                        </div>
                    <?php } ?>
                    <a href="javascript:void(0);" class="deleteMedia"
                       data-url="<?php echo base_url(BACKENDFOLDER . '/gallery/deleteMedia/' . $media->id) ?>">
                        Delete
                    </a>
                </div>
            <?php } ?>
            <?php } ?>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group" id="selectImages">
                <label for="cover">Cover Image *(less than 1 Mb and size 1950X500)</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="false"
                       name="cover" value="<?php echo $gallery->cover ?>" class="form-control" id="cover"
                       placeholder="Cover Image">
            </div>
            <?php if ($gallery->cover != '') { ?>
                <img src="<?php echo base_url($gallery->cover) ?>" width="20%"/>
            <?php } ?>
            <div class="form-group">
                <label for="category_id">Category</label>
                <select name="category_id" id="category_id" class="form-control">
                    <option value="">Select Category</option>
                    <?php if ($categories) : ?>
                        <?php foreach ($categories as $category) : ?>
                            <option
                                value="<?php echo $category->id ?>" <?php echo ($gallery->category_id == $category->id) ? 'selected' : '' ?>>
                                <?php echo $category->name ?>
                            </option>
                        <?php endforeach ?>
                    <?php endif ?>
                </select>
            </div>
            <div class="form-group">
                <label for="contentId">Content</label>
                <select name="contentId" id="contentId" class="form-control galleryContent">
                    <option value="">Select Content</option>
                    <?php if ($contentList) : ?>
                        <?php foreach ($contentList as $content) : ?>
                            <option
                                value="<?php echo $content->id ?>" <?php echo ($gallery->contentId == $content->id) ? 'selected' : '' ?>>
                                <?php echo $content->name ?>
                            </option>
                        <?php endforeach ?>
                    <?php endif ?>
                </select>
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option
                        value="1" <?php echo $gallery->status == 'Active' || $gallery->status == '' ? 'selected' : '' ?>>
                        Publish
                    </option>
                    <option value="0" <?php echo $gallery->status == 'InActive' ? 'selected' : '' ?>>UnPublish</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</form>

<!-- Modal -->
<div class="modal fade" id="galleryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Media Details</h4>
            </div>
            <div class="modal-body">
                <form action="<?php echo base_url(BACKENDFOLDER . '/gallery/saveMediaDetail') ?>" method="post">
                    <div class="form-group">
                        <label for="mediaTitle">Title</label>
                        <input class="form-control" type="text" name="mediaTitle" id="mediaTitle"
                               placeholder="Media Title"/>
                    </div>
                    <div class="form-group">
                        <label for="mediaCaption">Caption</label>
                        <textarea rows="8" class="form-control" name="mediaCaption" id="mediaCaption"
                                  placeholder="Media Caption"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="mediaDescription">Description</label>
                        <textarea rows="8" class="form-control" name="mediaDescription" id="mediaDescription"
                                  placeholder="Media Description"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Save" class="btn btn-primary"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>