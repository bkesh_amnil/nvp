<table class="table table-bordered table-hover list-datatable">
    <thead>
    <tr>
        <th>SN</th>
        <th>Gallery Name</th>
        <th>Media Type</th>
        <th>Category</th>
        <th>Action</th>
    </tr>
    </thead>
    <tfoot id="table-search-row">
    <tr>
        <th></th>
        <th>Gallery Name</th>
        <th>Media Type</th>
        <th>Category</th>
        <th></th>
    </tr>
    </tfoot>
    <tbody>
    <?php if ($galleries) : $serial_number = 1; ?>
        <?php foreach ($galleries as $gallery) : ?>
            <tr>
                <td><?php echo $serial_number; $serial_number++; ?></td>
                <td><?php echo $gallery->name ?></td>
                <td><?php echo $gallery->mediaType ?></td>
                <td><?php echo $gallery->category_name ?></td>
                <td>
                    <?php
                    $this->data['actionBtnData'] = [
                        'module' => 'gallery',
                        'moduleData' => $gallery,
                        'options' => 'EDS'
                    ];
                    $ci = &get_instance();
                    $ci->partialRender(BACKENDFOLDER.'/include/actionButton');
                    ?>
                </td>
            </tr>
        <?php endforeach ?>
    <?php else : ?>
        <tr>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>