<?php
// getting module permission
$ci = &get_instance();
$permission = $ci->checkModulePermission($ci->data['active_module_id']);
?>

<?php if((!isset($actionBtnData['no_edit']) || !in_array($actionBtnData['moduleData']->id, $actionBtnData['no_edit'])) && (substr($actionBtnData['options'], 0, 1) == 'E' || $actionBtnData['options'] == 'E')) { ?> <!-- checking to display options -->
    <?php if($activeModulePermission['edit']) {
        $action = true; ?>
        <a title="Edit Data" href="<?php echo base_url(BACKENDFOLDER.'/'.$actionBtnData['module'].'/create/'.$actionBtnData['moduleData']->id) ?>" class="btn btn-primary">
            <i class="fa fa-edit fa-fw"></i>
        </a>
    <?php } ?>
<?php } ?>

<?php if((!isset($actionBtnData['no_delete']) || !in_array($actionBtnData['moduleData']->id, $actionBtnData['no_delete'])) && (substr($actionBtnData['options'], 1, 1) == 'D' || $actionBtnData['options'] == 'D')) { ?> <!-- checking to display options -->
    <?php if($activeModulePermission['delete']) {
        $action = true; ?>
        <a title="Delete Data" href="<?php echo base_url(BACKENDFOLDER.'/'.$actionBtnData['module'].'/delete/'.$actionBtnData['moduleData']->id) ?>" class="btn btn-danger" onclick="return confirm('Are you sure?')">
            <i class="fa fa-trash fa-fw"></i>
        </a>
    <?php } ?>
<?php } ?>

<?php if((!isset($actionBtnData['no_publish']) || !in_array($actionBtnData['moduleData']->id, $actionBtnData['no_publish'])) && (substr($actionBtnData['options'], 2, 1) == 'S' || $actionBtnData['options'] == 'S')) { ?> <!-- checking to display options -->
    <?php if($activeModulePermission['edit']) {
        $action = true; ?>
        <?php
        if($actionBtnData['moduleData']->status == 'InActive') {
            $icon_class = 'fa-eye';
            $button_class = 'btn-success';
            $button_text = '';
        } else {
            $icon_class = 'fa-eye-slash';
            $button_class = 'btn-info';
            $button_text = '';
        }
        ?>
        <a title="Change Status of Data" href="<?php echo base_url(BACKENDFOLDER.'/'.$actionBtnData['module'].'/status/'.$actionBtnData['moduleData']->status.'/'.$actionBtnData['moduleData']->id) ?>" class="btn <?php echo $button_class ?>" onclick="return confirm('Are you sure?')">
            <i class="fa <?php echo $icon_class ?> fa-fw"></i><?php echo $button_text ?>
        </a>
    <?php } ?>
<?php } ?>

<?php echo !(isset($action)) ? 'No permission granted for other actions' : '' ?>