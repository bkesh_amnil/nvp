<?php
$ci = &get_instance();
$ci->load->model('common_model', 'common');
$query = "SELECT
              ap.`name` as project_name, pv.`hours`, a.`name` as agency_name, v.`fullName` as volunteer_name
            FROM
              nvp_agency_project ap
              JOIN nvp_project_volunteer pv
                ON pv.agencyProjectId = ap.id
              JOIN nvp_volunteer v
                ON v.id = pv.volunteerId
              JOIN nvp_agency a
                ON a.id = ap.agencyId
            WHERE pv.volunteerId = ".$volunteer_id;
$hour_log_detail = $ci->common->query($query);
?>
<div class="modal fade" id="volunteer-<?= $volunteer_id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Hour Log Detail</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Agency Name</th>
                        <th>Project Name</th>
                        <th>Volunteer Name</th>
                        <th>Hours</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if($hour_log_detail) { foreach($hour_log_detail as $key => $hour_log) { ?>
                        <tr>
                            <td>
                                <?= $key+1 ?>
                            </td>
                            <td>
                                <?= $hour_log->agency_name ?>
                            </td>
                            <td>
                                <?= $hour_log->project_name ?>
                            </td>
                            <td>
                                <?= $hour_log->volunteer_name ?>
                            </td>
                            <td>
                                <?= $hour_log->hours ?>
                            </td>
                        </tr>
                    <?php } } else { echo "<tr><td colspan='5'>No Data</td></tr>"; }?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>