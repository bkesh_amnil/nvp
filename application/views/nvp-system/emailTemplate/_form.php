<?php if (validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="name">Template Name</label>
                <input type="text" name="name" value="<?php echo $emailTemplate->name ?>" class="form-control" id="name"
                       placeholder="Title">
            </div>
            <div class="form-group">
                <label for="name">Template Category</label>
                <select name="emailCategoryId" class="form-control">
                    <?php foreach ($emailTemplateCategory as $key => $category) {
                        if (isset($emailTemplate->emailCategoryId) && $key == $emailTemplate->emailCategoryId) {
                            $selected = 'selected';
                        } else {
                            $selected = '';
                        } ?>
                        <option value="<?php echo $key ?>" <?php echo $selected ?>> <?php echo $category; ?>
                        </option>
                    <?php } ?></select>
            </div>
            <div class="form-group">
                <label for="adminEmail">Admin Email</label>
                <input type="text" name="adminEmail" value="<?php echo $emailTemplate->adminEmail ?>"
                       class="form-control" id="adminEmail" placeholder="Admin Email">
            </div>
            <div class="form-group">
                <label for="adminSubject">Admim Subject</label>
                <input type="text" name="adminSubject" value="<?php echo $emailTemplate->adminSubject ?>"
                       class="form-control" id="adminSubject" placeholder="Admim Subject">
            </div>
            <div class="form-group">
                <label for="adminMessage">Admin Message</label>
                <textarea rows="7" name="adminMessage" class="form-control" id="adminMessage"
                          placeholder="Admin Message"><?php echo $emailTemplate->adminMessage ?></textarea>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="userSubject">User Subject</label>
                <input type="text" name="userSubject" value="<?php echo $emailTemplate->userSubject ?>"
                       class="form-control" id="userSubject" placeholder="User Subject">
            </div>
            <div class="form-group">
                <label for="userMessage">User Message</label>
                <textarea rows="7" name="userMessage" class="form-control" id="userMessage"
                          placeholder="User Message"><?php echo $emailTemplate->userMessage ?></textarea>
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option
                        value="1" <?php echo $emailTemplate->status == 'Active' || $emailTemplate->status == '' ? 'selected' : '' ?>>
                        Publish
                    </option>
                    <option value="0" <?php echo $emailTemplate->status == 'InActive' ? 'selected' : '' ?>>UnPublish
                    </option>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</form>
<script>
    window.onload = function () {
        load_ckeditor('adminMessage', true);
        load_ckeditor('userMessage', true);
    };
</script>