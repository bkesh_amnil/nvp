<select name="emailTemplateCategory" class="emailTemplateCategory">
    <option value="all">All Email Template</option>
    <?php foreach ($emailTemplateCategory as $key => $category) {
        if (isset($emailTemplateCategoryId) && $key == $emailTemplateCategoryId) {
            $selected = 'selected';
        } else {
            $selected = '';
        } ?>
        <option value="<?php echo $key ?>" <?php echo $selected ?>> <?php echo $category; ?>
        </option>
    <?php } ?></select>
<table class="table table-bordered table-hover list-datatable">
    <thead>
    <tr>
        <th>SN</th>
        <th>Template Name</th>
        <th>Template Category</th>
        <th>Action</th>
    </tr>
    </thead>
    <tfoot id="table-search-row">
    <tr>
        <th></th>
        <th>Template Name</th>
        <th>Template Category</th>
        <th></th>
    </tr>
    </tfoot>
    <tbody>
    <?php if ($emailTemplates) : $serial_number = 1; ?>
        <?php foreach ($emailTemplates as $emailTemplate) : ?>
            <tr>
                <td><?php echo $serial_number;
                    $serial_number++; ?></td>
                <td><?php echo $emailTemplate->name ?></td>
                <td><?php echo $emailTemplate->categoryName ?></td>
                <td>
                    <?php
                    $this->data['actionBtnData'] = [
                        'module' => 'emailTemplate',
                        'moduleData' => $emailTemplate,
                        'options' => 'EDS'
                    ];
                    $ci = &get_instance();
                    $ci->partialRender(BACKENDFOLDER . '/include/actionButton');
                    ?>
                </td>
            </tr>
        <?php endforeach ?>
    <?php else : ?>
        <tr>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>