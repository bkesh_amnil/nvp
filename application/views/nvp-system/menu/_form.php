<?php if (validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<?php ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="title">Menu Title</label>
                <input type="text" name="menu_title" value="<?php echo $menu->menu_title; ?>" class="form-control title"
                       id="title" placeholder="Menu Title">
            </div>
            <div class="form-group">
                <label for="link">Menu Alias</label>
                <input type="text" name="menu_alias" value="<?php echo $menu->menu_alias; ?>" class="form-control alias"
                       id="alias"
                       placeholder="Menu Alias">
            </div>
            <div class="form-group">
                <label for="link">Menu Description</label>
                <textarea name="description" class="form-control"
                       id="description"
                       placeholder="Menu Description"><?php echo $menu->description; ?></textarea>
            </div>
            <div class="form-group">
                <label for="menu_parent">Menu Parent</label>
                <select name="menu_parent" id="menu_parent" class="form-control">
                    <option value="">Select Menu Parent</option>
                    <?php if ($parentMenus) {
                        foreach ($parentMenus as $menuParent) { ?>
                            <option
                                value="<?php echo $menuParent->id ?>" <?php echo ($menuParent->id == $menu->menu_parent) ? 'selected' : '' ?>>
                                <?php echo $menuParent->menu_title ?>
                            </option>
                        <?php }
                    } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="menu_type">Menu Type</label>
                <select name="menu_type" class="form-control" id="menu_type">
                    <?php if ($menu_type) : ?>
                        <?php foreach ($menu_type as $type) : ?>
                            <option
                                value="<?php echo $type->menutype ?>" <?php echo ($menu->menu_type == $type->menutype) ? 'selected' : '' ?>><?php echo $type->title; ?></option>
                        <?php endforeach ?>
                    <?php endif ?>
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="menu_linktype">Menu Link Type</label>
                <select name="menu_link_type" id="menu-link-type" class="form-control">
                    <?php if ($menu_link_type) : ?>
                        <?php foreach ($menu_link_type['linktypes'] as $key => $type) : ?>
                            <option
                                value="<?php echo $key ?>" <?php echo ($menu->menu_link_type == $key) ? 'selected' : '' ?>>
                                <?php echo ucwords($type); ?>
                            </option>
                        <?php endforeach ?>
                    <?php endif ?>
                </select>
            </div>

            <div class="form-group" id="menu-url" style="display: none">
                <label for="link">Link</label>
                <input type="text" name="url_link" value="<?php echo $menu->link; ?>" class="form-control alias"
                       placeholder="Menu Link">
            </div>

            <div class="form-group" id="menu-content" style="display: none">
                <label for="link">Select Content</label>
                <select name="content_link" class="form-control">
                    <?php foreach ($menu_link_type['link']['content'] as $key => $type) { ?>
                        <option value="<?php echo $key ?>" <?php echo ($menu->link == $key) ? 'selected' : '' ?>>
                            <?php echo ucwords($type); ?>
                        </option>
                    <?php } ?>
                </select>
            </div>

            <div class="form-group" id="menu-module" style="display: none">
                <label for="link">Select Module</label>
                <select id="module_link" name="module_link" class="form-control">
                    <option>Select Module</option>
                    <?php foreach ($menu_link_type['link']['module'] as $key => $type) {
                    foreach ($type as $value => $sub_menu){
                    if ($sub_menu == $key){ ?>
                    <optgroup label="<?php echo ucwords($key); ?>">
                        <?php } else { ?>
                            <option
                                value="<?php echo $value ?>" <?php echo ($menu->link == $value) ? 'selected' : '' ?>>
                                <?php echo ucwords($sub_menu); ?>
                            </option>
                        <?php }
                        if ($sub_menu == $key){ ?>
                        </optgroup> <?php }
                            }
                            } ?>
                </select>
            </div>

            <div class="form-group">
                <label for="status">Status </label>
                <select name="status" id="status" class="form-control">
                    <option
                        value="Active" <?php echo $menu->status == 'Active' || $menu->status == '' ? 'selected' : '' ?>>
                        Active
                    </option>
                    <option value="InActive" <?php echo $menu->status == 'Inactive' ? 'selected' : '' ?>>
                        InActive
                    </option>
                </select>
            </div>

            <div class="form-group">
                <label for="link">Sort Order Number</label>
                <input type="text" name="orderNumber" value="<?php echo $menu->orderNumber; ?>" class="form-control"
                       placeholder="Order Number">
            </div>

        </div>


    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</form>
