<table class="table table-bordered table-hover list-datatable">
    <thead>
    <tr>
        <th>SN</th>
        <th>Menu Title</th>
        <th>Menu Type</th>
        <th>Link Type</th>
        <th>Order Number</th>
        <th>Action</th>
    </tr>
    </thead>
    <tfoot id="table-search-row">
    <tr>
        <th></th>
        <th>Menu Title</th>
        <th>Menu Type</th>
        <th>Link Type</th>
        <th>Order Number</th>
        <th></th>
    </tr>
    </tfoot>
    <tbody>
    <?php if ($menus) : $serial_number = 1;?>
        <?php foreach ($menus as $menu) : ?>
            <tr>
                <td><?php echo $serial_number; $serial_number++; ?></td>
                <td><?php echo $menu['menu_title']; ?></td>
                <td><?php echo $menu['menu_type']; ?></td>
                <td><?php echo $menu['menu_link_type']; ?></td>
                <td><?php echo $menu['orderNumber']; ?></td>
                <td>
                    <?php
                    $this->data['actionBtnData'] = [
                        'module' => 'menu',
                        'moduleData' => (object) $menu,
                        'options' => 'EDS'
                    ];
                    $ci = &get_instance();
                    $ci->partialRender(BACKENDFOLDER.'/include/actionButton');
                    ?>
                </td>
            </tr>
            <?php foreach ($menu['childList'] as $menuchild) : ?>
                <tr>
                    <td><?php echo $serial_number; $serial_number++; ?></td>
                    <td><?php echo $menuchild['menu_title']; ?></td>
                    <td><?php echo $menuchild['menu_type']; ?></td>
                    <td><?php echo $menuchild['menu_link_type']; ?></td>
                    <td><?php echo $menuchild['orderNumber']; ?></td>
                    <td>
                        <?php
                        $this->data['actionBtnData'] = [
                            'module' => 'menu',
                            'moduleData' => (object) $menuchild,
                            'options' => 'EDS'
                        ];
                        $ci = &get_instance();
                        $ci->partialRender(BACKENDFOLDER.'/include/actionButton');
                        ?>
                    </td>
                </tr>
            <?php endforeach ?>
        <?php endforeach ?>
    <?php else : ?>
        <tr>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>
