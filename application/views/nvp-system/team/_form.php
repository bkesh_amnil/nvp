<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="categoryId">Category</label>
                <select name="categoryId" id="categoryId" class="form-control">
                    <option value="">Select Category</option>
                    <?php if($teamCategory) : ?>
                        <?php foreach($teamCategory as $category) : ?>
                            <option value="<?php echo $category->id ?>" <?php echo ($team->categoryId == $category->id) ? 'selected' : '' ?>>
                                <?php echo $category->name ?>
                            </option>
                        <?php endforeach ?>
                    <?php endif ?>
                </select>
            </div>
            <div class="form-group">
                <label for="name">Team Member Name</label>
                <input type="text" name="name" value="<?php echo $team->name ?>" class="form-control" id="name" placeholder="Team Member Name">
            </div>
            <div class="form-group">
                <label for="name">Team Member Description</label>
                <textarea rows="7" name="description" class="form-control" id="description" placeholder="Message"><?php echo $team->description ?></textarea>
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="Active" <?php echo $team->status == 'Active' || $team->status == '' ? 'selected' : '' ?>>Publish</option>
                    <option value="InActive" <?php echo $team->status == 'InActive' ? 'selected' : '' ?>>UnPublish</option>
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="image">Image (Best Image Visibility: W:260px, H:220)</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="false" name="image" value="<?php echo $team->image ?>" class="form-control" id="image" placeholder="Image">
                <?php if($team->image != '') { ?>
                    <div class="image-wrapper">
                        <img class="img-responsive" src="<?php echo base_url($team->image) ?>" />
                    </div>
                <?php } ?>
            </div>
            <div class="form-group">
                <label for="facebook">Facebook Link</label>
                <input type="text" name="facebook" value="<?php echo $team->facebook ?>" class="form-control" id="facebook" placeholder="Team Member Facebook Link">
            </div>
            <div class="form-group">
                <label for="twitter">Twitter Link</label>
                <input type="text" name="twitter" value="<?php echo $team->twitter ?>" class="form-control" id="twitter" placeholder="Team Member Twitter Link">
            </div>
            <div class="form-group">
                <label for="linkedIn">LinkedIn Link</label>
                <input type="text" name="linkedIn" value="<?php echo $team->linkedIn ?>" class="form-control" id="linkedIn" placeholder="Team Member LinkedIn Link">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</form>
<script>
    window.onload = function() {
        load_ckeditor('description', true);
    };
</script>