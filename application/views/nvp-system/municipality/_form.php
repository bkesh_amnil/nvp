<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="district">District</label>
                <select name="districtId" class="form-control" id="district">
                    <?php foreach($allDistricts as $district) { ?>
                        <option value="<?php echo $district->id ?>" <?php echo ($municipality->districtId == $district->id) ? 'selected' : '' ?>>
                            <?php echo $district->name ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="name">Municipality Name</label>
                <input type="text" name="name" value="<?php echo $municipality->name ?>" class="form-control" id="name" placeholder="Municipality Name">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</form>