<table class="table table-bordered table-hover list-datatable">
    <thead>
    <tr>
        <th>SN</th>
        <th>Municipality</th>
        <th>District</th>
        <th>Action</th>
    </tr>
    </thead>
    <tfoot id="table-search-row">
    <tr>
        <th></th>
        <th>Municipality</th>
        <th>District</th>
        <th></th>
    </tr>
    </tfoot>
    <tbody>
    <?php if ($municipalities) : $serial_number = 1; ?>
        <?php foreach ($municipalities as $municipality) : ?>
            <tr>
                <td><?php echo $serial_number; $serial_number++; ?></td>
                <td><?php echo $municipality->name ?></td>
                <td><?php echo $municipality->districtName ?></td>
                <td>
                    <?php
                    $this->data['actionBtnData'] = [
                        'module' => 'municipality',
                        'moduleData' => $municipality,
                        'options' => 'ED'
                    ];
                    $ci = &get_instance();
                    $ci->partialRender(BACKENDFOLDER.'/include/actionButton');
                    ?>
                </td>
            </tr>
        <?php endforeach ?>
    <?php else : ?>
        <tr>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>