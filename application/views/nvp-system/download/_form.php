<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="name">Title</label>
                <input type="text" name="name" value="<?php echo $download->name ?>" class="form-control" id="name" placeholder="Title">
            </div>
            <div class="form-group">
                <label for="file">File</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="all" data-multiple="false" name="file" value="<?php echo $download->file ?>" class="form-control" id="file" placeholder="File">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea rows="7" name="description" class="form-control" id="description" placeholder="Description"><?php echo $download->description ?></textarea>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="publishDate">Publish Date</label>
                <input class="form-control" type="text" id="publishDate" name="publishDate" value="<?php echo $download->publishDate ?>" placeholder="Publish Date"/>
            </div>
            <div class="form-group">
                <label for="categoryId">Category</label>
                <select name="categoryId" id="categoryId" class="form-control">
                    <option value="">Select Category</option>
                    <?php if($downloadCategory) : ?>
                        <?php foreach($downloadCategory as $category) : ?>
                            <option value="<?php echo $category->id ?>" <?php echo ($download->categoryId == $category->id) ? 'selected' : '' ?>>
                                <?php echo $category->name ?>
                            </option>
                        <?php endforeach ?>
                    <?php endif ?>
                </select>
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo $download->status == 'Active' || $download->status == '' ? 'selected' : '' ?>>Publish</option>
                    <option value="0" <?php echo $download->status == 'InActive' ? 'selected' : '' ?>>UnPublish</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</form>
<script>
    window.onload = function() {
        load_ckeditor('description', true);
    };
</script>