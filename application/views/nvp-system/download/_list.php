<table class="table table-bordered table-hover list-datatable">
    <thead>
    <tr>
        <th>SN</th>
        <th>Download</th>
        <th>File Type</th>
        <th>Action</th>
    </tr>
    </thead>
    <tfoot id="table-search-row">
    <tr>
        <th></th>
        <th>Download</th>
        <th>File Type</th>
        <th></th>
    </tr>
    </tfoot>
    <tbody>
    <?php if ($downloads) : $serial_number = 1; ?>
        <?php foreach ($downloads as $download) : ?>
            <tr>
                <td><?php echo $serial_number; $serial_number++; ?></td>
                <td><?php echo $download->name ?></td>
                <td><?php echo $download->categoryName ?></td>
                <td>
                    <?php
                    $this->data['actionBtnData'] = [
                        'module' => 'download',
                        'moduleData' => $download,
                        'options' => 'EDS'
                    ];
                    $ci = &get_instance();
                    $ci->partialRender(BACKENDFOLDER.'/include/actionButton');
                    ?>
                </td>
            </tr>
        <?php endforeach ?>
    <?php else : ?>
        <tr>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>