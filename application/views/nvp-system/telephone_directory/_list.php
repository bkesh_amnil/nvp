<table class="table table-bordered table-hover list-datatable">
    <thead>
    <tr>
        <th>SN</th>
        <th>First Name</th>
        <th>Middle Name</th>
        <th>Last Name</th>
        <th>Mobile Number</th>
        <th>Contact Number</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php if ($telephone_directories) : $serial_number = 1; ?>
        <?php foreach ($telephone_directories as $telephone_directory) : ?>
            <tr>
                <td><?php echo $serial_number; $serial_number++; ?></td>
                <td><?php echo ucwords($telephone_directory->first_name) ?></td>
                <td><?php echo ucwords($telephone_directory->middle_name) ?></td>
                <td><?php echo ucwords($telephone_directory->last_name) ?></td>
                <td><?php echo $telephone_directory->mobile_number ?></td>
                <td><?php echo $telephone_directory->contact_number ?></td>
                <td>
                    <?php
                    $this->data['actionBtnData'] = [
                        'module' => 'telephone_directory',
                        'moduleData' => $telephone_directory,
                        'options' => 'EDS'
                    ];
                    $ci = &get_instance();
                    $ci->partialRender(BACKENDFOLDER.'/include/actionButton');
                    ?>
                </td>
            </tr>
        <?php endforeach ?>
    <?php else : ?>
        <tr>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>