<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="first_name">First Name</label>
                <input type="text" name="first_name" value="<?php echo $telephone_directory->first_name ?>" class="form-control" id="first_name" placeholder="First name">
            </div>
            <div class="form-group">
                <label for="middle_name">Middle Name</label>
                <input type="text" name="middle_name" value="<?php echo $telephone_directory->middle_name ?>" class="form-control" id="middle_name" placeholder="Middle name">
            </div>
            <div class="form-group">
                <label for="last_name">Last Name</label>
                <input type="text" name="last_name" value="<?php echo $telephone_directory->last_name ?>" class="form-control" id="last_name" placeholder="Last name">
            </div>
            <div class="form-group">
                <label for="mobile_number">Mobile Number</label>
                <input type="text" name="mobile_number" value="<?php echo $telephone_directory->mobile_number ?>" class="form-control" id="mobile_number" placeholder="Mobile Number">
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="contact_number">Phone Number</label>
                <input type="text" name="contact_number" value="<?php echo $telephone_directory->contact_number ?>" class="form-control" id="contact_number" placeholder="Contact number">
            </div>
            <!--<div class="form-group">
                <label for="ext_number">Extention Number</label>
                <input type="text" name="ext_number" value="<?php /*echo $telephone_directory->ext_number */?>" class="form-control" id="ext_number" placeholder="Extention Number">
            </div>-->

            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="Active" <?php echo $telephone_directory->status == 'Active' || $telephone_directory->status == '' ? 'selected' : '' ?>>Publish</option>
                    <option value="InActive" <?php echo $telephone_directory->status == 'InActive' ? 'selected' : '' ?>>UnPublish</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</form>