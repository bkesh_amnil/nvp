<?php foreach ($content_data as $content) { ?>
    <div class="detail-banner-img news-title-wrap">
        <!--<img class="img-responsive" src="<?php //echo base_url($newsDetail->image) ?>" alt="<?php //echo $newsDetail->name ?>"/>-->
        <h3><?php echo $content->name; ?></h3>
    </div>
    <section class="detail-wrap">
        <div class="container">
            <div class="details">
                <?php echo $content->long_description ?>
            </div>
        </div>
    </section>
<?php } ?>