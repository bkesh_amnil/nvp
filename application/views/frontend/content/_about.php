<section class="about">
    <div class="row">
        <div class="col s12 m12 l6">
            <div id="about-banner" class="owl-carousel">
                <?php foreach ($banner_data as $banner) { ?>
                    <div class="about-banner-content">
                        <img src="<?php echo base_url($banner->image); ?>" alt=""/>
                        <span><?php echo $banner->title; ?></span>
                    </div>
                <?php } ?>
            </div>
        </div>

        <div class="col s12 m12 l4">
            <div class="about-content-wrap">
                <?php foreach ($content_data as $content) { ?>
                    <h2 class="page-title"><?php echo $content->name; ?></h2>

                    <span class="sub-tiltle"><?php echo $content->sub_title; ?></span>
                    <?php echo $content->long_description; ?>

                <?php } ?>
            </div>
        </div>
    </div>
</section>