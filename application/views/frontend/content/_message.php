<section class="about message-wraper">
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <div class="about-content-wrap">
                    <?php foreach ($content_data as $content) { ?>
                        <div class="center">
                            <?php $this->load->view('frontend/include/headings', array('headingData' => array(array('title' => $content->name)))) ?>
                        </div>
                        <?php echo $content->long_description; ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>