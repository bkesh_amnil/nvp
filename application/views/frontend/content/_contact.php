<script src="https://maps.googleapis.com/maps/api/js"></script>

<div class="container">
    <div class="center">
        <?php $this->load->view('frontend/include/headings', array('headingData' => $contactContents)) ?>
    </div>

    <div class="contact-wrap">
        <div id="map-canvas"></div>
        
        <div class="row">
            <div class="col s12 m6 l6">
                <div style="display: none;" class="processing">
                    <div class="alert alert-info">
                        Processing ....
                        <div class="progress">
                            <div class="indeterminate"></div>
                        </div>
                    </div>
                </div>

                <div class="message-holder" style="display: none;">
                    <div class="alert alert-success">
                        <span class="close right"><i class="mdi-content-clear"></i></span>
                        <?php echo(!empty($form_data->success_msg) ? $form_data->success_msg : 'Thank you for contacting us.') ?>
                    </div>
                </div>
                
                <h3>SEND US A Message</h3>

                <form action="<?php echo base_url('contact/form') ?>" id="contact-form" method="post">
                    <div class="row">
                        <div class="input-field col s12">
                            <i class="mdi-social-person prefix"></i>
                            <input id="full-name" name="full-name" type="text" class="validate" placeholder="Your full name*">
                        </div>
                        <div class="input-field col s12">
                            <i class="mdi-communication-email prefix"></i>
                            <input id="contactEmail" name="email" type="email" class="validate" placeholder="Your email address*">
                        </div>
                        <div class="input-field col s12">
                            <i class="mdi-editor-insert-comment prefix"></i>
                            <textarea id="contact-msg" name="contact-msg" class="materialize-textarea" placeholder="Your Message*"></textarea>
                        </div>
                        <div class="col l12">
                            <div class="btn-large waves-effect waves-light btn-content right btn-form contact-btn-send">
                                <input type="submit" value="Send">
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col s12 m6 l6">
                <h3>Contact Information</h3>

                <div class="contact-infos">
                    <span>Address</span>
                    <?php echo strip_tags($contacts->address); ?>
                </div>
                <div class="contact-infos">
                    <span>Phone</span>
                    <?php echo $contacts->phone; ?>
                </div>
                <div class="contact-infos">
                    <span>Email</span>
                    <?php echo $contacts->site_email; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="longitude" value="<?php echo $contacts->longitude ?>">
<input type="hidden" id="latitude" value="<?php echo $contacts->latitude ?>">
