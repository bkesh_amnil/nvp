<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml"
      lang="en" xml:lang="en">
<head>
    <title><?php echo $meta_title ?></title>
    <meta charset="UTF-8">
    <link href="<?php echo base_url('assets/img/favicon.png') ?>" rel="shortcut icon"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>

    <meta name="description" content="<?php echo isset($meta_description) ? $meta_description : '' ?>">
    <meta name="keywords" content="<?php echo isset($meta_keywords) ? $meta_keywords : '' ?>">
    <meta name="author" content="<?php echo SITEMAIL ?>">

    <!-- social meta tags -->
    <meta property="og:title" content="<?php echo $ogTitle ?>"/>
    <meta property="og:description" content="<?php echo $ogDescription ?>"/>
    <meta property="og:image" content="<?php echo base_url($ogImage) ?>"/>
    <meta property="og:url" content="<?php echo current_url() ?>"/>
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:site" content="<?php echo base_url() ?>"/>
    <meta name="twitter:creator" content=""/>
    <meta name="twitter:title" content="<?php echo $twitterTitle ?>"/>
    <meta name="twitter:description" content="<?php echo $twitterDescription ?>"/>
    <meta name="twitter:image" content="<?php echo $twitterImage ?>"/>

    <!-- CSS -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/materialize.css') ?>" type="text/css" rel="stylesheet" media="all"/>
    <link href="<?php echo base_url('assets/js/vendor/owl-carasouel/owl.carousel.css') ?>" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo base_url('assets/js/vendor/owl-carasouel/owl.theme.css') ?>" rel="stylesheet"
          type="text/css"/>

    <!-- loading custom css -->
    <?php if (isset($addCss) && !empty($addCss)) {
        foreach ($addCss as $css) { ?>
            <link href="<?php echo base_url($css) ?>" rel="stylesheet" type="text/css"/>
        <?php }
    } ?>

    <!-- loading main css -->
    <link href="<?php echo base_url('assets/css/main.css') ?>" rel="stylesheet" type="text/css"/>
    <script>
        setTimeout(function () {
            document.body.className += " has-loader";
            document.getElementById('page-loader').style.display = 'none';
        }, 3000);
    </script>
    <?php //if (explode('/', $body)[1] == 'profile') { ?>
        <!-- share this plugin -->
        <?php if (segment(1) == 'whats-happening' || (segment(1) == 'news' && segment(2) != '') || (segment(1) == 'members' && segment(2) == 'profile')) { ?>
            <!--<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
            <script type="text/javascript">var switchTo5x = true;</script>
            <script type="text/javascript">
                stLight.options({
                    publisher: "dd6398cc-aebe-4384-a264-12951b46ecd4",
                    doNotHash: true,
                    doNotCopy: true,
                    hashAddressBar: false,
                    onhover: false,
                    shorten: false
                });
            </script>-->
            <!-- Go to www.addthis.com/dashboard to customize your tools -->
            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56b1a1a59fb1fc6d"></script>
            <script type="text/javascript">
                if (typeof addthis_config !== "undefined") {
                    addthis_config.ui_click = true
                } else {
                    var addthis_config = {
                        ui_click: true
                    };
                }
            </script>
            <?php //}
    } ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-75608859-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<input type="hidden" id="base-url" value="<?php echo base_url(); ?>">
<?php
$viewSegments = explode('/', $body);
$isDetailPage = false;
if (end($viewSegments) == '_detail' || end($viewSegments) == '_default') {
    $isDetailPage = true;
}
?>
<body ng-app="nvpApp" class="<?php echo ($isDetailPage) ? 'pattern-bg' : '' ?> has-loader">
<div id="page-loader" class="loader">
    <span>Loading...</span>

    <div class="loader-inner ball-clip-rotate-pulse">
        <div></div>
        <div></div>
    </div>
</div>
<?php
$isHomePage = true;
if (segment(1) != '') {
    $isHomePage = false;
}
?>
<header <?php echo (!$isHomePage) ? "class='inner-header'" : "" ?>>
    <section class="top-head-content">
        <!-- top nav -->
        <?php $this->load->view('frontend/include/header') ?>
        <!-- home page banner -->
        <?php
        if ($isHomePage) {
            $this->load->view('frontend/include/banner');
        }
        ?>
    </section>

    <?php $this->load->view('frontend/include/navigation') ?>
</header>

<?php if (!$isHomePage && !$isDetailPage) { ?>
<section class="inner-page-wrap"><?php } ?>
    <?php $this->load->view($body) ?>
    <?php if (!$isHomePage && !$isDetailPage) { ?></section><?php } ?>

<?php $this->load->view('frontend/include/footer') ?>

<!--  Scripts-->
<script src="<?php echo base_url('assets/js/jquery-1.11.3.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/materialize.js') ?>"></script>
<script src="<?php echo base_url('assets/js/init.js') ?>"></script>
<script src="<?php echo base_url('assets/js/vendor/owl-carasouel/owl.carousel.min.js') ?>"
        type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/owl-custom.js') ?>" type="text/javascript"></script>
<?php if (!$isHomePage) { ?>
    <script src="<?php echo base_url('assets/js/inner-page.js') ?>" type="text/javascript"></script>
<?php } ?>
<!-- loading custom js -->
<?php if (isset($addJs) && !empty($addJs)) {
    foreach ($addJs as $js) { ?>
        <script src="<?php echo base_url($js) ?>" type="text/javascript"></script>
    <?php }
} ?>
<div id="notificationModal" class="modal">
    <div class="modal-content">
        <h4 id="notification-title"></h4>
        <div id="notificationmessage"></div>
    </div>
    <div class="modal-footer">
        <a href="#" class="waves-effect waves-red btn-flat" onclick="$('#notificationModal').closeModal(); return false;">OK</a>
    </div>
</div>
<!-- loading main js -->
<script src="<?php echo base_url('assets/js/main.js') ?>" type="text/javascript"></script>
<script>
    $('.notification_count').on('click', function() {
        $('.notification_count .mdi-social-notifications-none span').html('0');
        $.post('<?php echo base_url("home/notificationcookie") ?>');
    });
    <?php if(get_userdata('active_user')) { ?>
        $(document).ready(function() {
            $.post('<?php echo base_url('home/change_picture') ?>');
        });
    <?php } ?>
</script>
</body>
</html>
