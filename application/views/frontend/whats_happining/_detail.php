<div class="detail-banner-img news-title-wrap">
    <!--<img class="img-responsive" src="<?php //echo base_url($newsDetail->image) ?>" alt="<?php //echo $newsDetail->name ?>"/>-->
    <h3><?php echo $newsDetail->name ?></h3>
    <span class="news-date">Posted at <?php echo date('jS F, Y', strtotime($newsDetail->publish_date)) ?></span>
</div>
<section class="detail-wrap">
    <div class="container">
        <div class="details">
            <a href="<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : current_url() ?>" class="mdi-content-clear right card__btn-close">
            </a>
            <?php echo $newsDetail->long_description ?>

            <?php if($contentGallery) { ?>
                <div class="card__copy">
                    <div class="row">
                        <?php foreach($contentGallery as $image) { ?>
                            <div class="col s3">
                                <a rel="<?php echo $newsDetail->id ?>"
                                   href="<?php echo base_url() . $image->media ?>"
                                   class="content-gallery-thumb">
                                    <img class="responsive-img" src="<?php echo image_thumb($image->media, 270, 180, '', true) ?>" alt="<?php echo $image->title ?>"/>
                                    <h5>
                                        <?php echo $image->title ?>
                                    </h5>
                                    <span>
                                        <?php echo $image->caption ?>
                                    </span>
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
            <div class="detail-social-link-wrap">
                <h6>Share On</h6>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_sharing_toolbox"></div>
                <!--<span class='st_sharethis' displayText='Share'></span>-->
            </div>
        </div>
    </div>
</section>