<div class="container">
    <div class="center">
        <?php $this->load->view('frontend/include/headings', array('headingData' => $projectContent)) ?>
        <div class="row">
            <?php foreach($allProjects as $project) { ?>
                <div class="col s12 m12 l12">
                    <a href="<?php echo base_url('projects/'.$project->slug) ?>">
                        <div class="projects card-panel">
                            <h3 class="content-title"><?php echo $project->name ?></h3>
                            <div class="row valign-wrapper">
                                <div class="col s12 m3 l2">
                                    <div class="logo-holder">
                                        <img src="<?php echo base_url($project->logo) ?>" alt="<?php echo $project->name ?>"/>
                                    </div>
                                </div>
                                    <div class="col s12 m9 l10">
                                        <div class="flat-text">
                                        <?php echo $project->description ?>
                                    </div>
                                </div>
                            </div>
                            <div class="projects-bg">
                                <img class="responsive-img" src="<?php echo image_thumb($project->image, 1920, 800, '', true) ?>"/>
                            </div>
                        </div>
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>