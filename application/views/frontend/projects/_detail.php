<div class="detail-banner-img projects">
    <div class="logo-holder">
        <img src="<?php echo base_url($projectDetail->logo) ?>" alt="<?php echo $projectDetail->name ?>"/>
    </div>
</div>
<section class="detail-wrap project-detail-wrap">
    <div class="container">
        <div class="details">
            <a href="<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : current_url() ?>" class="mdi-content-clear right card__btn-close">
            </a>
            <h3><?php echo $projectDetail->name ?></h3>
            <?php echo $projectDetail->long_description ?>
        </div>
    </div>
</section>