<footer id="footer">
    <div class="container">
        <div class="row clearfix">
            <?php foreach ($bottomMenu['parents'] as $key => $parents) {
                if ($key == 1) { ?>
                    <div class="col s12 m4 l2">
                        <span class="footer-title">PROJECTS</span>
                        <ul>
                            <?php foreach ($allProjects as $projects) { ?>
                                <li>
                                    <a href="<?php echo base_url('projects/' . $projects->slug) ?>"><?php echo ucwords($projects->name) ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>

                    <div class="col s12 m4 l2">
                        <span class="footer-title">PARTNERS</span>
                        <ul>
                            <?php foreach ($PartnersCategories as $par) { ?>
                                <li>
                                    <a href="<?php echo base_url('partners') ?>"><?php echo ucwords($par->name) ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>

                    <div class="col s12 m4 l1">
                        <span class="footer-title">MEDIA</span>
                        <ul>
                            <li><a href="<?php echo base_url('photo-gallery') ?>">Photos</a></li>
                            <li><a href="<?php echo base_url('video-gallery') ?>">Videos</a></li>
                        </ul>
                    </div>

                    <div class="col s12 m4 l2">
                        <span class="footer-title">MAKE A CHANGE</span>
                        <a href="<?php echo base_url('members/register') ?>"
                           class="btn-large waves-effect waves-light btn-content">Join us</a>
                    </div>
                <?php }
                if ($parents->menu_alias == 'contact' || $parents->menu_alias == 'contacts') { ?>
                    <div class="col s12 m4 l3">
                        <?php 
                        echo strip_tags($parents->content, '<span>'); ?>
                        <!--span class="footer-title">Contact</span-->
                        <?php //echo strip_tags($contacts->address, '<span>'); ?>
                    </div>
                <?php } else { ?>
                    <div class="col s12 m4 l2">
                        <span class="footer-title"><?php echo strtoupper($parents->menu_title); ?></span>
                        <ul>
                            <?php if (array_key_exists($parents->id, $bottomMenu['children'])) { ?>
                                <?php foreach ($bottomMenu['children'] as $key => $childern) {
                                    if ($key == $parents->id) {
                                        foreach ($childern as $items) {
                                            if ($items->menu_alias == 'home') {
                                                $link = base_url();
                                            } else {
                                                $link = base_url($items->menu_alias);
                                            } ?>
                                            <li>
                                                <a href="<?php echo $link; ?>"><?php echo ucwords($items->menu_title) ?></a>
                                            </li>
                                        <?php }
                                    }
                                }
                            } ?>
                        </ul>
                    </div>
                <?php }
            } ?>
        </div>
        <div class="center copyright clearfix">
            &copy; Copyright <?php echo date('Y') ?> | National Volunteering Program. Developed at <a href="http://candidservice.com"
                                                                                   target="_blank">Candid Services</a>.
        </div>
    </div>
</footer>

<a class="cd-top scrollTop" href="#0">
    <span>
        <span class="part1"></span>
        <span class="part2"></span>
        <span class="part3"></span>
        <span class="part4"></span>
    </span>
</a>