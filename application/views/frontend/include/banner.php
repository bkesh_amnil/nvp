<div id="mainBanner" class="owl-carousel">
    <?php foreach($homeBanner as $banner) { ?>
        <div class="banner-contents">
            <div class="banner-desc">
                <div class="logo-holder">
                    <img class="responsive-img" src="
                    <?php echo base_url($banner->secondary_image) ?>" alt="<?php echo $banner->title ?>"/>
                </div>
                <h2><?php echo $banner->title ?></h2>
                <div class="flat-text">
                    <p>
                        <?php echo strip_tags($banner->description) ?>
                    </p>
                </div>
                <?php if($banner->link != '') { ?>
                <a href="<?php echo $banner->link ?>" target="_blank" class="btn-large waves-effect waves-light btn-content">
                    Know More
                </a>
                <?php } ?>
            </div>
            <img src="
            <?php echo base_url($banner->image) ?>" alt="<?php echo $banner->title ?>" />
        </div>
    <?php } ?>
</div>

<div class="overlay-top"></div>