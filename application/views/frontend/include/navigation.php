<nav class="white main-nav" role="navigation">
    <?php if(get_userdata('active_user')) { ?>
    <div class="mobile-view-notification">
        <div class="user-control-wrap">
            <div class="notification">
                <?php if(!empty($allUserNotifications)) { ?>
                    <ul id="mobile-notification-content" class="dropdown-content">
                        <?php foreach($allUserNotifications as $notification) {
                            if(!empty($notification->slug)) {
                                $notificationUrl = base_url('notification/'.$notification->slug);
                            } else {
                                $notificationUrl = '#';
                            } ?>
                            <li><a href="<?php echo $notificationUrl ?>"><?php echo $notification->title ?></a></li>
                        <?php } ?>
                    </ul>
                    <?php
                    $session_notification_count = get_userdata(get_userdata('userType').get_userdata('active_user'));
                    ?>
                    <a class="btn dropdown-button btn-notification notification_count" href="#!" data-activates="mobile-notification-content"><i class="mdi-social-notifications-none"><span><?php echo $session_notification_count ? $session_notification_count : 0 ?></span></i></a>
                <?php } else{ ?>
                    <a class="btn dropdown-button btn-notification" href="#" data-activates="mobile-notification-content"><i class="mdi-social-notifications-none"><span>0</span></i></a>
                <?php  }?>
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="nav-wrapper container">
        <a id="logo-container" href="<?php echo base_url() ?>" class="brand-logo">
            <span></span><img src="<?php echo base_url($globalConfig->site_logo) ?>" alt="<?php echo SITENAME ?>"/>
        </a>
        <ul class="right hide-on-med-and-down">
            <li><a href="<?php echo base_url() ?>">HOME</a></li>
            <?php if ($mainMenu) {
                foreach ($mainMenu['parents'] as $menuItem) {
                    $class = '';
                    if ($this->uri->segment(1) == $menuItem->menu_alias) {
                        $class = 'active';
                    }
                    foreach ($mainMenu['children'] as $parentId => $children) {
                        if ($parentId == $menuItem->id) {
                            foreach ($children as $child) {
                                if ($this->uri->segment(1) == $child->menu_alias) {
                                    $class = 'active';
                                }
                            }
                        }
                    }
                    if ($menuItem->menu_alias == 'nepali-ma') {
                        $menu_link = str_replace('\\', '/', $menuItem->link); ?>
                        <li class="nepali-ma">
                            <a href="<?php echo base_url($menu_link) ?>" target="_blank">
                                <img src="<?php echo base_url('assets/img/nepali-ma.png') ?>" class="nepali-ma-active"/>
                                <img src="<?php echo base_url('assets/img/nepali-ma-hover.png') ?>"
                                     class="nepali-ma-hover"/>
                            </a>
                        </li>
                    <?php } else { ?>
                        <li class="<?php echo $class ?>">
                            <?php
                            $parentClass = $parentData = $parentIcon = '';
                            $link = base_url($menuItem->menu_alias);
                            if (array_key_exists($menuItem->id, $mainMenu['children'])) {
                                $parentClass = 'class="dropdown main-dropdown"';
                                $parentData = 'data-activates="main-dropdown-' . $menuItem->id . '"';
                                $link = '#';
                                $parentIcon = '<i class="mdi-navigation-arrow-drop-down right"></i>';
                            }
                            ?>
                            <a <?php echo $parentClass . ' ' . $parentData ?> href="<?php echo $link ?>">
                                <?php
                                echo $menuItem->menu_title;
                                echo (isset($parentIcon)) ? $parentIcon : ''
                                ?>
                            </a>
                        </li>
                    <?php }
                }
            } ?>
        </ul>

        <ul id="nav-mobile" class="side-nav nvp-sidenav">
            <li class="mobile-login-start">
                <?php if(!get_userdata('active_user')) { ?>
                    <a class="header-login" href="<?php echo base_url('members') ?>">Login</a>
                    <a class="header-get-started" href="<?php echo base_url('members/register') ?>">Get started</a>
                <?php } else { ?>
                    <div class="user-control">
                        <a href="<?php echo base_url('members/profile') ?>">
                            <?php
                            $userProfilePic = get_userdata('profilePicture');
                            if($userProfilePic) { ?>
                                <img alt="" src="<?php echo $userProfilePic ?>" class="responsive-img">
                            <?php } else { ?>
                                <img alt="" src="<?php echo base_url('assets/img/user-default.png') ?>" class="responsive-img">
                            <?php } ?>
                        </a>
                        <ul id="mobile-user-option" class="dropdown-content">
                            <li><a href="<?php echo base_url('members/profile') ?>">Dashboard</a></li>
                            <li><a href="<?php echo base_url('members/profile/edit') ?>">My Profile</a></li>
                            <li><a href="<?php echo base_url('members/profile/logout') ?>">Logout</a></li>
                        </ul>
                        <a class="btn dropdown-button btn-user-option" href="#!" data-activates="mobile-user-option">Welcome, <?php echo get_userdata('userName') ?><i class="mdi-navigation-arrow-drop-down right"></i></a>
                    </div>
                <?php } ?>
            </li>
            <?php if ($mainMenu) {?>
                <li><a href="<?php echo base_url() ?>">HOME</a></li>
               <?php foreach ($mainMenu['parents'] as $menuItem) {
                    $class = '';
                    if ($this->uri->segment(1) == $menuItem->menu_alias) {
                        $class = 'active';
                    }
                    foreach ($mainMenu['children'] as $parentId => $children) {
                        if ($parentId == $menuItem->id) {
                            foreach ($children as $child) {
                                if ($this->uri->segment(1) == $child->menu_alias) {
                                    $class = 'active';
                                }
                            }
                        }
                    }
                    if ($menuItem->menu_alias == 'nepali-ma') {
                        $menu_link = str_replace('\\', '/', $menuItem->link); ?>
                        <li class="nepali-ma">
                            <a href="<?php echo base_url($menu_link) ?>" target="_blank">
                                <img src="<?php echo base_url('assets/img/nepali-ma.png') ?>" class="nepali-ma-active"/>
                                <img src="<?php echo base_url('assets/img/nepali-ma-hover.png') ?>"
                                     class="nepali-ma-hover"/>
                            </a>
                        </li>
                    <?php } else { ?>
                        <li class="<?php echo $class ?>">
                            <?php
                            $parentClass = $parentData = $parentIcon = '';
                            $link = base_url($menuItem->menu_alias);
                            if (array_key_exists($menuItem->id, $mainMenu['children'])) {
                                $parentClass = 'class="dropdown mobile-drop"';
                                $parentData = 'data-activates="mobile-dropdown-' . $menuItem->id . '"';
                                $parentIcon = '<i class="mdi-navigation-arrow-drop-down right"></i>';
                                $link = '#';
                            }
                            ?>
                            <a <?php echo $parentClass . ' ' . $parentData ?> href="<?php echo $link ?>">
                                <?php
                                echo $menuItem->menu_title;
                                echo (isset($parentIcon)) ? $parentIcon : '';
                                ?>
                            </a>
                        </li>
                    <?php }
                } ?>
            <?php } ?>
            <li class="side-nav-social clearfix">
                <span class="side-nav-ceontent-title">Follow Us</span>
                <?php if(!empty($twitter_link)) { ?>
                    <a target="_blank" class="twitter" href="<?php echo $twitter_link ?>"></a>
                <?php }  if(!empty($facebook_link)) { ?>
                    <a target="_blank" class="facebook" href="<?php echo $facebook_link ?>"></a>
                <?php }  if(!empty($youtube_link)) {?>
                <a target="_blank" class="youtube" href="<?php echo $youtube_link ?>"></a>
                <?php } if(!empty($gplus_link)) {?>
                    <a target="_blank" class="youtube" href="<?php echo $gplus_link ?>"></a>
                <?php } if(!empty($skype_link)) { ?>
                    <a target="_blank" class="youtube" href="<?php echo $skype_link ?>"></a>
                <?php }?>
            </li>
            <li class="side-nav-store">
                <span class="side-nav-ceontent-title">Get APP</span>
                <a class="playstore" href="#"></a>
                <a class="appstore" href="#"></a>
            </li>
        </ul>

        <?php if ($mainMenu['children']) {
            foreach ($mainMenu['children'] as $parentId => $menuItem) { ?>
                <ul id="main-dropdown-<?php echo $parentId ?>" class="dropdown-content">
                    <?php foreach ($mainMenu['children'][$parentId] as $childItem) {
                    if ($this->uri->segment(1) == $childItem->menu_alias) {
                        $class = 'active';
                    } else {
                        $class = '';
                    } ?><!--Dropdown content-->
                    <li class="<?php echo $class; ?>">
                        <a href="<?php echo base_url($childItem->menu_alias) ?>">
                            <?php echo $childItem->menu_title ?>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <?php } ?>
                </ul>

                <!--Mobile Dropdown content-->
                <ul id="mobile-dropdown-<?php echo $parentId ?>" class="dropdown-content">
                    <?php foreach ($mainMenu['children'][$parentId] as $childItem) {
                    if ($this->uri->segment(1) == $childItem->menu_alias) {
                        $class = 'active';
                    } else {
                        $class = '';
                    } ?><!--Dropdown content-->
                    <li class="<?php echo $class ?>">
                        <a href="<?php echo base_url($childItem->menu_alias) ?>">
                            <?php echo $childItem->menu_title ?>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <?php } ?>
                </ul>
            <?php } ?>
        <?php } ?>

        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="mdi-image-dehaze"></i></a>
    </div>
</nav>