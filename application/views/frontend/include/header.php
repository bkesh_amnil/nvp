<div class="container">
    <div class="right-align social-login">
        <a target="_blank" class="twitter" href="<?php echo $twitter_link ?>"></a>
        <a target="_blank" class="facebook" href="<?php echo $facebook_link ?>"></a>
        <a target="_blank" class="youtube" href="<?php echo $youtube_link ?>"></a>
        <?php if (!get_userdata('active_user')) { ?>
            <a class="header-login" href="<?php echo base_url('members') ?>">Login</a>
            <a class="header-get-started" href="<?php echo base_url('members/register') ?>">Get started</a>
        <?php } else { ?>
            <div class="user-control">
                <a href="<?php echo base_url('members/profile') ?>">
                    <?php
                    $userProfilePic = get_userdata('profilePicture');
                    if ($userProfilePic) { ?>
                        <img alt="" src="<?php echo $userProfilePic ?>" class="responsive-img">
                    <?php } else { ?>
                        <img alt="" src="<?php echo base_url('assets/img/user-default.png') ?>" class="responsive-img">
                    <?php } ?>
                </a>
                <ul id="user-option" class="dropdown-content">
                    <li><a href="<?php echo base_url('members/profile') ?>">Dashboard</a></li>
                    <li><a href="<?php echo base_url('members/profile/edit') ?>">My Profile</a></li>
                    <li><a href="<?php echo base_url('members/profile/logout') ?>">Logout</a></li>
                </ul>
                <a class="btn dropdown-button btn-user-option" href="#!"
                   data-activates="user-option">Welcome, <?php echo get_userdata('userName') ?><i
                        class="mdi-navigation-arrow-drop-down right"></i></a>
            </div>
            <div class="user-control-wrap">
                <div class="notification">
                    <?php if (!empty($allUserNotifications)) { ?>
                        <ul id="notification-content" class="dropdown-content">
                            <?php foreach ($allUserNotifications as $notification) { ?>
                                <li>
                                    <?php if (empty($notification->slug)) { ?>
                                        <a href="#" onclick="showNotificationModal('<?php echo strip_tags(trim($notification->short_description))?>','notificationModal','<?php echo strip_tags(trim($notification->title))?>'); return false;"><?php echo $notification->title ?></a>
                                    <?php } else { ?>
                                        <a href="<?php echo base_url('notification/' . $notification->slug) ?>"><?php echo $notification->title ?></a>
                                    <?php } ?>
                                </li>
                            <?php } ?>
                        </ul>
                        <?php
                        $session_notification_count = get_userdata(get_userdata('userType').get_userdata('active_user'));
                        ?>
                        <a class="btn dropdown-button btn-notification notification_count" href="#!" data-activates="notification-content"><i
                                class="mdi-social-notifications-none"><span><?php echo $session_notification_count ? $session_notification_count : 0 ?></span></i></a>
                    <?php } else { ?>
                        <a class="btn dropdown-button btn-notification" href="#"
                           data-activates="notification-content"><i
                                class="mdi-social-notifications-none"><span>0</span></i></a>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<script>
    function showNotificationModal(but, modal, title) {
        console.log('here');
        $('#notification-title').html(title);
        $('#notificationmessage').html(but);
        $('#' + modal).openModal();
    }
</script>