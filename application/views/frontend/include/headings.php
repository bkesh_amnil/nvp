<?php foreach ($headingData as $data) { ?>
    <h2 class="page-title"><?php echo $data['title'] ?></h2>
    <?php $this->load->view('frontend/include/gif') ?>
    <span class="content-sub-head flat-text">
        <?php if (!empty($data['description'])) {
            echo $data['description'];
        } ?>
    </span>
<?php } ?>