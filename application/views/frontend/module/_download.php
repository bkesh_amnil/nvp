<div class="container">
    <div class="center">
        <?php $this->load->view('frontend/include/headings', array('headingData' => $downloadContents)) ?>
    </div>
    <div class="pagination-container">
    <div class="download-content center">
        <div class="download-filter-wrap">
            <div class="input-field ">
                <form>
                    <input type="hidden" id="hidden_page_count" value="6">
                    <select id="download-category">
                        <option value="" disabled selected>Filter By Category</option>
                        <option value="" <?php echo (segment(2) == '') ? 'selected' : '' ?>>All</option>
                        <?php foreach ($category as $key => $cat) {
                            if (isset($categorySlug) && $categorySlug == $key) {
                                $selected = 'selected';
                            } else {
                                $selected = '';
                            } ?>
                            <option
                                value="<?php echo $key ?>" <?php echo isset($selected) ? $selected : ''; ?>><?php echo ucwords($cat); ?></option>
                        <?php } ?>
                    </select>
                    <label>Filter By Category</label>
                </form>
            </div>
        </div>

        <div class="row">
            <?php if($module_data) { ?>
                <ul class="content-pagination">
                    <?php foreach ($module_data as $data) {
                        $type = explode('.', $data->file);
                        if ($type[1] == 'pdf') {
                            $src = base_url() . 'assets/img/icon/icon-pdf.png';
                        } elseif ($type[1] == 'doc' || $type[1] == 'docx') {
                            $src = base_url() . 'assets/img/icon/icon-docx.png';
                        } elseif ($type[1] == 'ppt' || $type[1] == 'pptx') {
                            $src = base_url() . 'assets/img/icon/icon-docx.png';
                        } else {
                            $src = base_url() . 'assets/img/icon/icon-image.png';
                        } ?>
                        <div class="col s12 m4 l4">
                            <div class="downloadables">
                                <div class="card">
                                    <div class="card-image waves-effect waves-block waves-light">
                                        <img class="activator" src="<?php echo $src ?>" alt=""/>
                                    </div>
                                    <div class="card-content">
                                <span class="card-title activator grey-text text-darken-4"><?php echo $data->name ?>
                                    ...<i class="material-icons right">more_vert</i></span>
                                        <?php $date = explode('-', $data->publishDate); ?>
                                        <!--<span class="upload-time">Posted at 18 August, 2015 |06 pm</span>-->
                                <span
                                    class="upload-time">Posted at <?php echo date('d  M, Y', $data->updated_on) . '|' . date(' g:i a', $data->updated_on) ?></span>
                                        <a href="<?php echo base_url() . $data->file; ?>">Download</a>
                                    </div>
                                    <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4"><?php echo $data->name ?><i
                                        class="material-icons right">close</i></span>

                                        <p><?php echo strip_tags($data->description); ?></p>
                                        <a href="<?php echo base_url() . $data->file; ?>">Download</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </ul>
            <?php } else { ?>
                No Downloads
            <?php } ?>
        </div>

    </div>
        <div class="center">
            <div class="page_navigation">
            </div>
        </div>


    </div>
