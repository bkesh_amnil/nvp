<div class="container">
    <div class="center codrops-header">
        <h2 class="page-title"><?php echo $menu_content->menu_title; ?></h2>
        <span class="title-divide"><img src="<?php echo base_url(); ?>assets/img/nvp-titlebar-animation.gif"
                                        alt=""/></span>
        <?php if (!empty($menu_content->description)) { ?>
            <span class="content-sub-head flat-text"><?php echo $menu_content->description ?> </span>
        <?php } ?>
    </div>
    <input type="hidden" id="hidden_page_count" value="8">

    <div class="pagination-container">
        <div class="content">
            <div class="pattern pattern--hidden"></div>

            <div class="wrapper">
                <ul class="content-pagination">
                    <?php foreach ($gallery as $photo_gallery) { ?>
                        <div class="photo-album-wrap">
                            <div class="card">
                                <div class="card__container card__container--closed">
                                    <svg class="card__image" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1920 500"
                                         preserveAspectRatio="xMidYMid slice">
                                        <defs>
                                            <clipPath id="<?php echo $photo_gallery->id ?>">
                                                <!-- r = 992 = hyp = Math.sqrt(960*960+250*250) -->
                                                <circle class="clip" cx="960" cy="250" r="992"></circle>
                                            </clipPath>
                                        </defs>
                                        <image clip-path="url(#<?php echo $photo_gallery->id ?>)" width="1920"
                                               height="500" class="image-gallary-detail"
                                               xlink:href="<?php echo base_url() . $photo_gallery->cover; ?>"></image>
                                    </svg>

                                    <div class="card__content">
                                        <i class="material-icons right card__btn-close">close</i>

                                        <div class="card__caption">
                                            <h2 class="card__title"><?php echo $photo_gallery->name ?></h2>

                                        </div>
                                        <div class="card__copy">
                                            <p><?php echo strip_tags($photo_gallery->short_description); ?></p>

                                            <div class="row">
                                                <?php if (!empty ($photo_gallery->module_gallery_media)) { ?>
                                                    <?php foreach ($photo_gallery->module_gallery_media as $gallery_media) { ?>
                                                        <div class="col l3">
                                                            <a rel="<?php echo $photo_gallery->id ?>"
                                                               href="<?php echo base_url() . $gallery_media->media ?>"
                                                               class="photo-gallery-thumb">
                                                                <img class="responsive-img" width="650"
                                                                     src="<?php echo image_thumb($gallery_media->media, 270, 181, '', true) ?>"
                                                                     alt="<?php echo base_url() . $gallery_media->title ?>"/>
                                                            </a>
                                                        </div>
                                                    <?php }
                                                } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </ul>

            </div>
        </div>
        <div class="center codrops-header">
            <div class="page_navigation">
            </div>
        </div>
    </div>

</div>
