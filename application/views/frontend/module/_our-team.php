<div class="container">
    <div class="center codrops-header">
        <?php $this->load->view('frontend/include/headings', array('headingData' => array(array('title' => $menu_content->menu_title, 'description' => $menu_content->description)))) ?>
    </div>
    <div class="pagination-container">
        <div class="download-content center">
            
            <?php foreach ($module_data as $teamCategory => $teamData) { ?>
            <div class="ourteam-wrap">
                <div class="row">
                        <div class="col l12">
                            <h4><?php echo $teamCategory ?></h4>
                        </div>
                        <?php foreach($teamData as $data) { ?>
                            <div class="col s12 m4 l4">
                                <div class="downloadables teams-wrap">
                                    <div class="card">
                                        <div class="card-image waves-effect waves-block waves-light">
                                            <?php if(file_exists($data->image)) { ?>
                                                <img class="activator"
                                                     src="<?php echo image_thumb($data->image, 700, 597, '', true) ?>"
                                                     alt="<?php echo $data->name ?>"/>
                                            <?php } else { ?>
                                                <img class="activator"
                                                     src="<?php echo image_thumb('assets/img/no-image.jpg', 700, 597, '', true) ?>"
                                                     alt="<?php echo $data->name ?>"/>
                                            <?php } ?>
                                        </div>
                                        <div class="card-content">
                                            <span
                                                class="card-title activator grey-text text-darken-4"><?php echo $data->name ?>
                                                <i class="material-icons right">more_vert</i></span>
                                        </div>
                                        <div class="card-reveal">
                                            <span class="card-title grey-text text-darken-4"><?php echo $data->name ?><i
                                                    class="material-icons right">close</i>
                                            </span>
                                            <p><?php echo strip_tags($data->description); ?></p>
                                            <div class="side-nav-social clearfix">
                                                <?php if($data->twitter || $data->facebook || $data->linkedIn) { ?>
                                                    <h6>Follow Me On</h6>
                                                    <?php if(!empty($data->twitter)) { ?>
                                                        <a target="_blank" class="team-social-twitter" href="<?php echo prep_url($data->twitter) ?>"></a>
                                                    <?php }  if(!empty($data->facebook)) { ?>
                                                        <a target="_blank" class="team-social-facebook" href="<?php echo prep_url($data->facebook) ?>"></a>
                                                    <?php }  if(!empty($data->linkedIn)) {?>
                                                        <a target="_blank" class="team-social-linkedin" href="<?php echo prep_url($data->linkedIn) ?>"></a>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                </div>
            </div>
            <?php } ?>            
        </div>
    </div>
</div>
