<div class="container">
    <div class="center">
        <h2 class="page-title"><?php echo $menu_content->menu_title; ?></h2>
        <span class="title-divide"><img src="<?php echo base_url(); ?>assets/img/nvp-titlebar-animation.gif"
                                        alt=""/></span>
        <?php if (!empty($menu_content->description)) { ?>
            <span class="content-sub-head flat-text"><?php echo $menu_content->description ?> </span>
        <?php } ?>
    </div>
<?php $count = count($gallery[1]->module_gallery_media); ?>
    <input type="hidden" id="hidden_page_count" value="<?php echo $count ?>">
    <div class="row">
        <?php foreach ($gallery as $video_gallery) {
            if (isset($video_gallery->module_gallery_media) && !empty($video_gallery->module_gallery_media)) {
                foreach ($video_gallery->module_gallery_media as $gallery_media) {
                    $id = getYoutubeVideoId($gallery_media->media) ?>
                    <div class="col s12 m4 l3">
                        <a class="video-thumb" data-type="iframe"
                           href="<?php echo $gallery_media->media; ?>&amp;autoplay=1"
                           title="<?php echo $gallery_media->title ?>">
                            <img src="<?php echo 'http://img.youtube.com/vi/' . $id . '/mqdefault.jpg' ?>" alt=""/>

                            <div class="video-thumb-titles">
                                <h2><?php echo $gallery_media->title ?></h2>
                                <!--<span><?php /*echo $video_gallery->short_description;*/
                                ?></span>-->
                            </div>
                            <span class="video-play-button"></span>
                        </a>
                    </div>
                <?php }
            }
        } ?>
    </div>

    <div class="center codrops-header">
        <ul class="pagination">
            <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
            <li class="active"><a href="#!">1</a></li>
            <li class="waves-effect"><a href="#!">2</a></li>
            <li class="waves-effect"><a href="#!">3</a></li>
            <li class="waves-effect"><a href="#!">4</a></li>
            <li class="waves-effect"><a href="#!">5</a></li>
            <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
        </ul>
    </div>
</div>