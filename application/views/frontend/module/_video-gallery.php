<div class="container">
    <div class="center">
        <h2 class="page-title"><?php echo $menu_content->menu_title; ?></h2>
        <span class="title-divide"><img src="<?php echo base_url(); ?>assets/img/nvp-titlebar-animation.gif"
                                        alt=""/></span>
        <?php if (!empty($menu_content->description)) { ?>
            <span class="content-sub-head flat-text"><?php echo $menu_content->description ?> </span>
        <?php } ?>
    </div>
    <input type="hidden" id="hidden_page_count" value="6">

    <div class="pagination-container">
        <div class="row">
            <ul class="content-pagination">
                <?php foreach ($gallery as $video_gallery) {
                    if (isset($video_gallery->module_gallery_media) && !empty($video_gallery->module_gallery_media)) {
                        if (!empty($video_gallery->module_gallery_media)) {
                            foreach ($video_gallery->module_gallery_media as $gallery_media) {
                                $id = getYoutubeVideoId($gallery_media->media) ?>
                                <div class="col s12 m4 l4 page">
                                    <a class="video-thumb" data-type="iframe"
                                       href="<?php echo $gallery_media->media; ?>&amp;autoplay=1"
                                       title="<?php echo $gallery_media->title ?>"
                                       data-caption="<?php echo $gallery_media->caption; ?>">
                                        <img src="<?php echo 'http://img.youtube.com/vi/' . $id . '/hqdefault.jpg' ?>"
                                             alt=""/>

                                        <div class="video-thumb-titles">
                                            <h2><?php echo $gallery_media->title ?></h2>
                                            <!--<span><?php /*echo $gallery_media->caption;
                                            */ ?></span>-->
                                        </div>
                                        <span class="video-play-button"></span>
                                    </a>
                                </div>
                            <?php }
                        }
                    }
                } ?>
            </ul>
        </div>
        <div class="center codrops-header">
            <div class="page_navigation">
            </div>
        </div>
    </div>
</div>