<section class="who-we">
    <div class="container">
        <div class="center">
            <h2 class="content-title"><?php echo $homeContents[0]->name ?></h2>
            <?php $this->load->view('frontend/include/gif') ?>
            <div class="row">
                <div class="flat-text">
                    <?php echo $homeContents[0]->long_description ?>
                </div>
            </div>
            <a href="<?php echo base_url('about') ?>" class="btn-large waves-effect waves-light btn-content">Know
                More</a>
        </div>
    </div>
</section>

<section class="whats-happening">
    <div class="container">
        <div class="center">
            <h2 class="content-title"><?php echo $homeContents[1]->name ?></h2>
            <?php $this->load->view('frontend/include/gif') ?>
            <span class="content-sub-head">
                <?php echo strip_tags($homeContents[1]->long_description) ?>
            </span>

            <div class="row">
                <?php foreach ($happiningContents as $key => $happiningContent) {
                    if ($key < 6) {
                        if ($key < 3) {
                            $class = 'odd-contents posts clearfix';
                        } else {
                            $class = 'even-contents posts clearfix';
                        } ?>
                        <div class="col s6 m4 l4">
                            <a class="<?php echo $class ?>" href="<?php echo base_url('whats-happening/' . $happiningContent->slug) ?>">
                                <span class="img-happening">
                                    <img src="<?php echo image_thumb($happiningContent->cover_image,200,300,'',true)?>" alt=""/>
                                </span>

                                <h3><?php echo $happiningContent->name ?></h3>
                            </a>
                        </div>
                    <?php }
                } ?>
            </div>
        </div>
    </div>
</section>

<section class="parallax-container valign-wrapper create-impact">
    <div class="container">
        <div class="center">
            <h1><?php echo strip_tags($homeContents[2]->name) ?></h1>
            <?php $this->load->view('frontend/include/gif') ?>
            <div class="row">
                <div class="flat-text">
                    <?php echo $homeContents[2]->long_description ?>
                </div>
            </div>
            <a href="<?php echo base_url('members'); ?>" class="btn-large waves-effect waves-light btn-content">Join
                Us</a>
        </div>
    </div>
    <div class="parallax"><img src="<?php echo base_url($homeContents[2]->cover_image) ?>" alt=""></div>
</section>

<section class="news-events">
    <div class="container">
        <div class="center">
            <h2 class="content-title"><?php echo $homeContents[3]->name ?></h2>
            <?php $this->load->view('frontend/include/gif') ?>
            <span class="content-sub-head">
                <?php echo $homeContents[3]->long_description ?>
            </span>
        </div>
        <div class="row">
            <?php if(is_array($homeNews)){ 
            foreach ($homeNews as $key => $news) {
                if ($key < 4) {
                    ?>
                    <div class="col s6 m6 l3">
                        <a class="news-events-wrap" href="<?php echo base_url('news/' . $news->slug) ?>">
                            <div class="news-events-img">
                                <img class="responsive-img" src="
                            <?php echo image_thumb($news->cover_image, 410, 232, '', true) ?>" alt=""/>
                            </div>
                            <h2><?php echo substr($news->name, 0, 48);
                                echo (strlen($news->name) > 48) ? '...' : '' ?></h2>
                            <span><?php echo date('F d, Y', strtotime($news->publish_date)) ?></span>
                            <?php echo $news->short_description ?>
                        </a>
                    </div>
                <?php } 
                }
            } ?>
        </div>

        <div class="center">
            <a href="<?php echo base_url('news') ?>" class="btn-large waves-effect waves-light btn-content">Know
                More</a>
        </div>
    </div>
</section>

<section class="parallax-container valign-wrapper Services">
    <div class="container">
        <div class="row center" id="impact-carousel">
            <?php
            $impactColor = '#000';
            foreach ($homeImpacts as $impact) {
                $impactColor = ($impact->color != '') ? $impact->color : $impactColor;
                ?>
                <div class="col s12 m12 l12">
                    <div class="volunteer-wrap" style="color: <?php echo $impactColor ?>">
                        <img class="responsive-img" src="
                        <?php echo image_thumb($impact->icon, '200', '200', '', true) ?>"
                             alt="<?php echo $impact->name ?>"/>
                        <span class="counters impact-counters" data-end-value="<?php echo $impact->statistic ?>"></span>
                        <span><?php echo $impact->name ?></span>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="parallax"><img src="<?php echo base_url('assets/img/hand-bg.jpg') ?>" alt=""></div>
</section>

<section class="talking-about">
    <div class="container">
        <div class="center">
            <h2 class="content-title"><?php echo $homeContents[4]->name ?></h2>
            <?php $this->load->view('frontend/include/gif') ?>
            <span class="content-sub-head">
                <?php echo strip_tags($homeContents[4]->long_description) ?>
            </span>

            <form class="row">
                <div class="comments-wrap">
                    <div id="comments" class="owl-carousel">
                        <?php
                        $count = 0;
                        foreach ($homeTestimonials as $key => $homeTestimonial) { ?>
                            <?php
                            if ($count == 0 || $count == 2) { ?>
                                <div class="col s12 m12 l12"><?php }
                            $count++; ?>
                            <div class="comments">
                                <div class="row valign-wrapper">
                                    <div class="col s3">
                                        <?php
                                        if(file_exists($homeTestimonial->image)) {
                                            $imagePath = $homeTestimonial->image;
                                        } else {
                                            $imagePath = 'assets/img/icon/icon-user-default.png';
                                        }
                                        ?>
                                        <img src="<?php echo image_thumb($imagePath, 120, 120, '', true) ?>"
                                             alt="<?php echo $homeTestimonial->name ?>"
                                             class="circle responsive-img">
                                    </div>
                                    <div class="col s9">
                                        <div class="comment-user-name">
                                            <?php echo $homeTestimonial->name ?>
                                        </div>
                                        <span><?php echo date('F d, Y', $homeTestimonial->date) ?></span>

                                        <div class="black-text">
                                            <?php echo mySubStr(strip_tags($homeTestimonial->message), 207) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php if ($count == 2) {
                                $count = 0; ?>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
                <!-- Modal Trigger -->
                <a href="#modal1" class="btn-large waves-effect waves-light btn-content modal-trigger">Write Review</a>
            </form>
        </div>
    </div>
</section>


<section class="our-partners">
    <div class="container">
        <div class="row center">
            <h2 class="content-title"><?php echo $homeContents[5]->name ?></h2>
            <?php $this->load->view('frontend/include/gif') ?>
            <div class="flat-text">
                <?php echo $homeContents[5]->long_description ?>
            </div>
        </div>

        <div class="our-partners-tab">
            <div class="col s12">
                <ul class="tabs">
                    <?php foreach ($homePartners as $partnerCategory => $homePartner) { ?>
                        <li class="tab col s3">
                            <a class="active" href="#<?php echo $partnerCategory ?>">
                                <?php echo $homePartner[0]->categoryName ?>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <?php foreach ($homePartners as $partnerCategory => $homePartner) { ?>
                <div id="<?php echo $partnerCategory ?>" class="col s12">
                    <div class="logo-wrap">
                        <a href="<?php echo base_url('partners#'.$partnerCategory) ?>">
                            <?php foreach ($homePartner as $partnerRow) { ?>
                                <img src="<?php echo base_url($partnerRow->image) ?>"
                                     alt="<?php echo $partnerRow->name ?>"/>
                            <?php } ?>
                        </a>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>

<!-- Modal Structure -->
<div id="modal1" class="modal contact-wrap">
    <div class="modal-header">
        <a class="btn-floating btn-large waves-effect waves-light btn-close modal-close right"><i
                class="mdi-content-clear"></i></a>

        <div class="message-holder" style="display: none;">
            <span class="close"><i class="mdi-content-clear"></i></span>
            <span class="message"></span>
        </div>
    </div>
    <form class="submit-review" id="review-submit" action="<?php echo base_url('home/review') ?>"
          method="post" enctype="multipart/form-data">
        <div class="modal-content">
            <h4>Your Review</h4>

            <div class="row">
                <div class="input-field col s12">
                    <i class="mdi-social-person prefix"></i>
                    <input id="full-name" name="name" type="text" placeholder="Full Name*">
                </div>
                <div class="input-field col s12">
                    <i class="mdi-editor-insert-comment prefix"></i>
                    <textarea id="contact-msg" name="message" class="materialize-textarea"
                              placeholder="Write Your Message Here*"></textarea>
                </div>
                <div class="file-field input-field col s12">
                    <div class="btn btn-content">
                        <span>Picture*</span>
                        <input type="file" name="image" id="image">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path " type="text">
                    </div>
                </div>
            </div>
        </div>
        <div class="col l12">
            <div class="btn-large waves-effect waves-light btn-content right btn-form send_review">
                <input type="submit" value="Send Review">
            </div>
        </div>
    </form>
</div>