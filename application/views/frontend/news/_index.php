<div class="container">
    <div class="center codrops-header">
        <?php $this->load->view('frontend/include/headings', array('headingData' => $newsContent)) ?>
    </div>
    <div class="news-wrap">
        <div class="row">
            <div id="grid" data-columns>
                <?php foreach ($allNews as $key => $news) { ?>
                    <div class="news-contents-wrap col l12">
                        <?php if (!empty($news->cover_image)) { ?>
                            <div class="img-news-wrap">
                                <img class="responsive-img"
                                     src="<?php echo image_thumb($news->cover_image, 410, 232, '', true) ?>"
                                     alt="<?php echo $news->name ?>"/>
                            </div>
                        <?php } ?>
                        <div class="news-short-desc">
                            <h3><?php echo $news->name ?></h3>
                            <?php if($news->publish_date != '0000-00-00') { ?>
                            <span>Posted at <?php echo date('F d, Y', strtotime($news->publish_date)) ?></span>
                            <?php } ?>
                            <p>
                                <?php echo substr($news->short_description, 0, 195); echo (strlen($news->short_description) > 195) ? '.....' : '' ?>
                            </p>
                        </div>
                        <a class="link-readmore" href="<?php echo base_url('news/' . $news->slug) ?>">Read
                            more</a>
                    </div>
                    <?php
                } ?>
            </div>
        </div>
        <?php echo $newsPagination ?>
        <div id="load" style="display:none">
            Loading.....
        </div>
        <!-- <div id="complete" style="display:none">
            No more news!!
        </div> -->
    </div>
</div>