<div class="container">
    <div class="center codrops-header">
        <?php $this->load->view('frontend/include/headings', array('headingData' => $newsContent)) ?>
    </div>
    <div class="news-wrap">
        <div class="row">
            <?php if ($allNews) { ?>
                <div id="grid" data-columns>
                    <?php foreach ($allNews as $key => $news) { ?>
                        <div class="news-contents-wrap col l12">
                            <?php if (!empty($news->cover_image)) { ?>
                                <div class="img-news-wrap">
                                    <img class="responsive-img"
                                         src="<?php echo image_thumb($news->cover_image, 392, 220, '', true) ?>"
                                         alt="<?php echo $news->name ?>"/>
                                </div>
                            <?php } ?>
                            <div class="news-short-desc">
                                <h3><?php echo $news->name ?></h3>
                                <span>Posted at 18 August, 2015 |06 pm</span>

                                <p>
                                    <?php echo substr($news->short_description, 0, 195) . '.....' ?>
                                </p>
                            </div>
                            <a class="link-readmore" href="<?php echo base_url('news/' . $news->slug) ?>">Read
                                more</a>
                        </div>
                        <?php
                    } ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>