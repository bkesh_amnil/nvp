<div class="container">
    <div class="center codrops-header">
        <?php $this->load->view('frontend/include/headings', array('headingData' => $newsContent)) ?>
    </div>
    <div class="pagination-container">
    <div class="news-wrap">
        <div class="row">
            <input type="hidden" id="hidden_page_count" value="6">
            <?php if ($allNews) { ?>
                    <ul class="content-pagination">
                        <?php $i = 0;
                        foreach ($allNews as $key => $news) { ?>
                            <div class="col s12 m4 l4">
                                <div class="row">
                                    <div class="col s12 m12 l12">
                                        <div
                                            class="img-news-wrap <?php echo ($i % 2 != 0 && $i % 3 != 0) ? 'hide-on-med-and-up' : '' ?>">
                                            <img class="responsive-img"
                                                 src="<?php echo image_thumb($news->cover_image, 300, 220, '', true) ?>"
                                                 alt="<?php echo $news->name ?>"/>
                                        </div>
                                    </div>
                                    <div class="col s12 m12 l12">
                                        <div class="news-short-desc">
                                            <h3><?php echo $news->name ?></h3>
                                            <span>Posted at 18 August, 2015 |06 pm</span>

                                            <p>
                                                <?php echo $news->short_description ?>
                                            </p>
                                        </div>
                                        <a class="link-readmore" href="<?php echo base_url('news/' . $news->slug) ?>">Read more</a>
                                    </div>
                                    <?php if ($i % 2 != 0 && $i % 3 != 0) { ?>
                                        <div class="col s12 l12 hide-on-small-only">
                                            <div class="img-news-wrap">
                                                <img class="responsive-img"
                                                     src="<?php echo image_thumb($news->cover_image, 300, 220, '', true) ?>"
                                                     alt="<?php echo $news->name ?>"/>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php if($i<2){
                                $i++;
                            }else{
                                $i = 0;
                            }
                        } ?>
                    </ul>

            <?php } ?>
        </div>
    </div>
        <div class="center">
                <div aclass="page_navigation">

                </div>
        </div>
    </div>

    <!--<div class="center">
        <ul class="pagination">
            <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
            <li class="active"><a href="#!">1</a></li>
            <li class="waves-effect"><a href="#!">2</a></li>
            <li class="waves-effect"><a href="#!">3</a></li>
            <li class="waves-effect"><a href="#!">4</a></li>
            <li class="waves-effect"><a href="#!">5</a></li>
            <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
        </ul>
    </div>-->
</div>