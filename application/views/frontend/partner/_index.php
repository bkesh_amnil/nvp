<div class="container">
    <div class="center">
        <?php $this->load->view('frontend/include/headings', array('headingData' => $partnerContent)) ?>
    </div>

    <?php foreach($allPartners as $partnerCategory => $partner) { ?>
        <section class="partners">
            <h2 id="<?php echo $allPartners[$partnerCategory][0]->categorySlug ?>"><?php echo $allPartners[$partnerCategory][0]->categoryName ?></h2>
            <?php $count = count($allPartners[$partnerCategory]);
            if($count >5)
                $class='carousel';
                else
            $class=' no-carousel';?>
            <div class="partners-slider owl-carousel <?php echo $class?>">
            <?php foreach($allPartners[$partnerCategory] as $key => $partnerData) { ?>
                <a href="javascript:void(0);" class="partnerLink <?php echo ($key == 0) ? 'active' : '' ?>" data-content="#<?php echo $partnerData->id ?>" data-category="<?php echo $partnerCategory ?>">
                    <img src="<?php echo base_url($partnerData->image) ?>" alt="<?php echo $partnerData->name ?>"/>
                </a>
            <?php } ?>
            </div>
            <?php foreach($allPartners[$partnerCategory] as $key => $partnerData) { ?>
                <div id="<?php echo $partnerData->id ?>" class="partner-about-wrap data-holder-<?php echo $partnerCategory ?>" <?php echo ($key == 0) ? 'style="display: block;"' : 'style="display: none;"' ?>>
                    <div class="partner-contents">
                        <div class="partner-title"><?php echo $partnerData->name ?></div>
                        <?php echo $partnerData->description ?>
                    </div>
                </div>
            <?php } ?>
        </section>
    <?php } ?>
</div>