<?php if(isset($activationLink)) { ?>
    <h1>Thanks for creating an account with NVP.</h1>
    <p>
        You will need to activate your account in order to login to it. Please follow the link below to activate your account.
    </p>
    <a href="<?php echo $activationLink ?>">
        <?php echo $activationLink ?>
    </a>
    <p>
        Thank You,
    </p>
    <p>
        NVP
    </p>
<?php } else { ?>
    <h1>Thanks for creating an account with NVP.</h1>
    <p>
        Your account has been created successfully. Someone from the NVP team will be with you as soon as possible.
    </p>
    <p>
        Thank You,
    </p>
    <p>
        NVP
    </p>
<?php } ?>
