<!doctype html>

<html lang="en">

<head>
    <meta charset="utf-8">

    <title>Password Reset</title>
</head>

<body>
<table style="width: 600px; margin: 0 auto; font-family:Arial;">
    <thead>
    <tr>
        <td style="text-align: center;"><img src="<?php echo base_url($siteLogo) ?>" /></td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:rgb(201, 178, 108); padding-left: 30px;">Password Reset</td>
    </tr>
    <tr>
        <td style=" font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">Greetings <?php echo $name ?>,<br />
            <br />
            Your password has been reset successfully as per your request. Below is your new password.<br /><br />
            New Password: <?php echo $password ?><br /><br />
            Use this link to login to the system with your new password. <a href="<?php echo base_url(BACKENDFOLDER . '/login') ?>"><?php echo base_url(BACKENDFOLDER . '/login') ?></a><br /><br />
            Regards
        </td>
    </tr>
    <tr>
        <td style="padding: 30px;  font-size: 12px; font-weight: 400; color: #777; background-color:#fff; padding-left: 30px;">Visit us: <a href="#siteLink#" style="color: rgb(201, 178, 108); text-decoration: none;">www.nvp.org.np</a></td>
    </tr>
    </tbody>
</table>
</body>

</html>