<?php if($provider == 'facebook') { ?>
    <h1>Thanks for creating an account with NVP.</h1>
    <p>
        You will need to activate your account in order to login to it. Please follow the link below to activate your account.
        Since you used facebook to register with us. Your username and password is the email address you use with facebook.
    </p>
    <a href="<?php echo $activationLink ?>">
        <?php echo $activationLink ?>
    </a>
    <p>
        Thank You,
    </p>
    <p>
        NVP
    </p>
<?php } else { ?>
    <h1>Thanks for creating an account with NVP.</h1>
    <p>
        You will need to activate your account in order to login to it. Please follow the link below to activate your account.
        Since you used twitter to register with us. Your username and password is the twitter display name.
    </p>
    <a href="<?php echo $activationLink ?>">
        <?php echo $activationLink ?>
    </a>
    <p>
        Thank You,
    </p>
    <p>
        NVP
    </p>
<?php } ?>