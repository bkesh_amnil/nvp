<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*!
* HybridAuth
* http://hybridauth.sourceforge.net | http://github.com/hybridauth/hybridauth
* (c) 2009-2012, HybridAuth authors | http://hybridauth.sourceforge.net/licenses.html
*/

// ----------------------------------------------------------------------------------------
//	HybridAuth Config file: http://hybridauth.sourceforge.net/userguide/Configuration.html
// ----------------------------------------------------------------------------------------

$config =
    array(
        // set on "base_url" the relative url that point to HybridAuth Endpoint
        'base_url' => '/members/sociallogin/endpoint',

        "providers" => array(
            // openid providers
            "OpenID" => array(
                "enabled" => false
            ),

            "Yahoo" => array(
                "enabled" => false,
                "keys" => array("id" => "", "secret" => ""),
            ),

            "AOL" => array(
                "enabled" => false
            ),

            "Google" => array(
                "enabled" => false,
                "keys" => array("id" => "", "secret" => ""),
            ),

            "Facebook" => array(
                "enabled" => true,
                "keys" => array("id" => "934308949957590", "secret" => "6ede848949e1bd8817dc639f1b00c67e"),
                "scope" => "email, public_profile, user_birthday"
            ),

            "Twitter" => array(
                "enabled" => true,
                "keys" => array("key" => "73aNVrG8B8n4fwDYMg1JvSyrG", "secret" => "YbBEUos0B0PPeSCi1MoTfPpHozTCgIDIwWShwRH9mv2Ap6hJYr")
            ),

            // windows live
            "Live" => array(
                "enabled" => false,
                "keys" => array("id" => "", "secret" => "")
            ),

            "MySpace" => array(
                "enabled" => false,
                "keys" => array("key" => "", "secret" => "")
            ),

            "LinkedIn" => array(
                "enabled" => false,
                "keys" => array("key" => "", "secret" => "")
            ),

            "Foursquare" => array(
                "enabled" => false,
                "keys" => array("id" => "", "secret" => "")
            ),
        ),

        // if you want to enable logging, set 'debug_mode' to true  then provide a writable file by the web server on "debug_file"
        "debug_mode" => false,

        "debug_file" => APPPATH . '/logs/hybridauth.log',
    );


/* End of file hybridauthlib.php */
/* Location: ./application/config/hybridauthlib.php */