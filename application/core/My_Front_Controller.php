<?php

class My_Front_Controller extends CI_Controller
{

    public $template = 'frontend/layout/default';
    public $data = array();
    public $global_config;

    public function __construct()
    {
        parent::__construct();

        $this->load->helper('cookie');

        $this->load->model('configuration_model', 'configuration');
        $this->load->model('common_model', 'common');
        $this->load->model('menu_model', 'menu');
        $this->load->model('project_model', 'project');
        $this->load->model('partner_model', 'partner');
        $this->load->model('content_model', 'content');
        $this->global_config = $this->configuration->get(1);

        define('SITENAME', $this->global_config->site_title);
        define('SITEMAIL', $this->global_config->site_email);

        // website generic meta data
        $this->data['globalConfig'] = $this->global_config;
        $nepaliMaPdf = $this->common->query('select * from tbl_download d join tbl_category c on c.id = d.categoryId where d.status="Active" and c.slug="nepali-ma"');
        $this->data['nepaliMaPdf'] = '#';
        if($nepaliMaPdf) {
            $this->data['nepaliMaPdf'] = $nepaliMaPdf[0]->file;
        }

        $this->data['meta_title'] = SITENAME;
        $this->data['meta_keywords'] = $this->global_config->meta_keyword;
        $this->data['meta_description'] = $this->global_config->meta_description;
        $this->data['facebook_link'] = $this->global_config->facebook;
        $this->data['twitter_link'] = $this->global_config->twitter;
        $this->data['gplus_link'] = $this->global_config->gplus;
        $this->data['youtube_link'] = $this->global_config->youtube;
        $this->data['skype_link'] = $this->global_config->skype;
        $this->_getSocialMetas('');
        // website navigation
        $this->data['mainMenu'] = $this->menu->getNavigationByPosition('mainmenu');
        $this->data['bottomMenu'] = $this->menu->getNavigationByPosition('bottommenu');
        foreach($this->data['bottomMenu']['parents'] as $key=>$parents)
        {
            if($parents->menu_link_type == 'content'){
                $content = $this->content->get('1',['id' => $parents->link]);
                $this->data['bottomMenu']['parents'][$key]->content = $content->long_description;
            }
        }
        $this->data['contacts'] = $this->configuration->get('1');
        $this->data['allProjects'] = $this->project->get('4',['status'=>'Active']);
        $this->data['PartnersCategories'] = $this->partner->get_categories();
        $this->data['show_add_link'] = false;
        $usertype = get_cookie('rememberedUserType');
        $model = isset($usertype) ? strtolower($usertype) : '';
        if(!empty($model)){
            $this->load->model($model.'_model',$model);
            $data = $this->$model->get('1',['md5(id)'=>$_COOKIE['remembered']]);
            if(!empty($data)){
                if($usertype=='Volunteer'){
                    set_userdata(array(
                        'active_user' => $data->id,
                        'userType' => 'Volunteer',
                        'userName' => $data->fullName
                    ));
                    if (strpos($data->profilePicture, 'https') !== false || strpos($data->profilePicture, 'http') !== false) {
                        set_userdata(array(
                            'profilePicture' => $data->profilePicture
                        ));
                    } else {
                        set_userdata(array(
                            'profilePicture' => ($data->profilePicture) ? base_url($data->profilePicture) : base_url('assets/img/icon/icon-user-default.png')
                        ));

                    }
                }else{
                    set_userdata(array(
                        'active_user' => $data->id,
                        'userType' => 'Agency',
                        'userName' => $data->name,
                        'profilePicture' => $data->logo
                    ));

                    if (strpos($data->logo, 'https') !== false || strpos($data->logo, 'http') !== false) {
                        set_userdata(array(
                            'profilePicture' => $data->logo
                        ));
                    } else {
                        set_userdata(array(
                            'profilePicture' => ($data->logo) ? base_url($data->logo) : base_url('assets/img/icon/icon-user-default.png')
                        ));
                    }
                }
            }
        }
        $this->load->model('notification_model', 'notification');
        $notification_count = '0';
        if(get_userdata('active_user')) {
            $notificationQuery = "select
                                n.* , c.slug
                                from `tbl_notification` n
                                left join `tbl_content` c on c.id = n.content_id
                                where n.content_id IS NOT NULL OR n.content_id = 0
                                order by n.id desc
                                limit 10";
            $this->data['allUserNotifications'] = $this->notification->query($notificationQuery);
            $this->load->model('notificationcount_model', 'notificationcount');
            if($this->data['allUserNotifications']) {
                foreach($this->data['allUserNotifications'] as $not) {
                    $check = $this->notificationcount->get(
                        '1',
                        array(
                            'notification_id' => $not->id,
                            'user_id' => get_userdata('active_user'),
                            'user_type' => get_userdata('userType')
                        ));
                    if(!$check) {
                        $notification_count++;
                    }
                }
            }
        } else {
            $this->data['allUserNotifications'] = array();
        }
        $notificationCookie = get_userdata('userType').get_userdata('active_user');
        set_userdata(array($notificationCookie => $notification_count));

        if(segment(1) == 'members') {
            $allowedSegments = array(
                'login',
                'register',
                'sociallogin',
                'retrievepassword',
                'feeds'
            );
            if(!get_userdata('active_user') && !in_array(segment(2), $allowedSegments)) {
                redirect('members/login');
            }
            if((segment(2) == 'register' || segment(2) == 'login' || segment(2) == '') && get_userdata('active_user')) {
                redirect('members/profile');
            }
        }
    }

    public function _getSocialMetas($dataSlug)
    {
        $this->data['ogTitle'] = SITENAME;
        $this->data['ogDescription'] = $this->data['meta_description'];
        $this->data['ogImage'] = '';

        $this->data['twitterTitle'] = SITENAME;
        $this->data['twitterDescription'] = $this->data['meta_description'];
        $this->data['twitterImage'] = '';

        $this->load->model('social_model', 'social');
        $query = "select
                    sd.*,
                    m.slug as module
                    from tbl_social_data sd
                    join tbl_module m on m.id = sd.module_id
                    join tbl_content c on c.id = sd.data_id
                    where c.slug='{$dataSlug}'";
        $socialData = $this->social->query($query);
        if($socialData) {
            foreach($socialData as $key => $data) {
                if($data->social_site == 'Facebook') {
                    $this->data['ogTitle'] = $socialData[$key]->title;
                    $this->data['ogDescription'] = $socialData[$key]->description;
                    $this->data['ogImage'] = $socialData[$key]->image;
                } else if($data->social_site == 'Twitter') {
                    $this->data['twitterTitle'] = $socialData[$key]->title;
                    $this->data['twitterImage'] = $socialData[$key]->image;
                    $this->data['twitterDescription'] = $socialData[$key]->description;
                }
            }
        }
    }

    public function render()
    {
        $this->load->view($this->template, $this->data);
    }

}