<?php

class News extends My_Front_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    function _remap($method)
    {
        $param_offset = 2;

        // Default to index
        if ( ! method_exists($this, $method))
        {
            // We need one more param
            $param_offset = 1;
            $method = 'index';
        }

        // Since all we get is $method, load up everything else in the URI
        $params = array_slice($this->uri->rsegment_array(), $param_offset);

        // Call the determined method with all params
        call_user_func_array(array($this, $method), $params);
    }

    public function index($newsSlug = '')
    {
        $this->load->model('content_model', 'content');
        $this->load->model('menu_model', 'menu');
        $this->load->model('gallery_model', 'gallery');

        if($newsSlug != '' && ! is_numeric($newsSlug)) {
            $this->_getSocialMetas($newsSlug);
            $this->data['addJs'] = array(
                'assets/js/vendor/fancybox/source/jquery.fancybox.pack.js',
                'assets/js/vendor/fancybox/lib/jquery.mousewheel-3.0.6.pack.js',
                'assets/js/vendor/fancybox/source/helpers/jquery.fancybox-buttons.js',
                'assets/js/vendor/fancybox/source/helpers/jquery.fancybox-media.js',
                'assets/js/vendor/fancybox/source/helpers/jquery.fancybox-thumbs.js',
                'assets/js/news-detail.js'
            );
            $this->data['addCss'] = array(
                'assets/js/vendor/fancybox/source/jquery.fancybox.css',
                'assets/js/vendor/fancybox/source/helpers/jquery.fancybox-buttons.css',
                'assets/js/vendor/fancybox/source/helpers/jquery.fancybox-thumbs.css'
            );
            $this->data['body'] = 'frontend/news/_detail';
            $this->data['newsDetail'] = $this->content->getBySlug($newsSlug);
            if($this->data['newsDetail']->meta_description != '') {
                $this->data['meta_description'] = $this->data['newsDetail']->meta_description;
            }
            if($this->data['newsDetail']->meta_keyword != '') {
                $this->data['meta_keywords'] = $this->data['newsDetail']->meta_keyword;
            }
            if($this->data['newsDetail']->cover_image != '') {
                $this->data['ogImage'] = $this->data['newsDetail']->cover_image;
            }
            if($this->data['newsDetail']->short_description != '') {
                $this->data['ogDescription'] = strip_tags($this->data['newsDetail']->short_description);
            }
            if($this->data['newsDetail']->name != '') {
                $this->data['ogTitle'] = $this->data['newsDetail']->name;
            }

            $this->data['contentGallery'] = $this->gallery->getContentGallery($this->data['newsDetail']->id);
        } else {
            $this->data['body'] = 'frontend/news/_index';
            // news pagination start
            $offset = segment(2) == '' ? '0' : segment(2);
            $this->load->library('pagination');
            $config['base_url'] = base_url('news');
            $config['total_rows'] = count($this->content->getByCategoryNews('news-updates'));
            //config for bootstrap pagination class integration
            $config['full_tag_open'] = '<div class="center"><ul class="pagination">';
            $config['full_tag_close'] = '</ul></div>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['prev_link'] = '<i class="material-icons">chevron_left</i>';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = '<i class="material-icons">chevron_right</i>';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li class="waves-effect">';
            $config['num_tag_close'] = '</li>';
            $config['per_page'] = 6;
            $config['uri_segment'] = 2;
            $this->pagination->initialize($config);
            $this->data['newsPagination'] = $this->pagination->create_links();
            $this->data['allNews'] = $this->content->getByCategoryNewsPaginate('news-updates',$offset);
            // news  pagination end
            //$this->data['allNews'] = $this->content->getByCategoryNews('news-updates',0);
            $this->data['menu_alias'] = $this->uri->segment(1);
            $newsContents = $this->menu->get('1', ['menu_alias' => $this->data['menu_alias']]);
            $resp = array();
            if($newsContents) {
                    $resp[] = array(
                        'title' => $newsContents->menu_title,
                        'description' => strip_tags($newsContents->description)
                    );
            }
            $this->data['newsContent'] = $resp;
        }

        // loading custom js for home page
        if(isset($this->data['addJs'])) {
            $this->data['addJs'] = array_merge(
                $this->data['addJs'],
                array(
                    'assets/infinite-scroll/jquery.infinitescroll.min.js',
                    'assets/js/jquery.paginate.js',
                    'assets/js/paginate.js'
                ));
        } else {
            $this->data['addJs'] = array(
                'assets/js/salvattore.js',
                'assets/js/jquery.paginate.js',
                'assets/counter-master/waypoints.min.js',
                'assets/js/news.js'
            );
            $this->data['addCss'] = array(
                'assets/css/salvattore.css',
                'assets/css/news.css'
            );
        }
        $this->render();
    }

public function news_show(){
    $limit = segment(3);
    $this->load->model('content_model', 'content');
    $news = $this->content->getByCategoryNews('news-events',$limit);
    if(is_array($news)) {
        foreach ($news as $key => $new) {
            $data[$key] = $new;
            $data[$key]->cover_image = image_thumb($new->cover_image, 392, 220, '', true);
        }
        echo json_encode($data);
    }else{
        echo json_encode(array('fail'=>'false'));
    }
}
}