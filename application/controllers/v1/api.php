<?php

class Api extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        // allow cross origin
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000000');
        header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');

        // setting post value from app
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST))
            $_POST = json_decode(file_get_contents('php://input'), true);

    }

    public function getPostDetail() {
        $this->load->model('post_model', 'post');
        $newsSlug = segment(4);
        if($newsSlug != '') {
            $response = $this->post->getBySlug($newsSlug);
        } else {
            $response = $this->post->get();
        }
        $this->_returnJson($response);
    }

    public function getNewsFeed() {
        $this->load->model('post_model', 'post');
        $this->load->model('postcomment_model', 'postcomment');
        $perPage = 4;
        $offset = 0;
        $post = $_POST;
        $userId = isset($post['userId']) && $post['userId'] ? $post['userId'] : 1;
        $newsSlug = isset($post['postSlug']) ? $post['postSlug'] : '';
        if(isset($post['pageNumber'])) {
            $offset = $post['pageNumber'] * $perPage;
        }
        if($newsSlug != '') {
            $query = "SELECT
                              p.*,
                              (SELECT COUNT(id) FROM nvp_post_likes pl WHERE pl.`postId` = p.`id`) AS totalLikes,
                              (SELECT v.fullName FROM nvp_volunteer v WHERE v.id = p.userId) AS post_by,
                              (SELECT v.profilePicture FROM nvp_volunteer v WHERE v.id = p.userId) AS post_by_pic,
                              (SELECT COUNT(id) FROM nvp_post_comments pc WHERE pc.`postId` = p.`id`) AS totalComments,
                              (SELECT IFNULL(id, 0) FROM nvp_post_likes pl WHERE pl.`userId` = $userId AND pl.`postId` = p.`id`) AS userLiked
                            FROM
                              nvp_post p
                            WHERE p.slug = '$newsSlug'";
            $response['newsDetail'] = $this->post->query($query)[0];
            $response['comments'] = $this->postcomment->getComments($response['newsDetail']->id, false);
        } else {
            $query = "SELECT
                              p.*,
                              (SELECT COUNT(id) FROM nvp_post_likes pl WHERE pl.`postId` = p.`id`) AS totalLikes,
                              (SELECT v.fullName FROM nvp_volunteer v WHERE v.id = p.userId) AS post_by,
                              (SELECT v.profilePicture FROM nvp_volunteer v WHERE v.id = p.userId) AS post_by_pic,
                              (SELECT COUNT(id) FROM nvp_post_comments pc WHERE pc.`postId` = p.`id`) AS totalComments,
                              (SELECT IFNULL(id, 0) FROM nvp_post_likes pl WHERE pl.`userId` = $userId AND pl.`postId` = p.`id`) AS userLiked
                            FROM
                              nvp_post p
                            WHERE p.status = 'Active'
                            ORDER BY p.created_on DESC
                            LIMIT $offset, $perPage";
            $response = $this->post->query($query);
        }

        $this->_returnJson($response);
    }

    public function searchVolunteer()
    {
        $this->load->model('common_model', 'common');
        $searchParameters = $_POST;

        $searchQuery = "select
                        count(distinct(v.id)) as genderGroupCount,
                        g.name as genderName
                        from nvp_volunteer v
                        join nvp_gender g on g.id = v.genderId
                        left join nvp_volunteer_language vl on vl.volunteerId = v.id
                        left join nvp_volunteer_skill vs on vs.volunteerId = v.id
                        left join nvp_volunteer_training_interested_in vtii on vtii.volunteerId = v.id
                        left join nvp_volunteer_training_received vtr on vtr.volunteerId = v.id
                        where v.status = 'Active' and v.account_status = 'Active'";

        if(isset($searchParameters['zoneId']) && $searchParameters['zoneId'] != '' && !empty($searchParameters['zoneId'])) {
            //$zoneIds = array2wherein($searchParameters['zoneId']);
            $zoneIds = ($searchParameters['zoneId']);
            $searchConditions[] = "v.zoneId in ('{$zoneIds}')";
        }

        if(isset($searchParameters['districtId']) && $searchParameters['districtId'] != '' && !empty($searchParameters['districtId'])) {
            //$districtId = array2wherein($searchParameters['districtId']);
            $districtId = ($searchParameters['districtId']);
            $searchConditions[] = "v.districtId in ('{$districtId}')";
        }

        if(isset($searchParameters['municipalityId']) && $searchParameters['municipalityId'] != '' && !empty($searchParameters['municipalityId'])) {
            //$municipalityId = array2wherein($searchParameters['municipalityId']);
            $municipalityId = ($searchParameters['municipalityId']);
            $searchConditions[] = "v.municipalityId in ('{$municipalityId}')";
        }

        if(isset($searchParameters['genderId']) && $searchParameters['genderId'] != '' && !empty($searchParameters['genderId'])) {
            $genderIds = array2wherein($searchParameters['genderId']);
            $searchConditions[] = "v.genderId in ('{$genderIds}')";
        }

        if(isset($searchParameters['academicQualificationId']) && $searchParameters['academicQualificationId'] != '' && !empty($searchParameters['academicQualificationId'])) {
            $academicQualificationIds = array2wherein($searchParameters['academicQualificationId']);
            $searchConditions[] = "v.academicQualificationId in ('{$academicQualificationIds}')";
        }

        if(isset($searchParameters['languageId']) && $searchParameters['languageId'] != '' && !empty($searchParameters['languageId'])) {
            $languageIds = array2wherein($searchParameters['languageId']);
            $searchConditions[] = "vl.languageId in ('{$languageIds}')";
        }

        if(isset($searchParameters['skillId']) && $searchParameters['skillId'] != '' && !empty($searchParameters['skillId'])) {
            $skillIds = array2wherein($searchParameters['skillId']);
            $searchConditions[] = "vs.skillId in ('{$skillIds}')";
        }

        if(isset($searchParameters['trainingId']) && $searchParameters['trainingId'] != '' && !empty($searchParameters['trainingId'])) {
            $trainingIds = array2wherein($searchParameters['trainingId']);
            $searchConditions[] = "vtii.trainingId in ('{$trainingIds}')";
            $searchConditions[] = "vtr.trainingId in ('{$trainingIds}')";
        }

        if(isset($searchConditions)) {
            $searchConditions = implode(' and ', $searchConditions);
            $searchConditions = " and ($searchConditions) ";
        }

        $searchQuery .= $searchConditions . " group by g.id";

        $res = $this->common->query($searchQuery);

        $this->_returnJson($res);
    }

    public function getSearchMasterData()
    {
        $userId = $_POST['userId'];
        $this->load->model('agencyproject_model', 'agencyproject');
        $this->load->model('academicqualification_model', 'academicqualification');
        $this->load->model('common_model', 'common');
        $data['languages'] = $this->common->query('select * from nvp_language');
        $data['allGender'] = $this->common->query('select * from nvp_gender');
        $data['zones'] = $this->common->query('select * from nvp_zone');
        /*$data['districts'] = $this->common->query('select * from nvp_district');
        $data['municipalities'] = $this->common->query('select * from nvp_municipality');*/
        $data['academicQualifications'] = $this->academicqualification->get('', array('status' => 'Active'));
        $data['skills'] = $this->common->query('select * from nvp_skill');
        $data['trainings'] = $this->common->query('select * from nvp_training');
        $data['projectList'] = $this->agencyproject->get('', array('agencyId' => $userId, 'status != ' => 'InActive'), 'dateOfEvent desc');

        $this->_returnJson($data);
    }

    public function getProjects()
    {
        $this->load->model('project_model', 'project');
        $projectSlug = segment(4);
        if($projectSlug != '') {
            $response = $this->project->getBySlug($projectSlug);
        } else {
            $response = $this->project->get('', array('status' => 'Active'));
        }
        $this->_returnJson($response);
    }

    public function getProjectCriteria()
    {
        $post = $_POST;
        if($post) {
            $this->load->model('agencyprojectcriteria_model', 'agencyprojectcriteria');
            $this->load->model('agencyprojectcriteriatraining_model', 'agencyprojectcriteriatraining');
            $this->load->model('agencyprojectcriteriaskill_model', 'agencyprojectcriteriaskill');
            $this->load->model('agencyprojectcriteriagender_model', 'agencyprojectcriteriagender');
            $this->load->model('agencyprojectcriteriaacademic_model', 'agencyprojectcriteriaacademic');

            $projectId = $post['projectId'];
            $this->data['projectCriteriaData'] = $this->agencyprojectcriteria->get('1', array('agencyProjectId' => $projectId));
            if(!$this->data['projectCriteriaData']) {
                $this->data['projectCriteriaData'] = $this->agencyprojectcriteria;
                $this->data['projectCriteriaAcademicData'] = array();
                $this->data['projectCriteriaGenderData'] = array();
                $this->data['projectCriteriaSkillData'] = array();
                $this->data['projectCriteriaTrainingData'] = array();
            } else {
                $projectCriteriaAcademicData = $this->agencyprojectcriteriaacademic->getData($this->data['projectCriteriaData']->id);
                foreach($projectCriteriaAcademicData as $row) {
                    $this->data['projectCriteriaAcademicData'][$row] = true;
                }
                $projectCriteriaGenderData = $this->agencyprojectcriteriagender->getData($this->data['projectCriteriaData']->id);
                foreach($projectCriteriaGenderData as $row) {
                    $this->data['projectCriteriaGenderData'][$row] = true;
                }
                $projectCriteriaSkillData = $this->agencyprojectcriteriaskill->getData($this->data['projectCriteriaData']->id);
                foreach($projectCriteriaSkillData as $row) {
                    $this->data['projectCriteriaSkillData'][$row] = true;
                }
                $projectCriteriaTrainingData = $this->agencyprojectcriteriatraining->getData($this->data['projectCriteriaData']->id);
                foreach($projectCriteriaTrainingData as $row) {
                    $this->data['projectCriteriaTrainingData'][$row] = true;
                }
            }
            $responseMsg = $this->data;
            $responseCode = 200;
        } else {
            $responseMsg = 'No data';
            $responseCode = 400;
        }
        $this->_returnJson($responseMsg, $responseCode);
    }

    public function getPartners()
    {
        $this->load->model('partner_model', 'partner');
        $partnerSlug = segment(4);
        if($partnerSlug != '') {
            $response = $this->partner->getBySlug($partnerSlug);
        } else {
            $response = $this->partner->get('', array('status' => 'Active'));
        }
        $this->_returnJson($response);
    }

    public function getContent($slug)
    {
        $this->load->model('content_model', 'content');
        $data = $this->content->get('1', array('slug' => $slug));
        $this->_returnJson($data);
    }

    public function getContactDetail()
    {
        $this->load->model('configuration_model', 'configuration');
        $data = $this->configuration->get('1');
        $this->_returnJson($data);
    }

    public function login()
    {
        $this->load->model('login_model', 'login');
        $res = $this->login->validateUser($this->input->post(), true);
        if($res != 'InActive') {
            if($res && $res['type'] == 'Volunteer') {
                $this->load->model('agencyproject_model', 'agencyproject');
                $res['volunteerHours'] = $this->agencyproject->getVolunteerHours(get_userdata('active_user'))->totalHours;
            }
        }
        $this->_returnJson($res);
    }

    public function _returnJson($data, $errorCode = 200) {
        if($data && $errorCode == 200) {
            $status = 'success';
        } else {
            $status = 'error';
        }

        $responseData = array(
            'status' => $status,
            'data' => $data,
            'responseCode' => $errorCode
        );

        echo json_encode($responseData);
    }

    public function filterDistrict()
    {
        $zoneId = $_POST['zoneId'];
        $this->load->model('common_model', 'common');
        $query = "select * from nvp_district where zoneId = $zoneId order by name asc";
        $data = $this->common->query($query);
        $options = '<option value="">Select District</option>';

        if($data) {
            foreach($data as $district) {
                $options .= "<option value='$district->id'>".ucwords($district->name)."</option>";
            }
        }

        $this->_returnJson($options);
    }

    public function filterMunicipality()
    {
        $districtId = $_POST['districtId'];
        $this->load->model('common_model', 'common');
        $query = "select * from nvp_municipality where districtId = $districtId order by name asc";
        $data = $this->common->query($query);
        $options = '<option value="">Select VDC/Municipality</option>';

        if($data) {
            foreach($data as $municipality) {
                $options .= "<option value='$municipality->id'>".ucwords($municipality->name)."</option>";
            }
        }

        $this->_returnJson($options);
    }

    public function checkProfileStatus($volunteerId = '')
    {
        $ci = & get_instance();
        $ci->load->model('volunteer_model', 'volunteer');
        $data = $ci->volunteer->get('1', array('id' => $volunteerId));
        $profileComplete = true;
        $completion = 0;
        if($data) {
            $ci->load->model('volunteerlanguage_model', 'volunteerlanguage');
            $ci->load->model('volunteerskill_model', 'volunteerskill');
            $ci->load->model('volunteertraininginterestedin_model', 'volunteertraininginterestedin');
            $ci->load->model('volunteertraining_model', 'volunteertraining');
            $volunteerLanguage = $ci->volunteerlanguage->get('', array('volunteerId' => $data->id));
            $volunteerSkill = $ci->volunteerskill->get('', array('volunteerId' => $data->id));
            $volunteerTrainingInterestedIn = $ci->volunteertraininginterestedin->get('', array('volunteerId' => $data->id));
            $volunteerTrainingReceived = $ci->volunteertraining->get('', array('volunteerId' => $data->id));

            $compulsoryFields = array(
                'fullName',
                'genderId',
                'dateOfBirth',
                'phoneNumberMobile',
                'emergencyContactFullName',
                'email',
                'emergencyContactPhoneNumber',
                'zoneId',
                'districtId',
                'municipalityId',
                'wardNumber',
                'volunteerType',
                'primaryModeOfCommunication'
            );
            foreach($data as $key => $value) {
                if(in_array($key, $compulsoryFields)) {
                    if($value == '') {
                        $profileComplete = false;
                    } else {
                        $completion += 4;
                    }
                }
            }

            if($volunteerLanguage) {
                $completion += 10;
            }

            if($volunteerSkill) {
                $completion += 10;
            }

            if($volunteerTrainingInterestedIn) {
                $completion += 10;
            }

            if($volunteerTrainingReceived) {
                $completion += 10;
            }
        } else {
            $profileComplete = false;
        }

        $this->_returnJson(array('completion' => $completion, 'profileComplete' => $profileComplete));
    }

    public function getCurrentUserId()
    {
        echo get_userdata('active_user');
    }

    public function savePost()
    {
        $this->load->model('post_model', 'post');
        $file = $_FILES['file'];
        $post = $_POST;
        $saveData = array(
            'name' => $post['title'],
            'description' => $post['description'],
            'location' => isset($post['location']) ? $post['location'] : '',
            'userId' => $post['userId']
        );

        // check if user is verified
        $this->load->model('common_model', 'common');
        $this->common->table = 'nvp_verified_user';
        $verifiedUserData = $this->common->get('1', array('userId' => $post['userId'], 'userType' => $post['userType']));
        if($verifiedUserData) {
            $saveData['status'] = 'Active';
        } else{
            $saveData['status'] = 'InActive';
        }

        $saveData['slug'] = $this->post->createSlug($saveData['name']);
        $saveData['userType'] = $post['userType'];
        $message = '';
        if ($_FILES) {
            $target_dir = "uploads/posts/";
            if(!file_exists($target_dir)) {
                mkdir($target_dir);
            }
            $target_file = $target_dir . basename($file["name"]);
            $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
            $imageFileType = ($imageFileType) ? $imageFileType : 'jpg';
            $target_file = $target_dir . time() . '.' . $imageFileType;

            if (move_uploaded_file($file["tmp_name"], $target_file)) {
                $saveData['image'] = $target_file;
                $res = $this->post->save($saveData, '', true);
                if ($res) {
                    if(!$verifiedUserData && $post['email'] != 'null') {
                        $this->load->model('configuration_model', 'configuration');
                        $globalConfig = $this->configuration->get('1');
                        // data form email template
                        $siteLogo = base_url($globalConfig->site_logo);
                        $siteLink = base_url();
                        $postDetailLink = base_url(BACKENDFOLDER . '/post_data/view_detail/' . $res);
                        $this->load->model('emailtemplate_model', 'emailtemplate');
                        $newPostEmailTemplate = $this->emailtemplate->getAllData(6);
                        $adminMessage = str_replace(
                            array('#siteLogo#', '#siteLink#', '#emailSubject#', '#postDetailLink#'),
                            array($siteLogo, $siteLink, $newPostEmailTemplate[0]->adminSubject, $postDetailLink),
                            $newPostEmailTemplate[0]->adminMessage
                        );

                        $email_params = array(
                            'subject' => $newPostEmailTemplate[0]->adminSubject,
                            'from' => $post['email'],
                            'fromname' => $post['fullName'],
                            'to' => $newPostEmailTemplate[0]->adminEmail,
                            'toname' => $globalConfig->site_title,
                            'message' => $adminMessage
                        );
                        swiftsend($email_params);
                    }
                    $message = array('type' => 'success', 'message' => 'Post successful', 'verified' => 'true');
                }
            } else {
                $message = array('type' => 'error', 'message' => 'Sorry, there was an error uploading your file.');
            }
        } else {
            $message = array('type' => 'error', 'message' => 'Upload your image');
        }
        if($message['type'] == 'success') {
            if(!$verifiedUserData) {
                $message['message'] = 'Your post has been saved. NVP will review and publish it as soon as possible';
            }
        }

        $this->_returnJson($message);
    }

    public function getAgencyProjects($agencyId = '')
    {
        if($agencyId != '') {
            $this->load->model('agencyproject_model', 'agencyproject');
            $agencyProjectData = $this->agencyproject->get(
                '',
                array('agencyId' => $agencyId, 'status != ' => 'InActive'),
                'status desc');
            $this->_returnJson($agencyProjectData);
        } else {
            $this->load->model('agencyproject_model', 'agencyproject');
            $agencyProjectData = $this->agencyproject->get('', array('status != ' => 'InActive'));
            $this->_returnJson($agencyProjectData);
        }
    }

    public function registerSocial()
    {
        $this->load->model('volunteer_model', 'volunteer');
        $newUserData = $_POST;

        if(strtolower($newUserData['userType']) == 'facebook') {
            $userData = array(
                'fullName' => $newUserData['username'],
                'socialId' => $newUserData['userId'],
                'status' => 'Active'
            );
        } else if(strtolower($newUserData['userType']) == 'twitter') {
            $userData = array(
                'fullName' => $newUserData['username'],
                'socialId' => $newUserData['userId'],
                'status' => 'Active'
            );
        }

        // check if user already logged in
        $existingUserData = $this->volunteer->get('1', array('socialId' => $userData['socialId']));
        if($existingUserData) {
            $userId = $existingUserData->id;
            //$this->volunteer->save($userData, array('id' => $userId));
        } else {
            $userId = $this->volunteer->save($userData, '', true);
        }

        $loggedInUserData = $this->volunteer->get('1', array('socialId' => $userData['socialId']));
        $returnData = array(
            'userId' => $loggedInUserData->id,
            'username' => $loggedInUserData->fullName,
            'email' => $loggedInUserData->email,
            'phone' => $loggedInUserData->phoneNumberResidential,
            'profilePicture' => $loggedInUserData->profilePicture,
            'type' => 'Volunteer',
            'socialId' => $newUserData['userId'],
            'account_status' => $loggedInUserData->account_status,
            'notification_status' => $loggedInUserData->notification_status,
            'phone_mobile' => $loggedInUserData->phoneNumberMobile
        );
        $this->_returnJson($returnData);
    }

    public function registerUser()
    {
        $post = $this->input->post();

        $passwordHash = passwordHash($post['password']);

        $this->load->model('configuration_model', 'configuration');
        $globalConfig = $this->configuration->get('1');
        if(strtolower($post['category']) == 'volunteer') {
            $userData = array(
                'fullName' => $post['username'],
                'genderId' => $post['gender'],
                'dateOfBirth' => strtotime($post['dob']),
                'phoneNumberResidential' => isset($post['homePhoneNumber']) ? $post['homePhoneNumber'] : '',
                'phoneNumberMobile' => isset($post['mobileNumber']) ? $post['mobileNumber'] : '',
                'email' => isset($post['email']) ? $post['email'] : '',
                'password' => $passwordHash,
                'updated_by' => 7
            );
            $this->load->model('volunteer_model', 'volunteer');
            $res = $this->volunteer->save($userData, '', true);

            if($res) {
                // data form email template
                $siteLogo = base_url($globalConfig->site_logo);
                $siteLink = base_url();
                $accountDetailLink = base_url(BACKENDFOLDER . '/volunteer_data/view_detail/' . $res);
                $activationLink = base_url('members/register/activate/volunteer/' . md5($res));
                $this->load->model('emailtemplate_model', 'emailtemplate');
                $volunteerRegistrationEmailTemplate = $this->emailtemplate->getAllData(2);
                $adminMessage = str_replace(
                    array('#siteLogo#', '#siteLink#', '#emailSubject#', '#accountDetailLink#'),
                    array($siteLogo, $siteLink, $volunteerRegistrationEmailTemplate[0]->adminSubject, $accountDetailLink),
                    $volunteerRegistrationEmailTemplate[0]->adminMessage
                );
                $volunteerMessage = str_replace(
                    array('#siteLogo#', '#siteLink#', '#emailSubject#', '#accountActivationLink#'),
                    array($siteLogo, $siteLink, $volunteerRegistrationEmailTemplate[0]->userSubject, $activationLink),
                    $volunteerRegistrationEmailTemplate[0]->userMessage
                );

                $email_params = array(
                    'subject' => 'New Account Created',
                    'from' => $volunteerRegistrationEmailTemplate[0]->adminEmail,
                    'fromname' => $globalConfig->site_title,
                    'to' => $post['email'],
                    'toname' => $post['username'],
                    'message' => $volunteerMessage
                );
                swiftsend($email_params);

                $email_params = array(
                    'subject' => 'New Account Created',
                    'from' => $post['email'],
                    'fromname' => $post['username'],
                    'to' => $volunteerRegistrationEmailTemplate[0]->adminEmail,
                    'toname' => $globalConfig->site_title,
                    'message' => $adminMessage
                );
                swiftsend($email_params);
            }
        } else if(strtolower($post['category']) == 'agency') {
            $userData = array(
                'name' => $post['agencyName'],
                'phoneNumber' => $post['phoneNumber'],
                'email' => $post['email'],
                'primaryContactPersonFullName' => $post['personalName'],
                'primaryContactPersonDesignationInAgency' => $post['personalDesignation'],
                'primaryContactPersonPersonalPhoneNumber' => $post['personalContact'],
                'primaryContactPersonEmail' => $post['personalEmail'],
                'password' => $passwordHash,
                'address' => $post['address']
            );
            $this->load->model('agency_model', 'agency');
            $res = $this->agency->save($userData, '', true);
            if($res) {
                // data form email template
                $siteLogo = base_url($globalConfig->site_logo);
                $siteLink = base_url();
                $accountDetailLink = base_url(BACKENDFOLDER . '/agency_data/view_detail/' . $res);
                $this->load->model('emailtemplate_model', 'emailtemplate');
                $agencyRegistrationEmailTemplate = $this->emailtemplate->getAllData(3);
                $adminMessage = str_replace(
                    array('#siteLogo#', '#siteLink#', '#emailSubject#', '#accountDetailLink#'),
                    array($siteLogo, $siteLink, $agencyRegistrationEmailTemplate[0]->adminSubject, $accountDetailLink),
                    $agencyRegistrationEmailTemplate[0]->adminMessage
                );
                $agencyMessage = str_replace(
                    array('#siteLogo#', '#siteLink#', '#emailSubject#'),
                    array($siteLogo, $siteLink, $agencyRegistrationEmailTemplate[0]->userSubject),
                    $agencyRegistrationEmailTemplate[0]->userMessage
                );

                $email_params = array(
                    'subject' => $agencyRegistrationEmailTemplate[0]->userSubject,
                    'from' => $agencyRegistrationEmailTemplate[0]->adminEmail,
                    'fromname' => $globalConfig->site_title,
                    'to' => $post['email'],
                    'toname' => $post['name'],
                    'message' => $agencyMessage
                );
                swiftsend($email_params);

                $email_params = array(
                    'subject' => $agencyRegistrationEmailTemplate[0]->adminSubject,
                    'from' => $post['email'],
                    'fromname' => $post['name'],
                    'to' => $agencyRegistrationEmailTemplate[0]->adminEmail,
                    'toname' => $globalConfig->site_title,
                    'message' => $adminMessage
                );
                swiftsend($email_params);
            }
        }

        if($res && strtolower($post['category']) == 'volunteer') {
            $msg = 'Account created successfully. Please visit your email to activate it.';
        }
        if($res && strtolower($post['category']) == 'agency') {
            $msg = 'Account created successfully. Someone from NVP team will be in touch with you as soon as possible.';
        }
        if(!$res) {
            $msg = 'Account could not be created.';
        }
        $this->_returnJson($msg);
    }

    public function validateEmail()
    {
        $post = $_POST;
        $table=$post['category'];
        $this->load->model('volunteer_model', 'volunteer');
        $this->load->model('agency_model', 'agency');
        $res = $this->volunteer->query("select id from `nvp_volunteer` v where v.`email` = '{$post['email']}' or v.`phoneNumberMobile` = '{$post['phone']}'");
        if(!$res) {
            $res = $this->agency->query("select id from `nvp_agency` a where a.`email` = '{$post['email']}' or a.`phoneNumber` = '{$post['phone']}'");
        }
        if($res) {
            $res = 'true';
        } else {
            $res = 'false';
        }

        $this->_returnJson($res);
    }

    public function getUserDetail()
    {
        $userId = $_POST['userId'];
        $type = $_POST['userType'];

        if(strtolower($type) == 'volunteer') {
            $this->load->model('volunteer_model', 'volunteer');
            $this->load->model('volunteerlanguage_model', 'volunteerlanguage');
            $this->load->model('volunteerskill_model', 'volunteerskill');
            $this->load->model('volunteertraining_model', 'volunteertraining');
            $this->load->model('volunteertraininginterestedin_model', 'volunteertraininginterestedin');
            $data = $this->volunteer->get('1', array('id' => $userId));
            $userData['userDetail'] = $data;
            $userData['userLanguage'] = $this->volunteerlanguage->get('', array('volunteerId' => $userId));
            $userData['userSkill'] = $this->volunteerskill->get('', array('volunteerId' => $userId));
            $userData['userTraining'] = $this->volunteertraining->get('', array('volunteerId' => $userId));
            $userData['userTrainingInterestedIn'] = $this->volunteertraininginterestedin->get('', array('volunteerId' => $userId));
        } else {
            $this->load->model('agency_model', 'agency');
            $userData = $this->agency->get('1', array('id' => $userId));
        }

        $this->_returnJson($userData);
    }

    public function getMasterData()
    {
        $this->load->model('common_model', 'common');

        $gender = $this->common->query('select * from nvp_gender');
        $zone = $this->common->query('select * from nvp_zone');
        $district = $this->common->query('select * from nvp_district');
        $municipality = $this->common->query('select * from nvp_municipality');
        $skill = $this->common->query('select * from nvp_skill where status = "Active" order by orderNumber asc');
        $language = $this->common->query('select * from nvp_language where status = "Active" order by orderNumber asc');
        $training = $this->common->query('select * from nvp_training where status = "Active" order by orderNumber asc');
        $academicQualification = $this->common->query('select * from nvp_academic_qualification where status = "Active" order by orderNumber asc');
        $masterData = array(
            'gender' => $gender,
            'zones' => $zone,
            'district' => $district,
            'municipality' => $municipality,
            'modeOfCommunication' => $GLOBALS['modeOfCommunications'],
            'academicQualification' => $academicQualification,
            'availability' => $GLOBALS['volunteerAvailability'],
            'volunteerType' => $GLOBALS['volunteerTypes'],
            'volunteerSubType' => $GLOBALS['volunteerSubTypes'],
            'skill' => $skill,
            'language' => $language,
            'training' => $training,
            'projectType' => $GLOBALS['projectTypes']
        );

        $this->_returnJson($masterData);
    }

    public function getContact()
    {
        $query = "select * from `tbl_telephone_directory` where `status` = 'Active' order by first_name asc";
        $this->load->model('telephone_directory_model', 'contact');
        $contacts = $this->contact->query($query);

        $this->_returnJson($contacts);
    }

    public function likePost()
    {
        $post = $this->input->post();
        $postId = isset($post['postId']) ? $post['postId'] : '';
        $userId = isset($post['userId']) ? $post['userId'] : '';
        $ip = $post['ip'];
        if($postId != '' && $userId != '') {
            $this->load->model('post_model', 'post');
            $this->load->model('postlike_model', 'postlike');

            // check if already liked
            $existingLike = $this->postlike->get('1', array('postId' => $postId, 'userId' => $userId));
            if($existingLike) {
                $this->postlike->delete(array('id' => $existingLike->id));
                $res = 'unlike';
            } else {
                $likeData = array(
                    'postId' => $postId,
                    'userId' => $userId,
                    'ip' => $ip,
                    'userType' => $post['userType']
                );
                $this->postlike->save($likeData);
                $res = 'like';
            }
            $res = array(
                'action' => $res,
                'totalLikes' => $this->postlike->countLikes($postId)
            );
        } else {
            $res = 'post id or user id or user type not sent';
        }
        $this->_returnJson($res);
    }

    public function getComments($postIdExplicit = '')
    {
        $post = $this->input->post();
        $limit = isset($post['limit']) ? $post['limit'] : 2;
        if(isset($post['getAll'])) {
            $limit = false;
        }
        if($postIdExplicit != '') {
            $postId = $postIdExplicit;
        } else {
            $postId = ($post['postId']) ? $post['postId'] : '';
        }
        if($postId != '') {
            $this->load->model('post_model', 'post');
            $this->load->model('postcomment_model', 'postcomment');
            $res = $this->postcomment->getComments($postId, $limit);
        } else {
            $res = 'post slug not sent';
        }
        $this->_returnJson($res);
    }

    public function saveComments()
    {
        $post = $this->input->post();
        $postId = isset($post['postId']) ? $post['postId'] : '';
        $userId = isset($post['userId']) ? $post['userId'] : '';
        $comment = isset($post['comment']) ? $post['comment'] : '';
        $ip = $post['ip'];
        if($postId != '' && $userId != '') {
            $this->load->model('post_model', 'post');
            $this->load->model('postcomment_model', 'postcomment');

            $commentData = array(
                'postId' => $postId,
                'userId' => $userId,
                'ip' => $ip,
                'comment' => $comment,
                'userType' => $post['userType']
            );
            $this->postcomment->save($commentData);
            $res = $postId;
        } else {
            $res = 'post slug or user id or user type not sent';
        }
        $this->_returnJson($res);
    }

    public function getIP()
    {
        $this->_returnJson($_SERVER['REMOTE_ADDR']);
    }

    public function getAddress()
    {
        $this->load->model('configuration_model', 'configuration');
        $addressData = $this->configuration->get('1');
        $this->_returnJson($addressData->infobox);
    }

    public function getAddressFromLatLong()
    {
        $latLong = $this->input->post('latLong');
        $latLong = explode(',', $latLong);
        $lat = $latLong[0];
        $lng = $latLong[1];
        $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($lat) . ',' . trim($lng) . '&sensor=false';
        $json = @file_get_contents($url);
        $data = json_decode($json);
        $status = $data->status;
        if ($status == "OK")
            $this->_returnJson($data->results[0]->address_components[1]->short_name.' '.$data->results[0]->address_components[3]->short_name);
        else
            $this->_returnJson(false);
    }

    public function saveVolunteer()
    {
        if($_POST) {
            $userId = $_POST['userId'];
            $data = $_POST['data'];
            $dataType = $_POST['dataType'];
            switch ($dataType) {
                case 'personal':
                    $this->load->model('volunteer_model', 'volunteer');
                    $query = "select id from `nvp_volunteer` v where v.`id` != {$userId} and (v.`email` = '{$data['email']}' or v.`phoneNumberMobile` = '{$data['phoneNumberMobile']}')";
                    $dataRepeatCheck = $this->volunteer->query($query);
                    if(!$dataRepeatCheck) {
                        $dataRepeatCheck = $this->volunteer->query("select id from `nvp_agency` a where a.`email` = '{$data['email']}' or a.`phoneNumber` = '{$data['phoneNumberMobile']}'");
                    }
                    if(!$dataRepeatCheck) {
                        $saveData = $data;
                        $saveData['dateOfBirth'] = strtotime($data['dateOfBirth']);
                        $res = $this->volunteer->save($saveData, array('id' => $userId));
                    } else {
                        $repeatData = true;
                    }
                    break;
                case 'academic':
                    $this->load->model('volunteer_model', 'volunteer');
                    $res = $this->volunteer->save(
                        array('academicQualificationId' => $data),
                        array('id' => $userId)
                    );
                    break;
                case 'language':
                    $this->load->model('volunteerlanguage_model', 'volunteerlanguage');
                    $this->volunteerlanguage->delete(array('volunteerId' => $userId));
                    $res = false;
                    foreach($data as $key => $row) {
                        if($row) {
                            $languageData = array(
                                'volunteerId' => $userId,
                                'languageId' => $key
                            );
                            $res = $this->volunteerlanguage->save($languageData);
                        }
                    }
                    break;
                case 'training':
                    $this->load->model('volunteertraining_model', 'volunteertraining');
                    $this->volunteertraining->delete(array('volunteerId' => $userId));
                    $res = false;
                    foreach($data as $key => $row) {
                        if($row) {
                            $trainingData = array(
                                'volunteerId' => $userId,
                                'trainingId' => $key
                            );
                            $res = $this->volunteertraining->save($trainingData);
                        }
                    }
                    break;
                case 'skill':
                    $this->load->model('volunteerskill_model', 'volunteerskill');
                    $this->volunteerskill->delete(array('volunteerId' => $userId));
                    $res = false;
                    foreach($data as $key => $row) {
                        if($row) {
                            $skillData = array(
                                'volunteerId' => $userId,
                                'skillId' => $key
                            );
                            $res = $this->volunteerskill->save($skillData);
                        }
                    }
                    break;
                case 'trainingInterestedIn':
                    $this->load->model('volunteertraininginterestedin_model', 'volunteertraininginterestedin');
                    $this->volunteertraininginterestedin->delete(array('volunteerId' => $userId));
                    $res = false;
                    foreach($data as $key => $row) {
                        if($row) {
                            $volunteerTrainingInterestedInData = array(
                                'volunteerId' => $userId,
                                'trainingId' => $key
                            );
                            $res = $this->volunteertraininginterestedin->save($volunteerTrainingInterestedInData);
                        }
                    }
                    break;
                case 'availability':
                    $this->load->model('volunteer_model', 'volunteer');
                    $saveData = array(
                        'availability' => $data['availability'],
                        'volunteerType' => $data['volunteerType']
                    );
                    if($data['volunteerType'] == 'partTime') {
                        $saveData['volunteerSubType'] = $data['volunteerSubType'];
                    }
                    $res = $this->volunteer->save($saveData, array('id' => $userId));
                    break;
                case 'modeofCommunication':
                    $this->load->model('volunteer_model', 'volunteer');
                    $saveData = array(
                        'primaryModeOfCommunication' => $data['primaryModeOfCommunication'],
                        'secondaryModeOfCommunication' => $data['secondaryModeOfCommunication']
                    );
                    $res = $this->volunteer->save($saveData, array('id' => $userId));
                    break;
                default:
                    $res = false;
                    break;
            }
            if($res) {
                $responseMsg = 'Profile saved successfully';
                $responseCode = 200;
            } else {
                if(isset($repeatData)) {
                    $responseMsg = 'Email or phone number already in use';
                    $responseCode = 401;
                } else {
                    $responseMsg = 'Profile could not be saved';
                    $responseCode = 401;
                }
            }
        } else {
            $responseMsg = 'No data sent';
            $responseCode = 400;
        }
        $this->_returnJson($responseMsg, $responseCode);
    }

    public function changePassword()
    {
        $post = $_POST;
        if(!empty($post)) {
            if($post['userType'] == 'Volunteer') {
                $this->load->model('volunteer_model', 'volunteer');
                $userData = $this->volunteer->get('1', array('id' => $post['userId']));
                if($userData) {
                    if(passwordVerify($post['oldPassword'], $userData->password)) {
                        $res = $this->volunteer->save(
                            array('password' => passwordHash($post['newPassword'])),
                            array('id' => $post['userId'])
                        );
                        if($res) {
                            $responseMsg = 'Password changed successfully';
                            $responseCode = 200;
                        } else {
                            $responseMsg = 'Password could not be changed';
                            $responseCode = 401;
                        }
                    } else {
                        $responseMsg = 'Old password did not match';
                        $responseCode = 401;
                    }
                } else {
                    $responseMsg = 'Credentials did not match';
                    $responseCode = 401;
                }
            } else {
                $this->load->model('agency_model', 'agency');
                $userData = $this->agency->get('1', array('id' => $post['userId']));
                if($userData) {
                    if(passwordVerify($post['oldPassword'], $userData->password)) {
                        $res = $this->agency->save(
                            array('password' => passwordHash($post['newPassword'])),
                            array('id' => $post['userId'])
                        );
                        if($res) {
                            $responseMsg = 'Password changed successfully';
                            $responseCode = 200;
                        } else {
                            $responseMsg = 'Password could not be changed';
                            $responseCode = 401;
                        }
                    } else {
                        $responseMsg = 'Old password did not match';
                        $responseCode = 401;
                    }
                } else {
                    $responseMsg = 'Credentials did not match';
                    $responseCode = 401;
                }
            }
        } else {
            $responseMsg = 'No data sent';
            $responseCode = 400;
        }
        $this->_returnJson($responseMsg, $responseCode);
    }

    public function createProject()
    {
        $post = $_POST;
        if($post) {
            $this->load->model('agencyproject_model', 'agencyproject');
            $saveData = array(
                'name' => $post['name'],
                'projectType' => $post['type'],
                'totalNumberOfDaysVolunteersAreNeeded' => $post['totalNumberOfDaysVolunteersAreNeeded'],
                'totalNumberOfHoursPerVolunteer' => $post['totalNumberOfHoursPerVolunteer'],
                'dateOfEvent' => strtotime($post['dateOfEvent']),
                'timeOfEvent' => date('h:i A', strtotime($post['timeOfEvent'])),
                'purpose' => $post['purpose'],
                'additionalFacilitiesProvidedByAgency' => $post['additionalFacilitiesProvidedByAgency'],
                'additionalRemarks' => $post['additionalRemarks'],
                'agencyId' => $post['userId']
            );
            $saveData['slug'] = $this->agencyproject->createSlug($saveData['name']);
            $res = $this->agencyproject->save($saveData);
            if($res) {
                $responseMsg = 'Project created successfully. Now you can further add the project criteria.';
                $responseCode = 200;
            } else {
                $responseMsg = 'Project could not be created successfully.';
                $responseCode = 403;
            }
        } else {
            $responseMsg = 'No data sent';
            $responseCode = 400;
        }
        $this->_returnJson($responseMsg, $responseCode);
    }

    public function saveProjectData()
    {
        $post = $_POST;
        if($post) {
            $res = false;
            $this->load->model('agencyproject_model', 'agencyproject');
            $this->load->model('agencyprojectcriteria_model', 'agencyprojectcriteria');
            $this->load->model('agencyprojectcriteriaacademic_model', 'agencyprojectcriteriaacademic');
            $this->load->model('agencyprojectcriteriagender_model', 'agencyprojectcriteriagender');
            $this->load->model('agencyprojectcriteriaskill_model', 'agencyprojectcriteriaskill');
            $this->load->model('agencyprojectcriteriatraining_model', 'agencyprojectcriteriatraining');

            $projectId = $post['projectId'];
            $projectCriteriaId = $this->agencyprojectcriteria->get('1', array('agencyProjectId' => $projectId));
            if($projectCriteriaId) {
                $projectCriteriaId = $projectCriteriaId->id;
            }

            switch ($post['dataType']) {
                case 'projectDetail':
                    $saveData = array(
                        'name' => $post['projectDetail']['name'],
                        'projectType' => $post['projectDetail']['projectType'],
                        'totalNumberOfDaysVolunteersAreNeeded' => $post['projectDetail']['totalNumberOfDaysVolunteersAreNeeded'],
                        'totalNumberOfHoursPerVolunteer' => $post['projectDetail']['totalNumberOfHoursPerVolunteer'],
                        'dateOfEvent' => strtotime($post['projectDetail']['dateOfEvent']),
                        'timeOfEvent' => date('h:i A', strtotime($post['projectDetail']['timeOfEvent'])),
                        'purpose' => $post['projectDetail']['purpose'],
                        'additionalFacilitiesProvidedByAgency' => $post['projectDetail']['additionalFacilitiesProvidedByAgency'],
                        'additionalRemarks' => $post['projectDetail']['additionalRemarks']
                    );
                    $saveData['slug'] = $this->agencyproject->createSlug($saveData['name'], $projectId);
                    $res = $this->agencyproject->save($saveData, array('id' => $projectId));
                    break;
                case 'projectAcademicQualification':
                    if(!$projectCriteriaId) {
                        $this->_returnJson('Please fill up Project Criteria Setting first');
                        die;
                    }
                    $projectCriteriaData['isMinimumEducationRequired'] = 'Yes';
                    $this->agencyprojectcriteriaacademic->delete(array('agencyProjectCriteriaId' => $projectCriteriaId));
                    foreach($post['projectDetail'] as $qualification => $state) {
                        if($state) {
                            $insertData = array(
                                'agencyProjectCriteriaId' => $projectCriteriaId,
                                'academicQualificationId' => $qualification
                            );
                            $res = $this->agencyprojectcriteriaacademic->save($insertData);
                        }
                    }
                    break;
                case 'projectTraining':
                    if(!$projectCriteriaId) {
                        $this->_returnJson('Please fill up Project Criteria Setting first');
                        die;
                    }
                    $projectCriteriaData['isTrainingRequired'] = 'Yes';
                    $projectExtraData['otherTraining'] = $post['extraData']['otherTraining'];
                    $this->agencyprojectcriteriatraining->delete(array('agencyProjectCriteriaId' => $projectCriteriaId));
                    foreach($post['projectDetail'] as $training => $state) {
                        if($state) {
                            $insertData = array(
                                'agencyProjectCriteriaId' => $projectCriteriaId,
                                'trainingId' => $training
                            );
                            $res = $this->agencyprojectcriteriatraining->save($insertData);
                        }
                    }
                    break;
                case 'projectSkill':
                    if(!$projectCriteriaId) {
                        $this->_returnJson('Please fill up Project Criteria Setting first');
                        die;
                    }
                    $projectCriteriaData['isSkillRequired'] = 'Yes';
                    $projectExtraData['otherSkills'] = $post['extraData']['otherSkills'];
                    $this->agencyprojectcriteriaskill->delete(array('agencyProjectCriteriaId' => $projectCriteriaId));
                    foreach($post['projectDetail'] as $skill => $state) {
                        if($state) {
                            $insertData = array(
                                'agencyProjectCriteriaId' => $projectCriteriaId,
                                'skillId' => $skill
                            );
                            $res = $this->agencyprojectcriteriaskill->save($insertData);
                        }
                    }
                    break;
                case 'projectGender':
                    if(!$projectCriteriaId) {
                        $this->_returnJson('Please fill up Project Criteria Setting first');
                        die;
                    }
                    $projectCriteriaData['isGenderRequired'] = 'Yes';
                    $this->agencyprojectcriteriagender->delete(array('agencyProjectCriteriaId' => $projectCriteriaId));
                    foreach($post['projectDetail'] as $gender => $state) {
                        if($state) {
                            $insertData = array(
                                'agencyProjectCriteriaId' => $projectCriteriaId,
                                'genderId' => $gender
                            );
                            $res = $this->agencyprojectcriteriagender->save($insertData);
                        }
                    }
                    break;
                case 'projectMainCriteria':
                    $projectCriteriaData = $post['projectDetail'];
                    break;
                default:
                    break;
            }
            if(isset($projectCriteriaData)) {
                $projectCriteriaData['agencyProjectId'] = $projectId;
                if(!$projectCriteriaId)
                    $res = $this->agencyprojectcriteria->save($projectCriteriaData, '', true);
                else
                    $res = $this->agencyprojectcriteria->save($projectCriteriaData, array('id' => $projectCriteriaId));
            }
            if(isset($projectExtraData) && (isset($res) && $res)) {
                $res = $this->agencyproject->save($projectExtraData, array('id' => $projectId));
            }

            if($res) {
                $responseMsg = 'Project updated successfully';
                $responseCode = 200;
            } else {
                $responseMsg = 'Project could not be updated. Please try again later';
                $responseCode = 401;
            }
        } else {
            $responseMsg = 'No data sent';
            $responseCode = 400;
        }
        $this->_returnJson($responseMsg, $responseCode);
    }

    public function getAgencyTypes()
    {
        $this->load->model('agencytype_model', 'agencytype');
        $agencyTypes = $this->agencytype->get('', array('status' => 'Active'), 'orderNumber asc');
        if($agencyTypes) {
            $responseMsg = $agencyTypes;
            $responseCode = 200;
        } else {
            $responseMsg = 'Agency types could not be loaded';
            $responseCode = 401;
        }
        $this->_returnJson($responseMsg, $responseCode);
    }

    public function saveAgencyProfileData()
    {
        $post = $_POST;
        if($post) {
            $this->load->model('agency_model', 'agency');
            $query = "select id from `nvp_agency` a where  a.`id` != {$post['agencyData']['id']} and (a.`email` = '{$post['agencyData']['email']}' or a.`phoneNumber` = '{$post['agencyData']['phoneNumber']}')";
            $dataRepeatCheck = $this->agency->query($query);
            if(!$dataRepeatCheck) {
                $dataRepeatCheck = $this->agency->query("select id from `nvp_volunteer` v where v.`email` = '{$post['agencyData']['email']}' or v.`phoneNumberMobile` = '{$post['agencyData']['phoneNumber']}'");
            }
            if(!$dataRepeatCheck) {
                $res = $this->agency->save($post['agencyData'], array('id' => $post['agencyData']['id']));
            } else {
                $repeatData = true;
            }
            if($res) {
                $responseMsg = 'Profile saved successfully';
                $responseCode = 200;
            } else {
                if(isset($repeatData)) {
                    $responseMsg = 'Email or phone number already in use';
                    $responseCode = 401;
                } else {
                    $responseMsg = 'Profile could not be saved';
                    $responseCode = 401;
                }
            }
        } else {
            $responseMsg = 'No profile data sent';
            $responseCode = 400;
        }
        $this->_returnJson($responseMsg, $responseCode);
    }

    public function sendFeedBack()
    {
        $post = $_POST;
        if($post) {
            $this->load->model('configuration_model', 'configuration');
            $this->load->model('formdata_model', 'formdata');
            $this->load->library('email');

            // data form email template
            $globalConfig = $this->configuration->get('1');
            $siteLogo = base_url($globalConfig->site_logo);
            $siteLink = base_url();
            $this->load->model('emailtemplate_model', 'emailtemplate');
            $contactUsEmailTemplate = $this->emailtemplate->getAllData(1);
            $customerMessage = str_replace(
                array('#siteLogo#', '#siteLink#', '#emailSubject#'),
                array($siteLogo, $siteLink, $contactUsEmailTemplate[0]->userSubject),
                $contactUsEmailTemplate[0]->userMessage
            );
            $adminMessage = str_replace(
                array('#siteLogo#', '#siteLink#', '#emailSubject#', '#customerName#', '#customerEmail#', '#customerMessage#'),
                array($siteLogo, $siteLink, $contactUsEmailTemplate[0]->adminSubject, $post['name'], $post['email'], $post['message']),
                $contactUsEmailTemplate[0]->adminMessage
            );

            $email_params = array(
                'subject' => $contactUsEmailTemplate[0]->userSubject,
                'from' => $contactUsEmailTemplate[0]->adminEmail,
                'fromname' => $globalConfig->site_title,
                'to' =>  $post['email'],
                'toname' => $post['name'],
                'message' => $customerMessage
            );
            if(swiftsend($email_params)) {
                $post['type'] = 'contact';
                $res = $this->formdata->save($post);
                $data['action'] = 'success';

                $email_params = array(
                    'subject' => $contactUsEmailTemplate[0]->adminSubject,
                    'from' => $post['email'],
                    'fromname' => $post['name'],
                    'to' => $contactUsEmailTemplate[0]->adminEmail,
                    'toname' => $globalConfig->site_title,
                    'message' => $adminMessage
                );
                swiftsend($email_params);
            }
            if($res) {
                $responseMsg = 'Thank you for your feedback';
                $responseCode = 200;
            } else {
                $responseMsg = 'Feedback not sent successfully';
                $responseCode = 401;
            }
        } else {
            $responseMsg = 'Feedback data not passed';
            $responseCode = 400;
        }
        $this->_returnJson($responseMsg, $responseCode);
    }

    public function saveProfilePicture()
    {
        $profilePicture = $_FILES['file'];
        $userId = $_POST['userId'];
        $res = false;
        if(strtolower($_POST['imageType']) == 'volunteer') {
            if($profilePicture['name'] != '') {
                $this->load->model('volunteer_model', 'volunteer');
                $target_dir = "uploads/volunteer/profile-picture/";
                if(!file_exists($target_dir)) {
                    mkdir($target_dir);
                }
                $target_file = $target_dir . basename($profilePicture["name"]);
                $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
                $imageFileType = ($imageFileType) ? $imageFileType : 'jpg';
                $target_file = $target_dir . time() . '.' . $imageFileType;

                if (move_uploaded_file($profilePicture["tmp_name"], $target_file)) {
                    $saveData['profilePicture'] = $target_file;
                    $res = $this->volunteer->save($saveData, array('id' => $userId));
                }
                if($res) {
                    $responseMsg = array(
                        'msg' => 'Profile picture changed successfully',
                        'newProfilePicture' => $target_file
                    );
                    $responseCode = 200;
                } else {
                    $responseMsg = 'Profile picture could not be changed';
                    $responseCode = 401;
                }
            } else {
                $responseMsg = 'No image sent';
                $responseCode = 400;
            }
        } elseif(strtolower($_POST['imageType']) == 'logo') {
            if($profilePicture['name'] != '') {
                $this->load->model('agency_model', 'agency');
                $target_dir = "uploads/agency/logo/";
                if(!file_exists($target_dir)) {
                    mkdir($target_dir);
                }
                $target_file = $target_dir . basename($profilePicture["name"]);
                $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
                $imageFileType = ($imageFileType) ? $imageFileType : 'jpg';
                $target_file = $target_dir . time() . '.' . $imageFileType;

                if (move_uploaded_file($profilePicture["tmp_name"], $target_file)) {
                    $saveData['logo'] = $target_file;
                    $res = $this->agency->save($saveData, array('id' => $userId));
                }
                if($res) {
                    $responseMsg = array(
                        'msg' => 'Agency logo changed successfully',
                        'newProfilePicture' => $target_file
                    );
                    $responseCode = 200;
                } else {
                    $responseMsg = 'Agency logo could not be changed';
                    $responseCode = 401;
                }
            } else {
                $responseMsg = 'No image sent';
                $responseCode = 400;
            }
        } elseif(strtolower($_POST['imageType'] == 'registration')) {
            if($profilePicture['name'] != '') {
                $this->load->model('agency_model', 'agency');
                $target_dir = "uploads/agency/certificates/";
                if(!file_exists($target_dir)) {
                    mkdir($target_dir);
                }
                $target_file = $target_dir . basename($profilePicture["name"]);
                $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
                $imageFileType = ($imageFileType) ? $imageFileType : 'jpg';
                $target_file = $target_dir . time() . '.' . $imageFileType;

                if (move_uploaded_file($profilePicture["tmp_name"], $target_file)) {
                    $saveData['registrationCertificateImage'] = $target_file;
                    $res = $this->agency->save($saveData, array('id' => $userId));
                }
                if($res) {
                    $responseMsg = array(
                        'msg' => 'Agency certificate image changed successfully',
                        'newProfilePicture' => $target_file
                    );
                    $responseCode = 200;
                } else {
                    $responseMsg = 'Agency certificate image could not be changed';
                    $responseCode = 401;
                }
            } else {
                $responseMsg = 'No image sent';
                $responseCode = 400;
            }
        }
        $this->_returnJson($responseMsg, $responseCode);
    }

    public function registerAppUser()
    {
        $this->load->model('appuser_model', 'appuser');
        $res = false;
        $appUserData = $this->input->post();
        if($appUserData) {
            $appUserExists = $this->appuser->get('1', array(
                'deviceToken' => $appUserData['deviceToken']
            ));
            if(!$appUserExists) {
                $res = $this->appuser->save($appUserData);
            } else {
                $res = $this->appuser->save($appUserData, array('deviceToken' => $appUserData['deviceToken']));
            }
            if($res) {
                $resMsg = 'App user saved';
                $resCode = 200;
            } else {
                $resMsg = 'App user could not be saved';
                $resCode = 401;
            }
        } else {
            $resMsg = 'App user data not sent';
            $resCode = 400;
        }
        $this->_returnJson($resMsg, $resCode);
    }
    /*public function registerAppUser()
    {
        $this->load->model('appuser_model', 'appuser');
        $res = false;
        $appUserData = $this->input->post();
        if($appUserData) {
            $appUserExists = $this->appuser->get('1', array(
                'userId' => $appUserData['userId'],
                'userType' => $appUserData['userType']
            ));
            if(!$appUserExists) {
                $res = $this->appuser->save($appUserData);
            } else {
                $res = $this->appuser->save($appUserData, array('userId' => $appUserData['userId'], 'userType' => $appUserData['userType']));
            }
            if($res) {
                $resMsg = 'App user saved';
                $resCode = 200;
            } else {
                $resMsg = 'App user could not be saved';
                $resCode = 401;
            }
        } else {
            $resMsg = 'App user data not sent';
            $resCode = 400;
        }
        $this->_returnJson($resMsg, $resCode);
    }*/

    public function searchProject()
    {
        $searchParams = $_POST;
        $this->db->select('*');
        $this->db->from('nvp_agency_project');
        if(isset($searchParams['projectName']) && $searchParams['projectName'] != '') {
            $this->db->like('name', $searchParams['projectName']);
        }
        if(isset($searchParams['projectStatus']) && $searchParams['projectStatus'] != '') {
            switch ($searchParams['projectStatus']) {
                case 'completed':
                    $this->db->where('status', 'Completed');
                    break;
                case 'ongoing':
                    $this->db->where('status', 'Ongoing');
                    break;
                default:
                    $this->db->where('status != ', 'InActive');
                    break;
            }
        } else {
            $this->db->where('status != ', 'InActive');
        }
        $this->db->order_by('dateOfEvent', 'desc');
        $this->db->order_by('status', 'asc');
        $project = $this->db->get();
        return $project->num_rows() > 0 ? $this->_returnJson($project->result()) : $this->_returnJson(false, 403);
    }

    public function notificationResponse()
    {
        $post = $_POST;
        if($post) {
            $this->load->model('agencyprojectvolunteerinvitee_model', 'agencyprojectvolunteerinvitee');
            if($post['notificationResponse'] == 'accept') {
                $responseType = 'Accepted';
            } else {
                $responseType = 'Rejected';
            }
            $inviteeSaveData = array(
                'status' => $responseType,
                'responseDate' => time()
            );
            $res = $this->agencyprojectvolunteerinvitee->save(
                $inviteeSaveData,
                array(
                    'volunteerId' => $post['userId'],
                    'agencyProjectId' => $post['projectId'],
                    'notificationId' => $post['notificationId']
                )
            );
            
            if($res) {
                $resMsg = 'Your response is saved. Thank you';
                $resCode = 200;
            } else {
                $resMsg = 'Your response could not be saved. Please try again';
                $resCode = 403;
            }
        } else {
            $resMsg = 'No data sent';
            $resCode = 400;
        }
        $this->_returnJson($resMsg, $resCode);
    }

    public function getNotifications()
    {
        $post = $_POST;
        if($post) {
            $this->load->model('notification_model', 'notification');
            $this->load->model('agencyprojectvolunteerinvitee_model', 'agencyprojectvolunteerinvitee');
            $query = "select n.*, apvi.status as notificationStatus from nvp_agency_project_volunteer_invitee apvi
                      join tbl_notification n on n.id = apvi.notificationId
                      and apvi.volunteerId = ".$post['userId'] . " order by n.id desc";
            $generalNotification = $this->notification->get('', array('projectId' => 'null'), 'id desc');
            $projectNotification = $this->agencyprojectvolunteerinvitee->query($query);
            $resMsg = array(
                'generalNotification' => $generalNotification,
                'projectNotification' => $projectNotification
            );
            $resCode = 200;
        } else {
            $resMsg = 'No data sent';
            $resCode = 400;
        }
        $this->_returnJson($resMsg, $resCode);
    }

    public function increaseShare()
    {
        $post = $_POST['postId'];
        $this->db->where('id', $post);
        $this->db->set('totalShares', 'totalShares+1', FALSE);
        $this->db->update('nvp_post');
        $this->_returnJson('true');
    }

    public function changeAccountStatus()
    {
        $post = $_POST;
        $accountStatus = $post['account_status'] ? 'Active' : 'InActive';
        $this->load->model('volunteer_model', 'volunteer');
        $res = $this->volunteer->save(array('account_status' => $accountStatus), array('id' => $post['userId']));
        $msg = $res ? 'Account status changed successfully' : 'Account status could not be changed';
        $this->_returnJson($msg);
    }

    public function changeNotificationStatus()
    {
        $post = $_POST;
        $notificationStatus = $post['notification_status'] ? 'Active' : 'InActive';
        $this->load->model('volunteer_model', 'volunteer');
        $res = $this->volunteer->save(array('notification_status' => $notificationStatus), array('id' => $post['userId']));
        $msg = $res ? 'Notification status changed successfully' : 'Notification status could not be changed';
        $this->_returnJson($msg);
    }

    public function projectremove()
    {
        $post = $_POST;
        $this->load->model('agencyproject_model', 'agencyproject');
        $res = $this->agencyproject->save(['status'=>'InActive'],array('id' => $post['projectId']));
        if($res) {
            $msg = 'Project has been removed';
        } else {
            $msg = 'Project could not be removed';
        }
        $this->_returnJson($msg);
    }

    public function forgetPassword()
    {
        $post = $_POST;
        if($post) {
            $validTimestamp = time();

            $this->load->model('volunteer_model', 'volunteer');
            $this->load->model('configuration_model', 'configuration');
            $this->load->model('agency_model', 'agency');
            $userData = $this->volunteer->get('1', array('email' => $post['email']));
            if(!$userData) {
                $agency = true;
                $userData = $this->agency->get('1', array('email' => $post['email']));
            }

            if($userData) {
                if(isset($agency)) {
                    $this->agency->save(array('passwordChangeTimeStamp' => $validTimestamp), array('id' => $userData->id));
                } else {
                    $this->volunteer->save(array('passwordChangeTimeStamp' => $validTimestamp), array('id' => $userData->id));
                }

                $this->load->model('emailtemplate_model', 'emailtemplate');
                $volunteerRegistrationEmailTemplate = $this->emailtemplate->getAllData(5);
                $global_config = $this->configuration->get('1');
                $siteLogo = base_url($global_config->site_logo);
                $siteLink = base_url();
                $timeStamp = $validTimestamp . '~' . hash('md5', $userData->id);
                $passwordchangeLink = base_url('members/retrievepassword/resetpassword/' . $timeStamp);
                $volunteerMessage = str_replace(
                    array('#siteLogo#', '#siteLink#', '#emailSubject#', '#passwordChangeLink#'),
                    array($siteLogo, $siteLink, $volunteerRegistrationEmailTemplate[0]->userSubject, $passwordchangeLink),
                    $volunteerRegistrationEmailTemplate[0]->userMessage
                );
                //$emailBody = $this->load->view('email/resetpassword', array('timeStamp' => $validTimestamp . '~' . hash('md5', $userData->id)), true);
                $email_params = array(
                    'subject' => 'Reset Account Password',
                    'from' => $global_config->site_email,
                    'fromname' => $global_config->site_title,
                    'to' => $post['email'],
                    'message' => $volunteerMessage
                );
                swiftsend($email_params);
                $msg = 'Reset password link has been sent to your inbox';
                $code = 200;
            } else {
                $msg = 'User not found';
                $code = 200;
            }
        } else {
            $msg = 'Email address not sent';
            $code = 401;
        }
        $this->_returnJson($msg, $code);
    }

    public function sendRequest()
    {
        $post = $_POST;
        if($post) {
            $this->load->model('configuration_model', 'configuration');
            $this->load->model('agency_model', 'agency');
            $this->load->model('agencyproject_model', 'agencyproject');
            $this->load->model('emailtemplate_model', 'emailtemplate');
            $projectName = $this->agencyproject->get('1', array('id' => $post['projectId']))->name;
            $userData = $this->agency->get('1', array('id' => $post['agencyId']));
            $agencyEmail = $userData->email;
            $agencyName = $userData->name;
            $searchEmailTemplate = $this->emailtemplate->getAllData(4);
            $global_config = $this->configuration->get('1');
            $siteLogo = base_url($global_config->site_logo);
            $siteLink = base_url();
            $projectLink = base_url(BACKENDFOLDER . '/agency_project_data/view_detail/' . $post['projectId']);
            $adminMessage = str_replace(
                array('#siteLogo#', '#siteLink#', '#emailSubject#', '#projectDetail#', '#agencyName#', '#projectName#'),
                array($siteLogo, $siteLink, $searchEmailTemplate[0]->userSubject, $projectLink, $agencyName, $projectName),
                $searchEmailTemplate[0]->adminMessage
            );
            $userMessage = str_replace(
                array('#siteLogo#', '#siteLink#', '#emailSubject#'),
                array($siteLogo, $siteLink, $searchEmailTemplate[0]->userSubject, $projectLink),
                $searchEmailTemplate[0]->userMessage
            );
            $email_params = array(
                'subject' => $searchEmailTemplate[0]->userSubject,
                'from' => $global_config->site_email,
                'fromname' => $global_config->site_title,
                'to' => $agencyEmail,
                'message' => $userMessage
            );
            swiftsend($email_params);

            $email_params = array(
                'subject' => $searchEmailTemplate[0]->adminSubject,
                'from' => $agencyEmail,
                'fromname' => $agencyName,
                'to' => $searchEmailTemplate[0]->adminEmail,
                'message' => $adminMessage
            );
            swiftsend($email_params);
            $msg = 'Request has been sent';
            $code = 200;
        } else {
            $msg = 'Request could not be sent';
            $code = 401;
        }
        $this->_returnJson($msg, $code);
    }
    
    public function getDisasterTip()
    {
        $this->load->model('content_model', 'content');
        $content = $this->content->get('1', array('id' => 67));
        if($content) {
            $this->_returnJson($content->long_description);
        } else {
            $this->_returnJson('Disaster tip could not be loaded', 401);
        }
    }

    public function flagContent()
    {
        $flag_data = $this->input->post();
        if($flag_data) {
            if($flag_data['flagged_by_type'] == 'Agency') {
                $this->load->model('agency_model', 'agency');
                $user = $this->agency->get('1', array('id' => $flag_data['flagged_by']));
            } else {
                $this->load->model('volunteer_model', 'volunteer');
                $user = $this->volunteer->get('1', array('id' => $flag_data['flagged_by']));
            }
            $this->load->model('common_model', 'common');
            $this->common->table = 'tbl_flag_data';
            $res = $this->common->save($flag_data, '', true);
            $msg = $res ? 'Content flagged successfully' : 'Content could not be flagged';

            if($res) {
                $this->load->model('emailtemplate_model', 'emailtemplate');
                $this->load->model('configuration_model', 'configuration');
                $globalConfig = $this->configuration->get('1');
                $siteLogo = base_url($globalConfig->site_logo);
                $siteLink = base_url();
                $flag_link = $flag_data['data_type'] == 'Comment' ? base_url('nvp-system/comment/view_detail/'.$flag_data['data_id']) : base_url('nvp-system/post_data/view_detail/'.$flag_data['data_id']);
                $newPostEmailTemplate = $this->emailtemplate->get('1', ['name' => 'flag_notification']);
                $adminMessage = str_replace(
                    array('#siteLogo#', '#siteLink#', '#emailSubject#', '#reportLink#'),
                    array($siteLogo, $siteLink, $newPostEmailTemplate->adminSubject, $flag_link),
                    $newPostEmailTemplate->adminMessage
                );
                $email_params = array(
                    'subject' => $newPostEmailTemplate->adminSubject,
                    'from' => $user->email,
                    'fromname' => ($flag_data['flagged_by_type'] == 'Agency') ? $user->name : $user->fullName,
                    'to' => $newPostEmailTemplate->adminEmail,
                    'toname' => $globalConfig->site_title,
                    'message' => $adminMessage
                );
                swiftsend($email_params);
            }

            $this->_returnJson($msg, 200);
        } else {
            $this->_returnJson('Nothing to flag', 200);
        }
    }
}