<?php

class Projects extends My_Front_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    function _remap($method)
    {
        $param_offset = 2;

        // Default to index
        if ( ! method_exists($this, $method))
        {
            // We need one more param
            $param_offset = 1;
            $method = 'index';
        }

        // Since all we get is $method, load up everything else in the URI
        $params = array_slice($this->uri->rsegment_array(), $param_offset);

        // Call the determined method with all params
        call_user_func_array(array($this, $method), $params);
    }

    public function index($projectSlug = '')
    {
        $this->load->model('project_model', 'project');
        $this->load->model('content_model', 'content');
        $this->load->model('menu_model', 'menu');
        if($projectSlug != '') {
            $this->data['body'] = 'frontend/projects/_detail';
            $this->data['projectDetail'] = $this->project->getBySlug($projectSlug);
            if(!$this->data['projectDetail']) {
                show_404();
            }
        } else {
            $this->data['body'] = 'frontend/projects/_index';
            $this->data['allProjects'] = $this->project->getAllActive();
            $this->data['menu_alias'] = $this->uri->segment(1);
            $projectContents = $this->menu->get('1', ['menu_alias' => $this->data['menu_alias']]);
            $resp = array();
            if($projectContents) {
                    $resp[] = array(
                        'title' => $projectContents->menu_title,
                        'description' => strip_tags($projectContents->description)
                    );
            }
            $this->data['projectContent'] = $resp;
        }

        // loading custom js for home page
        $this->data['addJs'] = array(
            'assets/js/projects.js'
        );

        $this->render();
    }

}