<?php

class Contact extends My_Front_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        $this->load->model('configuration_model', 'configuration');
        $this->load->model('menu_model', 'menu');

        $this->data['menu_alias'] = $this->uri->segment(1);
        $contactContents = $this->menu->get('1', ['menu_alias' => $this->data['menu_alias']]);
        $resp = array();
        if($contactContents) {
            $resp[] = array(
                'title' => $contactContents->menu_title,
                'description' => strip_tags($contactContents->description)
            );
        }
        $this->data['contactContents'] = $resp;

        $this->data['contacts'] = $this->configuration->get('1');
        $this->data['body'] = 'frontend/content/_contact';
        $this->data['addJs'] = array(
            'assets/jquery-validate/jquery.validate.min.js',
            'assets/js/validateCustom.js',
            'assets/jquery-validate/additional-methods.min.js',
            'assets/js/form-validate.js',
            'assets/js/inner-page.js',
            'assets/js/contact.js'
        );
        $this->render();
    }

    public function form(){
        if($this->input->is_ajax_request()) {
            $post['name'] = $this->input->post('full-name');
            $post['email'] = $this->input->post('email');
            $post['message'] = $this->input->post('contact-msg');

            $this->load->model('configuration_model', 'configuration');
            $this->load->model('formdata_model', 'formdata');
            $this->load->library('email');

            // data form email template
            $siteLogo = base_url($this->global_config->site_logo);
            $siteLink = base_url();
            $this->load->model('emailtemplate_model', 'emailtemplate');
            $contactUsEmailTemplate = $this->emailtemplate->getAllData(1);
            $customerMessage = str_replace(
                array('#siteLogo#', '#siteLink#', '#emailSubject#'),
                array($siteLogo, $siteLink, $contactUsEmailTemplate[0]->userSubject),
                $contactUsEmailTemplate[0]->userMessage
            );
            $adminMessage = str_replace(
                array('#siteLogo#', '#siteLink#', '#emailSubject#', '#customerName#', '#customerEmail#', '#customerMessage#'),
                array($siteLogo, $siteLink, $contactUsEmailTemplate[0]->adminSubject, $post['name'], $post['email'], $post['message']),
                $contactUsEmailTemplate[0]->adminMessage
            );

            $email_params = array(
                'subject' => $contactUsEmailTemplate[0]->userSubject,
                'from' => SITEMAIL,
                'fromname' => SITENAME,
                'to' =>  $post['email'],
                'toname' => $post['name'],
                'message' => $customerMessage
            );
            if(swiftsend($email_params))  {
                $post['type'] = 'contact';
                $res=$this->formdata->save($post);
                $data['action'] = 'success';

                $email_params = array(
                    'subject' => $contactUsEmailTemplate[0]->adminSubject,
                    'from' => $post['email'],
                    'fromname' => $post['name'],
                    'to' =>  $contactUsEmailTemplate[0]->adminEmail,
                    'toname' => SITENAME,
                    'message' => $adminMessage
                );
                swiftsend($email_params);

            }
            echo json_encode($data);
        }
    }


}
