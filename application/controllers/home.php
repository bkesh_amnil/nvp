<?php

class Home extends My_Front_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {


        $this->load->model('banner_model', 'banner');
        $this->load->model('impact_model', 'impact');
        $this->load->model('content_model', 'content');
        $this->load->model('partner_model', 'partner');
        $this->load->model('testimonial_model', 'testimonial');

        $this->data['body'] = 'frontend/home/_index';
        $this->data['homeBanner'] = $this->banner->get_by_category('home-banner');
        $this->data['homeImpacts'] = $this->impact->get('', array('status' => 'Active'));
        $this->data['homeNews'] = $this->content->getFeaturedNews();
        $homePartners = $this->partner->getAllActive();
        $this->data['homeContents'] = $this->content->getByCategory('home-content');
        $this->data['happiningContents'] = $this->content->getByCategory('whats-happining');

        $resp = array();
        if ($homePartners) {
            foreach ($homePartners as $homePartner) {
                $resp[$homePartner->categorySlug][] = $homePartner;
            }
        }
        $this->data['homePartners'] = $resp;



        $this->data['homeTestimonials'] = $this->testimonial->get('', array('status' => 'Active'));

        // loading custom js for home page
        $this->data['addJs'] = array(
            'assets/jquery-validate/jquery.validate.min.js',
            'assets/js/validateCustom.js',
            'assets/jquery-validate/additional-methods.min.js',
            'assets/js/form-validate.js',
            'assets/js/home.js',
            'assets/counter-master/waypoints.min.js',
            'assets/counter-master/jquery.counterup.js',
            'assets/js/landing.js'
        );

        $this->render();
    }

    public function content_menu()
    {
        $this->load->model('banner_model', 'banner');
        $this->load->model('content_model', 'content');
        $this->load->model('menu_model', 'menu');

        $menu_alias = $this->uri->segment(1);
        $menu_content = $this->menu->get('1', ['menu_alias' => $menu_alias]);
        if ($menu_content->menu_link_type != 'none') {
        }
        $this->data['body'] = 'frontend/home/_about';
        $this->data['aboutBanner'] = $this->banner->get_by_category('about-banner');
        $this->data['about'] = $this->content->get('', array('status' => 'Active'));

        $this->data['addJs'] = array(
            'assets/js/inner-page.js'
        );

        $this->render();
    }

    public function review()
    {
        $this->load->model('testimonial_model', 'testimonial');
        if ($_POST) {
            $post['name'] = $_POST['name'];
            $post['message'] = $_POST['message'];
            if ($_FILES) {
                $target_dir = "uploads/images/testimonial";
                $target_file = $target_dir . basename($_FILES["image"]["name"]);
                $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
                // Check if image file is a actual image or fake image
                if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif"
                ) {
                    $message = array('type' => 'danger', 'message' => 'Sorry, only JPG, JPEG, PNG & GIF files are allowed.');
                } else {
                    if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
                        $post['image'] = $target_file;
                        $res = $this->testimonial->save($post);
                        if ($res) {
                            $message = array('type' => 'success', 'message' => 'Thanks for your review');
                        }
                    } else {
                        $message = array('type' => 'danger', 'message' => 'Sorry, there was an error uploading your file.');
                    }
                }
            } else {
                $message = array('type' => 'danger', 'message' => 'Upload your image');
            }
        } else {
            $message = array('type' => 'danger', 'message' => 'Fill the fields');
        }
        echo json_encode($message);
    }

    public function notificationcookie()
    {
        $this->load->model('notificationcount_model', 'notificationcount');
        $this->notificationcount->count_notification($this->data['allUserNotifications']);
        $notificationCookie = get_userdata('userType').get_userdata('active_user');
        if($_COOKIE[$notificationCookie]) {
            setcookie($notificationCookie, '0', '0', '/');
        }
    }

    public function change_picture()
    {
        if(get_userdata('userType') == 'Volunteer') {
            $model = 'volunteer';
        } else {
            $model = 'agency';
        }
        $this->load->model($model.'_model', $model);
        $user = $this->$model->get('1', array('id' => get_userdata('active_user')));
        if($user) {
            if($model == 'volunteer') {
                $picture = $user->profilePicture;
            } else {
                $picture = $user->logo;
            }
            if (strpos($picture, 'https') !== false || strpos($picture, 'http') !== false) {
                set_userdata(array(
                    'profilePicture' => $picture
                ));
            } else {
                set_userdata(array(
                    'profilePicture' => ($picture) ? base_url($picture) : base_url('assets/img/icon/icon-user-default.png')
                ));
            }
        }
    }

}