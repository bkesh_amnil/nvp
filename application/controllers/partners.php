<?php

class Partners extends My_Front_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->model('partner_model', 'partner');
        $this->load->model('content_model', 'content');
        $this->load->model('menu_model', 'menu');
        $this->data['body'] = 'frontend/partner/_index';
        $allPartners = $this->partner->getAllActive();
        foreach($allPartners as $partners) {
            $partner[$partners->categorySlug][] = $partners;
        }
        $this->data['allPartners'] = $partner;
        $this->data['menu_alias'] = $this->uri->segment(1);
        $partnerContents = $this->menu->get('1', ['menu_alias' => $this->data['menu_alias']]);
        $resp = array();
        if($partnerContents) {
                $resp[] = array(
                    'title' => $partnerContents->menu_title,
                    'description' => strip_tags($partnerContents->description)
                );
        }
        $this->data['partnerContent'] = $resp;

        // loading custom js for home page
        $this->data['addJs'] = array(
            'assets/js/partners.js'
        );

        $this->render();
    }

}