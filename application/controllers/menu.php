<?php

class Menu extends My_Front_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->model('menu_model', 'menu');

        $this->data['menu_alias'] = $this->uri->segment(1);
        $this->data['menu_content'] = $this->menu->get('1', ['menu_alias' => $this->data['menu_alias'], 'menu_type' => 'mainmenu']);
        $this->data['no_menu_content'] = $this->menu->get('1', ['menu_alias' => $this->data['menu_alias'], 'menu_type' => 'nofrontdisplay']);
        if (!empty($this->data['menu_content']) || !empty($this->data['no_menu_content'])) {
            if ($this->data['menu_content']->menu_link_type == 'content' || (isset($this->data['no_menu_content']) && $this->data['no_menu_content']->menu_link_type == 'content')) {
                $this->_content();
            } elseif ($this->data['menu_content']->menu_link_type == 'module') {
                $this->_module();
            }
        } else {
            show_404();
        }
    }

    public function _content()
    {
        $this->load->model('banner_model', 'banner');
        $this->load->model('content_model', 'content');

        if($this->data['no_menu_content']) {
            $this->data['content_data'] = $this->content->get('', ['id' => $this->data['no_menu_content']->link, 'status' => 'Active']);
        } else {
            $this->data['content_data'] = $this->content->get('', ['id' => $this->data['menu_content']->link, 'status' => 'Active']);
        }
        $this->data['banner_data'] = $this->banner->get('', ['category_id' => $this->data['content_data'][0]->category_id, 'status' => 'Active']);

        if (file_exists(realpath(APPPATH . '/views/frontend/content/_' . $this->data['menu_alias'] . '.php'))) {
            $this->data['body'] = 'frontend/content/_' . $this->data['menu_alias'];
        } else {
            $this->data['body'] = 'frontend/content/_default';
        }
            $this->data['addJs'] = array(
                'assets/js/inner-page.js'
            );
        $this->render();
    }

    public function _module()
    {
        $this->load->model('module_model', 'module');
        $this->data['module'] = $this->module->get('1', ['id' => $this->data['menu_content']->link]);

        $this->load->model($this->data['module']->slug . '_model', 'model');
        $this->data['module_data'] = $this->model->get('', ['status' => 'Active']);

        if ($this->data['module']->slug == 'gallery') {
            $this->load->model('gallerymedia_model', 'gallerymedia');
            if ($this->data['menu_content']->menu_alias == 'video-gallery') {
                $type = 'Video';
                $this->data['addJs'] = array(
                    'assets/js/inner-page.js',
                    'assets/js/video-gallery.js',
                    'assets/js/vendor/fancybox/source/jquery.fancybox.pack.js',
                    'assets/js/vendor/fancybox/lib/jquery.mousewheel-3.0.6.pack.js',
                    'assets/js/vendor/fancybox/source/helpers/jquery.fancybox-buttons.js',
                    'assets/js/vendor/fancybox/source/helpers/jquery.fancybox-media.js',
                    'assets/js/vendor/fancybox/source/helpers/jquery.fancybox-thumbs.js',
                    'assets/js/jquery.paginate.js',
                    'assets/js/paginate.js'
                );
                $this->data['addCss'] = array(
                    'assets/js/vendor/fancybox/source/jquery.fancybox.css',
                    'assets/js/vendor/fancybox/source/helpers/jquery.fancybox-buttons.css',
                    'assets/js/vendor/fancybox/source/helpers/jquery.fancybox-thumbs.css',
                    'assets/css/video-gallery.css'
                );

            } elseif ($this->data['menu_content']->menu_alias == 'photo-gallery') {
                $type = 'Image';
                $this->data['addJs'] = array(
                    'assets/js/vendor/fancybox/source/jquery.fancybox.pack.js',
                    'assets/js/vendor/fancybox/lib/jquery.mousewheel-3.0.6.pack.js',
                    'assets/js/vendor/fancybox/source/helpers/jquery.fancybox-buttons.js',
                    'assets/js/vendor/fancybox/source/helpers/jquery.fancybox-media.js',
                    'assets/js/vendor/fancybox/source/helpers/jquery.fancybox-thumbs.js',
                    'assets/js/image-gallery.js',
                    'assets/js/vendor/tweenmax/js/trianglify.min.js',
                    'assets/js/vendor/tweenmax/js/TweenMax.min.js',
                    'assets/js/vendor/tweenmax/js/ScrollToPlugin.min.js',
                    'assets/js/vendor/tweenmax/js/cash.min.js',
                    'assets/js/vendor/tweenmax/js/Card-circle.js',
                    'assets/js/vendor/tweenmax/js/demo.js',
                    'assets/js/jquery.paginate.js',
                    'assets/js/paginate.js'
                );
                $this->data['addCss'] = array(
                    'assets/js/vendor/tweenmax/css/card.css',
                    'assets/js/vendor/tweenmax/css/pattern.css',
                    'assets/js/vendor/tweenmax/css/custom.css',
                    'assets/js/vendor/fancybox/source/jquery.fancybox.css',
                    'assets/js/vendor/fancybox/source/helpers/jquery.fancybox-buttons.css',
                    'assets/js/vendor/fancybox/source/helpers/jquery.fancybox-thumbs.css',
                );
            }
            foreach ($this->data['module_data'] as $key => $module_data) {
                if ($module_data->type == $type) {
                    $this->data['gallery'][$key] = $module_data;
                    $this->data['gallery'][$key]->module_gallery_media = $this->gallerymedia->get('', ['type' => $type, 'gallery_id' => $module_data->id]);
                }
            }

        }

        if ($this->data['module']->slug == 'team') {
            $this->load->model('team_model', 'team');
            $this->data['module_data'] = $this->team->getAllData();
        }

        if (file_exists(realpath(APPPATH . '/views/frontend/module/_' . $this->data['menu_alias'] . '.php'))) {
            $this->data['body'] = 'frontend/module/_' . $this->data['menu_alias'];
        } else {
            $this->data['body'] = 'frontend/content/_default';
        }
        $this->render();
    }

}