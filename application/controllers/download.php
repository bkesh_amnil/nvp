<?php

class Download extends My_Front_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    function _remap($method)
    {
        $param_offset = 2;

        // Default to index
        if ( ! method_exists($this, $method))
        {
            // We need one more param
            $param_offset = 1;
            $method = 'index';
        }

        // Since all we get is $method, load up everything else in the URI
        $params = array_slice($this->uri->rsegment_array(), $param_offset);

        // Call the determined method with all params
        call_user_func_array(array($this, $method), $params);
    }

    public function index($categorySlug = '')
    {
        $this->load->model('download_model', 'download');
        $this->load->model('category_model', 'category');
        $this->data['body'] = 'frontend/module/_download';
        $this->data['menu_alias'] = $this->uri->segment(1);
        $downloadContents = $this->menu->get('1', ['menu_alias' => $this->data['menu_alias']]);
        $resp = array();
        if($downloadContents) {
            $resp[] = array(
                'title' => $downloadContents->menu_title,
                'description' => strip_tags($downloadContents->description)
            );
        }
        $this->data['downloadContents'] = $resp;
        if($categorySlug != '') {
            $categoryId = $this->category->get('1',['slug'=>$categorySlug]);
            $categoryId = $categoryId->id;
            $this->data['module_data'] = $this->download->get('',['categoryId'=>$categoryId,'status'=>'Active']);
            $this->data['category'] = $this->download->get_category();
            $this->data['categorySlug'] = $categorySlug;
            if(!$this->data['module_data']) {
                show_404();
            }
        } else {
            $this->data['module_data'] = $this->download->get('',['status'=>'Active']);
            $this->data['category'] = $this->download->get_category();
        }

        $this->data['addJs'] = array(
            'assets/js/jquery.paginate.js',
            'assets/js/paginate.js',
            'assets/js/download.js'
        );
        $this->render();
    }

}