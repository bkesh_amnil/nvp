<?php

class Registration_msg extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('registrationmsg_model','registrationmsg');
        $this->data['module_name'] = 'Registration Message Manager';
        $this->data['show_add_link'] = false;
    }

    public function index()
    {
        $id = 1;
        if($_POST) {
            $post = $_POST;
            $this->registrationmsg->id = $id;

            $this->form_validation->set_rules($this->registrationmsg->rules);
            if($this->form_validation->run()) {
                if($id == '') {
                    $res = $this->registrationmsg->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->registrationmsg->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/registration_msg');
            } else {
                $this->form($id, 'registrationmsg');
            }
        } else {
            $this->form($id, 'registrationmsg');
        }
    }

}