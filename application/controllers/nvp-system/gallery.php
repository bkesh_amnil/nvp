<?php

class Gallery extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('gallery_model', 'gallery');
        $this->data['module_name'] = 'Gallery Manager';
        $this->data['show_add_link'] = true;
    }

    public function index()
    {
        $query = "SELECT g.*,c.name as category_name, g.type as mediaType
                  FROM `tbl_gallery` g
                  JOIN `tbl_category` c ON g.`category_id` = c.`id`
                  GROUP BY g.`id`
                  ORDER BY g.`id` DESC";
        $this->data['sub_module_name'] = 'Gallery List';
        $this->data['galleries'] = $this->gallery->query($query);
        $this->data['body'] = BACKENDFOLDER . '/gallery/_list';
        $this->render();
    }

    public function create()
    {
        $id = segment(4);
        $this->load->model('content_model', 'content');
        $this->data['contentList'] = $this->content->get('', array('status' => 'Active'));
        if ($_POST) {
            $post = $_POST;
            $post['type'] = $post['mediaTypeSelect'];
            if ($id == '') {
                if ($post['type'] == 'video') {
                    $result = $this->gallery->check_video_gallary();
                    if (!empty($result)) {
                        set_flash('msg', 'Video gallery already exist. Only one gallery is allowed ');
                        redirect(BACKENDFOLDER . '/gallery');
                    }
                }
            }
            $mediaType = $post['mediaTypeSelect'];
            if ($mediaType == 'image') {
                $medias = $post['media'];
                $mediaTitles = $post['title'];
                $mediaDescriptions = $post['description'];
                unset($post['media'], $post['title'], $post['description']);
            } else {
                $media = $post['media'];
                if ($media != '') {
                    $medias = $post['media'];
                    $mediaTitles = $post['title'];
                    $mediaDescriptions = $post['description'];
                }
                unset($post['media'], $post['title'],$post['description']);
            }
            unset($post['mediaTypeSelect'], $post['image'], $post['video']);
            $this->gallery->id = $id;

            $this->form_validation->set_rules($this->gallery->rules($id));
            if ($this->form_validation->run()) {
                if ($id == '') {
                    $res = $this->gallery->save($post, '', true);
                    $id = $res;
                } else {
                    $condition = array('id' => $id);
                    $res = $this->gallery->save($post, $condition);
                }

                // saving gallery media
                $this->load->model('gallerymedia_model', 'gallerymedia');
                $this->gallerymedia->delete(array('gallery_id' => $id));
                foreach ($medias as $key => $singleMedia) {
                    $mediaInsertData = array(
                        'gallery_id' => $id,
                        'media' => $singleMedia,
                        'type' => $mediaType,
                        'title' => isset($mediaTitles) ? $mediaTitles[$key] : '',
                        'caption' => isset($mediaDescriptions) ? $mediaDescriptions[$key] : ''
                    );
                    $this->gallerymedia->save($mediaInsertData);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER . '/gallery');
            } else {
                $this->form($id, 'gallery');
            }
        } else {
            $this->data['addJs'] = array('assets/' . BACKENDFOLDER . '/dist/js/gallery.js');

            if(isset($this->data['addCss'])) {
                $this->data['addCss'] = array_merge($this->data['addCss'], array(
                    'assets/select2/css/select2.min.css'
                ));
            } else {
                $this->data['addCss'] = array(
                    'assets/select2/css/select2.min.css'
                );
            }

            if(isset($this->data['addJs'])) {
                $this->data['addJs'] = array_merge($this->data['addJs'], array(
                    'assets/select2/js/select2.full.min.js',
                    'assets/js/select2.init.js'
                ));
            } else {
                $this->data['addJs'] = array(
                    'assets/select2/js/select2.full.min.js',
                    'assets/js/select2.init.js'
                );
            }

            if ($id != '')
                $this->data['savedMedia'] = $this->gallery->getSavedMedia($id);
            $this->form($id, 'gallery');
        }
    }

    public function delete()
    {
        $id = segment(4);
        $res = $this->gallery->delete(array('id' => $id));

        $res ? set_flash('msg', 'Data deleted') : set_flash('msg', 'Data could not be deleted');

        redirect(BACKENDFOLDER . '/gallery');
    }

    public function status()
    {
        $res = $this->changeStatus('gallery');
        if ($res)
            set_flash('msg', 'Status changed');
        else
            set_flash('msg', 'Status could not be changed');

        redirect(BACKENDFOLDER . '/gallery');
    }

    public function deleteMedia()
    {
        $id = segment(4);
        $this->load->model('gallerymedia_model', 'gallerymedia');
        echo $this->gallerymedia->delete(array('id' => $id));
    }

}