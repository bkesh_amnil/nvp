<?php

class Skill extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('skill_model', 'skill');
        $this->data['module_name'] = 'Skill Manager';
        $this->data['show_add_link'] = true;
        $this->data['show_sort_link'] = true;
    }

    public function index()
    {
        $this->data['sub_module_name'] = 'Skill List';
        $this->data['skills'] = $this->skill->get('', '', 'orderNumber ASC');
        $this->data['body'] = BACKENDFOLDER.'/skill/_list';
        $this->render();
    }

    public function create()
    {
        $id = segment(4);
        if($_POST) {
            $post = $_POST;
            $this->skill->id = $id;

            $this->form_validation->set_rules($this->skill->rules($id));
            if($this->form_validation->run()) {
                if($id == '') {
                    $res = $this->skill->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->skill->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/skill');
            } else {
                $this->form($id, 'skill');
            }
        } else {
            $this->form($id, 'skill');
        }
    }

    public function delete()
    {
        $id = segment(4);
        $res = $this->skill->delete(array('id' => $id));

        $res ? set_flash('msg', 'Data deleted') : set_flash('msg', 'Data could not be deleted');

        redirect(BACKENDFOLDER.'/skill');
    }

    public function status()
    {
        $res = $this->changeStatus('skill');
        if($res)
            set_flash('msg', 'Status changed');
        else
            set_flash('msg', 'Status could not be changed');

        redirect(BACKENDFOLDER.'/skill');
    }

}