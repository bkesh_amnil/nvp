<?php

class Agency_project_data extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('agencyproject_model', 'agencyproject');
        $this->load->model('Agencyprojectcriteria_model', 'agencyprojectcriteria');
        $this->data['module_name'] = 'Agency Project Manager';
        $this->data['show_add_link'] = false;
        $this->data['show_sort_link'] = false;
    }

    public function index()
    {
        $query = "SELECT
                  ap.*,
                  (select a.name from nvp_agency a where a.id = ap.agencyId) as agencyName,
                  IFNULL((SELECT count(id) FROM nvp_project_volunteer pv WHERE pv.agencyProjectId = ap.id), 0) as totalVolunteers,
                  IFNULL((SELECT sum(hours) FROM nvp_project_volunteer pv WHERE pv.agencyProjectId = ap.id), 0) as totalHours
                  FROM `nvp_agency_project` ap
                  ORDER BY ap.`id` desc";
        $this->data['sub_module_name'] = 'Agency Project List';
        $this->data['agency_projects'] = $this->agencyproject->query($query);
        $this->data['body'] = BACKENDFOLDER . '/memberFormData/_agencyProjectData';
        $this->render();
    }

    public function view_detail()
    {
        $agencyProjectId = segment(4);
        if($this->input->post()) {
            $post = $this->input->post();
            $res = $this->agencyproject->save($post, array('id' => $agencyProjectId));
            $res ? set_flash('msg', 'Project description saved') : set_flash('msg', 'Project description could not be saved');
        }
        $this->data['id'] = $agencyProjectId;
        $this->data['sub_module_name'] = 'Agency Project Detail List';
        $this->data['type'] = 'agency_project_data';
        $this->data['data'] = $this->agencyproject->get_agency_project($agencyProjectId);
        $data = new stdClass();
        $criteria = $this->agencyprojectcriteria->get_criteria($agencyProjectId);
        if (!empty($criteria)) {
            $data->totalNumberOfVolunteersRequired = $criteria->totalNumberOfVolunteersRequired;
            $data->ageGroupFrom = $criteria->ageGroupFrom;
            $data->ageGroupTo = $criteria->ageGroupTo;
            $data->zone = $criteria->zone;
            $data->district = $criteria->district;
            $data->municipality = $criteria->municipality;
            $data->wardNumber = $criteria->wardNumber;
            if ($criteria->isSkillRequired == 'Yes') {
                $data->isSkillRequired = $criteria->isSkillRequired;
                $skills = $this->agencyprojectcriteria->get_skills($criteria->id);
                foreach ($skills as $skill) {
                    $criteria_skill[] = $skill->name;
                }
                $data->skills = implode(',', $criteria_skill);
            } else {
                $data->isSkillRequired = $criteria->isSkillRequired;
            }
            if ($criteria->isMinimumEducationRequired == 'Yes') {
                $data->isMinimumEducationRequired = $criteria->isMinimumEducationRequired;
                $academicQualifications = $this->agencyprojectcriteria->get_qualification($criteria->id);
                foreach ($academicQualifications as $academicQualification) {
                    $criteria_academicQualification[] = $academicQualification->name;
                }
                $data->academicQualification = implode(',', $criteria_academicQualification);
            } else {
                $data->isMinimumEducationRequired = $criteria->isMinimumEducationRequired;
            }
            if ($criteria->isTrainingRequired == 'Yes') {
                $data->isTrainingRequired = $criteria->isTrainingRequired;
                $trainings = $this->agencyprojectcriteria->get_training($criteria->id);
                foreach ($trainings as $training) {
                    $criteria_training[] = $training->name;
                }
                $data->trainings = implode(',', $criteria_training);
            } else {
                $data->isTrainingRequired = $criteria->isTrainingRequired;
            }
            if ($criteria->isGenderRequired == 'Yes') {
                $data->isGenderRequired = $criteria->isGenderRequired;
                $data->gender = $this->agencyprojectcriteria->get_gender($criteria->id)->name;
            } else {
                $data->isGenderRequired = $criteria->isGenderRequired;
            }
            $this->data['agencyProjectCriteria'] = $data;
        }
        $this->data['body'] = BACKENDFOLDER . '/memberFormData/_agencyProjectDetail';
        $this->render();

    }

    public function volunteer()
    {
        $this->load->model('volunteer_model', 'volunteer');

        if ($_POST) {
            $post = $_POST;
            $this->db->delete('nvp_project_volunteer', ['agencyProjectId' => $post['agencyProjectId']]);
            foreach ($post['volunteerId'] as $key => $volunteerId) {
                $check_volunteer_data = $this->db->get_where('nvp_project_volunteer', array('volunteerId' => $volunteerId, 'agencyprojectId' => $post['agencyProjectId']))->result();
                $data = array(
                    'agencyprojectId' => $post['agencyProjectId'],
                    'volunteerId' => $volunteerId,
                    'hours' => $post['hours'][$key]
                );
                if(empty($check_volunteer_data)) {
                    $data['hours'] = $post['hours'][$key];
                    $res = $this->db->insert('nvp_project_volunteer', $data);
                } else {
                    $data['hours'] = $check_volunteer_data[0]->hours + $post['hours'][$key];
                    $res = $this->db->update('nvp_project_volunteer', $data, array('id' => $check_volunteer_data[0]->id));
                }
            }
            $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
            redirect(BACKENDFOLDER . '/agency_project_data');
        } else {
            $agencyProjectId = segment(4);
            $query = "SELECT p.*,v.fullName
                  FROM `nvp_project_volunteer` p
                  left join nvp_volunteer v on v.id=p.volunteerId
                  where p.agencyProjectId = '$agencyProjectId'";
            $this->data['agency_volunteers'] = $this->agencyproject->query($query);
            $this->data['sub_module_name'] = 'Agency Project volunteer Details';
            $this->data['addJs'] = array(
                'assets/select2/js/select2.full.min.js',
                'assets/js/select2.init.js'
            );
            $this->data['addCss'] = array(
                'assets/select2/css/select2.min.css'
            );
            $this->data['data'] = $this->agencyproject->get_agency_project($agencyProjectId);
            $this->data['volunteers'] = $this->volunteer->get();
            $this->data['body'] = BACKENDFOLDER . '/memberFormData/_agencyProjectVolunteer';
            $this->render();
        }
    }

    public function volunteer_delete()
    {
        $Id = segment(4);
        $res = $this->db->delete('nvp_project_volunteer', ['id' => $Id]);
        if ($res) {
            $message = 'Sucessfully Deleted Volunteer from the project';
        } else {
            $message = 'Unable to delete volunteer from the project';
        }
        echo json_encode($message);
    }

    function status()
    {
        $status = segment(4);
        $id = segment(5);
        $post['status'] = $status;
        $res = $this->agencyproject->save($post, ['id' => $id]);
        if ($res)
            set_flash('msg', 'Status changed');
        else
            set_flash('msg', 'Status could not be changed');

        redirect(BACKENDFOLDER . '/agency_project_data');
    }
    function delete(){
        $id=segment(4);
        $this->load->library('restrict_delete');
        $params = "nvp_project_volunteer.agencyProjectId|nvp_agency_project_criteria.agencyProjectId|nvp_agency_project_volunteer_invitee.agencyProjectId";

        if($this->restrict_delete->check_for_delete($params, $id)) {
            $res = $this->chapter->delete(array('id' => $id));
            $success_msg = $res ? 'Data deleted' : 'Error in deleting data';
        } else {
            $msg = 'This data cannot be deleted. It is being used in system.';
        }

        $success_msg ? set_flash('msg', $success_msg) : set_flash('msg', $msg);

        /*$query = "select * from nvp_project_volunteer where agencyProjectId = '$id' ";
        $check = $this->agencyproject->query($query);
        if(is_array($check)){
            set_flash('msg', 'Cannot delete Agency Project used in the system');
        }else {
            $res = $this->agencyprojectcriteria->delete(['agencyProjectId' => $id]);
            $res1 = $this->agencyproject->delete(['id' => $id]);
            if ($res && $res1)
                set_flash('msg', 'Agency Project Deleted');
            else
                set_flash('msg', 'Agency Project Deleted Could not be deleted ');
        }*/
        redirect(BACKENDFOLDER . '/agency_project_data');
    }

    public function generatereport()
    {
        $projectId = segment(4);
        $this->load->model('agencyproject_model', 'agencyproject');
        $query = "SELECT
                  ap.*,
                  GROUP_CONCAT(v.fullName) as volunteerName,
                  GROUP_CONCAT(pv.hours) as volunteerHours
                  FROM `nvp_agency_project` ap
                  JOIN `nvp_project_volunteer` pv on pv.agencyProjectId = ap.id
                  JOIN `nvp_volunteer` v on v.id = pv.volunteerId
                  WHERE ap.`id` = $projectId";

        $agencyProjects = $this->agencyproject->query($query);
        if($agencyProjects) {
            foreach($agencyProjects as $project) {
                $volunteerList = explode(',', $project->volunteerName);
                $volunteerHourList = explode(',', $project->volunteerHours);
                $reportData = array(
                    'projectData' => $project,
                    'volunteerList' => $volunteerList,
                    'volunteerHourList' => $volunteerHourList
                );

                $this->_generateReport($reportData);
            }
        }
    }

    private function _generateReport($reportData)
    {
        $reportView = $this->load->view(BACKENDFOLDER .'/memberFormData/_report', $reportData, true);
        include APPPATH.'/third_party/dompdf/dompdf_config.inc.php';

        $dompdf = new Dompdf();
        $dompdf->load_html($reportView);
        $dompdf->set_paper('A4');
        $dompdf->render();
        $dompdf->stream("Agency Report, ".date('F-d-Y', time()).".pdf");
    }
}