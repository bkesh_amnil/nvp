<?php

class Telephone_directory extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('telephone_directory_model', 'telephone_directory');
        $this->data['module_name'] = 'Telephone Directory Manager';
        $this->data['show_add_link'] = true;
    }

    public function index()
    {
        $query = "SELECT *
                  FROM `tbl_telephone_directory`";
        $this->data['sub_module_name'] = 'Telephone List';
        $this->data['telephone_directories'] = $this->telephone_directory->query($query);
        $this->data['body'] = BACKENDFOLDER . '/telephone_directory/_list';
        $this->render();
    }

    public function create()
    {
        $id = segment(4);
        if($id == '')
            $this->data['telephone'] = $this->telephone_directory->get();
        else
            $this->data['telephone'] = $this->telephone_directory->get('', array('id !=' => $id));
        if($_POST) {
            $post = $_POST;
            $this->telephone_directory->id = $id;

            //$this->form_validation->set_rules($this->content->rules);
            $this->form_validation->set_rules($this->telephone_directory->rules($id));
            if($this->form_validation->run()) {
                if($id == '') {
                    $res = $this->telephone_directory->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->telephone_directory->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER . '/telephone_directory');
            } else {
                $this->form($id, 'telephone_directory');
            }
        } else {
            $this->form($id, 'telephone_directory');
        }
    }

    public function delete()
    {
        $id = segment(4);
        $res = $this->telephone_directory->delete(array('id' => $id));

        $res ? set_flash('msg', 'Data deleted') : set_flash('msg', 'Data could not be deleted');

        redirect(BACKENDFOLDER . '/telephone_directory');
    }

    public function status()
    {
        $res = $this->changeStatus('telephone_directory');
        if($res)
            set_flash('msg', 'Status changed');
        else
            set_flash('msg', 'Status could not be changed');

        redirect(BACKENDFOLDER . '/telephone_directory');
    }

}