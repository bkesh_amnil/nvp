<?php

class EmailTemplate extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('emailtemplate_model', 'emailTemplate');
        $this->data['module_name'] = 'Email Template Manager';
        $this->data['show_add_link'] = true;
        $this->data['show_sort_link'] = false;
    }

    function _remap($method)
    {
        $param_offset = 2;

        // Default to index
        if (!method_exists($this, $method)) {
            // We need one more param
            $param_offset = 1;
            $method = 'index';
        }

        // Since all we get is $method, load up everything else in the URI
        $params = array_slice($this->uri->rsegment_array(), $param_offset);

        // Call the determined method with all params
        call_user_func_array(array($this, $method), $params);
    }

    public function index($emailTemplateCategory = '')
    {
        $this->data['sub_module_name'] = 'Email Template List';
        $this->data['emailTemplateCategory'] = $this->emailTemplate->getEmail_category();
        if ($emailTemplateCategory != '') {
            if ($emailTemplateCategory == 'all') {
                $this->data['emailTemplates'] = $this->emailTemplate->getAllData();
            } else {
                $this->data['emailTemplateCategoryId'] = $emailTemplateCategory;
                $this->data['emailTemplates'] = $this->emailTemplate->getAllData($emailTemplateCategory);
            }
        } else {
            $this->data['emailTemplates'] = $this->emailTemplate->getAllData();
        }
        $this->data['body'] = BACKENDFOLDER . '/emailTemplate/_list';
        $this->render();

    }

    public
    function create()
    {
        $id = segment(4);
        $this->data['emailTemplateCategory'] = $this->emailTemplate->getEmail_category();
        if ($_POST) {
            $post = $_POST;
            $this->emailTemplate->id = $id;

            $this->form_validation->set_rules($this->emailTemplate->rules($id));
            if ($this->form_validation->run()) {
                if ($id == '') {
                    $res = $this->emailTemplate->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->emailTemplate->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER . '/emailTemplate');
            } else {
                $this->form($id, 'emailTemplate');
            }
        } else {
            $this->form($id, 'emailTemplate');
        }
    }

    public
    function delete()
    {
        $id = segment(4);
        $res = $this->emailTemplate->delete(array('id' => $id));

        $res ? set_flash('msg', 'Data deleted') : set_flash('msg', 'Data could not be deleted');

        redirect(BACKENDFOLDER . '/emailTemplate');
    }

    public
    function status()
    {
        $res = $this->changeStatus('emailTemplate');
        if ($res)
            set_flash('msg', 'Status changed');
        else
            set_flash('msg', 'Status could not be changed');

        redirect(BACKENDFOLDER . '/emailTemplate');
    }

}