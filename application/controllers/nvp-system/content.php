<?php

class Content extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('content_model', 'content');
        $this->data['module_name'] = 'Content Manager';
        $this->data['show_add_link'] = true;
        $this->data['show_sort_link'] = true;
    }

    public function index()
    {
        $query = "SELECT g.*,c.name as category_name
                  FROM `tbl_content` g
                  JOIN `tbl_category` c ON g.`category_id` = c.`id`
                  ORDER BY g.`orderNumber`";
        $this->data['sub_module_name'] = 'Content List';
        $this->data['contents'] = $this->content->query($query);
        $this->data['body'] = BACKENDFOLDER.'/content/_list';
        $this->render();
    }

    public function create()
    {
        $id = segment(4);
        if($id == '')
            $this->data['contents'] = $this->content->get();
        else
            $this->data['contents'] = $this->content->get('', array('id !=' => $id));
        if($_POST) {
            $post = $_POST;
            $this->content->id = $id;

            //$this->form_validation->set_rules($this->content->rules);
            $this->form_validation->set_rules($this->content->rules($id));
            if($this->form_validation->run()) {
                $post['featured'] = isset($post['featured']) ? 'Yes' : 'No';
                $post['slug'] = $this->content->createSlug($post['name'], $id);
                if($id == '') {
                    $res = $this->content->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->content->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/content');
            } else {
                $this->data['addJs'] = array('assets/datepicker/bootstrap-datepicker.js', 'assets/'.BACKENDFOLDER.'/dist/js/content.js');
                $this->data['addCss'] = array('assets/datepicker/datepicker3.css');
                $this->form($id, 'content');
            }
        } else {
            $this->data['addJs'] = array('assets/datepicker/bootstrap-datepicker.js', 'assets/'.BACKENDFOLDER.'/dist/js/content.js');
            $this->data['addCss'] = array('assets/datepicker/datepicker3.css');
            $this->form($id, 'content');
        }
    }

    public function delete()
    {
        $id = segment(4);
        $res = $this->content->delete(array('id' => $id));

        $res ? set_flash('msg', 'Data deleted') : set_flash('msg', 'Data could not be deleted');

        redirect(BACKENDFOLDER.'/content');
    }

    public function status()
    {
        $res = $this->changeStatus('content');
        if($res)
            set_flash('msg', 'Status changed');
        else
            set_flash('msg', 'Status could not be changed');

        redirect(BACKENDFOLDER.'/content');
    }

}