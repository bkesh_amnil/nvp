<?php

class Zone extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('zone_model', 'zone');
        $this->data['module_name'] = 'Zone Manager';
        $this->data['show_add_link'] = true;
    }

    public function index()
    {
        $this->data['sub_module_name'] = 'Zone List';
        $this->data['zones'] = $this->zone->get();
        $this->data['body'] = BACKENDFOLDER.'/zone/_list';
        $this->render();
    }

    public function create()
    {
        $id = segment(4);
        if($_POST) {
            $post = $_POST;
            $this->zone->id = $id;

            $this->form_validation->set_rules($this->zone->rules($id));
            if($this->form_validation->run()) {
                if($id == '') {
                    $res = $this->zone->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->zone->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/zone');
            } else {
                $this->form($id, 'zone');
            }
        } else {
            $this->form($id, 'zone');
        }
    }

    public function delete()
    {
        $id = segment(4);
        $res = $this->zone->delete(array('id' => $id));

        $res ? set_flash('msg', 'Data deleted') : set_flash('msg', 'Data could not be deleted');

        redirect(BACKENDFOLDER.'/zone');
    }

}