<?php

class Contact extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('formdata_model', 'formdata');
        $this->data['module_name'] = 'Contact Manager';
        $this->data['show_add_link'] = false;
        $this->data['show_sort_link'] = false;
    }

    public function index()
    {
        $query = "SELECT *
                  FROM `tbl_form_data`
                  where `type` = 'contact' ORDER BY id desc";
        $this->data['sub_module_name'] = 'FeedBack List';
        $this->data['contacts'] = $this->formdata->query($query);
        $this->data['body'] = BACKENDFOLDER . '/formdata/_contact';
        $this->render();
    }
    public function form(){
            $id = segment(4);
            $data['data'] = $this->formdata->get('1',['id'=>$id]);
            $modal = $this->load->view(BACKENDFOLDER.'/formdata/_modal',$data,true);
            echo $modal;

    }
    public function send(){
        $post=$_POST;
        $email_params = array(
            'subject' => $post['subject'],
            'from' => SITEMAIL,
            'fromname' => SITENAME,
            'to' =>  $post['email'],
            'toname' => $post['name'],
            'message' => $post['message']
        );
        if(swiftsend($email_params)){
            set_flash('msg', 'Email Send');
            redirect(BACKENDFOLDER.'/contact');
        }
    }

}