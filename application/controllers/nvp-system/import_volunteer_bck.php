<?php

class Import_volunteer extends My_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->data['module_name'] = 'Import Data Manager';
        $this->data['show_add_link'] = false;
        // password hashing library for older PHP version
        require APPPATH.'/third_party/password.php';
    }

    public function index()
    {
        $this->data['sub_module_name'] = 'Import Volunteer Data';
        $this->data['body'] = BACKENDFOLDER . '/memberFormData/_import_volunteer';
        $this->render();
    }

    public function import()
    {
        $this->load->model('volunteer_model', 'volunteer');
        $this->load->model('volunteerskill_model', 'volunteerskill');
        $this->load->model('volunteertraining_model', 'volunteertraining');

        if (!empty($_FILES['excel']['name'])) {
            $file = $_FILES['excel'];
            $temp_file_path = $file['tmp_name'];
            $ext = explode(".", $file['name']);
            $ext = end($ext);
            if (!file_exists('temp')) {
                mkdir('temp');
            }
            $upload_path = 'temp/' . time() . '.' . $ext;
            if (move_uploaded_file($temp_file_path, $upload_path)) {
                include APPPATH . "third_party/phpexcel/PHPExcel.php";
                $objPHPExcel = PHPExcel_IOFactory::load($upload_path);

                $sheet = $objPHPExcel->getSheet(0);
                $raw_data = $sheet->toArray('No Data', TRUE, TRUE, FALSE);
                $cleaned_data = $this->_clean_data($raw_data);
                $cleaned_data = (array_slice($cleaned_data, 1));
                $row_name = 1;
                $a=1;
                foreach ($cleaned_data as $data) {
                    if ($a == 1) {
                        $count = count($data);
                        $a = 0;
                    } else {
                        break;
                    }
                }
                foreach ($cleaned_data as $data) {
                    $check_volunteer = $this->volunteer->get('1', ['email' => $data[4],'phoneNumberMobile'=>$data[3]]);
                    if (!empty($check_volunteer)) {
                        $existing_data[$row_name] = $data;
                        $existing_data[$row_name]['id'] = $check_volunteer->id;
                    }elseif($data[3] == 'No Data' || $data[4] == 'No Data'){
                        $emptydata[$row_name] = $data;
                    }else{
                        if (strtolower($data[1]) == 'm') {
                            $genderId = '1';
                        } elseif (strtolower($data[1]) == 'f') {
                            $genderId = '2';
                        } else {
                            $genderId = '3';
                        }
                        $dateOfBirth = date("m/d/Y", PHPExcel_Shared_Date::ExcelToPHP($data[2]));
                        $zoneId = $this->_get_zoneId($data[7]);
                        $districtId = $this->_get_districtId($data[8], $zoneId);
                        $municipalityId = $this->_get_municipalityId($data[9], $districtId);
                        $academicqualificationId = $this->_get_academicqualificationId($data[11]);

                        if ($data[14] == 'No Data') {
                            $volunteerType = 'partTime';
                        } else {
                            $volunteerType = 'fullTime';
                        }
                        $primaryModeOfCommunication = $this->_get_primaryModeOfCommunication($data[16]);
                        $secondaryModeOfCommunication = $this->_get_secondaryModeOfCommunication($data[17]);

                        $insert_data = array(
                            'fullName' => $data[0],
                            'genderId' => $genderId,
                            'dateOfBirth' => strtotime($dateOfBirth),
                            'phoneNumberMobile' => $data[3],
                            'email' => $data[4],
                            'password' => password_hash('volunteer', PASSWORD_BCRYPT, array('cost' => 10)),
                            'emergencyContactFullName' => $data[5],
                            'emergencyContactPhoneNumber' => $data[6],
                            'zoneId' => $zoneId,
                            'districtId' => $districtId,
                            'municipalityId' => $municipalityId,
                            'wardNumber' => $data[10],
                            'academicQualificationId' => $academicqualificationId,
                            'volunteerType' => $volunteerType,
                            'primaryModeOfCommunication' => $primaryModeOfCommunication,
                            'secondaryModeOfCommunication' => $secondaryModeOfCommunication,
                            'status' => 'Active'
                        );
                        $res = $this->volunteer->save($insert_data);
                        if ($res) {
                            $volunteer = $this->volunteer->get('1', ['email' => $data[4]]);
                            if ($volunteer) {
                                $volunteerId = $volunteer->id;
                                $skillId = $this->_get_skillId($data[12]);
                                $trainingId = $this->_get_trainingId($data[13]);
                                $check_skill = $this->volunteerskill->get('1', ['volunteerId' => $volunteerId, 'skillId' => $skillId]);
                                if (empty($check_skill)) {
                                    $this->volunteerskill->save(['volunteerId' => $volunteerId, 'skillId' => $skillId]);
                                }
                                $check_training = $this->volunteertraining->get('1', ['volunteerId' => $volunteerId, 'trainingId' => $trainingId]);
                                if (empty($check_training)) {
                                    $this->volunteertraining->save(['volunteerId' => $volunteerId, 'trainingId' => $trainingId]);
                                }
                            }
                        }
                    }
                    $row_name = $row_name+1;
                }
                $this->data['sub_module_name'] = 'Importing Volunteer';
                $this->data['existing_data'] = isset($existing_data) ? $existing_data: '';
                $this->data['empty_data'] = isset($emptydata) ? $emptydata: '';
                $this->data['body'] = BACKENDFOLDER . '/memberFormData/_import_volunteer_data';
                $this->render();
            }
        }
    }

    private function _get_zoneId($name)
    {
        $this->load->model('zone_model', 'zone');
        $zone = $this->zone->get('1',['LOWER(name)' => trim(strtolower($name))]);
        if ($zone) {
            $zoneId = $zone->id;
        } else {
            $this->zone->save(['name' => $name]);
            $zone = $this->zone->get('1', ['LOWER(name)' => trim(strtolower($name))]);
            $zoneId = $zone['id'];
        }
        return $zoneId;
    }

    private function _get_districtId($name, $zoneId)
    {
        $this->load->model('district_model', 'district');
        $district = $this->district->get('1', ['LOWER(name)' => trim(strtolower($name))]);
        if ($district) {
            $districtId = $district->id;
        } else {
            $this->district->save(['name' => $name, 'zoneId' => $zoneId]);
            $district = $this->district->get('1', ['LOWER(name)' => trim(strtolower($name))]);
            $districtId = $district->id;
        }
        return $districtId;
    }

    private function _get_municipalityId($name, $districtId)
    {
        $this->load->model('municipality_model', 'municipality');
        $municipality = $this->municipality->get('1', ['LOWER(name)' => trim(strtolower($name)), 'districtId' => $districtId]);
        if ($municipality) {
            $municipalityId = $municipality->id;
        } else {
            $this->municipality->save(['name' => $name, 'districtId' => $districtId]);
            $municipality = $this->municipality->get('1', ['LOWER(name)' => trim(strtolower($name))]);
            $municipalityId = $municipality->id;
        }
        return $municipalityId;
    }

    private function _get_academicqualificationId($name)
    {
        $this->load->model('academicqualification_model', 'academicqualification');
        $academicqualification = $this->academicqualification->get('1', ['LOWER(name)' => strtolower($name)]);
        if ($academicqualification) {
            $academicqualificationId = $academicqualification->id;
        } else {
            $this->academicqualification->save(['name' => $name]);
            $academicqualification = $this->academicqualification->get('1', ['LOWER(name)' => strtolower($name)]);
            $academicqualificationId = $academicqualification->id;
        }
        return $academicqualificationId;
    }

    public function _get_primaryModeOfCommunication($name)
    {
        if (trim(strtolower($name)) == "email") {
            $primaryModeOfCommunication = '0';
        } elseif (trim(strtolower($name)) == "phonecall") {
            $primaryModeOfCommunication = '1';
        } elseif (trim(strtolower($name)) == "text message") {
            $primaryModeOfCommunication = '2';
        } elseif (trim(strtolower($name)) == "facebook messenger") {
            $primaryModeOfCommunication = '3';
        }
        return $primaryModeOfCommunication;
    }

    public function _get_secondaryModeOfCommunication($name)
    {
        if (trim(strtolower($name)) == 'email') {
            $secondaryModeOfCommunication = '0';
        } elseif (trim(strtolower($name)) == 'phonecall') {
            $secondaryModeOfCommunication = '1';
        } elseif (trim(strtolower($name)) == 'text message') {
            $secondaryModeOfCommunication = '2';
        } elseif (trim(strtolower($name)) == 'facebook messenger') {
            $secondaryModeOfCommunication = '3';
        }
        return $secondaryModeOfCommunication;
    }

    private function _get_skillId($name)
    {
        $this->load->model('skill_model', 'skill');
        $skill = $this->skill->get('1', ['LOWER(name)' => strtolower($name)]);
        if ($skill) {
            $skillId = $skill->id;
        } else {
            $this->skill->save(['name' => $name]);
            $skill = $this->skill->get('1', ['LOWER(name)' => strtolower($name)]);
            $skillId = $skill->id;
        }
        return $skillId;
    }

    private function _get_trainingId($name)
    {
        $this->load->model('training_model', 'training');
        $training = $this->training->get('1', ['LOWER(name)' => strtolower($name)]);
        if ($training) {
            $trainingId = $training->id;
        } else {
            $this->training->save(['name' => $name]);
            $training = $this->training->get('1', ['LOWER(name)' => strtolower($name)]);
            $trainingId = $training->id;
        }
        return $trainingId;
    }


    private function _clean_data($raw_data)
    {
        foreach ($raw_data as $key => $data) {
            $is_empty = false;
            for ($i = 0; $i < count($data); $i++) {
                if ($data[$i] == 'No Data') {
                    $is_empty = true;
                } else {
                    $is_empty = false;
                    break;
                }
            }
            if ($is_empty) unset($raw_data[$key]);
        }
        return $raw_data;
    }

}