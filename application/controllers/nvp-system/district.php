<?php

class District extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('district_model', 'district');
        $this->load->model('zone_model', 'zone');
        $this->data['module_name'] = 'District Manager';
        $this->data['show_add_link'] = true;
    }

    public function index()
    {
        $this->data['sub_module_name'] = 'District List';
        $this->data['districts'] = $this->district->getAllData();
        $this->data['body'] = BACKENDFOLDER.'/district/_list';
        $this->render();
    }

    public function create()
    {
        $id = segment(4);
        $this->data['allZones'] = $this->zone->get('');
        if($_POST) {
            $post = $_POST;
            $this->district->id = $id;

            $this->form_validation->set_rules($this->district->rules($id));
            if($this->form_validation->run()) {
                if($id == '') {
                    $res = $this->district->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->district->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/district');
            } else {
                $this->form($id, 'district');
            }
        } else {
            $this->form($id, 'district');
        }
    }

    public function delete()
    {
        $id = segment(4);
        $res = $this->district->delete(array('id' => $id));

        $res ? set_flash('msg', 'Data deleted') : set_flash('msg', 'Data could not be deleted');

        redirect(BACKENDFOLDER.'/district');
    }

}