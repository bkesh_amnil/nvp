<?php

class Training extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('training_model', 'training');
        $this->data['module_name'] = 'Training Manager';
        $this->data['show_add_link'] = true;
        $this->data['show_sort_link'] = true;
    }

    public function index()
    {
        $this->data['sub_module_name'] = 'Training List';
        $this->data['trainings'] = $this->training->get('', '', 'orderNumber ASC');
        $this->data['body'] = BACKENDFOLDER.'/training/_list';
        $this->render();
    }

    public function create()
    {
        $id = segment(4);
        if($_POST) {
            $post = $_POST;
            $this->training->id = $id;

            $this->form_validation->set_rules($this->training->rules($id));
            if($this->form_validation->run()) {
                if($id == '') {
                    $res = $this->training->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->training->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/training');
            } else {
                $this->form($id, 'training');
            }
        } else {
            $this->form($id, 'training');
        }
    }

    public function delete()
    {
        $id = segment(4);
        $res = $this->training->delete(array('id' => $id));

        $res ? set_flash('msg', 'Data deleted') : set_flash('msg', 'Data could not be deleted');

        redirect(BACKENDFOLDER.'/training');
    }

    public function status()
    {
        $res = $this->changeStatus('training');
        if($res)
            set_flash('msg', 'Status changed');
        else
            set_flash('msg', 'Status could not be changed');

        redirect(BACKENDFOLDER.'/training');
    }

}