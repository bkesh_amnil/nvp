<?php

class Testimonial extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('testimonial_model', 'testimonial');
        $this->data['module_name'] = 'Testimonial Manager';
        $this->data['show_add_link'] = true;
    }

    public function index()
    {
        $this->data['sub_module_name'] = 'Testimonial List';
        $this->data['testimonials'] = $this->testimonial->get();
        $this->data['body'] = BACKENDFOLDER.'/testimonial/_list';
        $this->render();
    }

    public function create()
    {
        $id = segment(4);
        if($_POST) {
            $post = $_POST;
            $post['date'] = strtotime($post['date']);
            $this->testimonial->id = $id;

            $this->form_validation->set_rules($this->testimonial->rules($id));
            if($this->form_validation->run()) {
                if($id == '') {
                    $res = $this->testimonial->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->testimonial->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/testimonial');
            } else {
                $this->data['addJs'] = array(
                    'assets/datepicker/bootstrap-datepicker.js',
                    'assets/'.BACKENDFOLDER.'/dist/js/testimonial.js'
                );
                $this->data['addCss'] = array('assets/datepicker/datepicker3.css');
                $this->form($id, 'testimonial');
            }
        } else {
            $this->data['addJs'] = array(
                'assets/datepicker/bootstrap-datepicker.js',
                'assets/'.BACKENDFOLDER.'/dist/js/testimonial.js'
            );
            $this->data['addCss'] = array('assets/datepicker/datepicker3.css');
            $this->form($id, 'testimonial');
        }
    }

    public function delete()
    {
        $id = segment(4);
        $res = $this->testimonial->delete(array('id' => $id));

        $res ? set_flash('msg', 'Data deleted') : set_flash('msg', 'Data could not be deleted');

        redirect(BACKENDFOLDER.'/testimonial');
    }

    public function status()
    {
        $res = $this->changeStatus('testimonial');
        if($res)
            set_flash('msg', 'Status changed');
        else
            set_flash('msg', 'Status could not be changed');

        redirect(BACKENDFOLDER.'/testimonial');
    }

}