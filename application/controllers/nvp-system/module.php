<?php

class Module extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('module_model', 'module');
        $this->data['module_name'] = 'Module Manager';
        $this->data['show_add_link'] = true;
        $this->data['show_sort_link'] = true;
    }

    public function index()
    {
        $this->data['sub_module_name'] = $this->data['module_name'];
        $this->data['body'] = BACKENDFOLDER.'/module/_list';
        $this->data['modules'] = $this->module->get('', '', 'orderNumber ASC');
        $this->render();
    }
    
    public function create()
    {
        $modules = $this->module->get();
        $select_module = get_select($modules, 'Select Parent Module');
        $this->data['modules'] = $select_module;
        $id = segment(4);
        if($_POST) {
            $post = $_POST;
            $this->module->id = $id;

            $this->form_validation->set_rules($this->module->rules);
            if($this->form_validation->run()) {
                if($id == '') {
                    $res = $this->module->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->module->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/module');
            } else {
                $this->form($id, 'module');
            }
        } else {
            $this->form($id, 'module');
        }
    }

    public function delete()
    {
        $id = segment(4);
        $res = $this->module->delete(array('id' => $id));

        $res ? set_flash('msg', 'Data deleted') : set_flash('msg', 'Data could not be deleted');

        redirect(BACKENDFOLDER.'/module');
    }

}