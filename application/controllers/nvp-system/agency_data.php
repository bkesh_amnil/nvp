<?php

class Agency_data extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('agency_model', 'agency');
        $this->data['module_name'] = 'Agency Manager';
        $this->data['show_add_link'] = false;
        $this->data['show_sort_link'] = false;
    }

    public function index()
    {
        $query = "SELECT a.*, vu.`id` as verifiedUser
                  FROM `nvp_agency` a
                  LEFT JOIN `nvp_verified_user` vu ON vu.`userId` =  a.`id`
                  ORDER BY `name`";
        $this->data['sub_module_name'] = 'Agency List';
        $this->data['agencys'] = $this->agency->query($query);
        $this->data['body'] = BACKENDFOLDER.'/memberFormData/_agencyData';
        $this->render();
    }

    public function verifyUser($userId = '')
    {
        $this->load->model('common_model', 'common');
        $this->common->table = 'nvp_verified_user';

        $currentUserData = $this->common->get('1', array('userId' => $userId));
        if(!$currentUserData) {
            $verifiedUserData = array(
                'userId' => $userId,
                'userType' => 'Agency'
            );
            $this->common->save($verifiedUserData);
            set_flash('msg', 'User verified successfully');
        } else {
            $this->common->delete(array('userId' => $currentUserData->userId));
            set_flash('msg', 'User discredited successfully');
        }

        redirect($_SERVER['HTTP_REFERER']);
    }

    public function view_detail(){
        $id = segment(4);
        $this->data['sub_module_name'] = 'Agency Detail List';
        $this->data['type'] = 'agency_data';
        $this->data['data'] = $this->agency->get_agency($id);
        $this->data['body'] = BACKENDFOLDER.'/memberFormData/_detail' ;
        $this->render();

    }

    public function status()
    {
        $page = segment(4);
        $status = segment(5);
        $id = segment(6);
        $post['status'] = $status;
        $res = $this->agency->save($post,['id'=>$id]);
        if($res)
            set_flash('msg', 'Status changed');
        else
            set_flash('msg', 'Status could not be changed');
if($page == 'detail'){
    redirect(BACKENDFOLDER.'/agency_data/view_detail/'.$id);
}else{
    redirect(BACKENDFOLDER.'/agency_data');
}
    }

}