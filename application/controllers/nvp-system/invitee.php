<?php

class Invitee extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('agencyprojectvolunteerinvitee_model', 'agencyprojectvolunteerinvitee');
        $this->data['module_name'] = 'Invitee';
        $this->data['show_add_link'] = false;
        $this->data['show_sort_link'] = false;
    }

    public function index()
    {
        $this->data['sub_module_name'] = 'Invitee List';
        $this->data['agencyprojectvolunteerinvitees'] = $this->agencyprojectvolunteerinvitee->getAllData();
        $this->data['body'] = BACKENDFOLDER . '/agencyprojectvolunteerinvitee/_list';
        $this->render();
    }

    public function status()
    {
        $post = $_POST;
        echo $this->agencyprojectvolunteerinvitee->save(
            array('status' => $post['status'], 'responseDate' => time()),
            array('id' => $post['invitationId'])
        );
    }
}