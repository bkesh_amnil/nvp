<?php
class Menu extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('menu_model', 'menu');
        $this->data['module_name'] = 'Menu Manager';
        $this->data['show_add_link'] = true;
        $this->data['show_sort_link'] = true;
    }

    public function index()
    {
        $this->data['menus'] = $this->menu->getAllData();
        $this->data['sub_module_name'] = 'Menu List';
        $this->data['body'] = BACKENDFOLDER.'/menu/_list';
        $this->render();
    }

    public function create()
    {
        $this->data['menu_type'] = $this->menu->get_menutypes();
        $this->data['menu_parents'] = $this->menu->getAllData();
        $this->data['parentMenus'] = $this->menu->getAllMenuParent();
        $this->data['menu_link_type']['linktypes'] = $this->menu->get_menulinktype();
        foreach ($this->data['menu_link_type']['linktypes'] as $key =>$data){
            $this->data['menu_link_type']['link'][$key] = $this->menu->get_menulinks($key);
        }
        $this->data['addJs'] = array('assets/'.BACKENDFOLDER.'//dist/js/menu.js');
        $id = segment(4);
        if($_POST) {
            $post = $_POST;
            $this->menu->id = $id;
            $this->form_validation->set_rules($this->menu->rules);
            if($this->form_validation->run()) {
                switch($post['menu_link_type']) {
                    case 'url':
                        $post['link'] = $post['url_link'];
                        unset($post['url_link']);
                        unset($post['content_link']);
                        unset($post['module_link']);
                        break;
                    case 'content':
                        $post['link'] = $post['content_link'];
                        unset($post['url_link']);
                        unset($post['content_link']);
                        unset($post['module_link']);
                        break;
                    case 'module':
                        $post['link'] = $post['module_link'];
                        unset($post['url_link']);
                        unset($post['content_link']);
                        unset($post['module_link']);
                        break;
                    default:
                        $post['link'] = '';
                        unset($post['url_link']);
                        unset($post['content_link']);
                        unset($post['module_link']);
                        break;
                }
                if($id == '') {
                    $res = $this->menu->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->menu->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/menu');
            } else {
                $this->form($id, 'menu');
            }
        } else {
            $this->form($id, 'menu');
        }
    }

    public function delete()
    {
        $id = segment(4);
        $res = $this->menu->delete(array('id' => $id));

        $res ? set_flash('msg', 'Menu deleted') : set_flash('msg', 'Data could not be deleted');

        redirect(BACKENDFOLDER.'/menu');
    }

    public function status()
    {
        $res = $this->changeStatus('menu');
        if($res)
            set_flash('msg', 'Status changed');
        else
            set_flash('msg', 'Status could not be changed');

        redirect(BACKENDFOLDER.'/menu');
    }

}