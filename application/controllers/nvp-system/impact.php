<?php

class Impact extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('impact_model', 'impact');
        $this->data['module_name'] = 'Impact Manager';
        $this->data['show_add_link'] = true;
    }

    public function index()
    {
        $this->data['sub_module_name'] = 'Impact List';
        $this->data['impacts'] = $this->impact->get();
        $this->data['body'] = BACKENDFOLDER.'/impact/_list';
        $this->render();
    }

    public function create()
    {
        $id = segment(4);
        if($_POST) {
            $post = $_POST;
            $this->impact->id = $id;

            //$this->form_validation->set_rules($this->impact->rules);
            $this->form_validation->set_rules($this->impact->rules($id));
            if($this->form_validation->run()) {
                if($id == '') {
                    $res = $this->impact->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->impact->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/impact');
            } else {
                $this->form($id, 'impact');
            }
        } else {
            $this->form($id, 'impact');
        }
    }

    public function delete()
    {
        $id = segment(4);
        $res = $this->impact->delete(array('id' => $id));

        $res ? set_flash('msg', 'Data deleted') : set_flash('msg', 'Data could not be deleted');

        redirect(BACKENDFOLDER.'/impact');
    }

    public function status()
    {
        $res = $this->changeStatus('impact');
        if($res)
            set_flash('msg', 'Status changed');
        else
            set_flash('msg', 'Status could not be changed');

        redirect(BACKENDFOLDER.'/impact');
    }

}