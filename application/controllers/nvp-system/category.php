<?php

class Category extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('category_model', 'category');
        $this->data['module_name'] = 'Category Manager';
        $this->data['show_add_link'] = true;
    }

    public function index()
    {
        $this->data['sub_module_name'] = 'Category List';
        $this->data['categories'] = $this->category->getAllData();
        $this->data['body'] = BACKENDFOLDER.'/category/_list';
        $this->render();
    }

    public function create()
    {
        $id = segment(4);
        $this->data['categoryType'] = $this->category->getCategoryType();
        if($_POST) {
            $post = $_POST;
            $this->category->id = $id;

            $this->form_validation->set_rules($this->category->rules);
            if($this->form_validation->run()) {
                $post['slug'] = $this->category->createSlug($post['name'], $id);
                if($id == '') {
                    $res = $this->category->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->category->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/category');
            } else {
                $this->form($id, 'category');
            }
        } else {
            $this->form($id, 'category');
        }
    }

    public function delete()
    {
        $id = segment(4);
        $res = $this->category->delete(array('id' => $id));

        $res ? set_flash('msg', 'Data deleted') : set_flash('msg', 'Data could not be deleted');

        redirect(BACKENDFOLDER.'/category');
    }

    public function unique_category($name)
    {
        $id = $this->category->id;
        $category = $this->category->get(1, array('id' => $id));
        if($category) {
            $old_category = $category->name;
            if ($name == $old_category)
                return true;
        }
        $unique_category = $this->category->get(1, array('name' => $name));
        if ($unique_category) {
            $this->form_validation->set_message('unique_category', 'The Category already exists.');

            return false;
        }
        return true;
    }

    public function status()
    {
        $res = $this->changeStatus('category');
        if($res)
            set_flash('msg', 'Status changed');
        else
            set_flash('msg', 'Status could not be changed');

        redirect(BACKENDFOLDER.'/category');
    }

}