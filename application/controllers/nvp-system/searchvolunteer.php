<?php

class Searchvolunteer extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->data['module_name'] = 'Search Volunteers';
        $this->data['show_add_link'] = false;
        $this->data['show_sort_link'] = false;
    }

    public function index()
    {
        $this->data['sub_module_name'] = 'Search Volunteers';
        $this->load->model('academicqualification_model', 'academicqualification');
        $this->load->model('common_model', 'common');
        $this->data['languages'] = $this->common->query('select * from nvp_language');
        $this->data['allGender'] = $this->common->query('select * from nvp_gender');
        $this->data['zones'] = $this->common->query('select * from nvp_zone order by name asc');
        $this->data['districts'] = $this->common->query('select * from nvp_district order by name asc');
        $this->data['municipalities'] = $this->common->query('select * from nvp_municipality order by name asc');
        $this->data['academicQualifications'] = $this->academicqualification->get('', array('status' => 'Active'), 'orderNumber asc');
        $this->data['skills'] = $this->common->query('select * from nvp_skill');
        $this->data['trainings'] = $this->common->query('select * from nvp_training');
        $this->data['body'] = BACKENDFOLDER.'/searchvolunteer/_form';
        $this->data['addCss'] = array(
            'assets/select2/css/select2.min.css'
        );
        $this->data['addJs'] = array(
            'assets/select2/js/select2.full.min.js',
            'assets/'.BACKENDFOLDER.'/dist/js/searchvolunteer.js',
            'assets/js/select2.init.js',
            'assets/js/locationFilter.js'
        );

        if($this->input->get()) {
            $this->load->model('agencyproject_model', 'agencyproject');
            $searchParameters = $this->input->get(NULL, TRUE);
            $this->data['projects'] = $this->agencyproject->get('', array('status' => 'Ongoing'));
            $this->load->model('common_model', 'common');

            $searchQuery = "select
                        v.id,
                        v.fullName as volunteerName,v.email,
                        g.name as genderName
                        from nvp_volunteer v
                        join nvp_gender g on g.id = v.genderId
                        left join nvp_volunteer_language vl on vl.volunteerId = v.id
                        left join nvp_volunteer_skill vs on vs.volunteerId = v.id
                        left join nvp_volunteer_training_interested_in vtii on vtii.volunteerId = v.id
                        left join nvp_volunteer_training_received vtr on vtr.volunteerId = v.id
                        where v.status = 'Active' and v.account_status = 'Active' and v.notification_status = 'Active'";

            if(isset($searchParameters['zoneId']) && $searchParameters['zoneId'] != '') {
                $zoneId = $searchParameters['zoneId'];
                $searchConditions[] = "v.zoneId in ('{$zoneId}')";
            }

            if(isset($searchParameters['districtId']) && $searchParameters['districtId'] != '') {
                $districtId = $searchParameters['districtId'];
                $searchConditions[] = "v.districtId in ('{$districtId}')";
            }

            if(isset($searchParameters['municipalityId']) && $searchParameters['municipalityId'] != '') {
                $municipalityId = $searchParameters['municipalityId'];
                $searchConditions[] = "v.municipalityId in ('{$municipalityId}')";
            }

            if(isset($searchParameters['genderId']) && $searchParameters['genderId'] != '') {
                $genderIds = $searchParameters['genderId'];
                $searchConditions[] = "v.genderId in ('{$genderIds}')";
            }

            if(isset($searchParameters['academicQualificationId']) && $searchParameters['academicQualificationId'] != '') {
                $academicQualificationIds = $searchParameters['academicQualificationId'];
                $searchConditions[] = "v.academicQualificationId in ('{$academicQualificationIds}')";
            }

            if(isset($searchParameters['languageId']) && $searchParameters['languageId'] != '') {
                $languageIds = $searchParameters['languageId'];
                $searchConditions[] = "vl.languageId in ('{$languageIds}')";
            }

            if(isset($searchParameters['skillId']) && $searchParameters['skillId'] != '') {
                $skillIds = $searchParameters['skillId'];
                $searchConditions[] = "vs.skillId in ('{$skillIds}')";
            }

            if(isset($searchParameters['trainingId']) && $searchParameters['trainingId'] != '') {
                $trainingIds = $searchParameters['trainingId'];
                $searchConditions[] = "vtii.trainingId in ('{$trainingIds}')";
                $searchConditions[] = "vtr.trainingId in ('{$trainingIds}')";
            }

            if(isset($searchConditions)) {
                $searchConditions = implode(' and ', $searchConditions);
                $searchConditions = " and ($searchConditions) ";
            }

            $searchQuery .= $searchConditions . ' group by v.id';
            $res = $this->common->query($searchQuery);
            $this->data['volunteerList'] = $res;
        }
        $this->render();
    }

    public function filterDistrict()
    {
        $zoneId = $_POST['zoneId'];
        $this->load->model('common_model', 'common');
        $query = "select * from nvp_district where zoneId = $zoneId order by name asc";
        $data = $this->common->query($query);
        $options = '<option value="">Select District</option>';

        if($data) {
            foreach($data as $district) {
                $options .= "<option value='$district->id'>".ucwords($district->name)."</option>";
            }
        }

        echo json_encode(
            array(
                'status' => 'Success',
                'data' => $options,
                'responseCode' => 200
            )
        );
    }

    public function filterMunicipality()
    {
        $districtId = $_POST['districtId'];
        $this->load->model('common_model', 'common');
        $query = "select * from nvp_municipality where districtId = $districtId order by name asc";
        $data = $this->common->query($query);
        $options = '<option value="">Select VDC/Municipality</option>';

        if($data) {
            foreach($data as $municipality) {
                $options .= "<option value='$municipality->id'>".ucwords($municipality->name)."</option>";
            }
        }

        echo json_encode(
            array(
                'status' => 'Success',
                'data' => $options,
                'responseCode' => 200
            )
        );
    }

}