<?php

class Appuser extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('appuser_model', 'appuser');
        $this->data['module_name'] = 'App Users Manager';
        $this->data['show_add_link'] = false;
        $this->data['show_sort_link'] = false;
    }

    public function index()
    {
        $this->data['sub_module_name'] = 'App Users List';
        $query = "select au.*,
                    IFNULL (
                      (select v.fullName from nvp_volunteer v where v.id=au.userId),
                      (select a.name from nvp_agency a where a.id=au.userId)
                    ) as name
                    from nvp_app_user au";
        $this->data['appusers'] = $this->appuser->query($query);
        $this->data['body'] = BACKENDFOLDER.'/appuser/_list';
        $this->render();
    }

    public function delete()
    {
        $id = segment(4);
        $res = $this->appuser->delete(array('id' => $id));

        $res ? set_flash('msg', 'Data deleted') : set_flash('msg', 'Data could not be deleted');

        redirect(BACKENDFOLDER.'/appuser');
    }

}