<?php

class Volunteer_data extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('volunteer_model', 'volunteer');
        $this->load->model('content_model', 'content');
        $this->data['module_name'] = 'Volunteer Manager';
        $this->data['show_add_link'] = false;
        $this->data['show_sort_link'] = false;
    }

    public function index()
    {
        $query = "SELECT v.*, vu.`id` as verifiedUser, (select IFNULL(sum(`hours`), 0) as totalHours from nvp_project_volunteer where volunteerId =v.id) as total_hours
                  FROM `nvp_volunteer` v
                  LEFT JOIN `nvp_verified_user` vu ON vu.`userId` =  v.`id`
                  ORDER BY `fullName`";
        $this->data['sub_module_name'] = 'Volunteer List';
        $this->data['volunteers'] = $this->volunteer->query($query);
        $this->data['body'] = BACKENDFOLDER . '/memberFormData/_volunteerData';
        $this->render();
    }

    public function verifyUser($userId = '')
    {
        $this->load->model('common_model', 'common');
        $this->common->table = 'nvp_verified_user';

        $currentUserData = $this->common->get('1', array('userId' => $userId));
        if (!$currentUserData) {
            $verifiedUserData = array(
                'userId' => $userId,
                'userType' => 'Volunteer'
            );
            $this->common->save($verifiedUserData);
            set_flash('msg', 'User verified successfully');
        } else {
            $this->common->delete(array('userId' => $currentUserData->userId));
            set_flash('msg', 'User discredited successfully');
        }

        redirect($_SERVER['HTTP_REFERER']);
    }

    public function view_detail()
    {
        $id = segment(4);
        $this->data['sub_module_name'] = 'Volunteer Detail List';
        $this->data['type'] = 'volunteer_data';
        $volunteer_data = $this->volunteer->get_volunteer($id);

        $data = new stdClass();
        if (!empty($volunteer_data)) {
            $data->id = $volunteer_data->id;
            $data->dateOfRegistration = $volunteer_data->dateOfRegistration;
            $data->fullName = $volunteer_data->fullName;
            $data->dateOfBirth = $volunteer_data->dateOfBirth;
            $data->gender = $volunteer_data->gender;
            $data->phoneNumberResidential = $volunteer_data->phoneNumberResidential;
            $data->phoneNumberMobile = $volunteer_data->phoneNumberMobile;
            $data->email = $volunteer_data->email;
            $data->zone = $volunteer_data->zone;
            $data->district = $volunteer_data->district;
            $data->municipality = $volunteer_data->municipality;
            $data->academinQualification = $volunteer_data->academinQualification;
            $data->emergencyContactFullName = $volunteer_data->emergencyContactFullName;
            $data->emergencyContactPhoneNumber = $volunteer_data->emergencyContactPhoneNumber;
            $data->wardNumber = $volunteer_data->wardNumber;
            $data->volunteerType = $volunteer_data->volunteerType;
            if ($volunteer_data->volunteerType == 'partTime') {
                if ($volunteer_data->volunteerSubType == '0') {
                    $data->volunteerSubType = '9:00 AM -12:00 PM';
                } elseif ($volunteer_data->volunteerSubType == '1') {
                    $data->volunteerSubType = '12:00 PM – 4:00 PM';
                } elseif ($volunteer_data->volunteerSubType == '2') {
                    $data->volunteerSubType = 'Evenings';
                }elseif($volunteer_data->volunteerSubType == '3'){
                    $data->volunteerSubType = 'Weekends';
                }else{
                    $data->volunteerSubType = '';
                }
            }
            if ($volunteer_data->primaryModeOfCommunication == '0') {
                $data->primaryModeOfCommunication = 'E-mail';
            } elseif ($volunteer_data->primaryModeOfCommunication == '1') {
                $data->primaryModeOfCommunication = 'Phone call';
            } elseif ($volunteer_data->primaryModeOfCommunication == '2') {
                $data->primaryModeOfCommunication = 'Text Message';
            } elseif ($volunteer_data->primaryModeOfCommunication == '3') {
                $data->primaryModeOfCommunication = 'Facebook messenger';
            }
            if ($volunteer_data->secondaryModeOfCommunication == '0') {
                $data->secondaryModeOfCommunication = 'E-mail';
            } elseif ($volunteer_data->secondaryModeOfCommunication == '1') {
                $data->secondaryModeOfCommunication = 'Phone call';
            } elseif ($volunteer_data->secondaryModeOfCommunication == '2') {
                $data->secondaryModeOfCommunication = 'Text Message';
            } elseif ($volunteer_data->secondaryModeOfCommunication == '3') {
                $data->secondaryModeOfCommunication = 'Facebook messenger';
            }
            if ($volunteer_data->available == '0') {
                $data->available = 'Anywhere within Nepal';
            } elseif ($volunteer_data->available == '1') {
                $data->available = 'Within my State/Zone';
            } elseif ($volunteer_data->available == '2') {
                $data->available = 'Within my District';
            } elseif ($volunteer_data->available == '3') {
                $data->available = 'Within my VDC/Municipality';
            } elseif ($volunteer_data->available == '4') {
                $data->available = 'Within my Current Location';
            } else {
                $data->available = '';
            }

            $data->status = $volunteer_data->status;
            $this->data['data'] = $data;
        }
        $this->data['body'] = BACKENDFOLDER . '/memberFormData/_detail';
        $this->render();

    }

    public function status()
    {
        $page = segment(4);
        $status = segment(5);
        $id = segment(6);
        $post['status'] = $status;
        $res = $this->volunteer->save($post, ['id' => $id]);
        if ($res)
            set_flash('msg', 'Status changed');
        else
            set_flash('msg', 'Status could not be changed');
        if ($page == 'detail') {
            redirect(BACKENDFOLDER . '/volunteer_data/view_detail/' . $id);
        } else {
            redirect(BACKENDFOLDER . '/volunteer_data');
        }
    }


    public function create()
    {
        $this->data['sub_module_name'] = 'Volunteer Registration';
        $getGender = "select * from `nvp_gender`";
        $this->data['allGender'] = $this->content->query($getGender);
        $this->data['type'] = 'volunteer_data';
        $this->data['body'] = BACKENDFOLDER . '/memberFormData/_volunteerDataForm';
        $this->data['addJs'] = array(
            'assets/jquery-validate/jquery.validate.min.js',
            'assets/js/validateCustom.js',
            'assets/jquery-validate/additional-methods.min.js',
            'assets/js/volunteer_registerform.js',
            'assets/datepicker/bootstrap-datepicker.js',
            'assets/'.BACKENDFOLDER.'/dist/js/testimonial.js'
        );
        $this->data['addCss'] = array('assets/datepicker/datepicker3.css');
        $this->render();
    }
    public function user_status_verify($userId,$status)
    {
        $post['status'] = $status;
        $this->volunteer->save($post, ['id' => $userId]);
        $this->load->model('common_model', 'common');
        $this->common->table = 'nvp_verified_user';
        $verifiedUserData = array(
            'userId' => $userId,
            'userType' => 'Volunteer'
        );   
        $this->common->save($verifiedUserData);
    }

    public function register_volunteer()
    {
        if($_POST) {
            $post = $_POST;
            if ($post['userType'] == 'Volunteer') {
                $post['password'] = password_hash($post['password'], PASSWORD_BCRYPT, array('cost' => 10));
                unset($post['repassword'], $post['userType']);
                $post['dateOfBirth'] = strtotime($post['dateOfBirth']);
                $post['dateOfRegistration'] = date('Y-m-d');
                $userId = $this->volunteer->save($post, '', true);
                $this->user_status_verify($userId,'active');
                if ($userId)
                    set_flash('msg', 'Volunteer Registered Successfully');
                redirect(BACKENDFOLDER . '/volunteer_data');
                // data form email template
                // $siteLogo = base_url($this->global_config->site_logo);
                // $siteLink = base_url();
                // $accountDetailLink = base_url(BACKENDFOLDER . '/volunteer_data/view_detail/' . $userId);
                // $activationLink = base_url('members/register/activate/volunteer/' . md5($userId));
                // $this->load->model('emailtemplate_model', 'emailtemplate');
                // $volunteerRegistrationEmailTemplate = $this->emailtemplate->getAllData(2);
                // $adminMessage = str_replace(
                //     array('#siteLogo#', '#siteLink#', '#emailSubject#', '#accountDetailLink#'),
                //     array($siteLogo, $siteLink, $volunteerRegistrationEmailTemplate[0]->adminSubject, $accountDetailLink),
                //     $volunteerRegistrationEmailTemplate[0]->adminMessage
                // );
                // $volunteerMessage = str_replace(
                //     array('#siteLogo#', '#siteLink#', '#emailSubject#', '#accountActivationLink#'),
                //     array($siteLogo, $siteLink, $volunteerRegistrationEmailTemplate[0]->userSubject, $activationLink),
                //     $volunteerRegistrationEmailTemplate[0]->userMessage
                // );
                // $email_params = array(
                //     'subject' => 'New Account Created',
                //     'from' => $volunteerRegistrationEmailTemplate[0]->adminEmail,
                //     'fromname' => SITENAME,
                //     'to' => $post['email'],
                //     'toname' => $post['fullName'],
                //     'message' => $volunteerMessage
                // );
                // swiftsend($email_params);
                // $email_params = array(
                //     'subject' => 'New Account Created',
                //     'from' => $post['email'],
                //     'fromname' => $post['fullName'],
                //     'to' => $volunteerRegistrationEmailTemplate[0]->adminEmail,
                //     'toname' => SITENAME,
                //     'message' => $adminMessage
                // );
                // swiftsend($email_params);
                
            }
        }
    }


}