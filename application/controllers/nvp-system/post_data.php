<?php

class Post_data extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('post_model', 'post');
        $this->data['module_name'] = 'Post Manager';
        $this->data['show_add_link'] = false;
        $this->data['show_sort_link'] = false;
    }

    public function index()
    {
        $query = "SELECT *
                  FROM `nvp_post`
                  ORDER BY `id` desc";
        $this->data['sub_module_name'] = 'Post Data List';
        $this->data['posts'] = $this->post->query($query);
        $this->data['body'] = BACKENDFOLDER . '/memberFormData/_postData';
        $this->render();
    }

    public function view_detail()
    {
        $this->data['sub_module_name'] = 'Post Data Detail';
        $id = segment(4);
        $this->data['data'] = $this->post->get('1', ['id' => $id]);
        $usertype = strtolower($this->data['data']->userType);
        $this->load->model($usertype . '_model', 'type');
        $this->data['user'] = $this->type->get('1', ['id' => $this->data['data']->userId]);
        $this->data['body'] = BACKENDFOLDER . '/memberFormData/_postDataDetail';
        $this->render();

    }

    public function status()
    {
        $page = segment(4);
        $status = segment(5);
        $id = segment(6);
        $post['status'] = $status;
        $res = $this->post->save($post, ['id' => $id]);
        if ($res)
            set_flash('msg', 'Status changed');
        else
            set_flash('msg', 'Status could not be changed');

        if ($page == 'list')
            redirect(BACKENDFOLDER . '/post_data');
        else
            redirect(BACKENDFOLDER . '/post_data/view_detail/' . $id);
    }

}