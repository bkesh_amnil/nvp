<?php

class Comment extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('postcomment_model', 'postcomment');
        $this->data['module_name'] = 'CommentManager';
        $this->data['show_add_link'] = false;
        $this->data['show_sort_link'] = false;
    }

    public function index()
    {
        $query = "SELECT *
                  FROM `nvp_post`
                  ORDER BY `id` desc";
        $this->data['sub_module_name'] = 'Comment List';
        $this->data['posts'] = $this->post->query($query);
        $this->data['body'] = BACKENDFOLDER . '/memberFormData/_postData';
        $this->render();
    }

    public function view_detail()
    {
        $this->data['sub_module_name'] = 'Comment Detail';
        $id = segment(4);
        $query = "select pc.*, p.id as post_id, p.name as post_title from nvp_post_comments pc join nvp_post p on p.id = pc.postId where pc.id = $id";
        $this->data['data'] = (object) $this->postcomment->query($query)[0];
        $usertype = strtolower($this->data['data']->userType);
        $this->load->model($usertype . '_model', 'type');
        $this->data['user'] = $this->type->get('1', ['id' => $this->data['data']->userId]);
        $this->data['body'] = BACKENDFOLDER . '/memberFormData/_comment_detail';
        $this->render();

    }

    public function status()
    {
        $status = segment(4);
        $id = segment(5);
        $post['status'] = $status;
        $res = $this->postcomment->save($post, ['id' => $id]);
        if ($res)
            set_flash('msg', 'Status changed');
        else
            set_flash('msg', 'Status could not be changed');

        redirect(BACKENDFOLDER . '/comment/view_detail/' . $id);
    }

}