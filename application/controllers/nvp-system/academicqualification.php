<?php

class Academicqualification extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('academicqualification_model', 'academicqualification');
        $this->data['module_name'] = 'Academic Qualification Manager';
        $this->data['show_add_link'] = true;
        $this->data['show_sort_link'] = true;
    }

    public function index()
    {
        $this->data['sub_module_name'] = 'Academic Qualification List';
        $this->data['academicqualifications'] = $this->academicqualification->get('', '', 'orderNumber ASC');
        $this->data['body'] = BACKENDFOLDER.'/academicqualification/_list';
        $this->render();
    }

    public function create()
    {
        $id = segment(4);
        if($_POST) {
            $post = $_POST;
            $this->academicqualification->id = $id;

            $this->form_validation->set_rules($this->academicqualification->rules($id));
            if($this->form_validation->run()) {
                if($id == '') {
                    $res = $this->academicqualification->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->academicqualification->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/academicqualification');
            } else {
                $this->form($id, 'academicqualification');
            }
        } else {
            $this->form($id, 'academicqualification');
        }
    }

    public function delete()
    {
        $id = segment(4);
        $res = $this->academicqualification->delete(array('id' => $id));

        $res ? set_flash('msg', 'Data deleted') : set_flash('msg', 'Data could not be deleted');

        redirect(BACKENDFOLDER.'/academicqualification');
    }

    public function status()
    {
        $res = $this->changeStatus('academicqualification');
        if($res)
            set_flash('msg', 'Status changed');
        else
            set_flash('msg', 'Status could not be changed');

        redirect(BACKENDFOLDER.'/academicqualification');
    }

}