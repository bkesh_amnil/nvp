<?php

class Agencytype extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('agencytype_model', 'agencytype');
        $this->data['module_name'] = 'Agency Type Manager';
        $this->data['show_add_link'] = true;
        $this->data['show_sort_link'] = true;
    }

    public function index()
    {
        $this->data['sub_module_name'] = 'Agency Type List';
        $this->data['agencytypes'] = $this->agencytype->get('', '', 'orderNumber asc');
        $this->data['body'] = BACKENDFOLDER.'/agencytype/_list';
        $this->render();
    }

    public function create()
    {
        $id = segment(4);
        if($_POST) {
            $post = $_POST;
            $this->agencytype->id = $id;

            $this->form_validation->set_rules($this->agencytype->rules($id));
            if($this->form_validation->run()) {
                if($id == '') {
                    $res = $this->agencytype->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->agencytype->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/agencytype');
            } else {
                $this->form($id, 'agencytype');
            }
        } else {
            $this->form($id, 'agencytype');
        }
    }

    public function delete()
    {
        $id = segment(4);
        $res = $this->agencytype->delete(array('id' => $id));

        $res ? set_flash('msg', 'Data deleted') : set_flash('msg', 'Data could not be deleted');

        redirect(BACKENDFOLDER.'/agencytype');
    }

    public function status()
    {
        $res = $this->changeStatus('agencytype');
        if($res)
            set_flash('msg', 'Status changed');
        else
            set_flash('msg', 'Status could not be changed');

        redirect(BACKENDFOLDER.'/agencytype');
    }

}