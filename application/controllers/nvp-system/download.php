<?php

class Download extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('download_model', 'download');
        $this->data['module_name'] = 'Download Manager';
        $this->data['show_add_link'] = true;
        $this->data['show_sort_link'] = true;
    }

    public function index()
    {
        $this->data['sub_module_name'] = 'Download List';
        $this->data['downloads'] = $this->download->getAllData();
        $this->data['body'] = BACKENDFOLDER.'/download/_list';
        $this->render();
    }

    public function create()
    {
        $id = segment(4);
        $this->data['downloadCategory'] = $this->download->activeCategories(3);
        if($_POST) {
            $post = $_POST;
            $this->download->id = $id;

            $this->form_validation->set_rules($this->download->rules($id));
            if($this->form_validation->run()) {
                if($id == '') {
                    $res = $this->download->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->download->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/download');
            } else {
                $this->data['addJs'] = array(
                    'assets/datepicker/bootstrap-datepicker.js',
                    'assets/'.BACKENDFOLDER.'/dist/js/download.js'
                );
                $this->data['addCss'] = array('assets/datepicker/datepicker3.css');
                $this->form($id, 'download');
            }
        } else {
            $this->data['addJs'] = array(
                'assets/datepicker/bootstrap-datepicker.js',
                'assets/'.BACKENDFOLDER.'/dist/js/download.js'
            );
            $this->data['addCss'] = array('assets/datepicker/datepicker3.css');
            $this->form($id, 'download');
        }
    }

    public function delete()
    {
        $id = segment(4);
        $res = $this->download->delete(array('id' => $id));

        $res ? set_flash('msg', 'Data deleted') : set_flash('msg', 'Data could not be deleted');

        redirect(BACKENDFOLDER.'/download');
    }

    public function status()
    {
        $res = $this->changeStatus('download');
        if($res)
            set_flash('msg', 'Status changed');
        else
            set_flash('msg', 'Status could not be changed');

        redirect(BACKENDFOLDER.'/download');
    }

}