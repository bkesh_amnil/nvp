<?php

class Municipality extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('municipality_model', 'municipality');
        $this->load->model('district_model', 'district');
        $this->data['module_name'] = 'Municipality Manager';
        $this->data['show_add_link'] = true;
    }

    public function index()
    {
        $this->data['sub_module_name'] = 'Municipality List';
        $this->data['municipalities'] = $this->municipality->getAllData();
        $this->data['body'] = BACKENDFOLDER.'/municipality/_list';
        $this->render();
    }

    public function create()
    {
        $id = segment(4);
        $this->data['allDistricts'] = $this->district->get();
        if($_POST) {
            $post = $_POST;
            $this->municipality->id = $id;

            $this->form_validation->set_rules($this->municipality->rules($id));
            if($this->form_validation->run()) {
                if($id == '') {
                    $res = $this->municipality->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->municipality->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/municipality');
            } else {
                $this->form($id, 'municipality');
            }
        } else {
            $this->form($id, 'municipality');
        }
    }

    public function delete()
    {
        $id = segment(4);
        $res = $this->municipality->delete(array('id' => $id));

        $res ? set_flash('msg', 'Data deleted') : set_flash('msg', 'Data could not be deleted');

        redirect(BACKENDFOLDER.'/municipality');
    }

}