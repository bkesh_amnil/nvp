<?php

class Flagged extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('common_model', 'common');
        $this->data['module_name'] = 'Flagged Content Manager';
        $this->data['show_add_link'] = false;
        $this->data['show_sort_link'] = false;
    }

    public function index()
    {
        $query = "select * from tbl_flag_data order by id desc";
        $this->data['sub_module_name'] = 'Flagged Content List';
        $this->data['flagged'] = $this->common->query($query);
        $this->data['body'] = BACKENDFOLDER.'/flagged/_list';
        $this->render();
    }

}