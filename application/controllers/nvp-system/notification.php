<?php

class Notification extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('notification_model', 'notification');
        $this->load->model('content_model', 'content');
        $this->data['module_name'] = 'Notification Manager';
        $this->data['show_add_link'] = true;
        $this->data['show_sort_link'] = false;
    }

    public function index()
    {
        $this->data['sub_module_name'] = 'Notification List';
        $this->data['notifications'] = $this->notification->getAllData();
        $this->data['body'] = BACKENDFOLDER . '/notification/_list';
        $this->render();
    }

    public function create()
    {
        if (isset($this->data['addCss'])) {
            $this->data['addCss'] = array_merge($this->data['addCss'], array(
                'assets/select2/css/select2.min.css'
            ));
        } else {
            $this->data['addCss'] = array(
                'assets/select2/css/select2.min.css'
            );
        }

        if (isset($this->data['addJs'])) {
            $this->data['addJs'] = array_merge($this->data['addJs'], array(
                'assets/select2/js/select2.full.min.js',
                'assets/js/select2.init.js'
            ));
        } else {
            $this->data['addJs'] = array(
                'assets/select2/js/select2.full.min.js',
                'assets/js/select2.init.js'
            );
        }

        $id = segment(4);
        $contents = $this->content->get('', ['status' => 'Active']);
        foreach ($contents as $content) {
            $data[$content->id] = $content->name;
        }
        $this->data['contents'] = $data;

        if ($_POST) {
            $post = $_POST;
            $this->notification->id = $id;

            $this->form_validation->set_rules($this->notification->rules($id));
            if ($this->form_validation->run()) {
                if ($id == '') {
                    $res = $this->notification->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->notification->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                if($res) {
                    $this->send_notification($post, '', true);
                }
                redirect(BACKENDFOLDER . '/notification');
            } else {
                $this->form($id, 'notification');
            }
        } else {
            $this->form($id, 'notification');
        }
    }

    public function delete()
    {
        $id = segment(4);
        $res = $this->notification->delete(array('id' => $id));

        $res ? set_flash('msg', 'Data deleted') : set_flash('msg', 'Data could not be deleted');

        redirect(BACKENDFOLDER . '/notification');
    }

    public function _saveInvitee($users, $projectId, $notificationId)
    {
        $this->load->model('agencyprojectvolunteerinvitee_model', 'agencyprojectvolunteerinvitee');
        foreach($users as $user) {
            $inviteeSaveData = array(
                'volunteerId' => $user,
                'agencyProjectId' => $projectId,
                'invitationSentDateTime' => time(),
                'invitationSentBy' => get_userdata('user_id'),
                'notificationId' => $notificationId
            );
            $this->agencyprojectvolunteerinvitee->save($inviteeSaveData);
        }
        return true;
    }

    public function send_notification($notificationData, $notificationId, $sendAll = false)
    {
        $this->load->model('appuser_model', 'appuser');
        if($sendAll) {
            $devicesTokens = $this->appuser->get();
            if($devicesTokens) {
                $this->load->model('volunteer_model', 'volunteer');
                foreach($devicesTokens as $devicesToken) {
                    if($devicesToken->userType == 'Volunteer') {
                        $isNotificationOn = $this->volunteer->get('1', array('id' => $devicesToken->userId));
                        if($isNotificationOn->notification_status && $isNotificationOn->notification_status == 'Active') {
                            $tokens[] = $devicesToken->deviceToken;
                        }
                    } else {
                        $tokens[] = $devicesToken->deviceToken;
                    }
                }
            }
        } else {
            $users = explode(',', $notificationData['userList']);
            $this->_saveInvitee($users, $notificationData['project_id'], $notificationId);
            if(count($users) > 1) {
                $users = "'" . implode("','", $users) . "'";
                $query = "select deviceToken, deviceType from nvp_app_user au where au.userId in ($users) and au.userType = 'Volunteer'";
                $devicesTokens = $this->appuser->query($query);
            } else {
                $devicesTokens = $this->appuser->get('', array('userId' => $notificationData['userList'], 'userType' => 'Volunteer'));
            }
        }

        $notification_user = [];
        if ($devicesTokens) {
            foreach ($devicesTokens as $device_token) {
                $notification_user[] = $device_token->deviceToken;
            }
        }
        $androidMessage = array(
            'message' => strip_tags($notificationData['short_description']),
            'title' => $notificationData['title'],
            'projectId' => isset($notificationData['project_id']) ? $notificationData['project_id'] : '',
            'notId' => rand(1, 9999),
            'id' => $notificationId
        );
        $iosMessage = array(
            'id' => $notificationId,
            'alert' => strip_tags($notificationData['short_description']),
            "sound" => "default",
            'projectId' => isset($notificationData['project_id']) ? $notificationData['project_id'] : ''
        );
        $res = $this->sendNotificationAndroid($androidMessage, $notification_user);
        $res = $this->sendNotificationIos($iosMessage, $notification_user);
        return $res;
    }

    public function projectnotification()
    {
        if ($this->input->post()) {
            $this->load->model('notification_model', 'notification');
            $post = $this->input->post();
            $notificationData = [
                'title' => $post['title'],
                'short_description' => $post['short_description'],
                'projectId' => $post['project_id'],
                'lastRespondDate' => strtotime($post['lastRespondDate'])
            ];
            $res = $this->notification->save($notificationData, '', true);
            if ($res) {
                $res = $this->send_notification($post, $res);
                $res ? set_flash('msg', 'Notification sent') : set_flash('msg', 'Notification not sent');
                redirect(BACKENDFOLDER . '/searchvolunteer/');
            }
        }
    }

    public function sendNotificationAndroid($notificationMessage, $notification_user = '')
    {
        $appId = PARSEAPPID;;
        $resApiKey = PARSERESTAPIKEY;;
        $url = 'https://api.parse.com/1/push';
        if ($notification_user == '') {
            $data = array(
                'where' => array(
                    'deviceType' => 'android'
                ),
                'message' => $notificationMessage['message'],
                'data' => $notificationMessage
            );
        } else {
            $data = array(
                'where' => array(
                    'deviceType' => 'android',
                    'deviceToken' => array('$in' => $notification_user)
                ),
                'message' => $notificationMessage['message'],
                'data' => $notificationMessage
            );
        }
        $_data = json_encode($data);
        $headers = array(
            'X-Parse-Application-Id: ' . $appId,
            'X-Parse-REST-API-Key: ' . $resApiKey,
            'Content-Type: application/json',
            'Content-Length: ' . strlen($_data),
        );
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $_data);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($curl);
        return $response;
    }

    public function sendNotificationIos($notificationMessage, $notification_user = '')
    {
        $appId = PARSEAPPID;
        $resApiKey = PARSERESTAPIKEY;
        $url = 'https://api.parse.com/1/push';
        if ($notification_user == '') {
            $data = array(
                'where' => array(
                    'deviceType' => 'ios'
                ),
                'alert' => $notificationMessage['alert'],
                'data' => $notificationMessage
            );
        } else {
            $data = array(
                'where' => array(
                    'deviceType' => 'ios',
                    'deviceToken' => array('$in' => $notification_user)
                ),
                'alert' => $notificationMessage['alert'],
                'data' => $notificationMessage
            );
        }
        $_data = json_encode($data);
        $headers = array(
            'X-Parse-Application-Id: ' . $appId,
            'X-Parse-REST-API-Key: ' . $resApiKey,
            'Content-Type: application/json',
            'Content-Length: ' . strlen($_data),
        );

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $_data);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($curl);
        return $response;
    }

}