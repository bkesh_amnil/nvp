<?php

class Partner extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('partner_model', 'partner');
        $this->data['module_name'] = 'Partner Manager';
        $this->data['show_add_link'] = true;
    }

    public function index()
    {
        $this->data['sub_module_name'] = 'Partner List';
        $this->data['partners'] = $this->partner->getAllData();
        $this->data['body'] = BACKENDFOLDER.'/partner/_list';
        $this->render();
    }

    public function create()
    {
        $id = segment(4);
        $this->data['partnerCategory'] = $this->partner->activeCategories(2);
        if($_POST) {
            $post = $_POST;
            $this->partner->id = $id;

            $this->form_validation->set_rules($this->partner->rules($id));
            if($this->form_validation->run()) {
                $post['slug'] = $this->partner->createSlug($post['name'], $id);
                if($id == '') {
                    $res = $this->partner->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->partner->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/partner');
            } else {
                $this->form($id, 'partner');
            }
        } else {
            $this->form($id, 'partner');
        }
    }

    public function delete()
    {
        $id = segment(4);
        $res = $this->partner->delete(array('id' => $id));

        $res ? set_flash('msg', 'Data deleted') : set_flash('msg', 'Data could not be deleted');

        redirect(BACKENDFOLDER.'/partner');
    }

    public function unique_partner($name)
    {
        $id = $this->partner->id;
        $partner = $this->partner->get(1, array('id' => $id));
        if($partner) {
            $old_partner = $partner->name;
            if ($name == $old_partner)
                return true;
        }
        $unique_partner = $this->partner->get(1, array('name' => $name));
        if ($unique_partner) {
            $this->form_validation->set_message('unique_partner', 'The Partner     already exists.');

            return false;
        }
        return true;
    }

    public function status()
    {
        $res = $this->changeStatus('partner');
        if($res)
            set_flash('msg', 'Status changed');
        else
            set_flash('msg', 'Status could not be changed');

        redirect(BACKENDFOLDER.'/partner');
    }

}