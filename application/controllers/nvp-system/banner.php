<?php

class Banner extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('banner_model', 'banner');
        $this->data['module_name'] = 'Banner Manager';
        $this->data['show_add_link'] = true;
        $this->data['show_sort_link'] = true;
    }

    public function index()
    {
        $query = "SELECT b.*,c.name as category_name
                  FROM `tbl_banner` b
                  JOIN `tbl_category` c ON b.`category_id` = c.`id`
                  ORDER BY orderNumber ASC";
        $this->data['sub_module_name'] = 'Banner List';
        $this->data['banners'] = $this->banner->query($query);
        $this->data['body'] = BACKENDFOLDER.'/banner/_list';
        $this->render();
    }

    public function create()
    {
        $id = segment(4);
        if($_POST) {
            $post = $_POST;
            $this->banner->id = $id;

            $this->form_validation->set_rules($this->banner->rules);
            if($this->form_validation->run()) {
                if($id == '') {
                    $res = $this->banner->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->banner->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/banner');
            } else {
                $this->form($id, 'banner');
            }
        } else {
            $this->form($id, 'banner');
        }
    }

    public function delete()
    {
        $id = segment(4);
        $res = $this->banner->delete(array('id' => $id));

        $res ? set_flash('msg', 'Data deleted') : set_flash('msg', 'Data could not be deleted');

        redirect(BACKENDFOLDER.'/banner');
    }

    public function status()
    {
        $res = $this->changeStatus('banner');
        if($res)
            set_flash('msg', 'Status changed');
        else
            set_flash('msg', 'Status could not be changed');

        redirect(BACKENDFOLDER.'/banner');
    }

}