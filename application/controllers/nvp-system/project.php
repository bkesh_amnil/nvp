<?php

class Project extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('project_model', 'project');
        $this->data['module_name'] = 'Project Manager';
        $this->data['show_add_link'] = true;
    }

    public function index()
    {
        $this->data['sub_module_name'] = 'Project List';
        $this->data['projects'] = $this->project->get();
        $this->data['body'] = BACKENDFOLDER.'/project/_list';
        $this->render();
    }

    public function create()
    {
        $id = segment(4);
        if($_POST) {
            $post = $_POST;
            $this->project->id = $id;

            $this->form_validation->set_rules($this->project->rules($id));
            if($this->form_validation->run()) {
                $post['slug'] = $this->project->createSlug($post['name'], $id);
                if($id == '') {
                    $res = $this->project->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->project->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/project');
            } else {
                $this->form($id, 'project');
            }
        } else {
            $this->form($id, 'project');
        }
    }

    public function delete()
    {
        $id = segment(4);
        $res = $this->project->delete(array('id' => $id));

        $res ? set_flash('msg', 'Data deleted') : set_flash('msg', 'Data could not be deleted');

        redirect(BACKENDFOLDER.'/project');
    }

    public function status()
    {
        $res = $this->changeStatus('project');
        if($res)
            set_flash('msg', 'Status changed');
        else
            set_flash('msg', 'Status could not be changed');

        redirect(BACKENDFOLDER.'/project');
    }

}