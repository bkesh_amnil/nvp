<?php

class Team extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('team_model', 'team');
        $this->data['module_name'] = 'Team';
        $this->data['show_add_link'] = true;
        $this->data['show_sort_link'] = true;
    }

    public function index()
    {
        $this->data['sub_module_name'] = 'Team List';
        $this->data['teams'] = $this->team->get('', '', 'orderNumber ASC');
        $this->data['body'] = BACKENDFOLDER.'/team/_list';
        $this->render();
    }

    public function create()
    {
        $id = segment(4);
        $this->data['teamCategory'] = $this->team->activeCategories(4);
        if($_POST) {
            $post = $_POST;
            $this->team->id = $id;

            $this->form_validation->set_rules($this->team->rules($id));
            if($this->form_validation->run()) {
                if($id == '') {
                    $res = $this->team->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->team->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/team');
            } else {
                $this->form($id, 'team');
            }
        } else {
            $this->form($id, 'team');
        }
    }

    public function delete()
    {
        $id = segment(4);
        $res = $this->team->delete(array('id' => $id));

        $res ? set_flash('msg', 'Data deleted') : set_flash('msg', 'Data could not be deleted');

        redirect(BACKENDFOLDER.'/team');
    }

    public function status()
    {
        $res = $this->changeStatus('team');
        if($res)
            set_flash('msg', 'Status changed');
        else
            set_flash('msg', 'Status could not be changed');

        redirect(BACKENDFOLDER.'/team');
    }

}