<?php

class Language extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('language_model', 'language');
        $this->data['module_name'] = 'Language Manager';
        $this->data['show_add_link'] = true;
        $this->data['show_sort_link'] = true;
    }

    public function index()
    {
        $this->data['sub_module_name'] = 'Language List';
        $this->data['languages'] = $this->language->get('', '', 'orderNumber ASC');
        $this->data['body'] = BACKENDFOLDER.'/language/_list';
        $this->render();
    }

    public function create()
    {
        $id = segment(4);
        if($_POST) {
            $post = $_POST;
            $this->language->id = $id;

            $this->form_validation->set_rules($this->language->rules($id));
            if($this->form_validation->run()) {
                if($id == '') {
                    $res = $this->language->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->language->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/language');
            } else {
                $this->form($id, 'language');
            }
        } else {
            $this->form($id, 'language');
        }
    }

    public function delete()
    {
        $id = segment(4);
        $res = $this->language->delete(array('id' => $id));

        $res ? set_flash('msg', 'Data deleted') : set_flash('msg', 'Data could not be deleted');

        redirect(BACKENDFOLDER.'/language');
    }

    public function status()
    {
        $res = $this->changeStatus('language');
        if($res)
            set_flash('msg', 'Status changed');
        else
            set_flash('msg', 'Status could not be changed');

        redirect(BACKENDFOLDER.'/language');
    }

}