<?php

class Sociallogin extends My_Front_Controller {

    public function __construct()
    {
        parent::__construct();

        // require_once( APPPATH . "third_party/hybridauth/hybridauth/Hybrid/Auth.php" );

        // password hashing library for older PHP version
        require APPPATH.'/third_party/password.php';
    }

    function _remap($method)
    {
        $param_offset = 2;

        // Default to index
        if ( ! method_exists($this, $method))
        {
            // We need one more param
            $param_offset = 1;
            $method = 'index';
        }

        // Since all we get is $method, load up everything else in the URI
        $params = array_slice($this->uri->rsegment_array(), $param_offset);

        // Call the determined method with all params
        call_user_func_array(array($this, $method), $params);
    }

    public function index($provider = '')
    {
        $profileStatus = false;
        $config = getHybridAuthConfig();

        $this->load->model('volunteer_model', 'volunteer');
        if($provider == 'facebook') {
            $hybridauth = new Hybrid_Auth( $config );

            $adapter = $hybridauth->authenticate( "Facebook" );

            $user_profile = $adapter->getUserProfile();

            $dob = isset($user_profile->birthYear) ?  $user_profile->birthYear.'-'.$user_profile->birthMonth.'-'.$user_profile->birthDay : '';
            $gender = '';
            if(isset($user_profile->gender)) {
                if($user_profile->gender == 'male') {
                    $gender = 1;
                } else if($user_profile->gender == 'female') {
                    $gender = 2;
                }
            }
            $userData = array(
                'fullName' => $user_profile->firstName . ' ' . $user_profile->lastName,
                'email' => $user_profile->email,
                'dateOfBirth' => strtotime($dob),
                'socialId' => $user_profile->identifier,
                'genderId' => $gender,
                'status' => 'Active'
            );

            // check if user already logged in
            $existingUserData = $this->volunteer->get('1', array('socialId' => $userData['socialId']));
            if($existingUserData) {
                $profileStatus = checkProfileStatus($existingUserData);
                $userId = $existingUserData->id;
                $userData['profilePicture'] = $existingUserData->profilePicture;
                // $this->volunteer->save($userData, array('id' => $userId));
            } else {
                $userId = $this->volunteer->save($userData, '', true);
            }
        } else if($provider == 'twitter') {
            $hybridauth = new Hybrid_Auth( $config );

            $adapter = $hybridauth->authenticate( "Twitter" );

            $user_profile = $adapter->getUserProfile();
            $adapter->logout();

            $userData = array(
                'fullName' => $user_profile->firstName,
                'socialId' => $user_profile->identifier,
                'status' => 'Active'
            );

            // check if user already logged in
            $existingUserData = $this->volunteer->get('1', array('socialId' => $userData['socialId']));
            if($existingUserData) {
                $profileStatus = checkProfileStatus($existingUserData);
                $userId = $existingUserData->id;
                $userData['profilePicture'] = $existingUserData->profilePicture;
                // $this->volunteer->save($userData, array('id' => $userId));
            } else {
                $userId = $this->volunteer->save($userData, '', true);
            }
        }
        set_userdata(array(
            'active_user' => $userId,
            'userType' => 'Volunteer',
            'userName' => $userData['fullName']
        ));

        if(strpos($userData['profilePicture'], 'https') !== false || strpos($userData['profilePicture'], 'http') !== false) {
            set_userdata(array(
                'profilePicture' => $userData['profilePicture']
            ));
        } else {
            set_userdata(array(
                'profilePicture' => base_url($userData['profilePicture'])
            ));
        }

        if(!$profileStatus) {
            redirect('members/profile/edit');
        } else {
            redirect('members/profile');
        }
    }

    public function process()
    {
        require_once( APPPATH . "third_party/hybridauth/hybridauth/Hybrid/Endpoint.php" );

        Hybrid_Endpoint::process();
    }

    public function login($provider)
    {
        try
        {
            $this->load->model('volunteer_model', 'volunteer');
            $this->load->library('HybridAuthLib');
            $profileStatus = false;

            if ($this->hybridauthlib->providerEnabled($provider))
            {
                $service = $this->hybridauthlib->authenticate($provider);

                if ($service->isUserConnected())
                {
                    $user_profile = $service->getUserProfile();

                    if($provider == 'Facebook') {
                        $dob = isset($user_profile->birthYear) ?  $user_profile->birthYear.'-'.$user_profile->birthMonth.'-'.$user_profile->birthDay : '';
                        $gender = '';
                        if(isset($user_profile->gender)) {
                            if($user_profile->gender == 'male') {
                                $gender = 1;
                            } else if($user_profile->gender == 'female') {
                                $gender = 2;
                            }
                        }
                        $userData = array(
                            'fullName' => $user_profile->displayName,
                            'email' => $user_profile->email,
                            'dateOfBirth' => strtotime($dob),
                            'socialId' => $user_profile->identifier,
                            'genderId' => $gender,
                            'profilePicture' => $user_profile->photoURL,
                            'status' => 'Active'
                        );

                        // check if user already logged in
                        $existingEmailData = $this->volunteer->get('1', array('email' => $userData['email']));
                        $existingUserData = $this->volunteer->get('1', array('socialId' => $userData['socialId']));
                        if($existingEmailData) {
                            $userData['email'] = '';
                        }
                        if($existingUserData) {
                            $profileStatus = checkProfileStatus($existingUserData);
                            $userId = $existingUserData->id;
                            $userData['profilePicture'] = $existingUserData->profilePicture;
                            // $this->volunteer->save($userData, array('id' => $userId));
                        } else {
                            $userId = $this->volunteer->save($userData, '', true);
                        }
                    } else if($provider == 'Twitter') {
                        $userData = array(
                            'fullName' => $user_profile->firstName,
                            'socialId' => $user_profile->identifier,
                            'profilePicture' => $user_profile->photoURL,
                            'status' => 'Active'
                        );

                        // check if user already logged in
                        $existingUserData = $this->volunteer->get('1', array('socialId' => $userData['socialId']));
                        if($existingUserData) {
                            $profileStatus = checkProfileStatus($existingUserData);
                            $userId = $existingUserData->id;
                            $userData['profilePicture'] = $existingUserData->profilePicture;
                            // $this->volunteer->save($userData, array('id' => $userId));
                        } else {
                            $userId = $this->volunteer->save($userData, '', true);
                        }
                    }
                    set_userdata(array(
                        'active_user' => $userId,
                        'userType' => 'Volunteer',
                        'userName' => $userData['fullName']
                    ));

                    if(strpos($userData['profilePicture'], 'https') !== false || strpos($userData['profilePicture'], 'http') !== false) {
                        set_userdata(array(
                            'profilePicture' => $userData['profilePicture']
                        ));
                    } else {
                        set_userdata(array(
                            'profilePicture' => base_url($userData['profilePicture'])
                        ));
                    }

                    $service->logout();
                    if(!$profileStatus) {
                        redirect('members/profile/edit');
                    } else {
                        redirect('members/profile');
                    }
                }
                else // Cannot authenticate user
                {
                    die('Cannot authenticate user');
                }
            }
            else // This service is not enabled.
            {
                show_404($_SERVER['REQUEST_URI']);
            }
        }
        catch(Exception $e)
        {
            if(ENVIRONMENT == 'development') {
                $error = 'Unexpected error';
                switch($e->getCode())
                {
                    case 0 : $error = 'Unspecified error.'; break;
                    case 1 : $error = 'Hybriauth configuration error.'; break;
                    case 2 : $error = 'Provider not properly configured.'; break;
                    case 3 : $error = 'Unknown or disabled provider.'; break;
                    case 4 : $error = 'Missing provider application credentials.'; break;
                    case 5 :
                        //redirect();
                        if (isset($service))
                        {
                            $service->logout();
                        }
                        break;
                    case 6 : $error = 'User profile request failed. Most likely the user is not connected to the provider and he should to authenticate again.';
                        break;
                    case 7 : $error = 'User not connected to the provider.';
                        break;
                }

                if (isset($service))
                {
                    $service->logout();
                }
            } else {
                redirect();
            }
        }
    }

    public function endpoint()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET')
        {
            $_GET = $_REQUEST;
        }

        require_once APPPATH.'/third_party/hybridauth/index.php';

    }

}