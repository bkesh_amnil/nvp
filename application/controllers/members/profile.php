<?php

class Profile extends My_Front_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('postlike_model', 'postlike');
        $this->load->model('postcomment_model', 'postcomment');
    }

    function _remap($method)
    {
        $param_offset = 2;

        // Default to index
        if (!method_exists($this, $method)) {
            // We need one more param
            $param_offset = 1;
            $method = 'index';
        }

        // Since all we get is $method, load up everything else in the URI
        $params = array_slice($this->uri->rsegment_array(), $param_offset);

        // Call the determined method with all params
        call_user_func_array(array($this, $method), $params);
    }

    public function index()
    {
        $this->load->model('post_model', 'post');
        $this->data['body'] = 'members/profile/_index';
        //$this->data['newsFeeds'] = $this->post->getNewsFeed();

        // news feed pagination
        $offset = segment(3) == '' ? '0' : segment(3);
        $this->load->library('pagination');
        $config['base_url'] = base_url('members/profile');
        $config['total_rows'] = count($this->post->get('', array('status' => 'Active')));
        //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<div class="center"><ul class="pagination">';
        $config['full_tag_close'] = '</ul></div>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<i class="material-icons">chevron_left</i>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '<i class="material-icons">chevron_right</i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="waves-effect">';
        $config['num_tag_close'] = '</li>';
        $config['per_page'] = 6;
        $config['uri_segment'] = 3;

        $this->pagination->initialize($config);

        $this->data['newsFeedPagination'] = $this->pagination->create_links();
        $this->data['newsFeeds'] = $this->post->getNewsFeedPagination($offset);
        // news feed pagination

        $this->load->model('agencyproject_model', 'agencyproject');
        if (get_userdata('userType') == 'Agency') {
            $this->load->model('academicqualification_model', 'academicqualification');
            $this->load->model('common_model', 'common');
            $this->data['languages'] = $this->common->query('select * from nvp_language');
            $this->data['allGender'] = $this->common->query('select * from nvp_gender');
            $this->data['zones'] = $this->common->query('select * from nvp_zone order by name asc');
            $this->data['districts'] = $this->common->query('select * from nvp_district order by name asc');
            $this->data['municipalities'] = $this->common->query('select * from nvp_municipality order by name asc');
            $this->data['academicQualifications'] = $this->academicqualification->get('', array('status' => 'Active'), 'orderNumber asc');
            $this->data['skills'] = $this->common->query("select * from nvp_skill where status = 'Active'");
            $this->data['trainings'] = $this->common->query("select * from nvp_training where status = 'Active'");
            /* $this->data['projectList'] = $this->agencyproject->get('', array('agencyId' => get_userdata('active_user')), 'dateOfEvent desc');*/
            $this->data['projectList'] = $this->agencyproject->getAllData(get_userdata('active_user'));
        } else if (get_userdata('userType') == 'Volunteer') {
            if ($this->input->get()) {
                $this->data['projectList'] = $this->agencyproject->filterProject($this->input->get());
            } else {
                $this->data['projectList'] = $this->agencyproject->filterProject('');
            }
        }

        $this->data['addCss'] = array(
            'assets/css/my.css',
            'assets/datatables/jquery.dataTables.min.css'
        );
        $this->data['addJs'] = array(
            'assets/slimscroll/jquery.slimscroll.js',
            'assets/js/locationFilter.js',
            'assets/jquery-validate/jquery.validate.min.js',
            'assets/js/validateCustom.js',
            'assets/js/jquery.paginate.js',
            'assets/datatables/jquery.dataTables.min.js',
            'assets/jquery-validate/additional-methods.min.js',
            'assets/js/user-page.js',
            'assets/angularjs/angular.min.js',
            'assets/js/angularJsConfig.js',
            'assets/js/projectSearch.js',
            'assets/moment/moment.min.js',
            'assets/js/feedCtrl.js'
        );
        $this->data['subBody'] = 'members/profile/_dashboard';
        $this->render();
    }

    public function logout()
    {
        $this->session->sess_destroy();
        $this->load->helper('cookie');
        delete_cookie('remembered');
        delete_cookie('rememberedUserType');
        redirect('members');
    }

    public function edit()
    {
        $userType = get_userdata('userType');
        if ($this->input->post()) {
            $post = $this->input->post();
            $userId = get_userdata('active_user');
            unset($post['id']);unset($post['table']);

            if ($userType == 'Volunteer') {
                $this->load->model('volunteer_model', 'volunteer');
                $post['dateOfBirth'] = strtotime($post['dateOfBirth']);

                $post['notification_status'] = isset($post['notification_status']) ? $post['notification_status'] : 'InActive';
                $post['account_status'] = isset($post['account_status']) ? $post['account_status'] : 'InActive';

                $res = $this->volunteer->save($post, array('id' => $userId));
                if ($res) {
                    set_flash('msg', 'Your account has been updated successfully');
                } else {
                    set_flash('error', 'Failed to update your account. Please try again later');
                }
                set_userdata(array(
                    'userName' => $post['fullName']
                ));
            } else if ($userType == 'Agency') {
                if ($_FILES['logo']['name'] != '') {
                    if ($_FILES['logo']['error'] == 0) {
                        $logo = $_FILES['logo'];
                        $target_dir = "uploads/agency/logo/";
                        $target_file = $target_dir . basename($logo["name"]);
                        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
                        $target_file = $target_dir . time() . '.' . $imageFileType;
                        if ($logo['size'] > 1000000) {
                            set_flash('msg', 'Sorry, Image should be less then 1 MB');
                            redirect('members/profile/edit');
                        } else {
                            if (move_uploaded_file($logo["tmp_name"], $target_file)) {
                                $post['logo'] = $target_file;
                                set_userdata(array(
                                    'profilePicture' => base_url().$post['logo']
                                ));
                            }
                        }
                    } else {
                        set_flash('msg', 'Sorry, Image should be less then 1 MB');
                        redirect('members/profile/edit');
                    }

                }

                if ($_FILES['registrationCertificateImage']['name'] != '') {
                    if ($_FILES['registrationCertificateImage']['error'] == 0) {
                        $registrationCertificateImage = $_FILES['registrationCertificateImage'];
                        $target_dir = "uploads/agency/certificates/";
                        $target_file = $target_dir . basename($registrationCertificateImage["name"]);
                        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
                        $target_file = $target_dir . time() . '.' . $imageFileType;

                        if ($registrationCertificateImage['size'] > 1000000) {
                            set_flash('msg', 'Sorry, Image should be less then 1 MB');
                            redirect('members/profile/edit');
                        } else {
                            if (move_uploaded_file($registrationCertificateImage["tmp_name"], $target_file)) {
                                $post['registrationCertificateImage'] = $target_file;
                            }
                        }
                    } else {
                        set_flash('msg', 'Sorry, Image should be less then 1 MB');
                        redirect('members/profile/edit');
                    }
                }

                $this->load->model('agency_model', 'agency');
                $res = $this->agency->save($post, array('id' => $userId));
                if ($res) {
                    set_flash('msg', 'Your account has been updated successfully');
                } else {
                    set_flash('error', 'Failed to update your account. Please try again later');
                }
                set_userdata(array(
                    'userName' => $post['name']
                ));
            }
            redirect('members/profile/edit');
        } else {
            $this->data['body'] = 'members/profile/_index';
            $this->data['addCss'] = array(
                'assets/css/my.css',
                'assets/crop-pic/assets/css/croppic.css'
            );
            $this->data['addJs'] = array(
                'assets/js/locationFilter.js',
                'assets/jquery-validate/jquery.validate.min.js',
                'assets/js/validateCustom.js',
                'assets/jquery-validate/additional-methods.min.js',
                'assets/js/form-validate.js',
                'assets/crop-pic/croppic.js',
                'assets/js/profile.js',
            );
            if ($userType == 'Volunteer') {
                $this->load->model('agencyproject_model', 'agencyproject');
                $this->data['volunteerHours'] = $this->agencyproject->getVolunteerHours(get_userdata('active_user'));
                $this->_setVolunteerData();
            } else {
                $this->_setAgencyData();
            }
        }

        $this->render();
    }

    private function _setVolunteerData()
    {
        $volunteerId = get_userdata('active_user');
        $this->load->model('volunteer_model', 'volunteer');
        $volunteer_data = $this->volunteer->get(1, array('id' => $volunteerId));
        $this->load->model('volunteerlanguage_model', 'volunteerlanguage');
        $this->load->model('volunteerskill_model', 'volunteerskill');
        $this->load->model('volunteertraininginterestedin_model', 'volunteertraininginterestedin');
        $this->load->model('volunteertraining_model', 'volunteertraining');
        $this->load->model('academicqualification_model', 'academicqualification');
        $this->load->model('common_model', 'common');
        $this->data['languages'] = $this->common->query('select * from nvp_language');
        $this->data['allGender'] = $this->common->query('select * from nvp_gender');
        $this->data['zones'] = $this->common->query('select * from nvp_zone order by name asc');
        $this->data['districts'] = $this->common->query('select * from nvp_district where zoneId = '.$volunteer_data->zoneId.' order by name asc');
        $this->data['municipalities'] = $this->common->query('select * from nvp_municipality where districtId = '.$volunteer_data->districtId.' order by name asc');
        $this->data['academicQualifications'] = $this->academicqualification->get('', array('status' => 'Active'), 'orderNumber asc');
        $this->data['skills'] = $this->common->query('select * from nvp_skill');
        $this->data['trainings'] = $this->common->query('select * from nvp_training');
        $this->data['volunteerData'] = $this->volunteer->get('1', array('id' => get_userdata('active_user')));
        $this->data['volunteerLanguage'] = $this->volunteerlanguage->getData($volunteerId);
        $this->data['volunteerSkill'] = $this->volunteerskill->getData($volunteerId);
        $this->data['volunteerTrainingInterestedIn'] = $this->volunteertraininginterestedin->getData($volunteerId);
        $this->data['volunteerTrainingReceived'] = $this->volunteertraining->getData($volunteerId);
        $this->data['volunteerData']->dateOfBirth = date('d F, Y', $this->data['volunteerData']->dateOfBirth);
        $this->data['subBody'] = 'members/profile/_volunteer_form';
    }

    private function _setAgencyData()
    {
        $this->load->model('agency_model', 'agency');
        $this->load->model('agencytype_model', 'agencytype');
        $this->data['agencyData'] = $this->agency->get('1', array('id' => get_userdata('active_user')));
        $this->data['agencyTypes'] = $this->agencytype->get('', array('status' => 'Active'), 'orderNumber asc');
        $this->data['subBody'] = 'members/profile/_agency_form';
    }

    public function settings()
    {
        $userType = get_userdata('userType');
        if ($this->input->post()) {
            $post = $this->input->post();

            if ($userType == 'Volunteer') {
                $res = $this->_saveVolunteerSettingData($post);
            }

            if ($res) {
                set_flash('msg', 'Profile saved successfully.');
            } else {
                set_flash('error', 'Profile could not be saved.');
            }
            redirect('members/profile/edit');
        }
    }

    private function _saveVolunteerSettingData($post)
    {
        $userId = get_userdata('active_user');

        $this->load->model('volunteer_model', 'volunteer');
        $this->load->model('volunteerlanguage_model', 'volunteerlanguage');
        $this->load->model('volunteerskill_model', 'volunteerskill');
        $this->load->model('volunteertraininginterestedin_model', 'volunteertraininginterestedin');
        $this->load->model('volunteertraining_model', 'volunteertraining');

        $this->volunteerlanguage->delete(array('volunteerId' => $userId));
        $this->volunteerskill->delete(array('volunteerId' => $userId));
        $this->volunteertraininginterestedin->delete(array('volunteerId' => $userId));
        $this->volunteertraining->delete(array('volunteerId' => $userId));

        // saving some volunteer data
        $volunteerInsertData = array(
            'academicQualificationId' => $post['academicQualificationId'],
            'availability' => $post['availability'],
            'volunteerType' => $post['volunteerType'],
            'volunteerSubType' => isset($post['volunteerSubType']) ? $post['volunteerSubType'] : '',
            'primaryModeOfCommunication' => isset($post['primaryModeOfCommunication']) ? $post['primaryModeOfCommunication'] : '',
            'secondaryModeOfCommunication' => isset($post['secondaryModeOfCommunication']) ? $post['secondaryModeOfCommunication'] : '',
        );
        $res = $this->volunteer->save($volunteerInsertData, array('id' => $userId));

        // saving volunteer language setting
        if (isset($post['volunteerLanguage'])) {
            foreach ($post['volunteerLanguage'] as $row) {
                $languageData = array(
                    'volunteerId' => $userId,
                    'languageId' => $row
                );
                $res = $this->volunteerlanguage->save($languageData);
            }
        }

        // saving volunteer skill setting
        if (isset($post['volunteerSkill'])) {
            foreach ($post['volunteerSkill'] as $row) {
                $skillData = array(
                    'volunteerId' => $userId,
                    'skillId' => $row
                );
                $res = $this->volunteerskill->save($skillData);
            }
        }

        // saving volunteer training received setting
        if (isset($post['volunteerTraining'])) {
            foreach ($post['volunteerTraining'] as $row) {
                $trainingReceivedData = array(
                    'volunteerId' => $userId,
                    'trainingId' => $row,
                    'note' => 'Dummy'
                );
                $res = $this->volunteertraining->save($trainingReceivedData);
            }
        }

        // saving volunteer training received setting
        if (isset($post['volunteerTrainingInterestedIn'])) {
            foreach ($post['volunteerTrainingInterestedIn'] as $row) {
                $volunteerTrainingInterestedInData = array(
                    'volunteerId' => $userId,
                    'trainingId' => $row
                );
                $res = $this->volunteertraininginterestedin->save($volunteerTrainingInterestedInData);
            }
        }

        return $res;
    }

    public function remove($type = '')
    {
        $this->load->model('agency_model', 'agency');
        if ($type == 'logo') {
            $data['logo'] = '';
        } elseif ($type == 'certificate') {
            $data['registrationCertificateImage'] = '';
        }
        $res = $this->agency->save($data, array('id' => get_userdata('active_user')));
        if ($res) {
            redirect('members/profile/edit');
        }
    }

    public function project($projectSlug = '')
    {
        $this->load->model('agencyproject_model', 'agencyproject');
        $this->load->model('agencyprojectcriteria_model', 'agencyprojectcriteria');
        $this->load->model('agencyprojectcriteriatraining_model', 'agencyprojectcriteriatraining');
        $this->load->model('agencyprojectcriteriaskill_model', 'agencyprojectcriteriaskill');
        $this->load->model('agencyprojectcriteriagender_model', 'agencyprojectcriteriagender');
        $this->load->model('agencyprojectcriteriaacademic_model', 'agencyprojectcriteriaacademic');
        $this->load->model('academicqualification_model', 'academicqualification');
        $this->load->model('common_model', 'common');

        if ($projectSlug != '') {
            $projectId = $this->agencyproject->get('1', array('slug' => $projectSlug));
            $this->data['projectData'] = $projectId;
            $projectId = $projectId->id;
        } else {
            $this->data['projectData'] = $this->agencyproject;
        }

        if ($this->input->post()) {
            $post = $this->input->post();
            $post['dateOfEvent'] = strtotime($post['dateOfEvent']);

            if ($projectSlug) {
                $post['slug'] = $this->agencyproject->createSlug($post['name'], $projectId);
                unset($post['projectId']);
                $this->agencyproject->save($post, array('slug' => $projectSlug));
                set_flash('msg', 'Project updated successfully.');
            } else {
                $post['agencyId'] = get_userdata('active_user');
                $post['slug'] = $this->agencyproject->createSlug($post['name']);
                unset($post['projectId']);
                $this->agencyproject->save($post);
                $projectSlug = $post['slug'];
                set_flash('msg', 'Project created successfully. Now you can further add the project criteria.');
            }
            redirect('members/profile/project/' . $projectSlug);
        } else {
            $this->data['body'] = 'members/profile/_index';
            $this->data['addJs'] = array(
                'assets/js/agency-project.js',
                'assets/js/locationFilter.js',
                'assets/js/lolliclock.js',
                'assets/js/timepicker.js',
                'assets/jquery-validate/jquery.validate.min.js',
                'assets/js/validateCustom.js',
                'assets/jquery-validate/additional-methods.min.js',
                'assets/js/form-validate.js'
            );
            $this->data['addCss'] = array(
                'assets/css/my.css',
                'assets/css/lolliclock.css'
            );

            $this->data['subBody'] = 'members/profile/_project_form';
            $this->data['allGender'] = $this->common->query('select * from nvp_gender');
            $this->data['projectCriteriaData'] = $this->agencyprojectcriteria;
            $this->data['zones'] = $this->common->query('select * from nvp_zone order by name asc');
            $this->data['districts'] = $this->common->query('select * from nvp_district order by name asc');
            $this->data['municipalities'] = $this->common->query('select * from nvp_municipality order by name asc');
            $this->data['academicQualifications'] = $this->academicqualification->get('', array('status' => 'Active'), 'orderNumber asc');
            $this->data['skills'] = $this->common->query('select * from nvp_skill', 'orderNumber asc');
            $this->data['trainings'] = $this->common->query('select * from nvp_training', 'orderNumber asc');
            if ($projectSlug != '') {
                $this->data['projectData'] = $this->agencyproject->get('1', array('slug' => $projectSlug));
                $this->data['projectCriteriaData'] = $this->agencyprojectcriteria->get('1', array('agencyProjectId' => $projectId));
                if (!$this->data['projectCriteriaData']) {
                    $this->data['projectCriteriaData'] = $this->agencyprojectcriteria;
                    $this->data['projectCriteriaAcademicData'] = array();
                    $this->data['projectCriteriaGenderData'] = array();
                    $this->data['projectCriteriaSkillData'] = array();
                    $this->data['projectCriteriaTrainingData'] = array();
                } else {
                    $this->data['projectCriteriaAcademicData'] = $this->agencyprojectcriteriaacademic->getData($this->data['projectCriteriaData']->id);
                    $this->data['projectCriteriaGenderData'] = $this->agencyprojectcriteriagender->getData($this->data['projectCriteriaData']->id);
                    $this->data['projectCriteriaSkillData'] = $this->agencyprojectcriteriaskill->getData($this->data['projectCriteriaData']->id);
                    $this->data['projectCriteriaTrainingData'] = $this->agencyprojectcriteriatraining->getData($this->data['projectCriteriaData']->id);
                }

                if ($this->data['projectData']->dateOfEvent != '')
                    $this->data['projectData']->dateOfEvent = date('d F, Y', $this->data['projectData']->dateOfEvent);
            }

            $this->render();
        }
    }

    public function projectdelete($projectSlug = '')
    {
        $this->load->model('agencyproject_model', 'agencyproject');
        $res = $this->agencyproject->delete(array('slug' => $projectSlug));

        if ($res) {
            set_flash('msg', 'Project has been deleted');
        } else {
            set_flash('error', 'Project could not be deleted');
        }
        redirect('members/profile');
    }

    public function projectremove($projectSlug = '')
    {
        $this->load->model('agencyproject_model', 'agencyproject');
        $res = $this->agencyproject->save(['status' => 'InActive'], array('slug' => $projectSlug));

        if ($res) {
            set_flash('msg', 'Project has been removed');
        } else {
            set_flash('error', 'Project could not be removed');
        }
        redirect('members/profile');
    }

    public function projectcriteria()
    {

        if ($this->input->post()) {

            $this->load->model('agencyproject_model', 'agencyproject');
            $this->load->model('agencyprojectcriteria_model', 'agencyprojectcriteria');
            $this->load->model('agencyprojectcriteriaacademic_model', 'agencyprojectcriteriaacademic');
            $this->load->model('agencyprojectcriteriagender_model', 'agencyprojectcriteriagender');
            $this->load->model('agencyprojectcriteriaskill_model', 'agencyprojectcriteriaskill');
            $this->load->model('agencyprojectcriteriatraining_model', 'agencyprojectcriteriatraining');
            if (segment(4) != '') {
                
                $projectId = $this->agencyproject->get('1', array('slug' => segment(4)));
                $projectId = $projectId->id;

                $projectCriteriaId = $this->agencyprojectcriteria->get('1', array('agencyProjectId' => $projectId));

                if ($projectCriteriaId) {
                    $projectCriteriaId = $projectCriteriaId->id;

                }
            }
            
            $post = $this->input->post();
            // saving project other specific data
            $this->agencyproject->save(array('otherSkills' => $post['otherSkills'], 'otherTraining' => $post['otherSkills']), array('slug' => segment(4)));

            // saving project main criteria
            ob_start();
            $projectCriteriaData = $post['projectCriteria'];
            $projectCriteriaData['agencyProjectId'] = $projectId;
            $projectCriteriaData['isSkillRequired'] = isset($projectCriteriaData['isSkillRequired']) ? $projectCriteriaData['isSkillRequired'] : 'No';
            $projectCriteriaData['isMinimumEducationRequired'] = isset($projectCriteriaData['isMinimumEducationRequired']) ? $projectCriteriaData['isMinimumEducationRequired'] : 'No';
            $projectCriteriaData['isTrainingRequired'] = isset($projectCriteriaData['isTrainingRequired']) ? $projectCriteriaData['isTrainingRequired'] : 'No';
            $projectCriteriaData['isGenderRequired'] = isset($projectCriteriaData['isGenderRequired']) ? $projectCriteriaData['isGenderRequired'] : 'No';
            if (!$projectCriteriaId)
                $projectCriteriaId = $this->agencyprojectcriteria->save($projectCriteriaData, '', true);
            else
                $this->agencyprojectcriteria->save($projectCriteriaData, array('id' => $projectCriteriaId));

            // save project criteria detail
            $this->agencyprojectcriteriaacademic->delete(array('agencyProjectCriteriaId' => $projectCriteriaId));
            foreach ($post['academicQualificationId'] as $qualification) {
                $insertData = array(
                    'agencyProjectCriteriaId' => $projectCriteriaId,
                    'academicQualificationId' => $qualification
                );
                $res = $this->agencyprojectcriteriaacademic->save($insertData);
            }

            $this->agencyprojectcriteriagender->delete(array('agencyProjectCriteriaId' => $projectCriteriaId));
            foreach ($post['genderId'] as $gender) {
                $insertData = array(
                    'agencyProjectCriteriaId' => $projectCriteriaId,
                    'genderId' => $gender
                );
                $res = $this->agencyprojectcriteriagender->save($insertData);
            }

            $this->agencyprojectcriteriaskill->delete(array('agencyProjectCriteriaId' => $projectCriteriaId));
            foreach ($post['skillId'] as $skill) {
                $insertData = array(
                    'agencyProjectCriteriaId' => $projectCriteriaId,
                    'skillId' => $skill
                );
                $res = $this->agencyprojectcriteriaskill->save($insertData);
            }

            $this->agencyprojectcriteriatraining->delete(array('agencyProjectCriteriaId' => $projectCriteriaId));
            foreach ($post['trainingId'] as $training) {
                $insertData = array(
                    'agencyProjectCriteriaId' => $projectCriteriaId,
                    'trainingId' => $training
                );
                $res = $this->agencyprojectcriteriatraining->save($insertData);
            }

            if ($res) {
                set_flash('msg', 'Project updated successfully');
            } else {
                if(!isset($post['trainingId']) && !isset($post['genderId']) && !isset($post['skillId']) && !isset($post['academicQualificationId'])) {
                    set_flash('error', 'None of the project criteria selected' );
                } else {
                    set_flash('error', 'Project could not be updated');
                }
            }

            redirect('members/profile/project/' . segment(4));
        }
    }

    public function changepassword()
    {
        if ($this->input->post()) {
            $post = $this->input->post();
            $userType = get_userdata('userType');
            $userId = get_userdata('active_user');
            if ($userType == 'Volunteer') {
                $this->load->model('volunteer_model', 'volunteer');
                $userData = $this->volunteer->get('1', array('id' => $userId));
            } else if ($userType == 'Agency') {
                $this->load->model('agency_model', 'agency');
                $userData = $this->agency->get('1', array('id' => $userId));
            }

            if ($post['newPassword'] === $post['confirmNewPassword']) {
                if ($userData) {
                    $oldPasswordHash = $userData->password;
                    if (passwordVerify($post['oldPassword'], $oldPasswordHash)) {
                        $newPasswordHash = passwordHash($post['newPassword']);
                        $setUserData = array('password' => $newPasswordHash);

                        if (isset($setUserData)) {
                            if ($userType == 'Volunteer') {
                                $res = $this->volunteer->save($setUserData, array('id' => $userId));
                            } else if ($userType == 'Agency') {
                                $res = $this->agency->save($setUserData, array('id' => $userId));
                            }
                        }

                        (isset($res) && $res) ? set_flash('msg', 'Password updated successfully.') : set_flash('error', 'Password could not be updated. Please try again later.');
                        redirect('members/profile/logout');
                    } else {
                        set_flash('error', 'Old password did not match.');
                    }
                } else {
                    set_flash('error', 'User not found.');
                }
            } else {
                set_flash('error', 'New password confirmation did not match.');
            }

            redirect('members/profile/edit');
        }
    }

    public function checkPhnumber()
    {
        $post = $_POST;
        $this->load->model('volunteer_model', 'volunteer');
        $this->load->model('agency_model', 'agency');
        $id  = $post['id'];
        $table  = $post['table'];
        $phone  = isset($post['phoneNumberMobile']) ? $post['phoneNumberMobile'] : $post['phoneNumber'];
        if($table == 'nvp_volunteer'){
            $query = "select id from `nvp_volunteer` v where v.`id` != $id and  v.`phoneNumberMobile` = '{$phone}'";
            $model = 'agency';
            $array = ['phoneNumber'=>$phone];
        }else{
            $query = "select id from `nvp_agency` v where v.`id` != $id and  v.`phoneNumber` = '{$phone}'";
            $model = 'volunteer';
            $array = ['phoneNumberMobile'=>$phone];
        }
        $res = $this->agency->query($query);
        if($res){
            echo 'false';
        }else {
            $res1 =$this->$model->get('',$array);
            if($res1){
                echo "false";
            }else{
                echo "true";
            }
        }
    }

    public function checkEmail()
    {
        $post = $_POST;
        $this->load->model('volunteer_model', 'volunteer');
        $this->load->model('agency_model', 'agency');
        $id  = $post['id'];
        $email  = $post['email'];
        $query = "select id from `nvp_volunteer` v where v.`id` != $id and  v.`email` = '$email'";
        $res = $this->volunteer->query($query);
        if($res){
            echo 'false';
        }else {
            $query = "select id from `nvp_agency` v where v.`id` != $id and  v.`email` = '$email'";
            $res1 = $this->agency->query($query);
            if($res1){
                echo "false";
            }else{
                echo "true";
            }
        }
    }

    public function uploadProfilePic()
    {
        /*
        *	!!! THIS IS JUST AN EXAMPLE !!!, PLEASE USE ImageMagick or some other quality image processing libraries
        */
        $imagePath = "temp/";

        $allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
        $temp = explode(".", $_FILES["img"]["name"]);
        $extension = end($temp);

        //Check write Access to Directory

        if (!is_writable($imagePath)) {
            $response = Array(
                "status" => 'error',
                "message" => 'Can`t upload File; no write Access'
            );
            print json_encode($response);
            return;
        }

        if (in_array($extension, $allowedExts)) {
            if ($_FILES["img"]["error"] > 0) {
                $response = array(
                    "status" => 'error',
                    "message" => 'ERROR Return Code: ' . $_FILES["img"]["error"],
                );
            } else {

                $filename = $_FILES["img"]["tmp_name"];
                list($width, $height) = getimagesize($filename);

                move_uploaded_file($filename, $imagePath . $_FILES["img"]["name"]);

                $response = array(
                    "status" => 'success',
                    "url" => base_url($imagePath . $_FILES["img"]["name"]),
                    "width" => $width,
                    "height" => $height
                );

            }
        } else {
            $response = array(
                "status" => 'error',
                "message" => 'something went wrong, most likely file is to large for upload. check upload_max_filesize, post_max_size and memory_limit in you php.ini',
            );
        }

        print json_encode($response);
    }

    public function cropProfilePic()
    {
        $imgUrl = $_POST['imgUrl'];

        $imgInitW = $_POST['imgInitW'];
        $imgInitH = $_POST['imgInitH'];

        $imgW = $_POST['imgW'];
        $imgH = $_POST['imgH'];

        $imgY1 = $_POST['imgY1'];
        $imgX1 = $_POST['imgX1'];

        $cropW = $_POST['cropW'];
        $cropH = $_POST['cropH'];

        $angle = $_POST['rotation'];

        $jpeg_quality = 100;

        $output_filename = "uploads/volunteer/profile-picture/" . time();

        $what = getimagesize($imgUrl);

        switch (strtolower($what['mime'])) {
            case 'image/png':
                $img_r = imagecreatefrompng($imgUrl);
                $source_image = imagecreatefrompng($imgUrl);
                $type = '.png';
                break;
            case 'image/jpeg':
                $img_r = imagecreatefromjpeg($imgUrl);
                $source_image = imagecreatefromjpeg($imgUrl);
                error_log("jpg");
                $type = '.jpg';
                break;
            case 'image/gif':
                $img_r = imagecreatefromgif($imgUrl);
                $source_image = imagecreatefromgif($imgUrl);
                $type = '.gif';
                break;
            default:
                die('image type not supported');
        }

        if (!is_writable(dirname($output_filename))) {
            $response = Array(
                "status" => 'error',
                "message" => 'Can`t write cropped File'
            );
        } else {
            $resizedImage = imagecreatetruecolor($imgW, $imgH);
            imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);
            // rotate the rezized image
            $rotated_image = imagerotate($resizedImage, -$angle, 0);
            // find new width & height of rotated image
            $rotated_width = imagesx($rotated_image);
            $rotated_height = imagesy($rotated_image);
            // diff between rotated & original sizes
            $dx = $rotated_width - $imgW;
            $dy = $rotated_height - $imgH;
            // crop rotated image to fit into original rezized rectangle
            $cropped_rotated_image = imagecreatetruecolor($imgW, $imgH);
            imagecolortransparent($cropped_rotated_image, imagecolorallocate($cropped_rotated_image, 0, 0, 0));
            imagecopyresampled($cropped_rotated_image, $rotated_image, 0, 0, $dx / 2, $dy / 2, $imgW, $imgH, $imgW, $imgH);
            // crop image into selected area
            $final_image = imagecreatetruecolor($cropW, $cropH);
            imagecolortransparent($final_image, imagecolorallocate($final_image, 0, 0, 0));
            imagecopyresampled($final_image, $cropped_rotated_image, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);
            // finally output png image
            //imagepng($final_image, $output_filename.$type, $png_quality);
            imagejpeg($final_image, $output_filename . $type, $jpeg_quality);

            $this->load->model('volunteer_model', 'volunteer');
            $volunteerData = $this->volunteer->get('1', array('id' => get_userdata('active_user')));
            if ($volunteerData) {
                $oldProfilePic = $volunteerData->profilePicture;
                if (file_exists($oldProfilePic)) {
                    unlink($oldProfilePic);
                }
            }

            $this->volunteer->save(array('profilePicture' => $output_filename . $type), array('id' => get_userdata('active_user')));
            set_flash('msg', 'Profile picture has been updated successfully.');
            set_userdata(array('profilePicture' => base_url($output_filename . $type)));

            $response = array(
                "status" => 'success',
                "url" => base_url($output_filename . $type)
            );
        }

        $tempFilePath = end(explode('/', $imgUrl));
        if (file_exists($tempFilePath)) {
            unlink('temp/' . $tempFilePath);
        }

        print json_encode($response);
    }
}