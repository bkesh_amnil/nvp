<?php

class Retrievepassword extends My_Front_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    function _remap($method)
    {
        $param_offset = 2;

        // Default to index
        if ( ! method_exists($this, $method))
        {
            // We need one more param
            $param_offset = 1;
            $method = 'index';
        }

        // Since all we get is $method, load up everything else in the URI
        $params = array_slice($this->uri->rsegment_array(), $param_offset);

        // Call the determined method with all params
        call_user_func_array(array($this, $method), $params);
    }

    public function index()
    {
        $post = $this->input->post();

        if($post) {
            $validTimestamp = time();

            $this->load->model('volunteer_model', 'volunteer');
            $this->load->model('agency_model', 'agency');
            $userData = $this->volunteer->get('1', array('email' => $post['retrieveEmail']));
            if(!$userData) {
                $agency = true;
                $userData = $this->agency->get('1', array('email' => $post['retrieveEmail']));
            }

            if($userData) {
                if(isset($agency)) {
                    $this->agency->save(array('passwordChangeTimeStamp' => $validTimestamp), array('id' => $userData->id));
                } else {
                    $this->volunteer->save(array('passwordChangeTimeStamp' => $validTimestamp), array('id' => $userData->id));
                }

                $this->load->model('emailtemplate_model', 'emailtemplate');
                $volunteerRegistrationEmailTemplate = $this->emailtemplate->getAllData(5);
                $siteLogo = base_url($this->global_config->site_logo);
                $siteLink = base_url();
                $timeStamp = $validTimestamp . '~' . hash('md5', $userData->id);
                $passwordchangeLink = base_url('members/retrievepassword/resetpassword/' . $timeStamp);
                $volunteerMessage = str_replace(
                    array('#siteLogo#', '#siteLink#', '#emailSubject#', '#passwordChangeLink#'),
                    array($siteLogo, $siteLink, $volunteerRegistrationEmailTemplate[0]->userSubject, $passwordchangeLink),
                    $volunteerRegistrationEmailTemplate[0]->userMessage
                );
                //$emailBody = $this->load->view('email/resetpassword', array('timeStamp' => $validTimestamp . '~' . hash('md5', $userData->id)), true);
                $email_params = array(
                    'subject' => 'Reset Account Password',
                    'from' => SITEMAIL,
                    'fromname' => SITENAME,
                    'to' => $post['retrieveEmail'],
                    'message' => $volunteerMessage
                );
                swiftsend($email_params);

                set_flash('msg', 'Reset password link has been sent to your inbox.');
                redirect('members/login');
            } else {
                set_flash('error', 'User not found.');
                redirect('members/login');
            }
        }
    }

    public function resetpassword($hash = '')
    {
        // clearing session when retrieving password
        $this->session->sess_destroy();
        $this->load->helper('cookie');
        delete_cookie('remembered');
        delete_cookie('rememberedUserType');

        $hashParse = explode('~', $hash);
        $timeStamp = $hashParse[0];
        $userIdHash = $hashParse[1];

        $this->load->model('volunteer_model', 'volunteer');
        $this->load->model('agency_model', 'agency');
        if($this->input->post()) {
            $post = $this->input->post();

            if($post['password'] == $post['repassword']) {
                $newPasswordHash = passwordHash($post['password']);
                if($post['userType'] == 'Volunteer') {
                    $this->volunteer->save(array('password' => $newPasswordHash,'passwordChangeTimeStamp'=>'NULL'), array('id' => $post['userId']));
                } else {
                    $this->agency->save(array('password' => $newPasswordHash,'passwordChangeTimeStamp'=>'NULL'), array('id' => $post['userId']));
                }

                set_flash('msg', 'Your password has been changed successfully. You can now login to your account using the new password.');
                redirect('members/login');
            } else {
                set_flash('error', 'Password confirmation did not match.');
            }
        } else {
            $currentTimeStamp = time();

            if(strtotime('+1 day', $timeStamp) > $currentTimeStamp) {
                $userData = $this->volunteer->get('1', array('md5(id)' => $userIdHash));
                if (!$userData) {
                    $agency = true;
                    $userData = $this->agency->get('1', array('md5(id)' => $userIdHash));
                }

                if ($userData) {
                    if ($userData->passwordChangeTimeStamp == 'NULL') {
                        set_flash('error', 'Password already changed.');
                        redirect('members/login');
                    }else {
                        $this->data['userType'] = isset($agency) ? 'Agency' : 'Volunteer';
                        $this->data['userId'] = $userData->id;
                        $this->data['addJs'] = array(
                            'assets/jquery-validate/jquery.validate.min.js',
                            'assets/js/validateCustom.js',
                            'assets/js/retrievepassword.js'
                        );
                        $this->data['body'] = 'members/retrievepassword/_form';
                        $this->render();
                    }
                } else {
                    set_flash('error', 'User account not found.');
                    redirect('members/login');
                }
            } else {
                set_flash('error', 'Token has expired.');
                redirect('members/login');
            }
        }
    }

}