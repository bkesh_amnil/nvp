<?php

class Register extends My_Front_Controller
{

    public function __construct()
    {
        parent::__construct();
        $data = $this->db->list_fields('nvp_volunteer');
        $this->load->model('content_model', 'content');

        // password hashing library for older PHP version
        require APPPATH . '/third_party/password.php';
    }

    public function index()
    {
        if ($this->input->post()) {
            $this->load->model('volunteer_model', 'volunteer');
            $post = $this->input->post();

            if ($post['userType'] == 'Volunteer') {
                $this->load->model('volunteer_model', 'volunteer');
                unset($post['repassword'], $post['userType'], $post['agree']);
                $post['password'] = password_hash($post['password'], PASSWORD_BCRYPT, array('cost' => 10));
                $post['dateOfBirth'] = strtotime($post['dateOfBirth']);
                $post['dateOfRegistration'] = date('Y-m-d');
                $userId = $this->volunteer->save($post, '', true);

                // data form email template
                $siteLogo = base_url($this->global_config->site_logo);
                $siteLink = base_url();
                $accountDetailLink = base_url(BACKENDFOLDER . '/volunteer_data/view_detail/' . $userId);
                $activationLink = base_url('members/register/activate/volunteer/' . md5($userId));
                $this->load->model('emailtemplate_model', 'emailtemplate');
                $volunteerRegistrationEmailTemplate = $this->emailtemplate->getAllData(2);
                $adminMessage = str_replace(
                    array('#siteLogo#', '#siteLink#', '#emailSubject#', '#accountDetailLink#'),
                    array($siteLogo, $siteLink, $volunteerRegistrationEmailTemplate[0]->adminSubject, $accountDetailLink),
                    $volunteerRegistrationEmailTemplate[0]->adminMessage
                );
                $volunteerMessage = str_replace(
                    array('#siteLogo#', '#siteLink#', '#emailSubject#', '#accountActivationLink#'),
                    array($siteLogo, $siteLink, $volunteerRegistrationEmailTemplate[0]->userSubject, $activationLink),
                    $volunteerRegistrationEmailTemplate[0]->userMessage
                );

                $email_params = array(
                    'subject' => 'New Account Created',
                    'from' => $volunteerRegistrationEmailTemplate[0]->adminEmail,
                    'fromname' => SITENAME,
                    'to' => $post['email'],
                    'toname' => $post['fullName'],
                    'message' => $volunteerMessage
                );
                swiftsend($email_params);

                $email_params = array(
                    'subject' => 'New Account Created',
                    'from' => $post['email'],
                    'fromname' => $post['fullName'],
                    'to' => $volunteerRegistrationEmailTemplate[0]->adminEmail,
                    'toname' => SITENAME,
                    'message' => $adminMessage
                );
                swiftsend($email_params);

                $registration_message = 'volunteer_registration_message';
                $type = 'Registration';

                $this->_account($registration_message, $type);

            } elseif ($post['userType'] == 'Agency') {
                $this->load->model('agency_model', 'agency');
                unset($post['repassword'], $post['userType'], $post['agency_agree']);
                $post['password'] = password_hash($post['password'], PASSWORD_BCRYPT, array('cost' => 10));
                $userId = $this->agency->save($post, '', true);

                // data form email template
                $siteLogo = base_url($this->global_config->site_logo);
                $siteLink = base_url();
                $accountDetailLink = base_url(BACKENDFOLDER . '/agency_data/view_detail/' . $userId);
                $this->load->model('emailtemplate_model', 'emailtemplate');
                $agencyRegistrationEmailTemplate = $this->emailtemplate->getAllData(3);
                $adminMessage = str_replace(
                    array('#siteLogo#', '#siteLink#', '#emailSubject#', '#accountDetailLink#'),
                    array($siteLogo, $siteLink, $agencyRegistrationEmailTemplate[0]->adminSubject, $accountDetailLink),
                    $agencyRegistrationEmailTemplate[0]->adminMessage
                );
                $agencyMessage = str_replace(
                    array('#siteLogo#', '#siteLink#', '#emailSubject#'),
                    array($siteLogo, $siteLink, $agencyRegistrationEmailTemplate[0]->userSubject),
                    $agencyRegistrationEmailTemplate[0]->userMessage
                );

                $email_params = array(
                    'subject' => $agencyRegistrationEmailTemplate[0]->userSubject,
                    'from' => $agencyRegistrationEmailTemplate[0]->adminEmail,
                    'fromname' => SITENAME,
                    'to' => $post['email'],
                    'toname' => $post['name'],
                    'message' => $agencyMessage
                );
                swiftsend($email_params);

                $email_params = array(
                    'subject' => $agencyRegistrationEmailTemplate[0]->adminSubject,
                    'from' => $post['email'],
                    'fromname' => $post['name'],
                    'to' => $agencyRegistrationEmailTemplate[0]->adminEmail,
                    'toname' => SITENAME,
                    'message' => $adminMessage
                );
                swiftsend($email_params);
                $type = 'Registration';
                $registration_message = 'agency_registration_message';
                $this->_account($registration_message, $type);
            }
        } else {
            $signUpContents = $this->content->getByCategory('signup-content');
            $resp = array();
            if ($signUpContents) {
                foreach ($signUpContents as $signUpContent) {
                    $resp[] = array(
                        'title' => $signUpContent->name,
                        'description' => strip_tags($signUpContent->long_description)
                    );
                }
            }
            $this->data['signUpContent'] = $resp;
            $getGender = "select * from `nvp_gender`";
            $this->data['allGender'] = $this->content->query($getGender);
            $this->data['addJs'] = array(
                'assets/jquery-validate/jquery.validate.min.js',
                'assets/js/validateCustom.js',
                'assets/jquery-validate/additional-methods.min.js',
                'assets/js/register-form.js'
            );
            $this->data['body'] = 'members/register/_form';
        }

        $this->render();
    }

    public function activate()
    {
        $userType = segment(4);
        $activationCode = segment(5);
        $table = ($userType == 'volunteer') ? 'nvp_volunteer' : 'nvp_agency';

        $query = "update $table set status = 'Active' where md5(id) = '{$activationCode}'";
        $this->db->query($query);
        $type = 'Activation';
        $registration_message = 'volunteer_email_activate_message';
        $this->_account($registration_message, $type);
        $this->render();
    }

   /* public function check_email()
    {
        $post = $_POST;
        $this->load->model('volunteer_model', 'volunteer');
        $this->load->model('agency_model', 'agency');
        if (isset($post['email'])) {
            $arrayVolunteer = array(
                'email' => $post['email']
            );
            $arrayAgency = array(
                'email' => $post['email']
            );
        } else {
            $arrayVolunteer = array(
                'phoneNumberMobile' => $post['phnumber']
            );
            $arrayAgency = array(
                'phoneNumber' => $post['phnumber']
            );
        }
        $res = $this->volunteer->get('1', $arrayVolunteer);
        if ($res) {
            echo json_encode('true');
        } else {
            $res1 = $this->agency->get('1', $arrayAgency);
            if ($res1) {
                echo json_encode('true');
            } else {
                echo json_encode('false');
            }
        }
    }*/

    public function check()
    {
        $post = $_POST;
        $this->load->model('volunteer_model', 'volunteer');
        $this->load->model('agency_model', 'agency');

        if (isset($post['email'])) {
            $volunteerarray = array('email' => $post['email']);
            $agencyarray = array('email' => $post['email']);
        } elseif (isset($post['phoneNumber'])) {
            $volunteerarray = array('phoneNumberMobile' => $post['phoneNumber']);
            $agencyarray = array('phoneNumber' => $post['phoneNumber']);
        } elseif (isset($post['phoneNumberMobile'])) {
            $volunteerarray = array('phoneNumberMobile' => $post['phoneNumberMobile']);
            $agencyarray = array('phoneNumber' => $post['phoneNumberMobile']);
        }

        $res = $this->volunteer->get('1', $volunteerarray);
        if ($res) {
            echo 'false';
        } else {
            $res1 = $this->agency->get('1', $agencyarray);
            if ($res1) {
                echo 'false';
            } else {
                echo 'true';
            }
        }
    }

    private function _account($registration_message, $type)
    {

        $this->load->model('registrationmsg_model', 'registrationmsg');
        $this->data['message'] = $this->registrationmsg->get('1');
        $this->data['registration_message'] = $registration_message;
        $this->data['type'] = $type;
        $this->data['body'] = 'members/register/_message';
        //$this->render();
    }
}