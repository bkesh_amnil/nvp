<?php

class Login extends My_Front_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('content_model', 'content');
    }

    public function index()
    {
        if($this->input->post()) {
            $this->load->model('login_model', 'login');
            $post = $this->input->post();

            $this->load->helper('cookie');

            if($this->login->validateUser($post)) {
                $userType = get_userdata('userType');
                if($userType == 'Volunteer') {
                    $this->load->model('volunteer_model', 'volunteer');
                    $currentUserProfileData = $this->volunteer->get('1', array('id' => get_userdata('active_user')));
                    $profileStatus = checkProfileStatus($currentUserProfileData);
                }

                if(isset($post['rememberMe'])) {
                    set_cookie('remembered', md5(get_userdata('active_user')), time() + 60 * 60 * 4, '', '/');
                    set_cookie('rememberedUserType', get_userdata('userType'), time() + 60 * 60 * 4, '', '/');
                } else {
                    delete_cookie('remembered');
                }
                if(get_userdata('userType') == 'Volunteer' && !$profileStatus)
                    redirect('members/profile/edit');
                else
                    redirect('members/profile');
            } else {
                set_flash('error', 'Incorrect Username or Password');

                redirect('members/login');
            }
        } else {
            $this->data['addJs'] = array(
                'assets/jquery-validate/jquery.validate.min.js',
                'assets/js/validateCustom.js',
                'assets/js/form-validate.js',
                'assets/js/login.js'
            );
            $signInContents = $this->content->getByCategory('signin-content');
            $resp = array();
            if($signInContents) {
                foreach($signInContents as $signInContent) {
                    $resp[] = array(
                        'title' => $signInContent->name,
                        'description' => strip_tags($signInContent->long_description)
                    );
                }
            }
            $this->data['signInContent'] = $resp;
            $this->data['body'] = 'members/login/_form';
        }

        $this->render();
    }

}