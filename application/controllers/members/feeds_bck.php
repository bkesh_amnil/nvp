<?php

class Feeds extends My_Front_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('post_model', 'post');
    }

    function _remap($method)
    {
        $param_offset = 2;

        // Default to index
        if ( ! method_exists($this, $method))
        {
            // We need one more param
            $param_offset = 1;
            $method = 'index';
        }

        // Since all we get is $method, load up everything else in the URI
        $params = array_slice($this->uri->rsegment_array(), $param_offset);

        // Call the determined method with all params
        call_user_func_array(array($this, $method), $params);
    }

    public function index($feedSlug = '')
    {
        if($this->input->post()) {
            $post = $this->input->post();
            $post['userId'] = get_userdata('active_user');
            $userType = get_userdata('userType');

            // check if user is verified
            $this->load->model('common_model', 'common');
            $this->common->table = 'nvp_verified_user';
            $verifiedUserData = $this->common->get('1', array('userId' => $post['userId'], 'userType' => $userType));
            if($verifiedUserData) {
                $post['status'] = 'Active';
            } else{
                $post['status'] = 'InActive';
            }

            $post['slug'] = $this->post->createSlug($post['name']);
            $post['userType'] = get_userdata('userType');
            $message = '';
            if ($_FILES) {
                $target_dir = "uploads/posts/";
                if(!file_exists($target_dir)) {
                    mkdir($target_dir);
                }
                $target_file = $target_dir . basename($_FILES["image"]["name"]);
                $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
                $target_file = $target_dir . time() . '.' . $imageFileType;

                // Check if image file is a actual image or fake image
                if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
                    $message = array('type' => 'danger', 'message' => 'Sorry, only JPG, JPEG, PNG & GIF files are allowed.');
                } else {
                    if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
                        $post['image'] = $target_file;
                        $res = $this->post->save($post, '', true);
                        if ($res) {
                            // data form email template
                            $siteLogo = base_url($this->global_config->site_logo);
                            $siteLink = base_url();
                            $postDetailLink = base_url(BACKENDFOLDER . '/post_data#post-' . $res);
                            $this->load->model('emailtemplate_model', 'emailtemplate');
                            $newPostEmailTemplate = $this->emailtemplate->getAllData(5);
                            $adminMessage = str_replace(
                                array('#siteLogo#', '#siteLink#', '#emailSubject#', '#postDetailLink#'),
                                array($siteLogo, $siteLink, $newPostEmailTemplate[0]->adminSubject, $postDetailLink),
                                $newPostEmailTemplate[0]->adminMessage
                            );

                            $email_params = array(
                                'subject' => 'New Account Created',
                                'from' => $post['email'],
                                'fromname' => $post['fullName'],
                                'to' => $newPostEmailTemplate[0]->adminEmail,
                                'toname' => SITENAME,
                                'message' => $adminMessage
                            );
                            swiftsend($email_params);
                            $message = array('type' => 'success', 'message' => 'Thanks for your review');
                        }
                    } else {
                        $message = array('type' => 'error', 'message' => 'Sorry, there was an error uploading your file.');
                    }
                }
            } else {
                $message = array('type' => 'error', 'message' => 'Upload your image');
            }
            echo json_encode($message);
            die();
        }
    }

}