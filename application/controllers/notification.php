<?php

class Notification extends My_Front_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    function _remap($method)
    {
        $param_offset = 2;

        // Default to index
        if ( ! method_exists($this, $method))
        {
            // We need one more param
            $param_offset = 1;
            $method = 'index';
        }

        // Since all we get is $method, load up everything else in the URI
        $params = array_slice($this->uri->rsegment_array(), $param_offset);

        // Call the determined method with all params
        call_user_func_array(array($this, $method), $params);
    }
    public function index($slug = '')
    {
        $this->load->model('content_model', 'content');
        $this->load->model('menu_model', 'menu');
        $this->load->model('gallery_model', 'gallery');

        if($slug != '') {
            $this->_getSocialMetas($slug);
            $this->data['addJs'] = array(
                'assets/js/vendor/fancybox/source/jquery.fancybox.pack.js',
                'assets/js/vendor/fancybox/lib/jquery.mousewheel-3.0.6.pack.js',
                'assets/js/vendor/fancybox/source/helpers/jquery.fancybox-buttons.js',
                'assets/js/vendor/fancybox/source/helpers/jquery.fancybox-media.js',
                'assets/js/vendor/fancybox/source/helpers/jquery.fancybox-thumbs.js',
                'assets/js/news-detail.js'
            );
            $this->data['addCss'] = array(
                'assets/js/vendor/fancybox/source/jquery.fancybox.css',
                'assets/js/vendor/fancybox/source/helpers/jquery.fancybox-buttons.css',
                'assets/js/vendor/fancybox/source/helpers/jquery.fancybox-thumbs.css'
            );
            $this->data['body'] = 'frontend/news/_detail';
            $this->data['newsDetail'] = $this->content->getBySlug($slug);
            if($this->data['newsDetail']->meta_description != '') {
                $this->data['meta_description'] = $this->data['newsDetail']->meta_description;
            }
            if($this->data['newsDetail']->meta_keyword != '') {
                $this->data['meta_keywords'] = $this->data['newsDetail']->meta_keyword;
            }
            if($this->data['newsDetail']->cover_image != '') {
                $this->data['ogImage'] = $this->data['newsDetail']->cover_image;
            }
            if($this->data['newsDetail']->short_description != '') {
                $this->data['ogDescription'] = strip_tags($this->data['newsDetail']->short_description);
            }
            if($this->data['newsDetail']->name != '') {
                $this->data['ogTitle'] = $this->data['newsDetail']->name;
            }

            $this->data['contentGallery'] = $this->gallery->getContentGallery($this->data['newsDetail']->id);
        } else {
            $this->data['body'] = 'frontend/news/_index';
            $this->data['allNews'] = $this->content->getByCategoryNews('news-updates',0);
            $this->data['menu_alias'] = $this->uri->segment(1);
            $newsContents = $this->menu->get('1', ['menu_alias' => $this->data['menu_alias']]);
            $resp = array();
            if($newsContents) {
                $resp[] = array(
                    'title' => $newsContents->menu_title,
                    'description' => strip_tags($newsContents->description)
                );
            }
            $this->data['newsContent'] = $resp;
        }

        // loading custom js for home page
        if(isset($this->data['addJs'])) {
            $this->data['addJs'] = array_merge(
                $this->data['addJs'],
                array(
                    'assets/infinite-scroll/jquery.infinitescroll.min.js',
                    'assets/js/jquery.paginate.js',
                    'assets/js/paginate.js'
                ));
        } else {
            $this->data['addJs'] = array(
                'assets/js/salvattore.js',
                'assets/js/jquery.paginate.js',
                'assets/counter-master/waypoints.min.js',
                'assets/js/news.js'
            );
            $this->data['addCss'] = array(
                'assets/css/salvattore.css',
                'assets/css/news.css'
            );
        }

        $this->render();
    }


}
