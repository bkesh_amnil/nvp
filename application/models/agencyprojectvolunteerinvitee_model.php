<?php

class Agencyprojectvolunteerinvitee_model extends My_Model
{

    protected $table = 'nvp_agency_project_volunteer_invitee';
    public $id = '', $agencyProjectId = '', $volunteerId = '', $status = '', $invitationSentDateTime = '',
        $invitationSentBy = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function getAllData()
    {
        $query = "select
                    apvi.*,
                    v.fullName as volunteerName,
                    v.secondaryModeOfCommunication,
                    v.primaryModeOfCommunication,
                    v.email,
                    v.phoneNumberMobile,
                    (select ap.name from nvp_agency_project ap where apvi.agencyProjectId = ap.id) as projectName,
                    (select u.name from tbl_user u where apvi.invitationSentBy = u.id) as adminName,
                    (select n.lastRespondDate from tbl_notification n where apvi.notificationId = n.id) as lastRespondDate
                    from $this->table apvi
                    join nvp_volunteer as v on v.id = apvi.volunteerId ORDER BY v.id";

        return $this->query($query);
    }

}