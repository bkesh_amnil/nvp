<?php

class Download_Model extends My_Model
{

    protected $table = 'tbl_download';
    public $id = '', $name = '', $updated_by = '', $updated_on = '', $file = '', $status = '', $description = '',
            $publishDate = '', $categoryId = '';

    public function __construct()
    {
        parent::__construct();
        $this->updated_timestamp = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $rules = array(
            array(
                'field' => 'name',
                'label' => 'Download Name',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'description',
                'label' => 'Description',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'publishDate',
                'label' => 'Publish Date',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'categoryId',
                'label' => 'categoryId',
                'rules' => 'trim|required',
            )
        );

        return $rules;
    }

    public function getAllData()
    {
        $query = "select
                    d.name,
                    d.id,
                    d.status,
                    c.name as categoryName
                    from tbl_download d
                    join tbl_category c where c.id = d.categoryId
                    order by d.orderNumber asc";

        return $this->query($query);
    }

    public function get_category(){
        $query = "select
                   slug,name
                    from tbl_category
                    where status = 'Active' and type = '3'
                    order by name asc";

        $results=$this->query($query);
        foreach($results as $result){
            $res[$result->slug] = $result->name;
        }
       return $res;
    }

}