<?php

class Notification_Model extends My_Model
{

    protected $table = 'tbl_notification';
    public $id = '', $title = '',$short_description='',$content_id='', $projectId = '', $lastRespondDate = '';

    public function rules($id)
    {
        $rules =
            array(
                array(
                    'field' => 'title',
                    'label' => 'title',
                    'rules' => 'trim|required|unique['.$this->table.'.title.'.$id.']',
                )
            );

        return $rules;
    }

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function getAllData(){

        $query = "select n.id,n.title,n.short_description,c.name,n.projectId from tbl_notification n
    left join tbl_content c on c.id = n.content_id order by id desc";
        return $this->db->query($query)->result();

    }
}