<?php

class Agencytype_Model extends My_Model
{

    protected $table = 'nvp_agency_type';
    public $id = '', $name = '', $orderNumber = '', $status = '';

    public function rules($id)
    {
        $rules =
            array(
                array(
                    'field' => 'name',
                    'label' => 'Agency Type Name',
                    'rules' => 'trim|required|unique['.$this->table.'.name.'.$id.']',
                )
            );

        return $rules;
    }

    public function __construct()
    {
        parent::__construct();
    }
}