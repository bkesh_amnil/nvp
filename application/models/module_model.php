<?php

class Module_Model extends My_Model
{

    public $table = 'tbl_module';
    public $id = '', $name = '', $priority = '', $parent_id = '', $slug = '', $icon_class = '', $social = '',
                $public_module = 'Yes';
    public $rules =
        array(
            array(
                'field' => 'name',
                'label' => 'Module Name',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'slug',
                'label' => 'Module Alias',
                'rules' => 'trim|required',
            ),
        );

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = false;
    }
    public function getAllSubModule($parent)
    {
        $query = "SELECT id,name
                  FROM tbl_module
                  WHERE parent_id='" . $parent . "'
                  ORDER BY  `orderNumber` DESC";

        return $this->query($query);
    }

}