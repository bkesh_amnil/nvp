<?php

class Emailtemplate_Model extends My_Model
{

    protected $table = 'tbl_email_template';
    public $id = '', $emailCategoryId = '', $name = '', $adminEmail = '', $adminSubject = '', $status = '', $adminMessage = '',
         $userSubject = '', $userMessage = '';

    public function __construct()
    {
        parent::__construct();
        $this->updated_timestamp = false;
        $this->updated_by = false;
    }

    public function rules($id)
    {
        $rules = array(
            array(
                'field' => 'name',
                'label' => 'Email Template Name',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'userSubject',
                'label' => 'userSubject',
                'rules' => 'trim',
            ),
            array(
                'field' => 'userMessage',
                'label' => 'userMessage',
                'rules' => 'trim',
            )
        );

        return $rules;
    }

    public function getAllData($emailCategoryId = '')
    {
        if ($emailCategoryId == '') {
            $query = "select t.id ,t.name ,t.status, c.name as categoryName
                    from tbl_email_template as t
                    left join tbl_email_category c on c.id = t.emailCategoryId
                    order by t.name asc";
        } else {
            $query = "select t.*, c.name as categoryName
                    from tbl_email_template as t
                    left join tbl_email_category c on c.id = t.emailCategoryId
                    where t.emailcategoryId='$emailCategoryId'
                    order by t.id desc
                    limit 1";
        }
        return $this->query($query);
    }

    public function get_category()
    {
        $query = "select
                   slug,name
                    from tbl_category
                    where status = 'Active' and type = '3'
                    order by name asc";

        $results = $this->query($query);
        foreach ($results as $result) {
            $res[$result->slug] = $result->name;
        }
        return $res;
    }

    public function getEmail_category()
    {
        $query = "select
                   id,name
                    from tbl_email_category
                    order by name asc";

        $results = $this->query($query);
        foreach ($results as $result) {
            $res[$result->id] = $result->name;
        }
        return $res;
    }

}