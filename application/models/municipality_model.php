<?php

class Municipality_Model extends My_Model
{

    protected $table = 'nvp_municipality';
    public $id = '', $name = '', $updated_by = '', $updated_on = '', $districtId = '';

    public function __construct()
    {
        parent::__construct();
        $this->updated_timestamp = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $rules = array(
            array(
                'field' => 'name',
                'label' => 'Municipality Name',
                'rules' => 'trim|required|unique[nvp_municipality.name.'.$id.']',
            ),
            array(
                'field' => 'name',
                'label' => 'District',
                'rules' => 'trim|required',
            ),
        );

        return $rules;
    }

    public function getAllData()
    {
        $query = "select
                    m.name,
                    m.id,
                    d.name as districtName
                    from `nvp_municipality` m
                    join `nvp_district` d on d.`id` = m.`districtId`";

        return $this->query($query);
    }

}