<?php

class User_Model extends My_Model
{

    public $table = 'tbl_user';
    public $id = '', $name = '', $username = '', $password = '', $email = '', $status = '', $role_id = '';
    public $rules =
        array(
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'trim|required|callback_unique_username',
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim'
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|valid_email|callback_unique_email'
            ),
            array(
                'field' => 'role_id',
                'label' => 'User Role',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim'
            )
        );

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = false;

        // password hashing library for older PHP version
        require APPPATH . '/third_party/password.php';
    }

    public function login($data)
    {
        $user = $this->get(1, array('username' => $data['username']));
        if($user && password_verify($data['password'], $user->password)) {
            $session_array = array(
                'username' => $user->username,
                'user_id' => $user->id,
                'role_id' => $user->role_id,
                'email' => $user->email,
                'name' => $user->name
            );
            set_userdata($session_array);

            $return = true;
        } else {
            $return = false;
        }

        return $return;
    }

    public function resetPassword($email)
    {
        $user = $this->get(1, array('email' => $email));
        if($user) {
            $new_password = random_string('alnum', 8);
            $email_data = array(
                'name' => $user->name,
                'password' => $new_password,
                'siteLogo' => $this->global_config->site_logo
            );

            $password_reset_email = $this->load->view('email/password_reset_email', $email_data, TRUE);

            $email_params = array(
                'subject' => 'Password Reset',
                'from' => SITEMAIL,
                'fromname' => SITENAME,
                'to' => $user->email,
                'toname' => $email_data['name'],
                'message' => $password_reset_email
            );

            if($this->save(array('password' => password_hash($new_password, PASSWORD_BCRYPT, array('cost' => 10))), array('id' => $user->id))) {
                $return = swiftsend($email_params);
            } else {
                $return = false;
            }
        } else $return = false;

        return $return;
    }

    public function changepassword($data)
    {
        $res = false;
        $userId = $data['userId'];
        $user = $this->get(1, array('id' => $userId));
        if($user && password_verify($data['oldPassword'], $user->password) && $data['newPassword'] == $data['reNewPassword']) {
            $save['password'] = password_hash($data['newPassword'], PASSWORD_BCRYPT, array('cost' => 10));
            $res = $this->user->save($save, array('id' => $userId));
        }
        return $res;
    }

}