<?php

class Agencyprojectcriteriaacademic_model extends My_Model
{

    protected $table = 'nvp_agency_project_criteria_academic_qualification';
    public $id = '', $agencyProjectCriteriaId = '', $academicQualificationId = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function getData($projectCriteriaId)
    {
        $data = $this->get('', array('agencyProjectCriteriaId' => $projectCriteriaId));
        $res = array();

        if($data) {
            foreach($data as $row) {
                $res[] = $row->academicQualificationId;
            }
        }

        return $res;
    }

}