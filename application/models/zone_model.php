<?php

class Zone_Model extends My_Model
{

    protected $table = 'nvp_zone';
    public $id = '', $name = '', $updated_by = '', $updated_on = '';

    public function __construct()
    {
        parent::__construct();
        $this->updated_timestamp = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $rules = array(
            array(
                'field' => 'name',
                'label' => 'Zone Name',
                'rules' => 'trim|required|unique[nvp_zone.name.'.$id.']',
            )
        );

        return $rules;
    }

}