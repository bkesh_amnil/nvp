<?php

class Registrationmsg_Model extends My_Model
{

    protected $table = 'nvp_registration_message';
    public $id = '', $volunteer_registration_message = '', $agency_registration_message = '', $volunteer_email_activate_message = '';
    public $rules =
        array(
            array(
                'field' => 'volunteer_registration_message',
                'label' => 'volunteer_registration_message',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'agency_registration_message',
                'label' => 'agency_registration_message',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'volunteer_email_activate_message',
                'label' => 'volunteer_email_activate_message',
                'rules' => 'trim|required'
            )
        );

    public function __construct()
    {
        parent::__construct();
    }

}