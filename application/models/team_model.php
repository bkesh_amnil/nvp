<?php

class Team_Model extends My_Model
{

    protected $table = 'nvp_team';
    public $id = '', $name = '', $updated_by = '', $updated_on = '', $status = '', $orderNumber = '', $description = '', $image = '',
        $facebook = '', $twitter = '', $linkedIn = '', $categoryId = '';

    public function __construct()
    {
        parent::__construct();
        $this->updated_timestamp = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $rules = array(
            array(
                'field' => 'name',
                'label' => 'Team Member Name',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'description',
                'label' => 'Team Member Message',
                'rules' => 'trim|required',
            ),
        );

        return $rules;
    }

    public function getAllData()
    {
        $query = "select
                    t.*,
                    c.name as categoryName
                    from nvp_team t
                    join tbl_category c on c.id = t.categoryId
                    where t.status = 'Active'
                    order by t.orderNumber asc";
        $teamData = $this->query($query);

        if($teamData) {
            foreach($teamData as $row) {
                $resData[$row->categoryName][] = $row;
            }
        }
        return isset($resData) ? $resData : false;
    }

}