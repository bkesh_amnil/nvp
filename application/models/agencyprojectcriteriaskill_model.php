<?php

class Agencyprojectcriteriaskill_model extends My_Model
{

    protected $table = 'nvp_agency_project_criteria_skill';
    public $id = '', $agencyProjectCriteriaId = '', $skillId = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function getData($projectCriteriaId)
    {
        $data = $this->get('', array('agencyProjectCriteriaId' => $projectCriteriaId));
        $res = array();

        if($data) {
            foreach($data as $row) {
                $res[] = $row->skillId;
            }
        }

        return $res;
    }

}