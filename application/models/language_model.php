<?php

class Language_Model extends My_Model
{

    protected $table = 'nvp_language';
    public $id = '', $name = '', $updated_by = '', $updated_on = '', $status = '', $orderNumber = '';

    public function __construct()
    {
        parent::__construct();
        $this->updated_timestamp = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $rules = array(
            array(
                'field' => 'name',
                'label' => 'Language Name',
                'rules' => 'trim|required|unique[nvp_language.name.'.$id.']',
            )
        );

        return $rules;
    }

}