<?php

class Banner_Model extends My_Model
{

    protected $table = 'tbl_banner';
    public $id = '', $title = '', $image = '', $status = '', $category_id = '', $description = '', $link = '', $secondary_image = '';
    public $rules =
        array(
            array(
                'field' => 'image',
                'label' => 'Banner Image',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'category_id',
                'label' => 'Category',
                'rules' => 'trim|required',
            )
        );

    public function __construct()
    {
        parent::__construct();
    }

    public function get_by_category($category)
    {
        $sql = "SELECT B.* FROM `tbl_banner` B
                JOIN `tbl_category` C ON C.`id` = B.`category_id`
                WHERE C.`slug` = '{$category}'
                AND B.`status` = 'Active'
                ORDER BY orderNumber ASC";

        $result = $this->query($sql);

        return $result;
    }

}