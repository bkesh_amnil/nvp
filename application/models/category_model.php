<?php

class Category_Model extends My_Model
{

    protected $table = 'tbl_category';
    public $id = '', $name = '', $slug = '', $status = '', $type = '';
    public $rules =
        array(
            array(
                'field' => 'name',
                'label' => 'Category Name',
                'rules' => 'trim|required|callback_unique_category',
            )
        );

    public function __construct()
    {
        parent::__construct();
    }

    public function getCategoryType()
    {
        $query = "select
                    *
                    from `tbl_category_type`";
        return $this->query($query);
    }

    public function getAllData()
    {
        $query = "select
                    cat.`id`,
                    cat.`status`,
                    cat.`name`,
                    catType.`name` as categoryType
                    from `tbl_category` cat
                    join `tbl_category_type` catType on cat.`type` = catType.`id`";
        return $this->query($query);
    }

}