<?php

class Postcomment_Model extends My_Model
{

    protected $table = 'nvp_post_comments';
    public $id = '', $userId = '', $postId = '', $comment = '', $ip = '';

    public function __construct()
    {
        parent::__construct();
        $this->updated_timestamp = true;
    }

    public function getComments($postId, $limit)
    {
        $query = "select
                    pc.*,
                    IFNULL(
                      (select fullName from nvp_volunteer v where v.id = pc.userId and pc.userType='Volunteer'),
                      (select name from nvp_agency a where a.id = pc.userId and pc.userType='Agency')
                    )as fullName,
                    IFNULL(
                    (select profilePicture from nvp_volunteer v where v.id = pc.userId and pc.userType='Volunteer'),
                    (select logo from nvp_agency a where a.id = pc.userId and pc.userType='Agency')
                    ) as profilePicture
                    from nvp_post_comments pc
                    where pc.postId = $postId and pc.status = 'Active'
                    order by pc.id desc";
        if($limit) {
            $query .= " limit $limit";
        }
        return $this->query($query);
    }
}