<?php

class Content_Model extends My_Model
{

    protected $table = 'tbl_content';
    public $id = '', $name = '',$cover_image='', $image = '', $status = '', $category_id = '', $source = '',
        $short_description = '', $long_description = '', $meta_description = '', $meta_keyword = '',
        $slug = '', $parent_id = '', $publish_date = '', $sub_title = '', $featured = 'No';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'name',
                'label' => 'Title',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'long_description',
                'label' => 'Long Description',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'category_id',
                'label' => 'Category',
                'rules' => 'trim|required',
            )
        );

        return $array;
    }

    public function get_footer_links()
    {
        $company_info = $this->get('', array('category_id' => 6, 'status' => 'Active'));
        $support = $this->get('', array('category_id' => 7, 'status' => 'Active'));
        $result = array(
            'company_info' => $company_info,
            'support' => $support
        );
        return $result;
    }

    public function getFeaturedNews()
    {
        $query = "select
                    c.`name`,
                    c.`short_description`,
                    c.`publish_date`,
                    c.`slug`,
                    c.`image`,
                    c.`cover_image`
                    from `tbl_content` c
                    join `tbl_category` cat
                    on cat.`id` = c.`category_id`
                    where c.`featured` = 'Yes'
                    and c.`status` = 'Active'
                    and cat.id = 2
                    order by c.`orderNumber` asc";

        return $this->query($query);
    }

    public function getByCategory($category)
    {
        $query = "select
                    c.`name`,
                    c.`long_description`,
                    c.`short_description`,
                    c.`slug`,
                    c.`image`,
                    c.`cover_image`
                    from `tbl_content` c
                    join `tbl_category` cat
                    on cat.`id` = c.`category_id`
                    where c.`status` = 'Active'
                    and cat.`slug` = '{$category}'
                    order by c.`orderNumber` asc";

        return $this->query($query);
    }
    public function getByCategoryNews($category)
    {
        $query = "select
                    c.`name`,
                    c.`long_description`,
                    c.`short_description`,
                    c.`slug`,
                    c.`image`,
                    c.`publish_date`,
                    c.`cover_image`
                    from `tbl_content` c
                    join `tbl_category` cat
                    on cat.`id` = c.`category_id`
                    where c.`status` = 'Active'
                    and cat.`slug` = '{$category}'
                    order by c.`orderNumber` asc";

        return $this->query($query);
    }

    public function getByCategoryNewsPaginate($category,$offset = 0, $per_page = 6)
    {
        $query = "select
                    c.`name`,
                    c.`long_description`,
                    c.`short_description`,
                    c.`slug`,
                    c.`image`,
                    c.`publish_date`,
                    c.`cover_image`
                    from `tbl_content` c
                    join `tbl_category` cat
                    on cat.`id` = c.`category_id`
                    where c.`status` = 'Active'
                    and cat.`slug` = '{$category}'
                    order by c.`orderNumber` asc LIMIT $offset, $per_page";
        return $this->query($query);
    }
    

    public function getBySlug($newsSlug)
    {
        $query = "select
                    c.`id`,
                    c.`name`,
                    c.`long_description`,
                    c.`short_description`,
                    c.`publish_date`,
                    c.`slug`,
                    c.`image`,
                    c.`cover_image`,
                    c.`meta_keyword`,
                    c.`meta_description`
                    from `tbl_content` c
                    where c.`status` = 'Active'
                    and c.`slug` = '{$newsSlug}'";

        return $this->db->query($query)->row();
    }



}