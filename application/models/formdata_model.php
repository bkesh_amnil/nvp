<?php

class Formdata_Model extends My_Model
{

    protected $table = 'tbl_form_data';
    public $id = '', $name = '', $email = '', $message = '', $type = '';

    public function __construct()
    {
        parent::__construct();
    }
}