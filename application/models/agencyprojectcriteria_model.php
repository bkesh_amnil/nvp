<?php

class Agencyprojectcriteria_model extends My_Model
{

    protected $table = 'nvp_agency_project_criteria';
    public $id = '', $agencyProjectId = '', $totalNumberOfVolunteersRequired = '', $ageGroupFrom = '', $ageGroupTo = '',
        $isSkillRequired = '', $isMinimumEducationRequired = '', $isTrainingRequired = '', $zoneId = '', $districtId = '',
        $municipalityId = '', $isGenderRequired = '', $wardNumber = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_criteria($agencyProjectId)
    {
        $query = "select apc.id ,apc.totalNumberOfVolunteersRequired , apc.ageGroupFrom , apc.ageGroupTo ,
        apc.isSkillRequired , apc.isMinimumEducationRequired , apc.isTrainingRequired , z.name as zone , d.name as district ,
        m.name as municipality , isGenderRequired , wardNumber from nvp_agency_project_criteria apc
        left join nvp_zone z on z.id = apc.zoneId
    left join nvp_district d on d.id = apc.districtId
    left join nvp_municipality m on m.id = apc.municipalityId
    where apc.agencyProjectId = '$agencyProjectId'";
        return $this->db->query($query)->row();
    }
    public function get_skills($agencyProjectCriteriaId){
        $query = "select s.name from nvp_agency_project_criteria_skill ncs
                  left join nvp_skill s on s.id = ncs.skillId
                  where ncs.agencyProjectCriteriaId = '$agencyProjectCriteriaId'
                  and s.status = 'Active'
                  order by s.name asc";
        return $this->db->query($query)->result();
    }
    public function get_qualification($agencyProjectCriteriaId){
        $query = "select q.name from nvp_agency_project_criteria_academic_qualification aq
                  left join nvp_academic_qualification q on q.id = aq.academicQualificationId
                  where aq.agencyProjectCriteriaId = '$agencyProjectCriteriaId'
                  and q.status = 'Active'";
        return $this->db->query($query)->result();
    }
    public function get_training($agencyProjectCriteriaId){
        $query = "select t.name from nvp_agency_project_criteria_training apct
                  left join nvp_training t on t.id = apct.trainingId
                  where apct.agencyProjectCriteriaId = '$agencyProjectCriteriaId'
                  and t.status = 'Active'
                  order by t.name asc";
        return $this->db->query($query)->result();
    }
    public function get_gender($agencyProjectCriteriaId){
        $query = "select g.name from nvp_agency_project_criteria_gender apcg
                  left join nvp_gender g on g.id = apcg.genderId
                  where apcg.agencyProjectCriteriaId = '$agencyProjectCriteriaId'";
        return $this->db->query($query)->row();
    }

}