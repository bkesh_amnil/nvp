<?php

class Notificationcount_Model extends My_Model
{

    protected $table = 'tbl_notification_count';
    public $id = '', $user_id = '',$user_type = '',$notification_id='';

    public function rules($id)
    {
        $rules =
            array(
                array(
                    'field' => 'title',
                    'label' => 'title',
                    'rules' => 'trim|required|unique['.$this->table.'.title.'.$id.']',
                )
            );

        return $rules;
    }

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = false;
        $this->updated_timestamp = false;
        $this->created_by = false;
        $this->updated_by = false;
    }

    public function count_notification($notifications)
    {
        $count = 0;
        foreach($notifications as $notification) {
            $check = $this->get(
                '1',
                array(
                    'notification_id' => $notification->id,
                    'user_id' => get_userdata('active_user'),
                    'user_type' => get_userdata('userType')
                ));
            if(!$check) {
                $count++;
                $this->save(array(
                    'notification_id' => $notification->id,
                    'user_id' => get_userdata('active_user'),
                    'user_type' => get_userdata('userType')
                ));
            }
        }
        return $count;
    }
}