<?php

class Volunteer_Model extends My_Model
{

    protected $table = 'nvp_volunteer';
    public $id, $fullName, $updated_by, $updated_on, $genderId,
        $dateOfBirth, $phoneNumberResidential, $phoneNumberMobile, $email,
        $emergencyContactFullName, $emergencyContactPhoneNumber, $zoneId, $districtId,
        $municipalityId, $wardNumber, $academicQualificationId, $availability,
        $volunteerType, $volunteerSubType, $primaryModeOfCommunication,$account_status='',$notification_status='',
        $secondaryModeOfCommunication;

    public function __construct()
    {
        parent::__construct();
        $this->updated_timestamp = true;
        $this->updated_by = true;
    }

    public function get_volunteer($id)
    {
        $query = "select v.id ,v.fullName,v.dateOfRegistration, v.dateOfBirth , g.name as gender ,v.phoneNumberResidential ,v.phoneNumberMobile ,v.email ,v.primaryModeOfCommunication,v.secondaryModeOfCommunication,v.volunteerSubType,
    z.name as zone,d.name as district ,m.name as municipality,aq.name as academinQualification ,v.emergencyContactFullName ,v.emergencyContactPhoneNumber,v.wardNumber ,v.availability as available,
    v.volunteerType,v.account_status,v.notification_status,
     v.status from nvp_volunteer v
    left join nvp_gender g on g.id = v.genderId
    left join nvp_zone z on z.id = v.zoneId
    left join nvp_district d on d.id = v.districtId
    left join nvp_municipality m on m.id = v.municipalityId
    left join nvp_academic_qualification aq on aq.id = v.academicQualificationId
    where v.id = '$id'";
        return $this->db->query($query)->row();
    }

    public function get_id($id,$column){
        $query = "select $column from nvp_volunteer where id ='$id'";
        return $this->db->query($query)->row();
    }

}