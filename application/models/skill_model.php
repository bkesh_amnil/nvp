<?php

class Skill_Model extends My_Model
{

    protected $table = 'nvp_skill';
    public $id = '', $name = '', $updated_by = '', $updated_on = '', $status = '', $orderNumber = '';

    public function __construct()
    {
        parent::__construct();
        $this->updated_timestamp = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $rules = array(
            array(
                'field' => 'name',
                'label' => 'Skill Name',
                'rules' => 'trim|required|unique[nvp_skill.name.'.$id.']',
            )
        );

        return $rules;
    }

}