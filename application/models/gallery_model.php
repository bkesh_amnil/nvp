<?php

class Gallery_Model extends My_Model
{

    protected $table = 'tbl_gallery';
    public $id = '', $name = '',$type='',$short_description='', $status = '', $category_id = '', $cover = '', $contentId = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'name',
                'label' => 'Gallery Name',
                'rules' => 'trim|required|unique[tbl_gallery.name.'.$id.']',
            ),
            array(
                'field' => 'category_id',
                'label' => 'Category',
                'rules' => 'trim|required',
            )
        );

        return $array;
    }

    public function getSavedMedia($id)
    {
        $query = "select *
                    from `tbl_gallery_media` gm
                    where gm.`gallery_id` = $id";

        return $this->query($query);
    }
    public function check_video_gallary(){
        $query = "select *
                    from `tbl_gallery` g
                    where g.`type` = 'Video'";

        return $this->query($query);
    }

    public function getContentGallery($contentId)
    {
        $query = "select
                    g.name as galleryName,
                    gm.*
                    from tbl_gallery g
                    join tbl_gallery_media gm on gm.gallery_id = g.id
                    where g.status = 'Active'
                    and g.contentId = $contentId";
        return $this->query($query);
    }

}