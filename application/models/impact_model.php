<?php

class Impact_Model extends My_Model
{

    protected $table = 'tbl_impact';
    public $id = '', $name = '', $statistic = '', $status = '', $icon = '', $color = '';

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'name',
                'label' => 'Impact Name',
                'rules' => 'trim|required|unique[tbl_impact.name.'.$id.']',
            ),
            array(
                'field' => 'icon',
                'label' => 'Impact Icon',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'statistic',
                'label' => 'Impact Statistic',
                'rules' => 'trim|required|numeric',
            )
        );

        return $array;
    }

    public function __construct()
    {
        parent::__construct();
    }

}