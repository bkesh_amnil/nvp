<?php

class Post_Model extends My_Model
{

    protected $table = 'nvp_post';
    public $id = '', $name = '', $slug = '', $status = '', $image = '', $description = '', $created_on = '', $userId = '', $userType = '',
            $location = '', $totalShares = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
    }

    public function getBySlug($newsSlug)
    {
        return $this->get('1', array('slug' => $newsSlug), 'created_on asc');
    }

    public function getNewsFeed()
    {
        $userId = get_userdata('active_user');
        $query = "SELECT
                      p.*,
                      (SELECT COUNT(id) FROM nvp_post_likes pl WHERE pl.`postId` = p.`id`) AS totalLikes,
                      (SELECT v.fullName FROM nvp_volunteer v WHERE v.id = p.userId) AS post_by,
                      (SELECT COUNT(id) FROM nvp_post_comments pc WHERE pc.`postId` = p.`id`) AS totalComments,
                      (SELECT IFNULL(id, 0) FROM nvp_post_likes pl WHERE pl.`userId` = $userId AND pl.`postId` = p.`id`) AS userLiked
                    FROM
                      nvp_post p
                    WHERE p.status = 'Active'
                    ORDER BY p.created_on DESC
                    LIMIT 6";

        return $this->query($query);
    }

    public function getNewsFeedPagination($offset = 0, $per_page = 6)
    {
        $userId = get_userdata('active_user');
        $query = "SELECT
                      p.*,
                      (SELECT COUNT(id) FROM nvp_post_likes pl WHERE pl.`postId` = p.`id`) AS totalLikes,
                      (SELECT v.fullName FROM nvp_volunteer v WHERE v.id = p.userId) AS post_by,
                      (SELECT COUNT(id) FROM nvp_post_comments pc WHERE pc.`postId` = p.`id`) AS totalComments,
                      (SELECT IFNULL(id, 0) FROM nvp_post_likes pl WHERE pl.`userId` = $userId AND pl.`postId` = p.`id`) AS userLiked
                    FROM
                      nvp_post p
                    WHERE p.status = 'Active'
                    ORDER BY p.created_on DESC
                    LIMIT $offset, $per_page";

        return $this->query($query);
    }


}