<?php

class Volunteerskill_Model extends My_Model {
    protected $table = 'nvp_volunteer_skill';
    public function __construct()
    {
        parent::__construct();
    }

    public function getData($volunteerId = '')
    {
        $query = "select s.name as skillName, s.id as skillId
                    from nvp_volunteer_skill vs
                    join nvp_volunteer v on v.id = vs.volunteerId
                    join nvp_skill s on vs.skillId = s.id
                    where v.status = 'Active'";

        if($volunteerId != '') {
            $query .= " and vs.volunteerId = $volunteerId";
        }

        $data = $this->query($query);
        if($data) {
            foreach($data as $row) {
                $res[] = $row->skillId;
            }
        } else {
            $res = false;
        }
        return $res;
    }
}