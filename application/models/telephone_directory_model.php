<?php

class Telephone_directory_Model extends My_Model
{

    protected $table = 'tbl_telephone_directory';
    public $id = '', $first_name = '', $middle_name = '', $last_name = '', $mobile_number = '', $contact_number = '',
        $ext_number = '',$status='',$image='', $created_on = '', $updated_on = '';
    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'first_name',
                'label' => 'First name',
                'rules' => 'trim|required|',
            ),
            array(
                'field' => 'last_name',
                'label' => 'Last name',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'contact_number',
                'label' => 'Contact number',
                'rules' => 'required|numeric',
            ),
            array(
                'field'=>'mobile_number',
                'label' => 'Mobile Number',
                'rules' => 'numeric'
            )
        );

        return $array;
    }

    public function get_footer_links()
    {
        $company_info = $this->get('', array('category_id' => 6, 'status' => 'Active'));
        $support = $this->get('', array('category_id' => 7, 'status' => 'Active'));
        $result = array(
            'company_info' => $company_info,
            'support' => $support
        );
        return $result;
    }

}