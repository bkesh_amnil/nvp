<?php

class Volunteertraining_Model extends My_Model {
    protected $table = 'nvp_volunteer_training_received';
    public function __construct()
    {
        parent::__construct();
    }

    public function getData($volunteerId = '')
    {
        $query = "select t.name as trainingName, t.id as trainingId
                    from nvp_volunteer_training_received vt
                    join nvp_volunteer v on v.id = vt.volunteerId
                    join nvp_training t on vt.trainingId = t.id
                    where v.status = 'Active'";

        if($volunteerId != '') {
            $query .= " and vt.volunteerId = $volunteerId";
        }

        $data = $this->query($query);
        if($data) {
            foreach($data as $row) {
                $res[] = $row->trainingId;
            }
        } else {
            $res = false;
        }
        return $res;
    }
}