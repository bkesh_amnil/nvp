<?php

class Project_Model extends My_Model
{

    protected $table = 'tbl_project';
    public $id = '', $name = '', $slug = '', $status = '', $image = '', $logo = '', $description = '', $long_description = '',
            $created_by = '', $created_on = '', $updated_by = '', $updated_on = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'name',
                'label' => 'Project Name',
                'rules' => 'trim|required|unique[tbl_project.name.'.$id.']',
            ),
            array(
                'field' => 'description',
                'label' => 'Project Description',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'logo',
                'label' => 'Project Logo',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'image',
                'label' => 'Project Image',
                'rules' => 'trim|required',
            )
        );

        return $array;
    }

    public function getAllActive()
    {
        $query = "select
                    p.`name`,
                    p.`slug`,
                    p.`description`,
                    p.`image`,
                    p.`logo`
                    from $this->table p
                    where p.`status` = 'Active'";

        return $this->query($query);
    }

    public function getBySlug($slug)
    {
        $query = "select
                    p.`name`,
                    p.`slug`,
                    p.`long_description`,
                    p.`logo`
                    from $this->table p
                    where p.`slug` = '{$slug}' and p.`status` = 'Active'";

        return $this->db->query($query)->row();
    }

}