<?php

class Agencyprojectcriteriagender_model extends My_Model
{

    protected $table = 'nvp_agency_project_criteria_gender';
    public $id = '', $name = '', $agencyProjectCriteriaId = '', $genderId = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function getData($projectCriteriaId)
    {
        $data = $this->get('', array('agencyProjectCriteriaId' => $projectCriteriaId));
        $res = array();

        if($data) {
            foreach($data as $row) {
                $res[] = $row->genderId;
            }
        }

        return $res;
    }

}