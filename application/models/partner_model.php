<?php

class Partner_Model extends My_Model
{

    protected $table = 'tbl_partner';
    public $id = '', $name = '', $slug = '', $status = '', $image = '', $logo = '', $description = '',
        $created_by = '', $created_on = '', $updated_by = '', $updated_on = '', $partner_category = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function rules($id)
    {
        $rules =
            array(
                array(
                    'field' => 'name',
                    'label' => 'Partner Name',
                    'rules' => 'trim|required|unique[tbl_partner.name.'.$id.']',
                ),
                array(
                    'field' => 'description',
                    'label' => 'Partner Description',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'image',
                    'label' => 'Partner Image',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'partner_category',
                    'label' => 'Partner Category',
                    'rules' => 'trim|required',
                )
            );

        return $rules;
    }

    public function getAllData()
    {
        $query = "select
                    p.`id`,
                    p.`status`,
                    p.`name`,
                    p.`image`,
                    cat.`name` as  categoryName
                    from `tbl_partner` p
                    join `tbl_category` cat on cat.`id` = p.`partner_category`";
        return $this->query($query);
    }

    public function getAllActive()
    {
        $query = "select
                    p.`id`,
                    p.`name`,
                    p.`slug`,
                    p.`description`,
                    p.`image`,
                    cat.`name` as categoryName,
                    cat.`slug` as categorySlug
                    from $this->table p
                    join `tbl_category` cat on cat.`id` = p.`partner_category`
                    where p.`status` = 'Active'";

        return $this->query($query);
    }
    public function get_categories(){
        $query = "SELECT name FROM tbl_category where type = 2";
        return $this->query($query);
    }

}