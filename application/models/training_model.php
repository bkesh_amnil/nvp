<?php

class Training_Model extends My_Model
{

    protected $table = 'nvp_training';
    public $id = '', $name = '', $updated_by = '', $updated_on = '', $status = '', $orderNumber = '';

    public function __construct()
    {
        parent::__construct();
        $this->updated_timestamp = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $rules = array(
            array(
                'field' => 'name',
                'label' => 'Training Name',
                'rules' => 'trim|required|unique[nvp_training.name.'.$id.']',
            )
        );

        return $rules;
    }

}