<?php

class District_Model extends My_Model
{

    protected $table = 'nvp_district';
    public $id = '', $name = '', $updated_by = '', $updated_on = '', $zoneId = '';

    public function __construct()
    {
        parent::__construct();
        $this->updated_timestamp = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $rules = array(
            array(
                'field' => 'name',
                'label' => 'District Name',
                'rules' => 'trim|required|unique[nvp_district.name.'.$id.']',
            ),
            array(
                'field' => 'zoneId',
                'label' => 'Zone',
                'rules' => 'trim|required',
            ),
        );

        return $rules;
    }

    public function getAllData()
    {
        $query = "select
                    d.name,
                    d.id,
                    z.name as zoneName
                    from `nvp_district` d
                    join `nvp_zone` z on z.`id` = d.`zoneId`";

        return $this->query($query);
    }

}