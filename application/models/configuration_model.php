<?php

class Configuration_Model extends My_Model
{

    protected $table = 'tbl_configuration';
    public $id = '', $site_title = '', $address = '', $phone = '', $facebook = '', $twitter = '', $gplus = '', $skype = '',
            $meta_keyword = '', $meta_description = '', $site_email = '', $site_logo = '', $latitude='',$longitude='', $infobox = '';
    public $rules =
        array(
            array(
                'field' => 'site_title',
                'label' => 'Site Title',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'site_email',
                'label' => 'Site Email',
                'rules' => 'trim|required|valid_email',
            ),
            array(
                'field' => 'address',
                'label' => 'Address',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'phone',
                'label' => 'Phone',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'facebook',
                'label' => 'Facebook Link',
                'rules' => 'trim'
            ),
            array(
                'field' => 'twitter',
                'label' => 'Twitter Link',
                'rules' => 'trim'
            ),
            array(
                'field' => 'gplus',
                'label' => 'Google Plus Link',
                'rules' => 'trim'
            ),
            array(
                'field' => 'skype',
                'label' => 'Skype Link',
                'rules' => 'trim'
            ),
            array(
                'field' => 'latitude',
                'label' => 'latitude',
                'rules' => 'required'
            ),
            array(
                'field' => 'longitude',
                'label' => 'longitude',
                'rules' => 'required'
            )
        );

    public function __construct()
    {
        parent::__construct();
    }

}