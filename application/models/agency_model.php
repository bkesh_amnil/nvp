<?php

class Agency_Model extends My_Model
{

    protected $table = 'nvp_agency';
    public $id = '', $name = '', $address = '', $phoneNumber = '', $email = '',
        $agencyTypeId = '', $agencyTypeName = '', $registrationNumber = '', $logo = '',
        $registrationCertificateImage = '', $primaryContactPersonFullName = '', $primaryContactPersonDesignationInAgency = '',
        $primaryContactPersonPersonalPhoneNumber = '', $primaryContactPersonEmail = '', $secondaryContactPersonFullname = '',
        $secondaryContactPersonDesignationInAgency = '', $secondaryContactPersonPersonalPhoneNumber = '',
        $dateOfRegistration = '', $dateOfVerification = '', $password = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function rules($id)
    {
        $rules = array(
            array(
                'field' => 'name',
                'label' => 'Volunteer Name',
                'rules' => 'trim|required',
            )
        );

        return $rules;
    }
    public function get_agency($id){
        $query = "select id,name, address, phoneNumber, email, agencyTypeName, registrationNumber, logo,registrationCertificateImage ,primaryContactPersonFullName ,primaryContactPersonDesignationInAgency ,
        primaryContactPersonPersonalPhoneNumber ,primaryContactPersonEmail ,secondaryContactPersonFullname ,
        secondaryContactPersonDesignationInAgency ,secondaryContactPersonPersonalPhoneNumber ,
        dateOfRegistration ,dateOfVerification,status from nvp_agency where `id` = '$id'";
        return $this->db->query($query)->row();
    }

}