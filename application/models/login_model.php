<?php

class Login_Model extends My_Model
{
    public function __construct()
    {
        parent::__construct();

        // password hashing library for older PHP version
        require APPPATH . '/third_party/password.php';
    }

    public function validateUser($userData, $returnUserData = '')
    { 
        $loginStatus = false;
        if($returnUserData) {
            $getUser = $this->query("select status, notification_status, account_status, profilePicture, id, password, email, phoneNumberResidential, phoneNumberMobile, fullName from nvp_volunteer where (email = '{$userData['username']}' or phoneNumberMobile = '{$userData['username']}')");
        } else {
            $getUser = $this->query("select status, notification_status, account_status, profilePicture, id, password, email, phoneNumberResidential, phoneNumberMobile, fullName from nvp_volunteer where (email = '{$userData['username']}' or phoneNumberMobile = '{$userData['username']}') and status = 'Active'");
        }

        if ($getUser) {
            $res = $getUser[0];
            if (password_verify($userData['password'], $res->password)) {
                set_userdata(array(
                    'active_user' => $res->id,
                    'userType' => 'Volunteer',
                    'userName' => $res->fullName
                ));
                $userType = 'Volunteer';

                if (strpos($res->profilePicture, 'https') !== false || strpos($res->profilePicture, 'http') !== false) {
                    set_userdata(array(
                        'profilePicture' => $res->profilePicture
                    ));
                } else {
                    set_userdata(array(
                        'profilePicture' => ($res->profilePicture) ? base_url($res->profilePicture) : base_url('assets/img/icon/icon-user-default.png')
                    ));

                    $loginStatus = true;
                }
            }
        } else {
            if($returnUserData) {
                $getUser = $this->query("select status, logo, name, id, password, email, phoneNumber from nvp_agency where (email = '{$userData['username']}' or phoneNumber = '{$userData['username']}')");
            } else {
                $getUser = $this->query("select status, logo, name, id, password, email, phoneNumber from nvp_agency where (email = '{$userData['username']}' or phoneNumber = '{$userData['username']}') and status = 'Active'");
            }
                //$getUser = $this->query("select logo, name, id, password, email, phoneNumber from nvp_agency where (email = '{$userData['username']}' or phoneNumber = '{$userData['username']}') and status = 'Active'");
                if ($getUser) {
                    $res = $getUser[0];
                    if (password_verify($userData['password'], $res->password)) {
                        set_userdata(array(
                            'active_user' => $res->id,
                            'userType' => 'Agency',
                            'userName' => $res->name,
                            'profilePicture' => $res->logo
                        ));
                        $userType = 'Agency';

                        if (strpos($res->logo, 'https') !== false || strpos($res->logo, 'http') !== false) {
                            set_userdata(array(
                                'profilePicture' => $res->logo
                            ));
                        } else {
                            set_userdata(array(
                                'profilePicture' => ($res->logo) ? base_url($res->logo) : base_url('assets/img/icon/icon-user-default.png')
                            ));
                        }
                        $loginStatus = true;
                    }
                }
            }

        if ($loginStatus && $returnUserData) {
            if($getUser[0]->status == 'InActive') {
                $loginStatus = 'InActive';
            } else {
                if ($userType == 'Volunteer') {
                    $loginStatus = array(
                        'userId' => $getUser[0]->id,
                        'username' => $getUser[0]->fullName,
                        'email' => $getUser[0]->email,
                        'phone' => $getUser[0]->phoneNumberResidential,
                        'profilePicture' => $getUser[0]->profilePicture,
                        'type' => get_userdata('userType'),
                        'socialId' => $getUser[0]->socialId,
                        'account_status' => $getUser[0]->account_status,
                        'notification_status' => $getUser[0]->notification_status,
                        'phone_mobile' => $getUser[0]->phoneNumberMobile
                    );
                } else if ($userType == 'Agency') {
                    $loginStatus = array(
                        'userId' => $getUser[0]->id,
                        'username' => $getUser[0]->name,
                        'email' => $getUser[0]->email,
                        'phone' => $getUser[0]->phoneNumber,
                        'profilePicture' => $getUser[0]->logo,
                        'type' => get_userdata('userType')
                    );
                }
            }
        }

        return $loginStatus;
        
    }
}