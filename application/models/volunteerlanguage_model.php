<?php

class Volunteerlanguage_Model extends My_Model {
    protected $table = 'nvp_volunteer_language';
    public function __construct()
    {
        parent::__construct();
    }

    public function getData($volunteerId = '')
    {
        $query = "select l.name as languageName, l.id as languageId
                    from nvp_volunteer_language vl
                    join nvp_volunteer v on v.id = vl.volunteerId
                    join nvp_language l on vl.languageId = l.id
                    where v.status = 'Active'";

        if($volunteerId != '') {
            $query .= " and vl.volunteerId = $volunteerId";
        }

        $data = $this->query($query);
        if($data) {
            foreach($data as $row) {
                $res[] = $row->languageId;
            }
        } else {
            $res = false;
        }
        return $res;
    }
}