<?php

class Testimonial_Model extends My_Model
{

    protected $table = 'tbl_testimonial';
    public $id = '', $name = '', $message = '', $status = '', $image = '', $date = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function rules()
    {
        $rules = array(
            array(
                'field' => 'name',
                'label' => 'Testimonial Name',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'message',
                'label' => 'Message',
                'rules' => 'trim|required',
            )
        );

        return $rules;
    }

}