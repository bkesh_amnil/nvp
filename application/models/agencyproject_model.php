<?php

class Agencyproject_model extends My_Model
{

    protected $table = 'nvp_agency_project';
    public $id = '', $name = '', $projectType = '', $purpose = '', $additionalFacilitiesProvidedByAgency ='',
        $additionalRemarks= '', $totalNumberOfDaysVolunteersAreNeeded = '', $totalNumberOfHoursPerVolunteer ='', $dateOfEvent ='',
        $timeOfEvent = '', $agencyId ='', $otherSkills = '', $otherTraining = '', $short_description = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_agency_project($id)
    {
        $query = "select ag.short_description, ag.id,a.name as agencyName, ag.name as projectName, ag.projectType, ag.numberOfVolunteersNeeded, ag.purpose, ag.additionalFacilitiesProvidedByAgency,
            ag.additionalRemarks, ag.totalNumberOfDaysVolunteersAreNeeded, ag.totalNumberOfHoursPerVolunteer, ag.dateOfEvent,
            ag.timeOfEvent, ag.location from nvp_agency_project ag
    left join nvp_agency a on a.id = ag.agencyId
    where ag.id = '$id'";
        return $this->db->query($query)->row();
    }

    public function getAllData($agencyId)
    {
        $query = "SELECT
                  ap.*,
                  IFNULL((SELECT count(id) FROM nvp_project_volunteer pv WHERE pv.agencyProjectId = ap.id), 0) as totalVolunteers,
                  IFNULL((SELECT sum(hours) FROM nvp_project_volunteer pv WHERE pv.agencyProjectId = ap.id), 0) as totalHours
                  from nvp_agency_project ap
                  where ap.agencyId = $agencyId
                  AND ap.status != 'InActive'
                  order by ap.dateOfEvent desc";
        return $this->db->query($query)->result();
    }

    public function get_id($id,$column){
        $query = "select $column from nvp_volunteer where id ='$id'";
        return $this->db->query($query)->row();
    }

    public function getVolunteerHours($id){
        $query = "select IFNULL(sum(`hours`), 0) as totalHours from nvp_project_volunteer where volunteerId ='$id'";
        return $this->db->query($query)->row();
    }

    public function filterProject($searchParams)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        if(isset($searchParams['projectName']) && $searchParams['projectName'] != '') {
            $this->db->like('name', $searchParams['projectName']);
        }
        if(isset($searchParams['projectStatus']) && $searchParams['projectStatus'] != '') {
            switch ($searchParams['projectStatus']) {
                case 'completed':
                    $this->db->where('status', 'Completed');
                    break;
                case 'ongoing':
                    $this->db->where('status', 'Ongoing');
                    break;
                default:
                    $this->db->where('status != ', 'InActive');
                    break;
            }
        } else {
            $this->db->where('status != ', 'InActive');
        }
        //feedCtrfee$this->db->order_by('dateOfEvent', 'desc');
        $this->db->order_by('status', 'asc');
        $project = $this->db->get();
        return $project->num_rows() > 0 ? $project->result() : false;
    }
}