<?php

class Menu_Model extends My_Model
{

    protected $table = 'tbl_menu';
    public $id = '', $menu_title = '', $decription='', $menu_alias = '', $menu_type = '', $menu_link_type = '',
        $menu_parent = '', $status = '', $link_target = '', $description = '', $link = '', $link_parent_id = '', $front_display = '',
        $orderNumber = '';

    public $rules =
        array(
            array(
                'field' => 'menu_title',
                'label' => 'Menu Title',
                'rules' => 'trim|required',
            )
        );

    public function __construct()
    {
        parent::__construct();
    }

    public function get_menutypes()
    {
        $sql = "SELECT * FROM `tbl_menu_types`";

        $result = $this->query($sql);

        return $result;
    }

    public function get_menulinktype()
    {
        $sql = "SELECT linktype, title  FROM `tbl_menu_link_types`";

        $results = $this->query($sql);

        if ($results) {
            foreach ($results as $key => $data) {
                $result[$data->linktype] = $data->title;
            }
        }
        return $result;
    }

    public function get_menulinks($key)
    {
        if ($key == 'content') {
            $sql = "SELECT c.id, c.name FROM tbl_content c JOIN tbl_category ca ON c.category_id = ca.id WHERE NOT ca.type = 'News'";
            $results = $this->query($sql);
            if (empty($results)) {
                return [];
            }
            foreach ($results as $data) {
                $result[$data->id] = $data->name;
            }
        } else if ($key == "module") {
            $sql = "SELECT * FROM tbl_module WHERE public_module = 'yes'";
            $results = $this->query($sql);
            if (empty($results)) {
                return [];
            }
            foreach ($results as $datas) {
                if ($datas->parent_id == 0) {
                    $result[$datas->name][$datas->id]=$datas->name;
                    foreach ($results as $data) {
                        if ($datas->id == $data->parent_id) {
                            $result[$datas->name][$data->id] = $data->name;
                        }
                    }
                }

            }
        } elseif ($key == 'url') {
            $result = 'text';
        } else {
            $result = 'none';
        }
        return $result;
    }

    public function getAllData($parent = 0, $padding = 0)
    {

        $query = "SELECT *
                  FROM tbl_menu
                  WHERE menu_parent='" . $parent . "'
                  ORDER BY `menu_type`, `orderNumber` DESC";

        $result = $this->query($query);

        $finList = array();

        if (isset($result) && !empty($result)) {

            foreach ($result as $ind => $resultval) {
                $menuListGen = "";

                if (!empty($resultval)) {
                    foreach ($resultval as $resultind => $val) {
                        $menuListGen[$resultind] = $val;
                    }

                    $menuListGen["cellpadding"] = $padding;
                    $menuListGen["childList"] = Menu_Model::getAllData($resultval->id, ($padding + 10));

                    array_push($finList, $menuListGen);

                }
            }

        }

        return $finList;
    }

    public function getNavigationByPosition($position)
    {
        $query = "select
                    m.`id`,
                    m.`menu_title`,
                    m.`menu_alias`,
                    m.`menu_parent`,
                    m.`menu_link_type`,
                    m.`link`,
                    m.`orderNumber`
                    from `tbl_menu` m
                    where m.`menu_type` = '{$position}'
                    and m.`status` = 'Active'
                    order by m.`orderNumber` asc";
        $menu = $this->query($query);
        $resp = '';
        if ($menu) {
            foreach ($menu as $menuRow) {
                if ($menuRow->menu_parent == 0) {
                    $resp['parents'][] = $menuRow;
                } else {
                    $resp['children'][$menuRow->menu_parent][] = $menuRow;
                }
            }
        }
        return $resp;
    }

    public function getAllMenuParent()
    {
        $query = "select
                    m.`id`,
                    m.`menu_title`
                    from `tbl_menu` m
                    where m.`menu_parent` = 0";
        return $this->query($query);
    }


}