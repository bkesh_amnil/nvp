<?php

class Postlike_Model extends My_Model
{

    protected $table = 'nvp_post_likes';
    public $id = '', $userId = '', $postId = '', $ip = '';

    public function __construct()
    {
        parent::__construct();
        $this->updated_timestamp = true;
    }

    public function countLikes($postId){
        $data = $this->db->query("SELECT COUNT(*) AS count FROM nvp_post_likes WHERE postId = $postId")->row();
        return $data->count;
    }
}