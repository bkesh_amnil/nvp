(function() {
    'use strict';

    $('#publishDate').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });
})();