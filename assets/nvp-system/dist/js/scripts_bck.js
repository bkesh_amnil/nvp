var base_url = $('#base-url').val();
var admin_base_url = $('#admin-base-url').val();
var admin_module = $('#admin-module').val();

function changeInvitationStatus(status, invitationId) {
    $.ajax({
        url: admin_base_url + '/invitee/status',
        data: {
            status: status,
            invitationId: invitationId
        },
        type: 'post',
        success: function(res) {
            if(res) {
                alert('Status changed');
            }
        }
    });
}
$(document).ready(function() {
    if($('#text').length) {
        var textAreaContent = $('#text').val();
        var characters= 250;
        if(textAreaContent == '') {
            $("#counter").append("You have  <strong>"+ characters+"</strong> characters remaining");
        } else {
            var rem = 250 - textAreaContent.length;
            $("#counter").append("You have  <strong>"+ rem +"</strong> characters remaining");
        }

        $("#text").keyup(function() {
            if ($(this).val().length > characters) {
                $(this).val($(this).val().substr(0, characters));
            }
            var remaining = characters - $(this).val().length;
            $("#counter").html("You have <strong>"+ remaining+"</strong> characters remaining");

            if(remaining <= 10) {
                $("#counter").css("color","red");
            } else {
                $("#counter").css("color","black");
            }
        });
    }
});

$(function () {
    var table = $('.list-datatable').DataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": true,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false
    });
    // Setup - add a text input to each footer cell
    $('#table-search-row th').each(function () {
        var title = $(this).text();
        if (title != '')
            $(this).html('<input type="text" class="form-control" placeholder="Search ' + title + '" />');
        else
            $(this).html('N/A');
    });
    // Apply the search
    table.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    /*$('.list-datatable').dataTable({
     "bPaginate": true,
     "bLengthChange": false,
     "bFilter": false,
     "bSort": true,
     "bInfo": true,
     "bAutoWidth": false
     });
     */
    var elf = $('#media-manager').elfinder({
        url: base_url + 'assets/elfinder/php/connector.php', // connector URL (REQUIRED)
        lang: 'en' // language (OPTIONAL)
    }).elfinder('instance');

    $('#destination').on('change', function () {
        var destination_id = $(this).val();
        $.ajax({
            url: base_url + 'admin/trip/getActivity',
            data: 'destination_id=' + destination_id,
            type: 'post',
            success: function (res) {
                $('#tag-activity').html(res);
            }
        })
    });
});

function load_ckeditor(textarea, customConfig) {
    if (customConfig) {
        configFile = base_url + 'assets/ckeditor/custom/minimal.js';
    } else {
        configFile = base_url + 'assets/ckeditor/custom/full.js';
    }

    CKEDITOR.replace(textarea, {
        customConfig: configFile
    });
}

$(document).on("keyup", ".title", function () {
    var txtValue = $(this).val();
    var newValue = txtValue.toLowerCase().replace(/[~!@#$%\^\&\*\(\)\+=|'"|\?\/;:.,<>\-\\\s]+/gi, '-');
    $('.alias').val(newValue);
});

$(document).ready(function () {
    $('.delete-activity-image').on('click', function (event) {
        event.preventDefault();
        var url = $(this).attr('href');
        $.ajax({
            url: url,
            success: function (res) {
                if (res) {
                    $("html, body").animate({scrollTop: 0}, "slow");
                    $('#image-' + res).remove();
                    var message = '<div class="alert alert-info alert-dismissable fade in">' +
                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                        'Image deleted' +
                        '</div>';
                    $('.message').append(message);
                }
            }
        })
    });

    $('.promotion_type').on('click', function () {
        var div_id = $(this).val();
        $('.divs').hide();
        $(div_id).show();
    });
    var body_class = $('.skin-green');
    body_class.on('submit', '.alert-form', function (e) {
        e.preventDefault();
        var form_data = $(this).serialize();
        $.ajax({
            url: base_url + 'admin/booking/booking_alert',
            data: form_data,
            type: 'post',
            success: function (res) {

            }
        })
    });
    body_class.on('submit', '.vehicle-form', function (e) {
        e.preventDefault();
        var form_data = $(this).serialize();
        $.ajax({
            url: base_url + 'admin/booking/booking_vehicle',
            data: form_data,
            type: 'post',
            beforeSend: function () {

            },
            success: function (res) {
                $('.vehicle-form').prepend(res);
            }
        })
    });
    $('.agent-status-change').on('change', function () {
        var status = $(this).val();
        var agent_id = $(this).data('agent');
        $.ajax({
            url: base_url + 'admin/agents/status',
            data: 'status=' + status + '&agent_id=' + agent_id,
            type: 'post',
            success: function (res) {
                $('.box-body').prepend(res);
            }
        })
    })
});

function save_booking_status() {
    var booking_status = $('#booking_status').val();
    var booking_id = $('#booking_id').val();
    $.ajax({
        url: base_url + 'admin/booking/booking_status',
        data: 'booking_status=' + booking_status + '&booking_id=' + booking_id,
        type: 'post',
        beforeSend: function () {

        },
        success: function (res) {
            $('.change-status').prepend(res);
        }
    })
}

$('.promotion-menu').on('click', function (e) {
    var data = $(this).val();
    $.ajax({
        url: base_url + 'admin/deals/menu',
        data: 'data=' + data,
        type: 'post',
        success: function (res) {
            $("html, body").animate({scrollTop: 0}, "slow");
            $('.box-body').prepend(res);
        }
    })
});

$('body').on('click', '#submitSocialData', function (e) {
    e.preventDefault();
    var that = $(this),
        form = that.parents('form'),
        url = form.attr('action'),
        data = form.serialize();

    $.ajax({
        url: url,
        data: data,
        type: 'post',
        success: function (res) {
            console.log(res);
        }
    })
});

(function () {
    $('body').on('change', '#selectData', function (e) {
        var form = $(this).parents('form'),
            url = form.attr('action'),
            moduleId = $('#moduleId').val();
        $.ajax({
            url: url + '/getSocialData',
            data: 'dataId=' + $(this).val() + '&moduleId=' + moduleId,
            type: 'post',
            dataType: 'json',
            success: function (res) {
                if (res) {
                    $('#facebook_title').val(res.Facebook.title);
                    $('#facebook_link').val(res.Facebook.link);
                    $('#facebook_image').val(res.Facebook.image);
                    $('#facebook_description').val(res.Facebook.description);

                    $('#twitter_title').val(res.Twitter.title);
                    $('#twitter_link').val(res.Twitter.link);
                    $('#twitter_image').val(res.Twitter.image);
                    $('#twitter_description').val(res.Twitter.description);
                } else {
                    $('#facebook_title').val('');
                    $('#facebook_link').val('');
                    $('#facebook_image').val('');
                    $('#facebook_description').val('');

                    $('#twitter_title').val('');
                    $('#twitter_link').val('');
                    $('#twitter_image').val('');
                    $('#twitter_description').val('');
                }
            }
        });
    });

    $('[data-toggle="ajaxModal"]').on('click',
        function (e) {
            $('#ajaxModal').remove();
            e.preventDefault();
            var $this = $(this)
                , $remote = $this.data('remote') || $this.attr('href')
                , $modal = $('<div class="modal" id="ajaxModal"><div class="modal-body"></div></div>');
            $('body').append($modal);

            $modal.modal({backdrop: 'static', keyboard: false});
            $modal.load($remote, function () {
                // sortable
                $("#sortable-data").sortable({
                    update: function (event, ui) {
                        var data = $(this).sortable('serialize');
                        // POST to server using $.post or $.ajax
                        $.ajax({
                            data: data,
                            type: 'POST',
                            url: admin_base_url + '/' + admin_module + '/sort',
                            success: function () {
                                $('#msg').remove();
                                $("#sortable-data").prepend('<span id="msg"></span>');
                                $('#msg').html('Order Updated')
                            }
                        });
                    }
                }).disableSelection();
            });
        }
    );

    $('.send-email').on('click',
        function (e) {
            var $this = $(this);

            $('#message-modal').remove();
            e.preventDefault();
            var $remote = $this.attr('href')
                , $modal = $('<div class="modal" id="message-modal"><div class="modal-body"></div></div>');
            $('body').append($modal);
            $modal.modal({backdrop: 'static', keyboard: false});
            $modal.load($remote);
        }
    );

    $('.status').on('change', function () {
        var that = $(this);
        var baseUrl = $('#base-url').val();
        var data = $('#data').val();
        var page = $('#page').val();
        var backendfolder = $('#backendfolder').val();
        var status = $(this).val();
        var id = that.parents('td').find('.id').val();
        window.location = baseUrl + backendfolder + '/' + data + '/status/' + page + '/' + status + '/' + id;
    })
    $('.post-data-status').on('change', function () {
        var that = $(this);
        var adminBaseUrl = $('#admin-base-url').val();
        var page = $('.post-page').val(); 
        var status = that.val();
        var id = that.parents('td').find('.post-id').val();
        window.location = adminBaseUrl +  '/post_data/status/'+page+'/' + status + '/' + id;
    })
    $('.emailTemplateCategory').on('change', function () {
        var that = $(this);
        var baseUrl = $('#base-url').val();
        var val = that.val();
        window.location = baseUrl + 'nvp-system/emailTemplate/' + val;
    })
    $('.volunteer').on('change', function () {
        var that = $(this);
        var val = that.val();
        var baseUrl = $('#base-url').val();
        var name = $('option:selected', that).text();
        var message = '<tr><td><div class="form-group">' +
            '<input type="hidden" value="' + val + '" name="volunteerId[]"/>' +
            '<input type="text" value="' + name + '" class="form-control" disabled/>' +
            '</div></td><td><div class="form-group">' +
            '<input type="text" value="" name="hours[]" class="form-control" placeholder="Hours"/>' +
            '</div></td>' +
            '<td><a href="' + baseUrl + 'nvp-system/agency_project_data/volunteer_delete/" class="delete-volunteer-new">Delete</a>' +
            ' </td></tr>';
        $('.add-volunteers').append(message);


    })
    $('.delete-volunteer').on('click', function (e) {
        e.preventDefault();
        var that = $(this);
        var url = $(this).attr('href');
        $.ajax({
            url: url,
            type: 'post',
            success: function (res) {
                $('.message-holder').toggle();
                $('.message').html(res);
                that.parents('tr').remove();
            }
        })
    });

    $('.close-volunteer').on('click', function (e) {
        $('.message-holder').toggle();
    })

})();

$(document).ready(function () {
    $('body').on('click', '.delete-volunteer-new', function (e) {
        e.preventDefault();
        var that = $(this);
        $('.message-holder').toggle();
        $('.message').html('Volunter Removed');
        that.parents('tr').remove();
    });

    $('.agency-project-status').on('change', function () {
        var that = $(this);
        var baseUrl = $('#base-url').val();
        var backendfolder = $('#backendfolder').val();
        var status = that.val();
        var id = that.parents('td').find('.project-id').val();
        window.location = baseUrl + backendfolder + '/agency_project_data/status/' + status + '/' + id;
    })
});