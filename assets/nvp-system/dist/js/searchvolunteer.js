var base_url = $('#base-url').val();
$('#respondDate').datepicker({
    minDate: 0
});

$('#volunteer-search').on('click', function () {
    // Check/uncheck checkboxes for all rows in the table
    $('input[type="checkbox"]', $(this).rows).prop('checked', this.checked);
});
$('#volunteer-search-table tbody').on('change', 'input[type="checkbox"]', function () {
    // If checkbox is not checked
    if (!this.checked) {
        var el = $('#volunteer-search').get(0);
        // If "Select all" control is checked and has 'indeterminate' property
        if (el && el.checked && ('indeterminate' in el)) {
            // Set visual state of "Select all" control
            // as 'indeterminate'
            el.indeterminate = true;
        }
    }
});

$("#sendBulkNotification").click(function () {
    var checked = parseInt($(".select-user:checked").length);
    if (checked) {
        var volunteers = [];
        $(".select-user:checked").each(function (k, v) {
            volunteers.push(v.value);
        });
        volunteers.join();
        $('#userList').val(volunteers);
        $('#sendNotificationModal').modal('show');
    } else {
        alert('Select some volunteers first.');
    }
});

$('.singleUser').on('click', function (event) {
    event.preventDefault();
    $('#userList').val($(this).data('userid'));
    $('#sendNotificationModal').modal('show');
});