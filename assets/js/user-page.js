(function () {
    $(document).ready(function () {
        $('.projects-list-table').DataTable({
            info: false,
            bFilter: false,
            bLengthChange: false,
            fnDrawCallback: function(){
                if($(".projects-list-table").find("tr:not(.ui-widget-header)").length<=10){
                    $('div.dataTables_paginate')[0].style.display = "none";
                } else {
                    $('div.dataTables_paginate')[0].style.display = "block";
                }
            }
        });
        // location
        var current = navigator.geolocation;
        if (current) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var location = position.coords.latitude + ',' + position.coords.longitude;
                $('#feedFromLocation').val(location);
                window.localStorage.setItem('location', location);
            });
        } else {
            console.log("Geolocation is not supported by this browser.");
        }
        console.log(current);

        // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
        $('.modal-trigger').leanModal({
            dismissible: false
        });
    });

    $('.modal-close').on('click', function (e) {
        e.preventDefault();
        $('#modal1').closeModal();
        $('.message-holder').hide();
    });

    //opening comment
    $('.btn-comment').on('click', function (e) {
        e.preventDefault();
        $('.feed-comments-wrap').addClass('comments-hide');
        var that = $(this);
        console.log(that);
        var commentBody = that.data('commentbody');
        $(commentBody).removeClass('comments-hide');
    });

    //closing comment
    $('.btn-close-comment').on('click', function (e) {
        $(this).parent('.feed-comments-wrap').slimscroll({destroy: true});
        e.preventDefault();
        $(this).parent('.feed-comments-wrap').addClass('comments-hide');
    });

    //open Write Post
    var windowWidth = $(window).width();
    $('#post-trigger').on('click', function (e) {
        e.preventDefault();
        $('.btn-write-close').css('display', 'block');
        if (windowWidth >= 640) {
            $('.show-write-post-form').animate({
                    height: '190px'
                },
                300, function () {
                });
        } else {
            $('.show-write-post-form').animate({
                    height: '330px'
                },
                300, function () {
                });
        }

    });

    //close write post form
    $('.btn-write-close').on('click', function (e) {
        e.preventDefault();
        $(this).hide();
        $('.show-write-post-form').animate({
                height: '0'
            },
            300, function () {
            });
    });

    $('body').on('click', '#save-post', function (e) { 
        // location
        var current = navigator.geolocation;
        if (current) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var location = position.coords.latitude + ',' + position.coords.longitude;
                $('#feedFromLocation').val(location);
                window.localStorage.setItem('location', location);
            });
        } else {
            console.log("Geolocation is not supported by this browser.");
        }
        e.preventDefault();

        var that = $(this),
            form = that.parents('form'),
            url = form.attr('action');
        var forms = form[0];
        var data = new FormData(forms);
        form.validate({
            rules: {
                name: {
                    required: true,
                    maxlength: 255
                },
                image: {
                    required: true,
                    accept: "image/*"
                },
                description: {
                    required: true
                }
            }
        });
        if(form.valid()) {
            $('.message-holder').show();
            $('.message').html('Your post is uploading.');
            $.ajax({
                url: url,
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                dataType: 'JSON',
                beforeSend: function() {
                    console.log($('#save-post'));
                    $('#post-save-loader').show();
                    $('#save-post').attr('disabled', 'disabled');
                },
                success: function (res) {
                    $('#post-save-loader').hide();
                    $('#save-post').removeAttr('disabled');
                    forms.reset();
                    $('.message-holder').show();
                    $('.message').html(res.message);
                }
            })
        }
    });

    var evenDivs = $("div.row.even");
    var oddDivs = $("div.row.odd");
    for (var i = 0; i < evenDivs.length; i += 3) {
        evenDivs.slice(i, i + 3).wrapAll("<div class='col s12 m6 l6'></div>");
    }
    for (i = 0; i < oddDivs.length; i += 3) {
        oddDivs.slice(i, i + 3).wrapAll("<div class='col s12 m6 l6'></div>");
    }
})();

