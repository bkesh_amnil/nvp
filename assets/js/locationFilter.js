(function() {
    'use strict';

    var baseUrl = $('#base-url').val();
    var body = $('body');
    var zone = $('#selectZone');
    var district = $('#selectDistrict');
    var municipality = $('#selectMunicipality');

    zone.on('change', function() {
        var zoneId = zone.val();

        // filter district
        $.ajax({
            url: baseUrl + 'v1/api/filterDistrict',
            data: {
                zoneId: zoneId
            },
            dataType: 'json',
            type: 'post',
            success: function(filteredOptions) {
                if(filteredOptions.status = 'success') {
                    district.html(filteredOptions.data);
                    district.material_select();
                    $("#selectDistrict").prop('required',true);
                }
            }
        });
    });

    district.on('change', function() {
        var districtId = district.val();

        // filter municipality
        $.ajax({
            url: baseUrl + 'v1/api/filterMunicipality',
            data: {
                districtId: districtId
            },
            dataType: 'json',
            type: 'post',
            success: function(filteredOptions) {
                if(filteredOptions.status = 'success') {
                    municipality.html(filteredOptions.data);
                    municipality.material_select();
                    $("#selectMunicipality").prop('required',true);
                }
            }
        });
    });
})();