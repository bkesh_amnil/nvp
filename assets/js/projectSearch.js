(function() {
    'use strict';
    var base_url = $('#base-url').val();

    nvpApp.controller('searchProjectCtrl', function($scope, myConfig, httpService) {
        $scope.sumUp = function() {
            $scope.totalVolunteers = $scope.maleVolunteers + $scope.femaleVolunteers + $scope.otherVolunteers;
        };

        $scope.maleVolunteers = $scope.femaleVolunteers = $scope.otherVolunteers = 0;
        $scope.sumUp();

        $scope.resetForm = function() {
            return {
                agencyProject: '',
                zoneId: '',
                districtId: '',
                municipalityId: '',
                genderId: '',
                ageGroup: '',
                skillId: '',
                academicQualificationId: '',
                languageId: '',
                trainingId: ''
            }
        };
        $scope.searchForm = $scope.resetForm();

        $scope.searchVolunteer = function() {
            $('html, body').animate({scrollTop : 0},800);

            $scope.maleVolunteers = $scope.femaleVolunteers = $scope.otherVolunteers = 0;
            $scope.sumUp();

            var innerPageLoader = $('#innerPageLoader');
            innerPageLoader.show();
            var url = myConfig.apiPath + 'searchVolunteer';
            var searchResult = httpService.httpRequest({
                method: 'post',
                data: $scope.searchForm,
                url: url
            });

            searchResult.then(function(data) {
                innerPageLoader.hide();
                if(data.status == 'success') {
                    angular.forEach(data.data, function(value, key) {
                        if(value.genderName == 'Male') {
                            $scope.maleVolunteers = parseInt(value.genderGroupCount);
                        } else if(value.genderName == 'Female') {
                            $scope.femaleVolunteers = parseInt(value.genderGroupCount);
                        } else if(value.genderName == 'Others') {
                            $scope.otherVolunteers = parseInt(value.genderGroupCount);
                        }
                    });
                    $scope.sumUp();
                }
            });

            searchResult.catch(function(err) {
                innerPageLoader.hide();
                console.log('api call error', err);
            });
        };
    });

    $('#send-request').on('click', function(e) {
        e.preventDefault();
        var that = $(this),
            agencyId = that.attr('data-agency'),
            projectId = $('#projectId').val();

        if(projectId == '') {
            alert('Please select a project first');
        } else if($('#total-volunteers').val() == '' || $('#total-volunteers').val() == '0') {
            alert('There are no volunteers matching your requirements');
        } else {
            $.ajax({
                url: base_url + 'v1/api/sendRequest',
                data: {
                    agencyId: agencyId,
                    projectId: projectId
                },
                dataType: 'json',
                type: 'post',
                success: function(response) {
                    alert(response.data);
                }
            })
        }
    })
})();