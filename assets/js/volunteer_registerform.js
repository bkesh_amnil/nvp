(function () {
    'use strict';
    jQuery.validator.addMethod("lettersonly", function (value, element) {
        return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "Letters and spaces only please");

    jQuery.validator.addMethod("notEqual", function (value, element, param) {
        return this.optional(element) || value != $(param).val();
    }, "Please specify a different value");
    var baseUrl = $('#base-url').val();
    var data = {};

    $(document).ready(function () {
        $('#volunteer_register').validate({
            onkeyup: false,
            rules: {
                fullName: {
                    required: true,
                    lettersonly: true
                },
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: baseUrl + 'members/register/check',
                        type: "post",
                        data: data,
                    }
                },
                password: {
                    required: true,
                    minlength: 8
                },
                phoneNumberMobile: {
                    required: true,
                    number: true,
                    remote: {
                        url: baseUrl + 'members/register/check',
                        type: "post",
                        data: data,
                    }
                },
                genderId: "required",
                dateOfBirth: "required",
                repassword: {
                    required: true,
                    minlength: 8,
                    equalTo: "#password"
                },
                
            },
            messages: {
                email:{
                    remote: jQuery.validator.format("{0} is already in use")
                },
                fullName: {
                    required: "This field is required",
                    lettersonly: "Letters and spaces only please"
                },
                phoneNumberMobile: {
                    required: "This field is required",
                    number: "Only Numbers allowed",
                    remote: jQuery.validator.format("{0} is already in use")
                },
                repassword: {
                    equalTo: "Password did not match"
                },
                
            }
        });

        
    });
})();