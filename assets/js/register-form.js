(function () {
    'use strict';
    jQuery.validator.addMethod("lettersonly", function (value, element) {
        return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "Letters and spaces only please");

    jQuery.validator.addMethod("notEqual", function (value, element, param) {
        return this.optional(element) || value != $(param).val();
    }, "Please specify a different value");
    var baseUrl = $('#base-url').val();
    var data = {};

    $(document).ready(function () {
        $('#register-form').validate({
            onkeyup: false,
            rules: {
                fullName: {
                    required: true,
                    lettersonly: true
                },
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: baseUrl + 'members/register/check',
                        type: "post",
                        data: data,
                    }
                },
                password: {
                    required: true,
                    minlength: 8
                },
                phoneNumberMobile: {
                    required: true,
                    number: true,
                    remote: {
                        url: baseUrl + 'members/register/check',
                        type: "post",
                        data: data,
                    }
                },
                genderId: "required",
                dateOfBirth: "required",
                repassword: {
                    required: true,
                    minlength: 8,
                    equalTo: "#password"
                },
                agree: "required"
            },
            messages: {
                email:{
                    remote: jQuery.validator.format("{0} is already in use")
                },
                fullName: {
                    required: "This field is required",
                    lettersonly: "Letters and spaces only please"
                },
                phoneNumberMobile: {
                    required: "This field is required",
                    number: "Only Numbers allowed",
                    remote: jQuery.validator.format("{0} is already in use")
                },
                repassword: {
                    equalTo: "Password did not match"
                },
                agree: "You must agree to NVP's Terms of Service."
            }
        });

        $('#agency-register-form').validate({
            onkeyup: false,
            rules: {
                name: {
                    required: true,
                    lettersonly: true
                },
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: baseUrl + 'members/register/check',
                        type: "post",
                        data: data,
                    }
                },
                password: {
                    required: true,
                    minlength: 8
                },
                phoneNumber: {
                    required: true,
                    number: true,
                    remote: {
                        url: baseUrl + 'members/register/check',
                        type: "post",
                        data: data,
                    }
                },
                address: "required",
                dateOfRegistration: "required",
                dateOfBirth: "required",
                primaryContactPersonFullName: "required",
                primaryContactPersonPersonalPhoneNumber: {
                    notEqual: '#agencyphnumber',
                    required: true
                },
                primaryContactPersonEmail: {
                    notEqual: '#email',
                    required: true,
                    email: true
                }
                ,
                repassword: {
                    required: true,
                    minlength: 8,
                    equalTo: "#agency-password"
                }
                ,
                agency_agree: "required"
            },
            messages: {
                email:{
                    remote: jQuery.validator.format("{0} is already in use")
                },
                name: {
                    required: "This field is required",
                    lettersonly: "Letters and spaces only please"
                }
                ,
                phoneNumber: {
                    required: "This field is required",
                    number: "Only Numbers allowed",
                    remote: jQuery.validator.format("{0} is already in use")
                }
                ,
                agency_agree: "You must agree to NVP's Terms of Service.",
                primaryContactPersonPersonalPhoneNumber: {
                    notEqual: "Primary contact number can't be same as agency phone number"
                }
            }
        })
        ;
    });
})();