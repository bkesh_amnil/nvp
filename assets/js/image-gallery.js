jQuery(function($) {
    $(document).ready(function () {
        $('.photo-gallery-thumb').fancybox({
            helpers : {
                thumbs : {
                    width  : 50,
                    height : 50
                }
            }
        });

        $('.image-gallary-detail').on('click',function(){

            $('body,html').animate({
                    scrollTop: 70 ,
                }, 1000
            );
        })

    });
});
