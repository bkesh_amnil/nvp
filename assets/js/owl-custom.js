//Main Banner
$('#mainBanner').owlCarousel({
    autoplay: true,
    autoplayTimeout: 5000,
    items: 1,
    loop: true,
    dots: true,
    nav: true,
    animateIn: 'fadeIn',
    animateOut: 'fadeOut',
    smartSpeed: 500
});

//People comments
$('#comments').owlCarousel({
    autoplay: false,
    autoplayTimeout: 5000,
    loop: true,
    dots: true,
    nav: false,
    smartSpeed: 500,
    responsive: {
        0: {
            items: 1
        },

        1024: {
            items: 2
        }
    }
});

//About
$('#about-banner').owlCarousel({
//    autoplay: true,
    autoplayTimeout: 5000,
    items: 1,
    loop: false,
    dots: true,
    nav: false,
    smartSpeed: 500
});

//Our Partners
$('.carousel').owlCarousel({
//    autoplay: true,
    autoplayTimeout: 5000,
    items:5,
    loop: true,
    margin: 25,
    dots: false,
    nav: true,
    smartSpeed: 500,
    responsive: {
        0: {
            items: 2
        },

        768: {
            items: 4
        },

        1024: {
            items: 5
        }
    }
});
$('.no-carousel').owlCarousel({
//    autoplay: true,
    autoplayTimeout: 5000,
    items:5,
    loop: true,
    margin: 25,
    dots: false,
    nav: false,
    smartSpeed: 500,
    responsive: {
        0: {
            items: 2
        },

        768: {
            items: 4
        },

        1024: {
            items: 5
        }
    }
});
