(function() {
    'use strict';
    $(document).ready(function () {
        $('.content-gallery-thumb').fancybox({
            helpers : {
                thumbs : {
                    width  : 50,
                    height : 50
                }
            }
        });
    });
})();