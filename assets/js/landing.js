/* added by Satish */
(function() {
    'use strict';

    $(document).ready(function() {
        var counterUp = function(targetElement, start, end) {
            var counter = new CountUp(targetElement, start, end, 0, 1);
            counter.start();
        };

        $('.Services').waypoint(function() {
            var counterElement = $('.impact-counters');
            counterElement.each(function(key, value) {
                var endValue = $(value).data('end-value');
                counterUp(value, 0, endValue);
            });
        }, {
            offset: '50%'
        });
    });
})();