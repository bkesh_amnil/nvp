//Dropdown Option
//main view

jQuery(function ($) {
    // loader toggle
    /*$(document).ready(function () {
        setTimeout(function () {
            $('body').removeClass('has-loader');
            $('#page-loader').fadeOut('slow');
        }, 700);
    });*/

    $(".main-dropdown").dropdown({
        hover: true,
        belowOrigin: true
    });

    $(".mobile-drop").dropdown({
        hover: false,
        belowOrigin: true
    });

//Tabs
    $(document).ready(function () {
        $('ul.tabs').tabs();
    });

//dropdown
//user option logout
    $('.btn-user-option').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrain_width: true,
        hover: false,
        gutter: 15,
        belowOrigin: true,
        alignment: 'left'
    });

//notification
    $('.btn-notification').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrain_width: false,
        hover: false,
        gutter: -15,
        belowOrigin: true,
        alignment: 'left'
    });

//Select
    $(document).ready(function () {
        $('select').material_select();
    });

    $('.close').on("click", function () {
        $(this).parent('div').hide();
    });

//Date picker
    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 75, // Creates a dropdown of 15 years to control year
        clear: false
    });

//parallax
    $(document).ready(function () {
        $('.parallax').parallax();
    });

    var rtime;
    var timeout = false;
    var delta = 200;
    $(window).resize(function () {
        rtime = new Date();
        if (timeout === false) {
            timeout = true;
            setTimeout(resizeend, delta);
        }
    });

    function resizeend() {
        if (new Date() - rtime < delta) {
            setTimeout(resizeend, delta);
        } else {
            timeout = false;
//        renderHeight();
        }
    }


//Scroll top
    $(document).ready(function ($) {
        // browser window scroll (in pixels) after which the "back to top" link is shown
        var offset = 300,
        //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
            offset_opacity = 1200,
        //duration of the top scrolling animation (in ms)
            scroll_top_duration = 700,
        //grab the "back to top" link
            $back_to_top = $('.cd-top');

//hide or show the "back to top" link
        $(window).scroll(function () {
            ( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
            if ($(this).scrollTop() > offset_opacity) {
                $back_to_top.addClass('cd-fade-out');
            }
        });

//smooth scroll to top
        $back_to_top.on('click', function (event) {
            event.preventDefault();
            $('body,html').animate({
                    scrollTop: 0,
                }, scroll_top_duration
            );
        });

    });


//sign up email check
  /*  $(document).ready(function () {*/
        /*$("#pemail").blur(function () {
            var that = $(this);
            var email = that.val();
            var baseUrl = $('#base-url').val();
            $.ajax({
                url: baseUrl + 'members/register/check_email',
                data: 'table=volunteer&email=' + email,
                type: 'post',
                dataType: 'json',
                success: function (res) {
                    if (res == 'true') {
                        if (that.parents('#volunteer-email').find('#email-error') !== "null"){
                            var message = 'Email address already used';
                            $('#email-error').html(message);
                            $('#email-error').show();
                        } else {
                            var message = '<span id="email-exist" class="error">Email address already used</span>';
                            $('#email-exist').remove();
                            that.parents('#volunteer-email').append(message);
                        }
                    }
                }
            })
        });
        $("#agency-email").blur(function () {
            var that = $(this);
            var email = that.val();
            var baseUrl = $('#base-url').val();
            $.ajax({
                url: baseUrl + 'members/register/check_email',
                data: 'table=agency&email=' + email,
                type: 'post',
                dataType: 'json',
                success: function (res) {
                    if (res == 'true') {
                        if (that.parents('#agency-email-error').find('#agency-email-error')) {
                            var message = 'Email address already used';
                            $('#agency-email-error').html(message);
                            $('#agency-email-error').show;
                        } else {
                            var message = '<span id="email-exist" class="error" for="agency-email">Email address already used</span>';
                            $('#email-exist').remove();
                            that.parents('#agency-sign-email').append(message);
                        }
                    }
                }
            })
        });
        $("#phnumber").blur(function () {
            var that = $(this);
            var phnumber = that.val();
            var baseUrl = $('#base-url').val();
            $.ajax({
                url: baseUrl + 'members/register/check_email',
                data: 'table=volunteer&phnumber=' + phnumber,
                type: 'post',
                dataType: 'json',
                success: function (res) {
                    if (res == 'true') {
                        var message = '<span id="ph-exist" class="error">Phone number already in use</span>';
                        $('#ph-exist').remove();
                        that.parents('#number').append(message);
                    } else {
                        $('#ph-exist').remove();
                    }
                }
            })
        });
        $("#agencyphnumber").blur(function () {
            var that = $(this);
            var phnumber = that.val();
            var baseUrl = $('#base-url').val();
            $.ajax({
                url: baseUrl + 'members/register/check_email',
                data: 'table=volunteer&phnumber=' + phnumber,
                type: 'post',
                dataType: 'json',
                success: function (res) {
                    if (res == 'true') {
                        var message = '<span id="agency-exist" class="error">Phone number already in use</span>';
                        $('#agency-exist').remove();
                        that.parents('#agencynumber').append(message);
                    } else {
                        $('#agency-exist').remove();
                    }
                }
            })
        });
    });
    $('#register-form').submit(function (e) {
        var that = $(this);
        var email = $('#email');
        var ph = $('#phnumber');
        var phnumber = $('#phnumber').val();
        var valid = email.data('valid');
        var valid1 = ph.data('valid');
        var baseUrl = $('#base-url').val();
        if (valid === 0 && valid1 === 0) { // Is valid, let's continue
            return;
        } else {
            e.preventDefault(); //prevent default form submit
            $.ajax({
                url: baseUrl + 'members/register/check',
                data: 'table=volunteer&email=' + email.val() + '&phnumber=' + phnumber,
                type: 'post',
                dataType: 'json',
                success: function (res) {
                    console.log(res)
                    if (res.status == 'true') {
                        var message = '<span id="exist" class="error">' + res.msg + '</span>';
                        if (res.type == 'email') {
                            $('#volunteer-email').append(message);
                        } else {
                            $('#number').append(message);
                        }

                    } else {
                        email.data('valid', 0);
                        ph.data('valid', 0);
                        that.submit();
                    }
                }
            });
        }
    });
    $('#agency-register-form').submit(function (e) {
        var that = $(this);
        var email = $('#agency-email');
        var ph = $('#agency-email');
        var phnumber = $('#agencyphnumber').val();
        var valid = email.data('valid');
        var valid1 = ph.data('valid');
        var baseUrl = $('#base-url').val();
        if (valid === 0 && valid1 === 0) { // Is valid, let's continue
            return;
        } else {
            e.preventDefault(); //prevent default form submit
            $.ajax({
                url: baseUrl + 'members/register/check',
                data: 'table=agency&email=' + email.val() + '&phnumber=' + phnumber,
                type: 'post',
                dataType: 'json',
                success: function (res) {
                    console.log(res);
                    if (res.status == 'true') {
                        var message = '<span id="exist" class="error">' + res.msg + '</span>';
                        if (res.type == 'email') {
                            $('#agency-sign-email').append(message);
                        } else {
                            $('#agencynumber').append(message);
                        }

                    } else {
                        email.data('valid', 0);
                        ph.data('valid', 0);
                        that.submit();
                    }
                }
            });
        }
    });*/

    /*$("#pnumber").blur(function () {
        var that = $(this);
        var phnumber = that.val();
        var id = $('#volunteer-id').val();
        var baseUrl = $('#base-url').val();
        $.ajax({
            url: baseUrl + 'members/profile/checkPhnumber',
            data: 'id=' + id + '&table=nvp_volunteer&phnumber=' + phnumber,
            type: 'post',
            dataType: 'json',
            success: function (res) {
                if (res == 'true') {
                    var message = '<span id="ph-exist" class="error">Phone number already in use</span>';
                    $('#ph-exist').remove();
                    that.parents('#vounteernumber').append(message);
                } else {
                    $('#ph-exist').remove();
                }
            }
        })
    });
    $("#phoneNumber").blur(function () {
        var that = $(this);
        var phnumber = that.val();
        var id = $('#agencyId').val();
        var baseUrl = $('#base-url').val();
        $.ajax({
            url: baseUrl + 'members/profile/checkPhnumber',
            data: 'id=' + id + '&table=nvp_agency&phnumber=' + phnumber,
            type: 'post',
            dataType: 'json',
            success: function (res) {
                console.log(res);
                if (res == 'true') {
                    var message = '<span id="ph-exist" class="error">Phone number already in use</span>';
                    $('#ph-exist').remove();
                    that.parents('#agencynumber').append(message);
                } else {
                    $('#ph-exist').remove();
                }
            }
        })
    });*/

});


        