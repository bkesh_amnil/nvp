jQuery(function ($) {
    jQuery.validator.addMethod("lettersonly", function (value, element) {
        return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "Letters and spaces only please");

    jQuery.validator.addMethod("notEqual", function(value, element, param) {
        return this.optional(element) || value != $(param).val();
    }, "Please specify a different value");

    jQuery.validator.setDefaults({
        ignore: [],
        errorElement: 'span',
        errorPlacement: function(error, element) {
            if (element.attr("name") == "agree" ) {
                error.insertAfter("#agree+label");
            } else if( element.attr("name") == "agency_agree" ) {
                error.insertAfter("#agency-agree+label");
            } else {
                error.insertAfter(element);
            }
        }

    });
});