(function() {
    'use strict';

    var base_url = $('#base-url').val();
    $(document).ready(function() {
        $('#change-password-form').validate({
            rules: {
                newPassword: {
                    required: true,
                    minlength: 8
                },
                confirmNewPassword: {
                    required: true,
                    minlength: 8,
                    equalTo: "#newPassword"
                },
                oldPassword: {
                    required: true,
                    minlength: 8
                }
            }
        });

        $.ajax({
            url: base_url + 'v1/api/getCurrentUserId',
            success: function(userId) {
                $.ajax({
                    url: base_url + 'v1/api/checkProfileStatus/' + userId,
                    dataType: 'json',
                    success: function(data) {
                        if(data.data.completion < 90) {
                            $('#progressMeter').css('width', data.data.completion);
                            $('#progressMeterText').html('Profile not complete! Please complete it.');
                        } else {
                            $('#progressMeter').css('width', '100');
                            $('#progressMeterText').html('Bravo! Your profile is 100% complete.');
                        }
                        initProgressBar();
                        $('#progressMeter').parents('.meter').show();
                    }
                });
            }
        });
    });

    var initProgressBar = function() {
        //progressbar
        $(".meter > span").each(function () {
            $(this)
                .data("origWidth", $(this).width())
                .width(0)
                .animate({
                    width: $(this).data("origWidth") + "%"
                }, 1200);
        });
    };

    $('.volunteer-type').on('change', function(e) {
        var that = $(this);
        var volunteerType = that.val();
        var volunteerSubType = $('.volunteer-subtype');

        if(volunteerType == 'fullTime') {
            volunteerSubType.removeClass('show-select');
            volunteerSubType.addClass('hide-select');
        } else if(volunteerType == 'partTime') {
            volunteerSubType.removeClass('hide-select');
            volunteerSubType.addClass('show-select');
        }
    });

    $('.agency-type').on('change', function(e) {
        var that = $(this);
        var agencyType = that.val();
        var agencyName = $('.agency-name');

        if(agencyType != '0') {
            agencyName.removeClass('show-select');
            agencyName.addClass('hide-select');
        } else if(agencyType == '0') {
            agencyName.removeClass('hide-select');
            agencyName.addClass('show-select');
        }
    });


    // crop profile picture
    var cropperOptions = {
        customUploadButtonId:'cropPP',
        uploadUrl: base_url + 'members/profile/uploadProfilePic',
        cropUrl: base_url + 'members/profile/cropProfilePic',
        modal: true,
        onAfterImgCrop: function() {
            window.location.reload(true);
        },
        loaderHtml:'<span style="color: rgb(0, 0, 0);">loading...</span>'
    };
    var cropperHeader = new Croppic('croppic', cropperOptions);
})();
