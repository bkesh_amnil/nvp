//google map
function initialize() {
    var base_url = $('#base-url').val();
    var lat = $('#latitude').val();
    var long = $('#longitude').val();
    $.post(base_url + 'v1/api/getAddress').success(function (address) {
        address = JSON.parse(address);
        var myLatLng = new google.maps.LatLng(lat, long);
        var mapCanvas = document.getElementById('map-canvas');
        var mapOptions = {
            center: myLatLng,
            zoom: 17,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(mapCanvas, mapOptions);

        var infowindow = new google.maps.InfoWindow({
            content: address.data
        });

        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: 'Hello World!'
        });

        marker.addListener('click', function () {
            infowindow.open(map, marker);
        });
    });
}
google.maps.event.addDomListener(window, 'load', initialize);


$('#contact-form').submit(function (e) {
    e.preventDefault();
    var that = $(this),
        form = $('#contact-form'),
        url = form.attr('action'),
        data = form.serialize();
    if ($('#contact-form').valid()) {
        $('.processing').toggle();
        $.ajax({
            url: url,
            data: data,
            type: 'POST',
            success: function (res) {
                console.log(res);
                form[0].reset();
                $('.processing').toggle();
                $('.message-holder').toggle();
            }
        })
    }
});