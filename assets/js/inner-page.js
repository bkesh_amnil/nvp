jQuery(function($) {
   //reseting to resize
function resetCalculation() {
    $('.inner-header .main-nav').removeAttr('style');
    $('#about-banner').removeAttr('style');
}

function renderHeight(){
    $( window ).off('scroll');
    resetCalculation();
    var windowWidth = $(window).width();
    var topHeadContent = $('.top-head-content').height();
    if(windowWidth <= 768){
        return false;
    }    
    
    if(windowWidth > 1023) {   
        //fixing nav to top
        $( window ).scroll(function(){
            var scroll = $(window).scrollTop();
            if(scroll > topHeadContent) {
                $('.inner-header .main-nav').css({
                    position: 'fixed',
                    top: '0'
                });
                
                $('.inner-header .social-login').css({
                    transform: 'scale(0)',
                    '-webkit-transform': 'scale(0)',
                    'transform-origin': '80% 50% 0',
                    '-webki-transform-origin': '80% 50% 0',
                    transition: '600ms ease'
                });
            } else {
                $('.inner-header .main-nav').removeAttr('style');
                
                $('.inner-header .social-login').css({
                    transform: 'scale(1)',
                    '-webkit-transform': 'scale(1)'
                });
            }
        });
    }
    
     $('body').on('click', '.partners-slider a', function(e) {
         e.preventDefault();
         hideContentDiv();
         $(this).addClass('active');
         setTimeout(function() {
             showContentDiv();
         }, 400);
         
    });
    
    function showContentDiv() {
        $('.partner-contents').css({
            opacity: '1'
        });
    }
    
    function hideContentDiv() {
        $('.partners-slider a').removeClass('active');
        $('.partner-contents').css({
            opacity: '0'
        });
    }
}

renderHeight();
$(window).off("resize");
$(window).on("resize", function(){
    renderHeight();
}); 
});