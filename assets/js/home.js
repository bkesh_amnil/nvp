//reseting to resize
function resetCalculation() {
    $('#mainBanner').removeAttr('style');
    $('.main-nav').removeAttr('style');
    $('.main-nav ul li a').removeAttr('style');
    $('.main-nav .brand-logo').removeAttr('style');
    $('.main-nav .brand-logo span').removeAttr('style');
    $('.who-we').removeAttr('style');
    $('#mainBanner .owl-stage-outer').removeAttr('style');
}

function renderHeight() {
    var windowWidth = $(window).width();
    var windowHeight = $(window).height();
    resetCalculation();
    if (windowWidth <= 768) {
        return false;
    }
    //calculating heights
    var sliderHeight = $('#mainBanner').height(),
        navhHeight = $('.main-nav').height(),
        topHeadContent = $('.top-head-content').height();

    if (windowWidth > 767) {
        //opacity on banner on scroll
        var fadeStart = 100 // 100px scroll or less will equiv to 1 opacity
            , fadeUntil = 691 // 200px scroll or more will equiv to 0 opacity
            , fading = $('#mainBanner .owl-stage-outer');

        $(window).bind('scroll', function () {
            var offset = $(document).scrollTop()
                , opacity = 0
                ;
            if (offset <= fadeStart) {
                opacity = 1;
            } else if (offset <= fadeUntil) {
                opacity = 1 - offset / fadeUntil;
            }
            fading.css('opacity', opacity);
        });
    } else {
        $('#mainBanner .owl-stage-outer').removeAttr('style');
    }

    //managing slider to full height
    var totalHeight = 0,
        headerHeight = 0;
    if (windowWidth > 1024) {
        totalHeight = windowHeight - 110,
            headerHeight = windowHeight - topHeadContent;
    } else if (windowWidth === 1024) {
        totalHeight = $('#mainBanner').height(),
            headerHeight = totalHeight + 110 - 60;
    }
    $('#mainBanner').css('height', totalHeight);
    $('.main-nav').css('top', totalHeight);


    //fixing nav to top
    if (windowWidth > 1023) {
        $(window).scroll(function () {
            var scroll = $(window).scrollTop();
            if (scroll > sliderHeight) {
                $('.main-nav').css({
                    height: 'auto',
                    position: 'fixed',
                    top: '0',
                    left: '0',
                    right: '0',
                    'box-shadow': '0 0 6px 3px rgba(0, 0, 0, 0.2)'
                });

                $('.main-nav ul li a').css({
                    padding: '0 6px'
                });

                $('.main-nav .brand-logo').css({
                    top: -6,
                    transform: 'scale(0.8)',
                    '-webkit-transform': 'scale(0.8)'
                });

                $('.main-nav .brand-logo span').css({
                    height: '200px',
                    'box-shadow': '0 14px 7px -6px rgba(0, 0, 0, 0.2)'
                });
            } else {
                $('.main-nav').css({
                    height: 110,
                    position: 'absolute',
                    top: totalHeight,
                    'box-shadow': 'none'
                });

                $('.main-nav ul li a').css({
                    padding: '23px 6px'
                });

                $('.main-nav .brand-logo').css({
                    top: -56,
                    transform: 'scale(1)',
                    '-webkit-transform': 'scale(1)'
                });

                $('.main-nav .brand-logo span').css({
                    height: '180px',
                    'box-shadow': 'none'
                });
            }
        });
    }
    
    //nav on large screen
    if (windowWidth > 1200) {
        $(window).scroll(function () {
            var scroll = $(window).scrollTop();
            if (scroll > sliderHeight) {
                 $('.main-nav ul li a').css({
                    padding: '0 10px'
                });
            } else {
                $('.main-nav ul li a').css({
                    padding: '23px 10px'
                });
            }
        });
    }

    //pushing below contents
    $('.who-we').css('margin-top', headerHeight);

    $(document).ready(function () {
        // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
        $('.modal-trigger').leanModal({
            dismissible: false
        });
    });

    $('.modal-close').on('click', function (e) {
        e.preventDefault();
        $('#modal1').closeModal();
        $('.message-holder').hide();
    });

    $('.send_review').on('click', function (e) {
        e.preventDefault();
        var that = $(this),
            form = that.parents('form'),
            url = form.attr('action');
        var forms = $(".submit-review")[0];
        //data = form.serialize(),
        // data = new FormData(form[0]);
        var data = new FormData(forms);

        form.validate({
            rules: {
                name: {
                    required: true,
                    lettersonly: true
                },
                message: {
                    required: true,
                },
                image: "required",
            }
        });
        if (form.valid()) {
            $.ajax({
                url: url,
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                dataType: 'JSON',
                success: function (res) {
                    forms.reset();
                    $('.message-holder').show();
                    $('.message').html(res.message);

                }
            })
        }
    });
}

$(document).ready(function () {
    setTimeout(function() {
        renderHeight();
    }, 1000);

    $('#impact-carousel').owlCarousel({
        autoplay: true,
        autoplayTimeout: 5000,
        items: 4,
        loop: true,
        dots: false,
        responsive: {
            0: {
                items: 1,
                nav: false
            },

            768: {
                items: 4,
                nav: true
            },

            1024: {
                items: 4,
                nav: true
            }
        }
    });
});

$(window).off("resize");
$(window).on("resize", function () {
    $(window).off('scroll');
    renderHeight();
});

