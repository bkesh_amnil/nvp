(function() {
    'use strict';

    nvpApp.controller('feedCtrl', function($scope, myConfig, httpService) {
        $scope.userComment = '';
        $scope.comments = [];
        // like post
        $scope.likePost = function(postId, userId, userType) {
            var currentLikeCount = parseInt($('#likecount-' + postId).html());
            httpService.httpRequest({
                method: 'get',
                url: myConfig.apiPath + 'getIP'
            }).then(function(ipData) {
                if(ipData.status == 'success') {
                    var likeData = {
                        ip: ipData.data,
                        postId: postId,
                        userId: userId,
                        userType: userType
                    };
                    httpService.httpRequest({
                        method: 'post',
                        data: likeData,
                        url: myConfig.apiPath + 'likePost'
                    }).then(function(response) {
                        if(response.data.action == 'like') {
                            $('#likecount-' + postId).html(response.data.totalLikes);
                        } else {
                            $('#likecount-' + postId).html(response.data.totalLikes);
                        }
                    });
                }
            });
        };

        // get comments
        $scope.getComments = function(postId, getAll) {
            $('#comment-' + postId).slimScroll({destroy:true});
            $scope.comments = [];
            httpService.httpRequest({
                method: 'post',
                data: {
                    postId: postId,
                    getAll: getAll
                },
                url: myConfig.apiPath + 'getComments'
            }).then(function(comments) {
                if(comments.status == 'success') {
                    $scope.comments = comments.data;
                    if(typeof getAll != 'undefined') {
                        $('#comment-' + postId).slimScroll();
                    }
                } else {
                    $scope.comments = [];
                }
            });
        };

        // save comment
        var activeUsername = $('#activeUsername').val(),
            activeUserProfilePicture = $('#activeUserProfilePicture').val();
        $scope.saveComment = function(postId, userId, userType) {
            var currentCommentCount = parseInt($('#commentcount-' + postId).html());
            $('#commentcount-' + postId).html(currentCommentCount + 1);
            if(!$scope.comments.length) {
                $scope.comments.push({
                    comment: $scope.userComment,
                    fullName: activeUsername,
                    profilePicture: activeUserProfilePicture
                });
            } else {
                $scope.comments.unshift({
                    comment: $scope.userComment,
                    fullName: activeUsername,
                    profilePicture: activeUserProfilePicture
                });
            }
            httpService.httpRequest({
                method: 'get',
                url: myConfig.apiPath + 'getIP'
            }).then(function(ipData) {
                if(ipData.status == 'success') {
                    var commentData = {
                        userId: userId,
                        postId: postId,
                        comment: $scope.userComment,
                        ip: ipData.data,
                        userType: userType
                    };
                    $scope.userComment = '';

                    httpService.httpRequest({
                        method: 'post',
                        data: commentData,
                        url: myConfig.apiPath + 'saveComments'
                    }).then(function(data) {
                        if(data.status = 'success') {
                            //$scope.getComments(data.data);
                        }
                    });
                }
            });
        };

        // processing image
        $scope.imageProcess = function(path) {
            var imagePath = '';
            if(path != null && (path.indexOf('https') >= 0 || path.indexOf('http') >= 0)) {
                imagePath = path;
            } else if(path == null) {
                imagePath = myConfig.baseUrl + 'assets/img/icon/icon-user-default.png';
            } else {
                imagePath = myConfig.baseUrl + path;
            }
            return imagePath;
        };


        // pulling feeds detail
        $scope.getFeedDetail = function(newsSlug) {
            var url = myConfig.apiPath + 'getPostDetail/' + newsSlug;

            var innerPageLoader = $('#innerPageLoader');
            innerPageLoader.show();
            var feedDetail = httpService.httpRequest({
                method: 'post',
                data: $scope.searchForm,
                url: url
            });

            feedDetail.then(function(data) {
                innerPageLoader.hide();
                if(data.status == 'success') {
                    var newsData = data.data;
                    $scope.newsDetailTitle = newsData.name;
                    $scope.newsDetailBody = newsData.description;

                    var date = moment(newsData.created_on*1000);
                    $scope.newsDetailCreatedOn = date.format('Do MMMM, YYYY');
                    $scope.newsDetailCreatedTime = date.format('hh:mm a');

                    var url = myConfig.apiPath + 'getAddressFromLatLong';

                    var addressDetail = httpService.httpRequest({
                        method: 'post',
                        data: {
                            latLong: newsData.location
                        },
                        url: url
                    });

                    addressDetail.then(function(data) {
                        $scope.newsDetailLocation = data.data;
                            $('#feedDetail').openModal();
                    });
                }
            });
        };

        $scope.getAddress = function(latLong) {
            var url = myConfig.apiPath + 'getAddressFromLatLong';

            var addressDetail = httpService.httpRequest({
                method: 'post',
                data: {
                    latLong: latLong
                },
                url: url
            });

            addressDetail.then(function(data) {
                if(data.status == 'success') {
                    return data.data;
                }
            });
        }

        $scope.increaseShare = function(postId) {
            /*var currentShareCount = parseInt($('#sharecount-' + postId).html());
            $('#sharecount-' + postId).html(currentShareCount + 1);
            httpService.httpRequest({
                method: 'post',
                data: {postId: postId},
                url: myConfig.apiPath + 'increaseShare'
            });*/
        }
    });

    $('.btn-like').on('click', function() {
        $(this).toggleClass('active');
    });

    $('.feedDetails').on('click', function(e) {
        e.preventDefault();

        var that = $(this),
            url = that.attr('href');

        $.ajax({
            url: url,
            success: function(data) {
                console.log(data);
            }
        })
    });

    // add this share event
    addthis.addEventListener('addthis.menu.share', function(evt) {
        console.log(evt.data);
        /*var currentShareCount = parseInt($('#sharecount-' + postId).html());
        $('#sharecount-' + postId).html(currentShareCount + 1);
        httpService.httpRequest({
            method: 'post',
            data: {postId: postId},
            url: myConfig.apiPath + 'increaseShare'
        });*/
    });
})();