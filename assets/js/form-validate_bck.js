jQuery(function ($) {
    var baseUrl = $('#base-url').val();
    var id = $('#id').val();
    var table = $('#table').val();
    var number = $('#pnumber').val();
    var data = {"phnumber": number, "id": id, "table": table};

    $(document).ready(function () {
        $('#sign-in-form'). validate({
            rules: {
                username: "required",
                password: "required"
            }
        })
        $('#agency-profile-form').validate({
            onkeyup: false,
            rules: {
                name: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    minlength: 8
                },
                phoneNumber: {
                    required: true,
                    number: true,
                    remote: {
                        url: baseUrl + 'members/profile/checkPhnumber',
                        type: "post",
                        data: data,
                    }
                },
                agencyTypeId: "required",
                agencyTypeName: {
                    required: function () {
                        if ($("#agengyType option:selected").text() == "Others") {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
                ,
                registrationNumber: "required",
                address: "required",
                dateOfRegistration: "required",
                dateOfBirth: "required",
                primaryContactPersonFullName: "required",
                primaryContactPersonDesignationInAgency: "required",
                primaryContactPersonPersonalPhoneNumber: {
                    notEqual: '#phoneNumber',
                    required: true,
                    number: true
                }
                ,
                primaryContactPersonEmail: {
                    notEqual: '#email',
                    required: true,
                    email: true
                }
                ,
                secondaryContactPersonFullName: "required",
                secondaryContactPersonDesignationInAgency: "required",
                secondaryContactPersonPersonalPhoneNumber: {
                    notEqual: '#phoneNumber',
                    notEqual: '#primaryContactPersonEmail',
                    required: true,
                    number: true
                }
                ,
                secondaryContactPersonEmail: {
                    notEqual: '#email',
                    notEqual: '#primaryContactPersonEmail',
                    required: true,
                    email: true
                }
                ,
                repassword: {
                    required: true,
                    minlength: 8,
                    equalTo: "#agency-password"
                }
                ,
                registrationCertificateImage: {
                    required: function () {
                        if ($("#registrationCertificateImageCheck").val() == 1) {
                            return false;
                        }
                        else {
                            return true;
                        }
                    }
                    ,
                    accept: "image/*"
                }
                ,
                logo: {
                    required: function () {
                        if ($("#logoCheck").val() == 1) {
                            return false;
                        }
                        else {
                            return true;
                        }
                    }
                    ,
                    accept: "image/*"
                }
                ,
                agency_agree: "required"
            },
            messages: {
                name: {
                    required: "This field is required",
                    lettersonly: "Letters and spaces only please"
                }
                ,
                phoneNumber: {
                    required: "This field is required",
                    number: "Only Numbers allowed",
                    remote: jQuery.validator.format("{0} is already in use")
                }
                ,
                primaryContactPersonPersonalPhoneNumber: {
                    required: "This field is required",
                    number: "Only Numbers allowed"
                }
                ,
                secondaryContactPersonPersonalPhoneNumber: {
                    required: "This field is required",
                    number: "Only Numbers allowed"
                }
                ,
                agency_agree: "You must agree to NVP's Terms of Service."
            }
            ,
            focusInvalid: false,
            invalidHandler: function (form, validator) {

                if (!validator.numberOfInvalids())
                    return;
                $('html, body').animate({
                    scrollTop: $(validator.errorList[0].element).offset().top - 230
                }, 400);

            }
        })
        ;
        $('#project-detail-form').validate({
            rules: {
                projectType: "required",
                name: {
                    required: true,
                },
                purpose: "required",
                totalNumberOfDaysVolunteersAreNeeded: {
                    required: true,
                    number: true,
                    min: 1
                },
                totalNumberOfHoursPerVolunteer: {
                    required: true,
                    number: true,
                    min: 1
                },
                dateOfEvent: "required",
                timeOfEvent: "required",

            },
            messages: {
                name: {
                    required: "This field is required"
                },
                totalNumberOfDaysVolunteersAreNeeded: {
                    required: "This field is required",
                    number: "Only Numbers allowed"
                },
                totalNumberOfHoursPerVolunteer: {
                    required: "This field is required",
                    number: "Only Numbers allowed"
                }
            },
            focusInvalid: false,
            invalidHandler: function (form, validator) {

                if (!validator.numberOfInvalids())
                    return;
                $('html, body').animate({
                    scrollTop: $(validator.errorList[0].element).offset().top - 230
                }, 400);

            }
        });
        $('#project-criteria-form').validate({
            rules: {
                'projectCriteria[totalNumberOfVolunteersRequired]': {
                    required: true,
                    number: true
                }
            },
            messages: {
                'projectCriteria[totalNumberOfVolunteersRequired]': {
                    required: "This field is required",
                    number: "Only Numbers allowed"
                }
            },
            focusInvalid: false,
            invalidHandler: function (form, validator) {

                if (!validator.numberOfInvalids())
                    return;
                $('html, body').animate({
                    scrollTop: $(validator.errorList[0].element).offset().top - 230
                }, 400);

            }
        });
        $('#volunteer-profile-form').validate({
            debug: true,
            onkeyup: false,
            rules: {
                fullName: {
                    required: true,
                    lettersonly: true
                },
                genderId: "required",
                email: {
                    required: true,
                    email: true
                },
                dob: "required",
                phoneNumberMobile: {
                    required: true,
                    number: true,
                    remote: {
                        url: baseUrl + 'members/profile/checkPhnumber',
                        type: "post",
                        data: data,
                    }
                },
                emergencyContactFullName: {
                    required: true,
                    lettersonly: false
                },
                emergencyContactPhoneNumber: {
                    notEqual: '#phoneNumberMobile',
                    required: true,
                    number: true
                },
                zoneId: "required",
                districtId: "required",
                municipalityId: "required",
                wardNumber: {
                    required: true,
                    number: true
                },
                primaryModeOfCommunication: "required",
                secondaryModeOfCommunication: "required",
            },
            messages: {
                fullName: {
                    required: "This field is required",
                    lettersonly: "Name cannot be a number"
                },
                phoneNumberMobile: {
                    required: "This field is required",
                    number: "Only Numbers allowed",
                    remote: jQuery.validator.format("{0} is already in use")

                },
                emergencyContactFullName: {
                    required: "This field is required",
                    lettersonly: "Letters and spaces only please"
                },
                emergencyContactPhoneNumber: {
                    required: "This field is required",
                    number: "Only Numbers allowed"
                },
                wardNumber: {
                    required: "This field is required",
                    number: "Only Numbers allowed"
                }
            },
            focusInvalid: false,
            invalidHandler: function (form, validator) {

                if (!validator.numberOfInvalids())
                    return;
                $('html, body').animate({
                    scrollTop: $(validator.errorList[0].element).offset().top - 230
                }, 400);

            }
        });
        $('#volunteer-setting-form').validate({
            rules: {
                availability: "required",
                volunteerType: "required"
            },
            focusInvalid: false,
            invalidHandler: function (form, validator) {

                if (!validator.numberOfInvalids())
                    return;
                $('html, body').animate({
                    scrollTop: $(validator.errorList[0].element).offset().top - 230
                }, 400);

            }
        });
        $('#contact-form').validate({
            rules: {
                'full-name': {
                    required: true,
                    lettersonly: true
                },
                email: {
                    required: true,
                    email: true
                },
                'contact-msg': "required",
            },
            focusInvalid: false,
            invalidHandler: function (form, validator) {

                if (!validator.numberOfInvalids())
                    return;
                $('html, body').animate({
                    scrollTop: $(validator.errorList[0].element).offset().top - 230
                }, 400);

            }
        });
    });

})
;
