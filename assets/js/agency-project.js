(function() {
    'use strict';

    $('.toggle-checkbox').on('click', function() {
        var that = $(this);
        var viewToggle = that.data('view-toggle');

        if(that.prop('checked')) {
            $('#' + viewToggle).removeClass('hide-select');
            $('#' + viewToggle).addClass('show-select');
        } else {
            $('#' + viewToggle + ' input[type="checkbox"]').prop('checked', false);
            $('#' + viewToggle).removeClass('show-select');
            $('#' + viewToggle).addClass('hide-select');
        }
    });
})();