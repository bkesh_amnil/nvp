jQuery(function ($) {
    var base_url = $('#base-url').val();
    var load = 0;
    var valid = 'true';

    function append(image, name, des, slug) {
        var grid = document.querySelector('#grid');
        var item = document.createElement('div');
        var h = '<div class="news-contents-wrap col l12">';
        if (!!image) {
            h += '<div class="img-news-wrap">';
            h += '<img class="responsive-img" src="' + image + '" alt="' + name + '"/> </div>';
        }
        h += '<div class="news-short-desc"> <h3>' + name + '</h3>';
        h += '<span>Posted at 18 August, 2015 |06 pm</span> <p>';
        h += des.substr(0, 195) + '.....</p> </div>';
        h += '<a class="link-readmore" href="' + base_url + 'news/' + slug + '">Read more</a> </div>';
        salvattore['append_elements'](grid, [item])
        item.outerHTML = h;
    }

    function getNews(limit) {
        $.getJSON(base_url + 'news/news_show/' + limit, function (data) {
            var count = Object.keys(data).length;
            if (data.fail == 'false') {
                $('#complete').fadeIn(100);
                $('#footer').removeAttr('id');
                valid = 'false';
            } else {
                $(data).each(function (i, news) {
                    append(news.cover_image, news.name, news.short_description, news.slug);
                });
            }
        });
    }

    $(document).ready(function () {


        var footerWaypoint = $('#footer');
        footerWaypoint.waypoint({
            handler:function getNextResults(){
                    if (valid == 'true') {
                        $('#load').show();
                        load++;
                        console.log(valid);
                        getNews(load * 5);
                        setTimeout(function () {
                            $('#load').hide();
                        }, 3000);
                    }
            },
            offset: 'bottom-in-view'
        });

        /*function getNextResults(){
            console.lg('destroy');
                footerWaypoint.waypoint('destroy');
                footerWaypoint.waypoint(getNextResults,{
                offset: '60%'
                });
        }*/
    });
});
