(function() {
    $('#retrievepassword').validate({
        rules: {
            password: {
                required: true,
                minlength: 8
            },
            repassword: {
                required: true,
                minlength: 8,
                equalTo: '#password'
            }
        }
    });
})();