$(document).ready(function() {
    $('.video-thumb').fancybox({
        openEffect  : 'none',
        closeEffect : 'none',
        helpers : {
            media : {}
        },
        afterLoad : function() {
            var data = $(this.element).data("caption");
            $('.fancybox-wrap').append('<div class = "cation">'+data+'</div>');
        }
    });
});
