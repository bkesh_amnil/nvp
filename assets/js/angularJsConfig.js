var nvpApp = angular.module('nvpApp', []);

nvpApp.constant('myConfig', {
        apiPath: 'http://nvp.org.np/v1/api/',
        baseUrl: 'http://nvp.org.np/'
    });

nvpApp.factory('httpService', function($http, $q) {
    var httpRequest = function (params) {
        var deferred = $q.defer();
        var request = {
            method: params.method,
            url: params.url
        };

        // checking if data needs to be passed with the request
        if (typeof params.data != 'undefined') {
            if (params.method == 'post') {
                request.data = params.data;
            } else {
                request.params = params.data;
            }
        }

        var requestMade = $http(request);
        requestMade.success(function (data) {
            if (data) {
                deferred.resolve(data);
            }
        });
        requestMade.error(function (err) {
            deferred.reject(err);
            logMessage('api error');
            logMessage(err);
        });
        return deferred.promise;
    }

    return {
        httpRequest: httpRequest
    }
});