/**
 * Created by satish on 12/10/2015.
 */


(function() {
    'use strict';

    $(document).ready(function() {
        var partnerLink = $('.partnerLink');
        partnerLink.on('click', function(e) {
            e.preventDefault();
            var that = $(this);
            var activeContentId = that.data('content');
            var category = that.data('category');
            hideContentDiv(category);
            that.addClass('active');
            showContentDiv(activeContentId);
        });
    });
    function showContentDiv(activeContentId) {
        $(activeContentId).show();
    }

    function hideContentDiv(category) {
        $('.partners-slider a[data-category="' + category + '"]').removeClass('active');
        $('.data-holder-' + category).hide();
    }
})();